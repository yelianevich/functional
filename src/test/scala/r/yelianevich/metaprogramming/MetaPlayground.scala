package r.yelianevich.metaprogramming

import org.scalameta.logger
import org.scalatest.funsuite.AnyFunSuite

import scala.meta.tokenizers.Tokenized.Success // useful for debugging

class MetaPlayground extends AnyFunSuite {

  import scala.meta._

  test("part 1: tokens") {
    val tokens = "val x = 2".tokenize.get
    logger.elem(tokens.syntax)
    logger.elem(tokens.structure)
  }

  test("part 2: trees") {
    val tree = "val x = 2".parse[Stat].get
    logger.elem(tree.syntax)
    logger.elem(tree.structure)
  }

  case class Model(f1: String, f2: Int)


  test("quasiquotes") {
    println("test fail".tokenize.get(1).syntax == "test notfail".tokenize.get(1).syntax)
    println("test fail".tokenize.get(1) == "test notfail".tokenize.get(1))
    println("fail test".tokenize.get(3).syntax == "test notfail".tokenize.get(1).syntax)

    // uou
    println("fail test".tokenize.get(3).structure == "test notfail".tokenize.get(1).structure)

    """val x = "not_closed_literal """.tokenize match {
      case Success(t) => logger.elem(t.structure)
      case tokenizers.Tokenized.Error(_, msg, _) => println(msg)
    }

    val parsedCaseClass: Defn.Class = q"case class User(name: String, age: Int)"
    println(parsedCaseClass)
    val toString = q"""def toString(): String = "User(name=, age=)""""

    val enhancedCaseClass = q"""case class User(name: String, age: Int) { $toString }"""
    println(enhancedCaseClass)

    toString match {
      case q"def $name(): String = $body" => println(s"Name is '${name.syntax}' and body is '${body.syntax}'")
      case _ => println("Not matched")
    }
  }

  test("match quasiquotes") {
    q"def `is a baby` = age < 1" match {
      case q"def $name = $body" => println(s"You ${name.syntax} if your ${body.syntax}")
      case _ => println("Not matched")
    }
  }

  test("Tries") {
    println("foo(bar)".parse[Stat].get.syntax)


  }
}
