package r.yelianevich.scala99

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers


class ListsTest extends AnyFunSuite with Matchers {

  test("last()") {
    Lists.last(List(1, 2, 3, 4, 5)) shouldBe 5
    Lists.last(List(1)) shouldBe 1
    intercept[NoSuchElementException](Lists.last(List.empty))
  }

  test("penultimate()") {
    Lists.penultimate(List(1, 2, 3, 4, 5)) shouldBe 4
    Lists.penultimate(List(1, 2)) shouldBe 1
    intercept[NoSuchElementException](Lists.penultimate(List(1)))
    intercept[NoSuchElementException](Lists.penultimate(List.empty))
  }

  test("nth()") {
    Lists.nth(0, List(1, 2, 3, 4, 5)) shouldBe 1
    Lists.nth(1, List(1, 2, 3, 4, 5)) shouldBe 2
    Lists.nth(4, List(1, 2, 3, 4, 5)) shouldBe 5
    intercept[NoSuchElementException](Lists.nth(-1, List(1)))
    intercept[NoSuchElementException](Lists.nth(4, List(0, 1, 2, 3)))
  }

  test("length()") {
    Lists.length(List(1, 2, 3, 4, 5)) shouldBe 5
    Lists.length(List.empty) shouldBe 0
  }

  test("reverse()") {
    Lists.reverse(List(1, 2, 3, 4)) shouldBe List(4, 3, 2, 1)
    Lists.reverse(List(1)) shouldBe List(1)
    Lists.reverse(List.empty) shouldBe List.empty
  }

  test("flattern()") {
    Lists.flatten(List(List(1, 1), 2, List(3, List(5, 8)))) shouldBe List(1, 1, 2, 3, 5, 8)
    Lists.flatten(List.empty) shouldBe List.empty
  }

  test("compress()") {
    Lists.compress(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)) shouldBe
        List('a, 'b, 'c, 'a, 'd, 'e)
    Lists.compress(List.empty) shouldBe List.empty
    Lists.compress(List(1, 1)) shouldBe List(1)
  }

  test("pack()") {
    Lists.pack(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)) shouldBe
        List(List('a, 'a, 'a, 'a), List('b), List('c, 'c), List('a, 'a), List('d), List('e, 'e, 'e, 'e))
    Lists.pack(List.empty) shouldBe List.empty
    Lists.pack(List(1, 1)) shouldBe List(List(1, 1))
  }

  test("encode()") {
    Lists.encode(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)) shouldBe
        List((4, 'a), (1, 'b), (2, 'c), (2, 'a), (1, 'd), (4, 'e))
  }

  test("encodeModified()") {
    Lists.encodeModified(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)) shouldBe
        List((4, 'a), 'b, (2, 'c), (2, 'a), 'd, (4, 'e))
    Lists.encodeWithEither(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)) shouldBe
        List(Left(4 -> 'a), Right('b), Left(2 -> 'c), Left(2 -> 'a), Right('d), Left(4 -> 'e))
  }

  test("decode()") {
    Lists.decode(List((4, 'a), (1, 'b), (2, 'c), (2, 'a), (1, 'd), (4, 'e))) shouldBe
        List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)
    Lists.decode(List.empty) shouldBe List.empty
    Lists.decode(List((1, 'a))) shouldBe List('a)
  }

  test("encodeDirect()") {
    Lists.encodeDirect(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)) shouldBe
        List((4, 'a), (1, 'b), (2, 'c), (2, 'a), (1, 'd), (4, 'e))
    Lists.encodeDirect(List.empty) shouldBe List.empty
    Lists.encodeDirect(List('a, 'a, 'a, 'a)) shouldBe List((4, 'a))
  }

  test("duplicate()") {
    Lists.duplicate(List('a, 'b, 'c, 'c, 'd)) shouldBe
        List('a, 'a, 'b, 'b, 'c, 'c, 'c, 'c, 'd, 'd)
    Lists.duplicate(List.empty) shouldBe List.empty
    Lists.duplicate(List('a, 'a)) shouldBe List('a, 'a, 'a, 'a)
  }

  test("duplicateN()") {
    Lists.duplicateN(3, List('a, 'b, 'c, 'c, 'd)) shouldBe
        List('a, 'a, 'a, 'b, 'b, 'b, 'c, 'c, 'c, 'c, 'c, 'c, 'd, 'd, 'd)
    Lists.duplicateN(3, List.empty) shouldBe List.empty
  }

  test("drop()") {
    Lists.drop(3, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)) shouldBe
        List('a, 'b, 'd, 'e, 'g, 'h, 'j, 'k)
    Lists.duplicateN(3, List.empty) shouldBe List.empty
  }

  test("split()") {
    Lists.split(3, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)) shouldBe
        (List('a, 'b, 'c), List('d, 'e, 'f, 'g, 'h, 'i, 'j, 'k))
  }

  test("slice()") {
    Lists.slice(3, 7, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)) shouldBe
        List('d, 'e, 'f, 'g)
  }

  ignore("rotate()") {
    Lists.rotate(3, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)) shouldBe
        List('d, 'e, 'f, 'g, 'h, 'i, 'j, 'k, 'a, 'b, 'c)
    Lists.rotate(-2, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)) shouldBe
        List('j, 'k, 'a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i)
  }
}
