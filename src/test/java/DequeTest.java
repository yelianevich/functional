import org.junit.jupiter.api.Test;
import r.yelianevich.datastruct.Deque;

import java.util.NoSuchElementException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;


class DequeTest {

  @Test
  void addFirstLast() {
    Deque<Integer> d = new Deque<>();
    d.addFirst(1);
    d.addFirst(2);
    d.addFirst(3);
    d.addLast(4);
    d.addLast(5);
    d.addLast(6);
    assertThat(d).containsExactlyInAnyOrder(1, 2, 3, 4, 5, 6);
    assertThat(d.size()).isEqualTo(6);
  }

  @Test
  void nextOnEmptyShouldFail() {
    assertThatExceptionOfType(NoSuchElementException.class).isThrownBy(() -> {
      Deque<Integer> d = new Deque<>();
      d.iterator().next();
    });
  }

  @Test
  void removeFirstShouldFailWhenEmpty() {
    assertThatExceptionOfType(NoSuchElementException.class).isThrownBy(() ->
        new Deque<>().removeFirst());
  }

  @Test
  void removeLastShouldFailWhenEmpty() {
    assertThatExceptionOfType(NoSuchElementException.class).isThrownBy(() ->
        new Deque<>().removeLast());
  }

  @Test
  void removeOnEmptyShouldFail() {
    assertThatExceptionOfType(UnsupportedOperationException.class).isThrownBy(() -> {
      Deque<Integer> d = new Deque<>();
      d.iterator().remove();
    });
  }

  @Test
  void addRemoveFirstLast() {
    Deque<Integer> d = new Deque<>();
    d.addFirst(3);
    d.addFirst(2);
    d.addFirst(1);
    d.addLast(4);
    d.addLast(5);
    d.addLast(6);
    assertThat(d).containsExactly(1, 2, 3, 4, 5, 6);
    assertThat(d.size()).isEqualTo(6);
    assertThat(d.removeFirst()).isEqualTo(1);
    assertThat(d.removeFirst()).isEqualTo(2);
    assertThat(d.removeLast()).isEqualTo(6);
    assertThat(d.removeLast()).isEqualTo(5);
    assertThat(d).containsExactly(3, 4);
    assertThat(d.size()).isEqualTo(2);
    d.removeLast();
    d.removeLast();
    d.addFirst(2);
    d.addFirst(1);
    assertThat(d).containsExactly(1, 2);
    assertThat(d.size()).isEqualTo(2);
  }

}