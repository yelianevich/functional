import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


class PointTest {

    @Test
    void slopeTo() {
        Point p1 = new Point(1, 1);
        Point p2 = new Point(2, 2);
        assertThat(p1.slopeTo(p2)).isEqualTo(1.0);
    }

    @Test
    void compareTo() {
        Point p1 = new Point(1, 1);
        Point p2 = new Point(2, 2);
        Point p2Copy = new Point(2, 2);
        assertThat(p1.compareTo(p2)).isEqualTo(-1);
        assertThat(p2.compareTo(p1)).isEqualTo(1);
        assertThat(p1.compareTo(p1)).isEqualTo(0);
        assertThat(p2.compareTo(p2Copy)).isEqualTo(0);
    }

}