package r.yelianevich.datastruct.map;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


class BasicHashTableTest {

  @Test
  void basicHashTable() {
    BasicHashTable<Integer, String> tab = new BasicHashTable<>(16);
    tab.put(1, "1");
    tab.put(2, "2");
    tab.put(3, "3");
    tab.put(4, "4");
    tab.put(5, "5");
    tab.put(17, "17");
    tab.put(7, "7");
    tab.put(21, "21");
    System.out.println(tab.toString());
    assertThat(tab.size()).isEqualTo(8);
    assertThat(tab.get(1)).isEqualTo("1");
    assertThat(tab.get(17)).isEqualTo("17");

    tab.remove(1);
    System.out.println(tab.toString());
    assertThat(tab.size()).isEqualTo(7);
    assertThat(tab.get(1)).isNull();
    assertThat(tab.get(17)).isEqualTo("17");
  }

}