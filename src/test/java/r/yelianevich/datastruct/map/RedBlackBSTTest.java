package r.yelianevich.datastruct.map;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import r.yelianevich.datastruct.AlgsTest;

import java.util.NoSuchElementException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

class RedBlackBSTTest {

  @Test
  void api() {
    //edu.princeton.cs.algs4.RedBlackBST<Integer, String> tab = new edu.princeton.cs.algs4.RedBlackBST<>();
    RedBlackBST<Integer, String> tab = new RedBlackBST<>();

    assertThatExceptionOfType(NoSuchElementException.class).isThrownBy(tab::min);
    System.out.println(tab);
    tab.put(1, "1");
    assertThat(tab.get(1)).isEqualTo("1");
    System.out.println(tab);
    tab.put(2, "2");
    System.out.println(tab);
    tab.put(3, "3");
    assertThat(tab.get(3)).isEqualTo("3");
    assertThat(tab.min()).isEqualTo(1);
    System.out.println(tab);
    tab.put(4, "4");
    System.out.println(tab);
    tab.put(5, "5");
    System.out.println(tab);
    tab.put(17, "17");
    assertThat(tab.get(17)).isEqualTo("17");
    assertThat(tab.keys()).containsExactly(1, 2, 3, 4, 5, 17);
    System.out.println(tab);
    tab.put(7, "7");
    System.out.println(tab);
    tab.put(21, "21");
    System.out.println(tab);

    assertThat(tab.size()).isEqualTo(8);
    tab.put(21, "21");
    assertThat(tab.size()).isEqualTo(8);

    assertThat(tab.get(1)).isEqualTo("1");
    assertThat(tab.get(17)).isEqualTo("17");

    // delete min and max
    int sizeMin = tab.size();
    assertThat(tab.contains(1)).isTrue();
    tab.deleteMin();
    assertThat(tab.size()).isEqualTo(sizeMin - 1);
    assertThat(tab.contains(1)).isFalse();
    tab.put(1, "1");

    int sizeMax = tab.size();
    assertThat(tab.contains(21)).isTrue();
    tab.deleteMax();
    assertThat(tab.size()).isEqualTo(sizeMax - 1);
    assertThat(tab.contains(21)).isFalse();
    tab.put(21, "21");
  }


  @Test
  void withTinyTale() {
    AlgsTest.AlgStats stats = AlgsTest.freqTest(1, "algs4-data/tinyTale.txt", new RedBlackBST<>());
    assertThat(stats.max).isIn("of", "it"); // the same number of occurrences
    assertThat(stats.maxFreq).isEqualTo(10);
    assertThat(stats.distinct).isEqualTo(20);
    assertThat(stats.words).isEqualTo(60);
  }

  @Test
  void withTale() {
    AlgsTest.AlgStats stats = AlgsTest.freqTest(8, "algs4-data/tale.txt", new RedBlackBST<>());
    assertThat(stats.max).isEqualTo("business");
    assertThat(stats.maxFreq).isEqualTo(122);
    assertThat(stats.distinct).isEqualTo(5126);
    assertThat(stats.words).isEqualTo(14351);
  }

  @Test
  @Disabled("Too big to commit test data to git")
  void withLeipzig1M() {
    AlgsTest.AlgStats stats = AlgsTest.freqTest(10, "algs4-data/leipzig1M.txt", new RedBlackBST<>());
    assertThat(stats.max).isEqualTo("government");
    assertThat(stats.maxFreq).isEqualTo(24763);
    assertThat(stats.distinct).isEqualTo(165555);
    assertThat(stats.words).isEqualTo(1610829);
  }

  @Test
  @Disabled("Too big to commit test data to git")
  void withLeipzig1MAllWords() {
    //TreeMap<String, Integer> st = new TreeMap<>(); // TreeMap is a bit better (1-2-3 seconds)
    //edu.princeton.cs.algs4.RedBlackBST<String, Integer> st = new edu.princeton.cs.algs4.RedBlackBST<>();
    AlgsTest.AlgStats stats = AlgsTest.freqTest(0, "algs4-data/leipzig1M.txt", new RedBlackBST<>());
    assertThat(stats.max).isEqualTo("the");
    assertThat(stats.maxFreq).isEqualTo(1160105);
    assertThat(stats.distinct).isEqualTo(534580);
    assertThat(stats.words).isEqualTo(21191455);
  }
}
