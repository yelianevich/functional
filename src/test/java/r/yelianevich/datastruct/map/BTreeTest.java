package r.yelianevich.datastruct.map;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class BTreeTest {

  @Test
  void buildTree() {
    BTree<Integer, String> bTree = new BTree<>(3);
    bTree.put(1, "x");
    bTree.put(2, "xx");
    bTree.put(3, "xxx");
    bTree.put(4, "xxxx");
    bTree.put(5, "xxxxx");
    bTree.put(11, "x");
    bTree.put(12, "xx");
    bTree.put(13, "xxx");
    bTree.put(14, "xxxx");
    bTree.put(15, "xxxxx");
    assertThat(bTree.put(15, "replaced_xxxxx")).isEqualTo("xxxxx");
    System.out.println(bTree.toString());
    assertThat(bTree.get(1)).isEqualTo("x");
    assertThat(bTree.get(5)).isEqualTo("xxxxx");
    assertThat(bTree.get(15)).isEqualTo("replaced_xxxxx");
    assertThat(bTree.get(14)).isEqualTo("xxxx");
  }

  @Test
  void buildExerciseTree() {
    BTree<String, String> bTree = new BTree<>(2);
    assertThat(bTree.getHeight()).isEqualTo(0);
    bTree.put("F", "F");
    bTree.put("S", "S");
    bTree.put("Q", "Q");

    bTree.put("K", "K");
    bTree.put("K", "K");

    bTree.put("C", "C");
    bTree.put("L", "L");
    bTree.put("H", "H");
    bTree.put("T", "T");
    bTree.put("V", "V");
    bTree.put("W", "W");
    bTree.put("M", "M");

    assertThat(bTree.getHeight()).isEqualTo(2);
    assertThat(bTree.size()).isEqualTo(11);
    assertThat(bTree.get("K")).isEqualTo("K");
    assertThat(bTree.get("T")).isEqualTo("T");

    bTree.put("R", "R");
    bTree.put("N", "N");
    bTree.put("P", "P");
    bTree.put("A", "A");
    bTree.put("B", "B");
    bTree.put("X", "X");
    bTree.put("Y", "Y");
    bTree.put("D", "D");
    bTree.put("Z", "Z");
    bTree.put("E", "E");

    assertThat(bTree.size()).isEqualTo(21);
    assertThat(bTree.get("P")).isEqualTo("P");
    assertThat(bTree.get("E")).isEqualTo("E");

    System.out.println(bTree.toString());
  }
}