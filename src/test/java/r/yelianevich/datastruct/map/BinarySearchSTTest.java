package r.yelianevich.datastruct.map;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import r.yelianevich.datastruct.AlgsTest;

import static org.assertj.core.api.Assertions.assertThat;

class BinarySearchSTTest {

  @Test
  void binarySearchST() {
    int capacity = 5;
    BinarySearchST<Integer, String> tab = new BinarySearchST<>(capacity);
    System.out.println(tab);
    tab.put(1, "1");
    System.out.println(tab);
    tab.put(2, "2");
    System.out.println(tab);
    tab.put(3, "3");
    System.out.println(tab);
    tab.put(4, "4");
    System.out.println(tab);
    tab.put(5, "5");
    assertThat(tab.capacity()).isEqualTo(5);
    System.out.println(tab);
    tab.put(17, "17");
    assertThat(tab.capacity()).as("Should increase capacity only when it's filled up").isEqualTo(7);
    System.out.println(tab);
    tab.put(7, "7");
    System.out.println(tab);
    tab.put(21, "21");
    System.out.println(tab);

    assertThat(tab.size()).isEqualTo(8);
    tab.put(21, "21");
    assertThat(tab.size()).isEqualTo(8);

    assertThat(tab.get(1)).isEqualTo("1");
    assertThat(tab.get(17)).isEqualTo("17");

    System.out.println("Remove 1");
    assertThat(tab.contains(1)).isTrue();
    String prev = tab.delete(1);
    System.out.println(tab);
    assertThat(prev).isEqualTo("1");
    assertThat(tab.contains(1)).isFalse();

    assertThat(tab.size()).isEqualTo(7);
    assertThat(tab.get(1)).isNull();
    assertThat(tab.get(17)).isEqualTo("17");
    assertThat(tab.get(7)).isEqualTo("7");
    assertThat(tab.get(21)).isEqualTo("21");

    System.out.println("Test shrink");
    tab.delete(21);
    System.out.println(tab);
    tab.delete(2);
    System.out.println(tab);
    tab.delete(3);
    System.out.println(tab);
    tab.delete(4);
    System.out.println(tab);
    tab.delete(5);
    System.out.println(tab);
    tab.delete(7);
    System.out.println(tab);
  }

  @Test
  void withTinyTale() {
    AlgsTest.AlgStats stats = AlgsTest.freqTest(1, "algs4-data/tinyTale.txt", new BinarySearchST<>(50000));
    assertThat(stats.max).isIn("of", "it"); // equal number of occurrences
    assertThat(stats.maxFreq).isEqualTo(10);
    assertThat(stats.distinct).isEqualTo(20);
    assertThat(stats.words).isEqualTo(60);
  }

  @Test
  void withTale() {
    AlgsTest.AlgStats stats = AlgsTest.freqTest(8, "algs4-data/tale.txt", new BinarySearchST<>(50000));
    assertThat(stats.max).isEqualTo("business");
    assertThat(stats.maxFreq).isEqualTo(122);
    assertThat(stats.distinct).isEqualTo(5126);
    assertThat(stats.words).isEqualTo(14351);
  }

  @Test
  @Disabled("Too big to commit test data to git")
  void withLeipzig1M() {
    AlgsTest.AlgStats stats = AlgsTest.freqTest(10, "algs4-data/leipzig1M.txt", new BinarySearchST<>(50000));
    assertThat(stats.max).isEqualTo("government");
    assertThat(stats.maxFreq).isEqualTo(24763);
    assertThat(stats.distinct).isEqualTo(165555);
    assertThat(stats.words).isEqualTo(1610829);
  }
}
