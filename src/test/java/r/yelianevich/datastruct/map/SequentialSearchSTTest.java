package r.yelianevich.datastruct.map;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import r.yelianevich.datastruct.AlgsTest;
import r.yelianevich.datastruct.AlgsTest.AlgStats;

import static org.assertj.core.api.Assertions.assertThat;

class SequentialSearchSTTest {

  @Test
  void linearProbingHashTable() {
    int capacity = 5;
    LinearProbingHashTable<Integer, String> tab = new LinearProbingHashTable<>(capacity, 0.5f);
    System.out.println(tab.toString());
    tab.put(1, "1");
    System.out.println(tab.toString());
    tab.put(2, "2");
    System.out.println(tab.toString());
    tab.put(3, "3");
    System.out.println(tab.toString());
    tab.put(4, "4");
    System.out.println(tab.toString());
    assertThat(tab.capacity()).isEqualTo(capacity * 2);
    tab.put(5, "5");
    System.out.println(tab.toString());
    tab.put(17, "17");
    System.out.println(tab.toString());
    tab.put(7, "7");
    System.out.println(tab.toString());
    tab.put(21, "21");
    System.out.println(tab.toString());
    assertThat(tab.size()).isEqualTo(8);
    assertThat(tab.get(1)).isEqualTo("1");
    assertThat(tab.get(17)).isEqualTo("17");

    System.out.println("Remove 1");
    assertThat(tab.contains(1)).isTrue();
    String prev = tab.delete(1);
    System.out.println(tab.toString());
    assertThat(prev).isEqualTo("1");
    assertThat(tab.contains(1)).isFalse();

    assertThat(tab.size()).isEqualTo(7);
    assertThat(tab.get(1)).isNull();
    assertThat(tab.get(17)).isEqualTo("17");
    assertThat(tab.get(7)).isEqualTo("7");
    assertThat(tab.get(21)).isEqualTo("21");

    System.out.println("Test shrink");
    tab.delete(21);
    System.out.println(tab.toString());
    tab.delete(2);
    System.out.println(tab.toString());
    tab.delete(3);
    System.out.println(tab.toString());
    tab.delete(4);
    System.out.println(tab.toString());
    tab.delete(5);
    System.out.println(tab.toString());
    tab.delete(7);
    System.out.println(tab.toString());
    assertThat(tab.capacity()).isEqualTo(5);
  }

  @Test
  void withTinyTale() {
    AlgStats stats = AlgsTest.freqTest(1, "algs4-data/tinyTale.txt", new SequentialSearchST<>());
    assertThat(stats.max).isIn("of", "it");
    assertThat(stats.maxFreq).isEqualTo(10);
    assertThat(stats.distinct).isEqualTo(20);
    assertThat(stats.words).isEqualTo(60);
  }

  @Test
  void withTale() {
    AlgStats stats = AlgsTest.freqTest(8, "algs4-data/tale.txt", new SequentialSearchST<>());
    assertThat(stats.max).isEqualTo("business");
    assertThat(stats.maxFreq).isEqualTo(122);
    assertThat(stats.distinct).isEqualTo(5126);
    assertThat(stats.words).isEqualTo(14351);
  }

  @Test
  @Disabled("Illustrates that it's very slow, no need to run regularly")
  void withLeipzig1M() {
    AlgStats stats = AlgsTest.freqTest(10, "algs4-data/leipzig1M.txt", new SequentialSearchST<>());
    assertThat(stats.max).isEqualTo("government");
    assertThat(stats.maxFreq).isEqualTo(24763);
    assertThat(stats.distinct).isEqualTo(165555);
    assertThat(stats.words).isEqualTo(1610829);
  }
}
