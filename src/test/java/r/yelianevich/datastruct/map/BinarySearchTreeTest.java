package r.yelianevich.datastruct.map;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import r.yelianevich.datastruct.AlgsTest;

import static org.assertj.core.api.Assertions.assertThat;

class BinarySearchTreeTest {

  @Test
  void api() {
    BinarySearchTree<Integer, String> tab = new BinarySearchTree<>();
    assertThat(tab.min()).isNull();
    assertThat(tab.max()).isNull();
    assertThat(tab.floor(1)).isNull();
    assertThat(tab.ceiling(1)).isNull();
    assertThat(tab.rank(1)).isEqualTo(0);
    assertThat(tab.select(1)).isNull();
    System.out.println(tab);
    tab.put(1, "1");
    System.out.println(tab);
    tab.put(2, "2");
    System.out.println(tab);
    tab.put(3, "3");
    assertThat(tab.min()).isEqualTo(1);
    assertThat(tab.max()).isEqualTo(3);
    assertThat(tab.floor(3)).isEqualTo(3);
    assertThat(tab.ceiling(3)).isEqualTo(3);
    assertThat(tab.rank(2)).isEqualTo(1);
    System.out.println(tab);
    tab.put(4, "4");
    System.out.println(tab);
    tab.put(5, "5");
    System.out.println(tab);
    tab.put(17, "17");
    assertThat(tab.floor(15)).isEqualTo(5);
    assertThat(tab.ceiling(15)).isEqualTo(17);
    assertThat(tab.rank(17)).isEqualTo(5);
    System.out.println(tab);
    tab.put(7, "7");
    System.out.println(tab);
    tab.put(21, "21");
    System.out.println(tab);
    assertThat(tab.max()).isEqualTo(21);

    assertThat(tab.size()).isEqualTo(8);
    tab.put(21, "21");
    assertThat(tab.size()).isEqualTo(8);

    assertThat(tab.get(1)).isEqualTo("1");
    assertThat(tab.get(17)).isEqualTo("17");
    assertThat(tab.rank(8)).isEqualTo(6);
    assertThat(tab.select(6)).isEqualTo(17);

    // delete min and max
    int sizeMin = tab.size();
    assertThat(tab.contains(1)).isTrue();
    tab.deleteMin();
    assertThat(tab.size()).isEqualTo(sizeMin - 1);
    assertThat(tab.contains(1)).isFalse();
    tab.put(1, "1");

    int sizeMax = tab.size();
    assertThat(tab.contains(21)).isTrue();
    tab.deleteMax();
    assertThat(tab.size()).isEqualTo(sizeMax - 1);
    assertThat(tab.contains(21)).isFalse();
    tab.put(21, "21");
  }

  @Test
  void withTinyTale() {
    AlgsTest.AlgStats stats = AlgsTest.freqTest(1, "algs4-data/tinyTale.txt", new BinarySearchTree<>());
    assertThat(stats.max).isIn("of", "it");
    assertThat(stats.maxFreq).isEqualTo(10);
    assertThat(stats.distinct).isEqualTo(20);
    assertThat(stats.words).isEqualTo(60);
  }

  @Test
  void withTale() {
    AlgsTest.AlgStats stats = AlgsTest.freqTest(8, "algs4-data/tale.txt", new BinarySearchTree<>());
    assertThat(stats.max).isEqualTo("business");
    assertThat(stats.maxFreq).isEqualTo(122);
    assertThat(stats.distinct).isEqualTo(5126);
    assertThat(stats.words).isEqualTo(14351);
  }

  @Test
  @Disabled("Too big to commit test data to git")
  void withLeipzig1M10Letters() {
    AlgsTest.AlgStats stats = AlgsTest.freqTest(10, "algs4-data/leipzig1M.txt", new BinarySearchTree<>());
    assertThat(stats.max).isEqualTo("government");
    assertThat(stats.maxFreq).isEqualTo(24763);
    assertThat(stats.distinct).isEqualTo(165555);
    assertThat(stats.words).isEqualTo(1610829);
  }

  @Test
  @Disabled("Too big to commit test data to git")
  void withLeipzig1MAllWords() {
    AlgsTest.AlgStats stats = AlgsTest.freqTest(0, "algs4-data/leipzig1M.txt", new BinarySearchTree<>());
    assertThat(stats.max).isEqualTo("the");
    assertThat(stats.maxFreq).isEqualTo(1160105);
    assertThat(stats.distinct).isEqualTo(534580);
    assertThat(stats.words).isEqualTo(21191455);
  }
}
