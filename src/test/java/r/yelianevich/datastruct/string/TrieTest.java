package r.yelianevich.datastruct.string;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class TrieTest {

  @Test
  void putGetScenario() {
    // she sells sea shells by the sea shore
    Trie<Integer> trie = new Trie<>();
    assertThat(trie.get("noSuchKey")).isNull();
    trie.put("she", 0);
    assertThat(trie.get("she")).isEqualTo(0);
    trie.put("sells", 1);
    trie.put("sea", 2);
    assertThat(trie.get("sea")).isEqualTo(2);
    trie.put("shells", 3);
    trie.put("by", 4);
    trie.put("the", 5);
    trie.put("sea", 6);
    assertThat(trie.get("sea")).isEqualTo(6);
    trie.put("shore", 7);
    assertThat(trie.get("shore")).isEqualTo(7);
    assertThat(trie.size()).isEqualTo(7);
  }

  @Test
  void size() {
    Trie<Integer> trie = new Trie<>();
    assertThat(trie.size()).isEqualTo(0);
    assertThat(trie.isEmpty()).isTrue();
    trie.put("hello", 1);
    assertThat(trie.size()).isEqualTo(1);
    assertThat(trie.isEmpty()).isFalse();
  }

  @Test
  void keys() {
    // she sells sea shells by the sea shore
    Trie<Integer> trie = buildTestTrie();
    assertThat(trie.keys()).containsExactlyInAnyOrder("she", "sells", "sea", "shells", "by", "the", "shore");
  }

  @Test
  void keysWithPrefix() {
    // she sells sea shells by the sea shore
    Trie<Integer> trie = buildTestTrie();
    trie.put("shore", 7);
    assertThat(trie.keysWithPrefix("s")).containsExactlyInAnyOrder("she", "sells", "shells", "sea", "shore");
    assertThat(trie.keysWithPrefix("se")).containsExactlyInAnyOrder("sells", "sea");
    assertThat(trie.keysWithPrefix("sh")).containsExactlyInAnyOrder("she", "shells", "shore");
    assertThat(trie.keysWithPrefix("")).containsExactlyInAnyOrder("she", "sells", "sea", "shells", "by", "the", "shore");
  }

  @Test
  void keysThatMatch() {
    // she sells sea shells by the sea shore
    Trie<Integer> trie = buildTestTrie();
    assertThat(trie.keysThatMatch("..e")).containsExactlyInAnyOrder("she", "the");
    assertThat(trie.keysThatMatch("..")).containsExactlyInAnyOrder("by");
    assertThat(trie.keysThatMatch("she")).containsExactlyInAnyOrder("she");
    assertThat(trie.keysThatMatch("")).isEmpty();
  }

  @Test
  void longestPrefixOf() {
    Trie<Integer> trie = buildTestTrie();
    assertThat(trie.longestPrefixOf("")).isEqualTo("");
    assertThat(trie.longestPrefixOf("shellsort")).isEqualTo("shells");
    assertThat(trie.longestPrefixOf("shelters")).isEqualTo("she");
    assertThat(trie.longestPrefixOf("shell")).isEqualTo("she");
    assertThat(trie.longestPrefixOf("she")).isEqualTo("she");
  }

  @Test
  void delete() {
    Trie<Integer> trie = buildTestTrie();
    assertThat(trie.get("sea")).isEqualTo(6);
    trie.delete("sea");
    assertThat(trie.get("sea")).isNull();

    assertThat(trie.get("she")).isEqualTo(0);
    trie.delete("she");
    assertThat(trie.get("she")).isNull();

    trie.delete("sells");
    trie.delete("shells");
    trie.delete("by");
    trie.delete("the");
    trie.delete("shore");
    assertThat(trie.isEmpty()).isTrue();
    trie.put("hello", 1);
    assertThat(trie.get("hello")).isEqualTo(1);
  }

  private Trie<Integer> buildTestTrie() {
    Trie<Integer> trie = new Trie<>();
    trie.put("she", 0);
    trie.put("sells", 1);
    trie.put("sea", 2);
    trie.put("shells", 3);
    trie.put("by", 4);
    trie.put("the", 5);
    trie.put("sea", 6);
    trie.put("shore", 7);
    return trie;
  }
}
