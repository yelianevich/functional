package r.yelianevich.datastruct.unionfind;

import static org.assertj.core.api.Assertions.assertThat;

public class UnionFindTest {

  public static void testUnionAndConnected(UF uf) {
    uf.union(1, 2);
    uf.union(3, 4);
    uf.union(5, 6);
    uf.union(5, 6);
    uf.union(0, 6);
    uf.union(2, 8);
    uf.union(2, 8);
    uf.union(7, 8);
    uf.union(7, 9);

    // {0, 5, 6}, {3, 4}, {1, 2, 7, 8, 9}
    assertThat(uf.connected(1, 9)).isTrue();
    assertThat(uf.connected(2, 7)).isTrue();
    assertThat(uf.connected(1, 5)).isFalse();
    assertThat(uf.connected(0, 5)).isTrue();
    assertThat(uf.connected(0, 9)).isFalse();
    assertThat(uf.connected(3, 4)).isTrue();

    assertThat(uf.count()).isEqualTo(3);
  }

}
