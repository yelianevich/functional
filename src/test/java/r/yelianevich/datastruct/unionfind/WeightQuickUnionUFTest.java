package r.yelianevich.datastruct.unionfind;

import org.junit.jupiter.api.Test;

class WeightQuickUnionUFTest {

  @Test
  void connected() {
    UnionFindTest.testUnionAndConnected(new WeightQuickUnionUF(10));
  }
}