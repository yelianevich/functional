package r.yelianevich.datastruct.unionfind;

import org.junit.jupiter.api.Test;

class QuickUnionUFTest {

  @Test
  void connected() {
    UnionFindTest.testUnionAndConnected(new QuickUnionUF(10));
  }

}