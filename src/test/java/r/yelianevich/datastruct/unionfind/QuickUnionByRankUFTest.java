package r.yelianevich.datastruct.unionfind;

import org.junit.jupiter.api.Test;

class QuickUnionByRankUFTest {

    @Test
    void connected() {
        UnionFindTest.testUnionAndConnected(new QuickUnionByRankUF(10));
    }
}