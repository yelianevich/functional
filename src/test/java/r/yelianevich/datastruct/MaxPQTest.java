package r.yelianevich.datastruct;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class MaxPQTest {

  @Test
  void insertAndFullDel() {
    MaxPQ<Integer> pq = new MaxPQ<>(1);
    pq.insert(1);
    pq.insert(0);
    pq.insert(7);
    pq.insert(10);
    pq.insert(5);
    pq.insert(6);
    pq.insert(8);

    List<Integer> sorted = new ArrayList<>();
    while (pq.nonEmpty()) {
      sorted.add(pq.delMax());
    }

    assertThat(sorted).containsExactly(10, 8, 7, 6, 5, 1, 0);
  }

  @Test
  void easyQuestion() {
    MaxPQ<String> pq = new MaxPQ<>(1);
    pq.insert("E");
    pq.insert("A");
    pq.insert("S");
    pq.insert("Y");
    pq.insert("Q");
    pq.insert("U");
    pq.insert("E");
    pq.insert("S");
    pq.insert("T");
    pq.insert("I");
    pq.insert("O");
    pq.insert("N");
    Object[] pqArray = pq.pq;
    assertThat(pqArray).startsWith(
        null, "Y", "T", "U", "S", "Q", "N", "E", "A", "S", "I", "O", "E", null
    );
  }

  @Test
  void auxConstructor() {
    MaxPQ<Integer> pq = new MaxPQ<>(new Integer[] { 1, 0, 7, 10, 5, 6, 8 });
    List<Integer> sorted = new ArrayList<>();
    while (pq.nonEmpty()) {
      sorted.add(pq.delMax());
    }
    assertThat(sorted).containsExactly(10, 8, 7, 6, 5, 1, 0);
  }

}