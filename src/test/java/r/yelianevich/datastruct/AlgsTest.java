package r.yelianevich.datastruct;

import edu.princeton.cs.algs4.StdOut;
import r.yelianevich.datastruct.map.SymbolTableApi;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Scanner;

public abstract class AlgsTest {

  public static void setStdIn(String resource) throws Exception {
    URI uri = ClassLoader.getSystemResource(resource).toURI();
    System.setIn(Files.newInputStream(Paths.get(uri)));
  }

  public static Scanner scanner(String resource) {
    Scanner scanner = new Scanner(reader(resource));
    scanner.useLocale(Locale.US);
    return scanner;
  }

  public static BufferedReader reader(String resource) {
    InputStream resourceAsStream = Objects.requireNonNull(
        AlgsTest.class.getClassLoader().getResourceAsStream(resource));
    return new BufferedReader(new InputStreamReader(resourceAsStream, StandardCharsets.UTF_8));
  }

  public static int[] readAllInts(String resource) {
    Scanner scanner = scanner(resource);
    List<Integer> ints = new ArrayList<>();
    while (scanner.hasNext()) {
      ints.add(scanner.nextInt());
    }
    return ints.stream().mapToInt(Integer::intValue).toArray();
  }

  public static AlgStats freqTest(int minLen, String resource, SymbolTableApi<String, Integer> st) {
    try (Scanner reader = AlgsTest.scanner(resource)) {
      int distinct = 0;
      int words = 0;

      // compute frequency counts
      while (reader.hasNext()) {
        String key = reader.next();
        if (key.length() < minLen) continue;
        words++;
        if (st.contains(key)) {
          st.put(key, st.get(key) + 1);
        } else {
          st.put(key, 1);
          distinct++;
        }
      }

      // find a key with the highest frequency count
      String max = "";
      st.put(max, 0);
      for (String word : st.keys()) {
        if (st.get(word) > st.get(max)) {
          max = word;
        }
      }

      StdOut.println(max + " " + st.get(max));
      StdOut.println("distinct = " + distinct);
      StdOut.println("words    = " + words);

      return new AlgStats(max, st.get(max), distinct, words);
    }
  }

  public final static class AlgStats {
    public final String max;
    public final Integer maxFreq;
    public final int distinct;
    public final int words;

    public AlgStats(String max, Integer maxFreq, int distinct, int words) {
      this.max = max;
      this.maxFreq = maxFreq;
      this.distinct = distinct;
      this.words = words;
    }
  }

}
