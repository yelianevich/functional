package r.yelianevich.datastruct;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class IndexMaxPQTest {

  @Test
  void insertAndRemoveAll() {
    IndexMaxPQ<Character> pq = new IndexMaxPQ<>(11);
    pq.insert(1, 'A');
    pq.insert(11, 'K');
    pq.insert(7, 'G');
    pq.insert(10, 'J');
    pq.insert(5, 'E');
    pq.insert(6, 'F');
    pq.insert(8, 'H');

    List<Character> sortedKeys = new ArrayList<>();
    List<Integer> sortedIndexes = new ArrayList<>();
    while (!pq.isEmpty()) {
      sortedKeys.add(pq.maxKey());
      sortedIndexes.add(pq.delMax());
    }
    assertThat(sortedKeys).containsExactly('K', 'J', 'H', 'G', 'F', 'E', 'A');
    assertThat(sortedIndexes).containsExactly(11, 10, 8, 7, 6, 5, 1);
  }

  @Test
  void playWithIndices() {
    edu.princeton.cs.algs4.IndexMaxPQ <Character> pq = new edu.princeton.cs.algs4.IndexMaxPQ <>(16);
    pq.insert(1, 'A');
    pq.insert(11, 'K');
    pq.insert(7, 'G');
    pq.insert(10, 'J');
    pq.insert(5, 'E');
    pq.insert(6, 'F');
    pq.insert(8, 'H');

    assertThat(pq.size()).isEqualTo(7);
    assertThat(pq.contains(7)).isTrue();
    assertThat(pq.keyOf(7)).isEqualTo('G');
    pq.changeKey(7, 'X');
    assertThat(pq.keyOf(7)).isEqualTo('X');
    pq.delete(10);

    assertThat(pq.keyOf(1)).isEqualTo('A');
    assertThat(pq.keyOf(11)).isEqualTo('K');

    assertThat(pq.contains(10)).isFalse();
    assertThat(pq.size()).isEqualTo(6);

    // TODO not sure that this is correct behavior
    // assertThat(pq.maxKey()).isEqualTo('X');
    // assertThat(pq.maxIndex()).isEqualTo(11);

    List<Character> sortedKeys = new ArrayList<>();
    List<Integer> sortedIndexes = new ArrayList<>();
    while (!pq.isEmpty()) {
      sortedKeys.add(pq.maxKey());
      sortedIndexes.add(pq.delMax());
    }
    assertThat(sortedKeys).containsExactly('X', 'K', 'H', 'F', 'E', 'A');
    assertThat(sortedIndexes).containsExactly(7, 11, 8, 6, 5, 1);
  }

}