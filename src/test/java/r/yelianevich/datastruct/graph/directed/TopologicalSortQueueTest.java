package r.yelianevich.datastruct.graph.directed;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.assertj.core.api.Assertions.assertThat;

public class TopologicalSortQueueTest {

  public static final Logger log = LoggerFactory.getLogger(TopologicalSortQueueTest.class);

  @Test
  void topologicalOrder() {
    Digraph digraph = DepthFirstOrderTest.TINY_DAG;
    log.debug("topologicalOrder() digraph: {}", digraph);
    TopologicalSortQueue topologicalSort = new TopologicalSortQueue(digraph);
    Iterable<Integer> topologicalOrder = topologicalSort.getTopologicalOrder();
    log.debug("topologicalOrder() with queue: {}", topologicalOrder);
    assertThat(topologicalOrder)
        .as("Check topological order when using a queue: " +
            "starting from leaves to the root in parallel from all branches (kind of BFS)")
        .containsExactly(2, 8, 3, 0, 7, 1, 5, 6, 4, 9, 10, 11, 12);
  }
}
