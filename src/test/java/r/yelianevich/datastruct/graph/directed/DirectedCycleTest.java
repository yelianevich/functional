package r.yelianevich.datastruct.graph.directed;

import edu.princeton.cs.algs4.In;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import r.yelianevich.datastruct.AlgsTest;

import static org.assertj.core.api.Assertions.assertThat;

public class DirectedCycleTest {

  public static final Logger log = LoggerFactory.getLogger(DirectedCycleTest.class);

  public static final Digraph TINY_DGRAPH = new Digraph(new In(AlgsTest.scanner("algs4-data/tinyDG.txt")));

  @Test
  void cycle() {
    Digraph digraph = DigraphTest.TINY_DGRAPH;
    log.debug("cycle() digraph: {}", digraph);
    DirectedCycle dicycle = new DirectedCycle(digraph);
    assertThat(dicycle.hasCycle()).isTrue();
    assertThat(dicycle.cycle()).containsExactly(3, 2, 3);
  }
}