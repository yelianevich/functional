package r.yelianevich.datastruct.graph.directed;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class TopologicalSortTest {

  @Test
  void topologicalOrder() {
    Digraph digraph = DepthFirstOrderTest.TINY_DAG;
    TopologicalSort topologicalSort = new TopologicalSort(digraph);
    assertThat(topologicalSort.isDag()).isTrue();
    assertThat(topologicalSort.getTopologicalOrder())
        .as("Check topological sort puts all vertices according to precedence constraint (post-reverse order)")
        .containsExactly(8, 7, 2, 3, 0, 5, 1, 6, 9, 11, 10, 12, 4);
  }

  @Test
  void topologicalSortDoNotExist() {
    Digraph digraph = DigraphTest.TINY_DGRAPH;
    TopologicalSort topologicalSort = new TopologicalSort(digraph);
    assertThat(topologicalSort.isDag())
        .as("Check that topological sort reports that such sort is imposable")
        .isFalse();
    assertThat(topologicalSort.getTopologicalOrder()).isEmpty();
  }
}