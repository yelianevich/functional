package r.yelianevich.datastruct.graph.directed;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.assertj.core.api.Assertions.assertThat;

class TransitiveClosureTest {

  private static final Logger log = LoggerFactory.getLogger(DepthFirstOrderTest.class);

  @Test
  void reachable() {
    TransitiveClosure tc = new TransitiveClosure(DigraphTest.TINY_DGRAPH);
    log.debug("reachable() digraph: {}", DigraphTest.TINY_DGRAPH);
    assertThat(tc.reachable(11, 0)).isTrue();
    assertThat(tc.reachable(11, 7)).isFalse();
    assertThat(tc.reachable(12, 10)).isTrue();
  }
}