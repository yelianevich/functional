package r.yelianevich.datastruct.graph.directed;

import edu.princeton.cs.algs4.In;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import r.yelianevich.datastruct.AlgsTest;

import static org.assertj.core.api.Assertions.assertThat;

public class DepthFirstOrderTest {
  public static final Logger log = LoggerFactory.getLogger(DepthFirstOrderTest.class);

  public static final Digraph TINY_DAG = new Digraph(new In(AlgsTest.scanner("algs4-data/tinyDAG.txt")));

  @Test
  void traversals() {
    Digraph digraph = TINY_DAG;
    log.debug("traversals() digraph: {}", digraph);
    DepthFirstOrder traversals = new DepthFirstOrder(digraph);

    assertThat(traversals.pre())
        .as("Check that pre order traversal has the order of dfs() recursive calls")
        .containsExactly(0, 6, 4, 9, 12, 10, 11, 1, 5, 2, 3, 7, 8);

    assertThat(traversals.post())
        .as("Check post order - order of vertices when they are fully visited")
        .containsExactly(4, 12, 10, 11, 9, 6, 1, 5, 0, 3, 2, 7, 8);

    assertThat(traversals.postReverse())
        .as("Check post reversed order - from leafs to roots (topological sort)")
        .containsExactly(8, 7, 2, 3, 0, 5, 1, 6, 9, 11, 10, 12, 4);
  }
}
