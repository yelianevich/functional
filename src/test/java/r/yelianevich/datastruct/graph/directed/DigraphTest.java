package r.yelianevich.datastruct.graph.directed;

import edu.princeton.cs.algs4.In;
import org.junit.jupiter.api.Test;
import r.yelianevich.datastruct.AlgsTest;

import static org.assertj.core.api.Assertions.assertThat;

public class DigraphTest {

  public static final Digraph TINY_DGRAPH = new Digraph(new In(AlgsTest.scanner("algs4-data/tinyDG.txt")));

  @Test
  void createGraph() {
    Digraph graph = TINY_DGRAPH;
    assertThat(graph.V()).isEqualTo(13);
    assertThat(graph.E()).isEqualTo(22);
    assertThat(graph.adj(4)).contains(2, 3);
    assertThat(graph.adj(11)).contains(4, 12);
    assertThat(graph.adj(6)).contains(0, 4, 9, 8);
  }
}