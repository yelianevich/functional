package r.yelianevich.datastruct.graph.undirected;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class DegreeOfSeparationClientTest {

  @Test
  void pathTo() {
    SymbolGraph sg = new SymbolGraph("algs4-data/routes.txt", " ");
    DegreeOfSeparationClient client = new DegreeOfSeparationClient(sg, "JFK");

    assertThat(client.connected("NOT_AN_AIRPORT")).isFalse();
    assertThat(client.connected("LAX")).isTrue();
    assertThat(client.pathTo("LAX")).containsExactly("JFK", "ORD", "PHX", "LAX");
  }
}
