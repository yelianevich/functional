package r.yelianevich.datastruct.graph.undirected;

import edu.princeton.cs.algs4.In;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import r.yelianevich.datastruct.AlgsTest;

import static org.assertj.core.api.Assertions.assertThat;

public class CycleTest {

  public static final Logger log = LoggerFactory.getLogger(CycleTest.class);

  @Test
  void cycle() {
    log.debug("cycle() graph: {}", GraphTest.TINY_GRAPH);
    Cycle tinyCycle = new Cycle(GraphTest.TINY_GRAPH);
    assertThat(tinyCycle.hasCycle()).isTrue();
    assertThat(tinyCycle.cycle()).containsExactly(3, 5, 4, 3);
  }

  @Test
  void noCycle() {
    Graph tinyAcyclicGraph = new Graph(new In(AlgsTest.scanner("algs4-data/tinyAcyclicG.txt")));
    log.debug("noCycle() graph: {}", tinyAcyclicGraph);
    assertThat(new Cycle(tinyAcyclicGraph).hasCycle()).isFalse();
  }
}
