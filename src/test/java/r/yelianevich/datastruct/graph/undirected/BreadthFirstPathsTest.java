package r.yelianevich.datastruct.graph.undirected;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class BreadthFirstPathsTest {

  @Test
  void hasPathTo() {
    BreadthFirstPaths paths = new BreadthFirstPaths(GraphTest.TINY_GRAPH, 0);

    assertThat(paths.hasPathTo(7))
        .as("Check not-connected component has not path to source 0")
        .isFalse();

    assertThat(paths.hasPathTo(11))
        .as("Check not-connected component has not path to source 0")
        .isFalse();

    assertThat(paths.hasPathTo(4))
        .as("Connected vertex 4 has path to source 0")
        .isTrue();
  }

  @Test
  void pathTo() {
    assertThat(new BreadthFirstPaths(GraphTest.TINY_GRAPH, 0).pathTo(3))
        .as("Check shortest path to 3")
        .containsExactly(0, 5, 3);

    assertThat(new BreadthFirstPaths(GraphTest.TINY_GRAPH, 12).pathTo(10))
        .as("Check shortest path to 10")
        .containsExactly(12, 9, 10);
  }
}