package r.yelianevich.datastruct.graph.undirected;

import edu.princeton.cs.algs4.In;
import org.junit.jupiter.api.Test;
import r.yelianevich.datastruct.AlgsTest;

import static org.assertj.core.api.Assertions.assertThat;

class BipartiteTest {

  @Test
  void isBipartite() {
    Graph graph = new Graph(new In(AlgsTest.scanner("algs4-data/tinyBipartiteG.txt")));
    assertThat(new Bipartite(graph).isBipartite()).isTrue();
  }

  @Test
  void isNotBipartite() {
    Bipartite query = new Bipartite(GraphTest.TINY_GRAPH);
    assertThat(query.isBipartite()).isFalse();
  }
}