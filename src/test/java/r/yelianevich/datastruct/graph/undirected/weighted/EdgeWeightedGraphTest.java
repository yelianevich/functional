package r.yelianevich.datastruct.graph.undirected.weighted;

import edu.princeton.cs.algs4.In;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import r.yelianevich.datastruct.AlgsTest;

import static org.assertj.core.api.Assertions.assertThat;

public class EdgeWeightedGraphTest {
  private static final Logger log = LoggerFactory.getLogger(EdgeWeightedGraphTest.class);

  public static final EdgeWeightedGraph graph = new EdgeWeightedGraph(new In(AlgsTest.scanner("algs4-data/tinyEWG.txt")));

  @Test
  void addEdge() {
    log.info(graph.toString());
    assertThat(graph.V()).isEqualTo(8);
    assertThat(graph.E()).isEqualTo(16);
    assertThat(graph.adj(0)).containsOnly(
        new Edge(0, 7, 0.16),
        new Edge(0, 4, 0.38),
        new Edge(0, 2, 0.26),
        new Edge(6, 0, 0.58)
    );
  }
}
