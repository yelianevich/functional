package r.yelianevich.datastruct.graph.undirected.weighted;

import org.assertj.core.data.Offset;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class PrimMstTest extends EdgeWeightedGraphTest {

  @Test
  void edges() {
    PrimMst mst = new PrimMst(graph);
    assertThat(mst.edges()).containsOnly(
        new Edge(0, 7, 0.16),
        new Edge(1, 7, 0.19),
        new Edge(0, 2, 0.26),
        new Edge(2, 3, 0.17),
        new Edge(5, 7, 0.28),
        new Edge(4, 5, 0.35),
        new Edge(6, 2, 0.4)
    );
  }

  @Test
  void weight() {
    PrimMst mst = new PrimMst(graph);
    double totalWeight = 0.16 + 0.19 + 0.26 + 0.17 + 0.28 + 0.35 + 0.4;
    assertThat(mst.weight()).isCloseTo(totalWeight, Offset.offset(0.01));
  }

}