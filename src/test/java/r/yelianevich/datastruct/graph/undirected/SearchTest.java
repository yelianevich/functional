package r.yelianevich.datastruct.graph.undirected;

import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

class SearchTest {

  @Test
  void checkSearchOnATinyGraph() {
    Graph graph = GraphTest.TINY_GRAPH;

    int source = 0;
    Search search = new Search(graph, source);
    Set<Integer> connectedNodes = new HashSet<>();
    for (int v = 0; v < graph.V(); v++) {
      if (search.marked(v)) {
        connectedNodes.add(v);
      }
    }

    assertThat(connectedNodes)
        .as("Check connected nodes to the source")
        .containsExactly(0, 1, 2, 3, 4, 5, 6);

    assertThat(search.count())
        .as("Check that graph is not connected")
        .isNotEqualTo(graph.V());
  }
}