package r.yelianevich.datastruct.graph.undirected;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class DepthFirstPathsTest {


  @Test
  void hasPathTo() {
    DepthFirstPaths paths = new DepthFirstPaths(GraphTest.TINY_GRAPH, 0);

    assertThat(paths.hasPathTo(7))
        .as("Check not-connected component has not path to source 0")
        .isFalse();

    assertThat(paths.hasPathTo(11))
        .as("Check not-connected component has not path to source 0")
        .isFalse();

    assertThat(paths.hasPathTo(4))
        .as("Connected vertex 4 has path to source 0")
        .isTrue();
  }

  @Test
  void pathTo() {
    DepthFirstPaths paths = new DepthFirstPaths(GraphTest.TINY_GRAPH, 0);

    assertThat(paths.pathTo(4))
        .as("Check existing path to 4")
        .containsExactly(0, 5, 4);
  }
}