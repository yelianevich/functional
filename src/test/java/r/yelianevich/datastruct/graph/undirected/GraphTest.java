package r.yelianevich.datastruct.graph.undirected;

import edu.princeton.cs.algs4.In;
import org.junit.jupiter.api.Test;
import r.yelianevich.datastruct.AlgsTest;

import static org.assertj.core.api.Assertions.assertThat;

public class GraphTest {

  public static final Graph TINY_GRAPH = new Graph(new In(AlgsTest.scanner("algs4-data/tinyG.txt")));

  @Test
  void createGraph() {
    Graph graph = TINY_GRAPH;
    assertThat(graph.E()).isEqualTo(13);
    assertThat(graph.V()).isEqualTo(13);
    assertThat(graph.adj(0)).contains(6, 2, 1, 5);
  }
}