package r.yelianevich.datastruct.graph.undirected;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class ConnectedComponentsTest {

  @Test
  void connectedComponents() {
    ConnectedComponents cc = new ConnectedComponents(GraphTest.TINY_GRAPH);
    @SuppressWarnings("unchecked")
    List<Integer>[] ccById = (List<Integer>[]) new ArrayList[cc.count()];
    for (int i = 0; i < cc.count(); ++i) {
      ccById[i] = new ArrayList<>();
    }
    for (int v = 0; v < GraphTest.TINY_GRAPH.V(); ++v) {
      ccById[cc.id(v)].add(v);
    }
    assertThat(ccById[0]).containsOnly(0, 1, 2, 3, 4, 5, 6);
    assertThat(ccById[1]).containsOnly(7, 8);
    assertThat(ccById[2]).containsOnly(9, 10, 11, 12);
  }

  @Test
  void connected() {
    ConnectedComponents cc = new ConnectedComponents(GraphTest.TINY_GRAPH);
    assertThat(cc.connected(0, 6)).isTrue();
    assertThat(cc.connected(7, 7)).isTrue();
    assertThat(cc.connected(9, 11)).isTrue();
    assertThat(cc.connected(0, 7)).isFalse();
    assertThat(cc.connected(8, 11)).isFalse();
  }

  @Test
  void count() {
    ConnectedComponents cc = new ConnectedComponents(GraphTest.TINY_GRAPH);
    assertThat(cc.count()).isEqualTo(3);
  }
}