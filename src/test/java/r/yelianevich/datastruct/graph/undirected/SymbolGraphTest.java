package r.yelianevich.datastruct.graph.undirected;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class SymbolGraphTest {

  @Test
  void routesGraph() {
    SymbolGraph sg = new SymbolGraph("algs4-data/routes.txt", " ");
    Graph graph = sg.graph();

    int jfk = sg.index("JFK");
    assertThat(graph.adj(jfk)).containsOnly(
        sg.index("ORD"),
        sg.index("ATL"),
        sg.index("MCO")
    );
    int lax = sg.index("LAX");
    assertThat(graph.adj(lax)).containsOnly(
        sg.index("LAS"),
        sg.index("PHX")
    );
  }

  @Test
  void routesDegreeOfSeparation() {
    SymbolGraph sg = new SymbolGraph("algs4-data/routes.txt", " ");
    Graph graph = sg.graph();

    int jfk = sg.index("JFK");
    assertThat(graph.adj(jfk)).containsOnly(
        sg.index("ORD"),
        sg.index("ATL"),
        sg.index("MCO")
    );
    int lax = sg.index("LAX");
    assertThat(graph.adj(lax)).containsOnly(
        sg.index("LAS"),
        sg.index("PHX")
    );
  }

  @Disabled("Please run manually, cause file is too big). Unpack algs4-data.zip into algs4-data-all folder and run")
  @Test
  void moviesGraph() {
    String filename = "algs4-data-all/movies.txt";
    String delim = "/";

    SymbolGraph sg = new SymbolGraph(filename, delim);
    Graph graph = sg.graph();

    assertThat(graph.adj(sg.index("Tin Men (1987)")))
        .contains(sg.index("Hershey, Barbara"));
  }
}
