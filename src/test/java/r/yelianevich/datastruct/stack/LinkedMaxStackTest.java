package r.yelianevich.datastruct.stack;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class LinkedMaxStackTest {

  @Test
  void max() {
    // create stack and verify that content is correct
    LinkedMaxStack<Integer> s = new LinkedMaxStack<>();
    s.push(5);
    s.push(4);
    s.push(3);
    s.push(10);
    s.push(9);
    s.push(8);
    s.push(1);
    assertThat(s).containsExactly(1, 8, 9, 10, 3, 4, 5);

    // pop an item and check the max
    assertThat(s.max()).isEqualTo(10);
    assertThat(s.pop()).isEqualTo(1);
    assertThat(s.max()).isEqualTo(10);
    assertThat(s.pop()).isEqualTo(8);
    assertThat(s.max()).isEqualTo(10);
    assertThat(s.pop()).isEqualTo(9);
    assertThat(s.max()).isEqualTo(10);
    assertThat(s.pop()).isEqualTo(10);
    assertThat(s.max()).isEqualTo(5);
    assertThat(s.pop()).isEqualTo(3);
    assertThat(s.max()).isEqualTo(5);
    assertThat(s.pop()).isEqualTo(4);
    assertThat(s.max()).isEqualTo(5);
    assertThat(s.pop()).isEqualTo(5);
    assertThat(s.isEmpty()).isTrue();
  }

  @Test
  void popMax() {
    LinkedMaxStack<Integer> s = new LinkedMaxStack<>();
    s.push(-29);
    s.push(-74);
    assertThat(s.popMax()).isEqualTo(-29);
    assertThat(s.popMax()).isEqualTo(-74);
    s.push(-4);
    s.push(20);
    s.push(68);
    s.push(83);
    s.push(73);
    assertThat(s.popMax()).isEqualTo(83);
    assertThat(s.popMax()).isEqualTo(73);
  }

}