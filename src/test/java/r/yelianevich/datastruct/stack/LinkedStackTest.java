package r.yelianevich.datastruct.stack;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


class LinkedStackTest {

  @Test
  void stackUsageScenario() {
    LinkedStack<Integer> s = new LinkedStack<>();
    assertThat(s.isEmpty()).isTrue();
    s.push(3);
    s.push(2);
    s.push(1);
    assertThat(s).containsExactly(1, 2, 3);
    assertThat(s.isEmpty()).isFalse();
    assertThat(s.pop()).isEqualTo(1);
    assertThat(s.pop()).isEqualTo(2);
    assertThat(s.pop()).isEqualTo(3);
    assertThat(s.isEmpty()).isTrue();
  }
}