package r.yelianevich.puzzle;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class PrimesTest {

  @Test
  void isPrime() {
    assertThat(Primes.isPrime(1)).isFalse();
    assertThat(Primes.isPrime(2)).isTrue();
    assertThat(Primes.isPrime(3)).isTrue();
    assertThat(Primes.isPrime(4)).isFalse();
    assertThat(Primes.isPrime(5)).isTrue();
    assertThat(Primes.isPrime(6)).isFalse();
    assertThat(Primes.isPrime(7)).isTrue();
    assertThat(Primes.isPrime(8)).isFalse();
    assertThat(Primes.isPrime(13)).isTrue();
    assertThat(Primes.isPrime(17)).isTrue();
    assertThat(Primes.isPrime(18)).isFalse();
  }

  @Test
  void sieveOfEratosthenes() {
    assertThat(Primes.sieveOfEratosthenes(19)).containsExactly(2, 3, 5, 7, 11, 13, 17, 19);
    assertThat(Primes.sieveOfEratosthenes(20)).containsExactly(2, 3, 5, 7, 11, 13, 17, 19);
    assertThat(Primes.sieveOfEratosthenes(1)).isEmpty();
    assertThat(Primes.sieveOfEratosthenes(8)).containsExactly(2, 3, 5, 7);
  }
}