package r.yelianevich.puzzle;

import org.assertj.core.data.Percentage;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class ApocalypseTest {

  @Test
  void simulatePopulation() {
    Apocalypse.Population population = Apocalypse.simulatePopulation(1_000_000);
    System.out.println(population);
    assertThat(population.women).isEqualTo(1_000_000);
    assertThat(population.men).isCloseTo(1_000_000, Percentage.withPercentage(5));
  }
}