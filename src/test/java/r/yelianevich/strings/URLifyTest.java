package r.yelianevich.strings;

import org.junit.jupiter.api.Test;
import r.yelianevich.algorithms.strings.URLify;

import static org.assertj.core.api.Assertions.assertThat;

class URLifyTest {

  @Test
  void replaceSpaces() {
    char[] str = "Mr John Smith    ".toCharArray();
    URLify.replaceSpaces(str, 13);
    assertThat(str).isEqualTo("Mr%20John%20Smith".toCharArray());
  }
}