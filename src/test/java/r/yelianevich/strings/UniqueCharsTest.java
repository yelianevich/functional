package r.yelianevich.strings;

import org.junit.jupiter.api.Test;
import r.yelianevich.algorithms.strings.UniqueChars;

import static org.assertj.core.api.Assertions.assertThat;

class UniqueCharsTest {

  @Test
  void isUniqueAsciiChars() {
    assertThat(UniqueChars.isUniqueAsciiChars("abcdefgABCDEFG"))
        .as("Unique ASCII string works")
        .isTrue();
    assertThat(UniqueChars.isUniqueAsciiChars("abcdefgABCDEFa"))
        .as("Check that duplicated 'a' makes it not unique")
        .isFalse();
  }

  @Test
  void isUniqueLowerEnglish() {
    assertThat(UniqueChars.isUniqueLowerEnglish("abcdefg"))
        .as("Unique a-z string works")
        .isTrue();
    assertThat(UniqueChars.isUniqueAsciiChars("abcdefga"))
        .as("Check that duplicated 'a' makes it not unique")
        .isFalse();
  }

  @Test
  void isUnique() {
    assertThat(UniqueChars.isUnique("abcdefgABCDEFG"))
        .as("Unique ASCII string works")
        .isTrue();
    assertThat(UniqueChars.isUnique("abcdefgABCDEFa"))
        .as("Check that duplicated 'a' makes it not unique")
        .isFalse();
    assertThat(UniqueChars.isUnique("±abcdefgABCDEFa±"))
        .as("Check that duplicated 'a' makes it not unique")
        .isFalse();
  }
}