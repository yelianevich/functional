package r.yelianevich.strings;

import org.junit.jupiter.api.Test;
import r.yelianevich.algorithms.strings.CharsPermutations;

import static org.assertj.core.api.Assertions.assertThat;

class CharsPermutationsTest {

  @Test
  void arePermutations() {
    assertThat(CharsPermutations.arePermutations("qwertyuiop", "poiuytrewq"))
        .as("Check that reversed strings are permutations")
        .isTrue();

    assertThat(CharsPermutations.arePermutations("qwertyuiop", "poiuytrew!"))
        .as("Check '!' makes them different")
        .isFalse();

    assertThat(CharsPermutations.arePermutations("aa", "aabb!")).isFalse();
  }

  @Test
  void areAsciiPermutations() {
    assertThat(CharsPermutations.areAsciiPermutations("qwertyuiop", "poiuytrewq"))
        .as("Check that reversed strings are permutations")
        .isTrue();

    assertThat(CharsPermutations.areAsciiPermutations("qwertyuiop", "poiuytrew!"))
        .as("Check '!' makes them different")
        .isFalse();

    assertThat(CharsPermutations.areAsciiPermutations("aa", "aabb!")).isFalse();
  }
}