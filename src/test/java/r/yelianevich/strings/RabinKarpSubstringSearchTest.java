package r.yelianevich.strings;

import org.junit.jupiter.api.Test;
import r.yelianevich.algorithms.strings.RabinKarpSubstringSearch;

import static org.assertj.core.api.Assertions.assertThat;

class RabinKarpSubstringSearchTest {

  @Test
  void indexOf() {
    assertThat(RabinKarpSubstringSearch.indexOf("abc", "abc")).isEqualTo(0);
    assertThat(RabinKarpSubstringSearch.indexOf("xabc", "abc")).isEqualTo(1);
    assertThat(RabinKarpSubstringSearch.indexOf("xxxxxabc", "abc")).isEqualTo(5);
    assertThat(RabinKarpSubstringSearch.indexOf("xxxxxabcyyyyy", "abc")).isEqualTo(5);
    assertThat(RabinKarpSubstringSearch.indexOf("xxxxxabcyyyyy", "abcd")).isEqualTo(-1);
  }

  @Test
  void indexOfLargePattern() {
    String text = "fourscoreandsevenyearsagoourfathersbroughtforthuponthiscontinentanewnation";
    String pattern = "ourfathersbroughtforthuponthiscontinentanewnation";
    assertThat(RabinKarpSubstringSearch.indexOf(text, pattern)).isEqualTo(25);
  }
}