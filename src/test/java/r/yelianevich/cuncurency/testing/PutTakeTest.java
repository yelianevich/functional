package r.yelianevich.cuncurency.testing;

import r.yelianevich.cuncurency.ExecutorUtil;

import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.LongAdder;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * Guidelines:
 * <li>Should be run on a multiprocessor system</li>
 * <li>Use more threads than CPUs to increase context switching</li>
 * <li>Start concurrent thread simultaneous to increase contention</li>
 */
public class PutTakeTest implements AutoCloseable {
  private final ExecutorService pool = Executors.newCachedThreadPool();
  private final LongAdder putSum = new LongAdder();
  private final LongAdder takeSum = new LongAdder();
  private final CyclicBarrier barrier;
  private final BarierTimer barierTimer = new BarierTimer();
  private final BoundedBuffer<Integer> queue;
  private final int trials;
  private final int pairs;

  public PutTakeTest(int capacity, int trials, int pairs) {
    this.queue = new BoundedBuffer<>(capacity);
    this.trials = trials;
    this.pairs = pairs;
    this.barrier = new CyclicBarrier(2 * pairs + 1, barierTimer);
  }

  public void start() {
    try {
      for (int i = 0; i < pairs; ++i) {
        pool.execute(new Producer());
        pool.execute(new Consumer());
      }
      barrier.await(); // waits everybody to ROCK
      barrier.await(); // waits everybody to finish
      long nsPerItem = barierTimer.getTime() / (trials * (long) pairs);
      System.out.println("Throughput: " + nsPerItem + " ns per item");
      System.out.println("Total time " + TimeUnit.NANOSECONDS.toMillis(barierTimer.getTime()) + " ms");
      assertThat(takeSum.longValue()).isEqualTo(putSum.longValue());
    } catch (Exception e) {
      throw new RuntimeException("Test failed", e);
    }
  }

  public void close() {
    ExecutorUtil.shutdownAndAwaitTermination(pool, 5, TimeUnit.SECONDS);
  }

  class Producer implements Runnable {
    public void run() {
      int seed = this.hashCode() ^ (int) System.nanoTime();
      long sum = 0;
      try {
        barrier.await();
        for (int i = 0; i < trials; ++i) {
          queue.put(seed);
          sum += seed;
          seed = xorShift(seed);
        }
        putSum.add(sum);
        barrier.await();
      } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }
  }

  /**
   * Cheap random function to avoid synchronization
   */
  public static int xorShift(int x) {
    x ^= (x << 6);
    x ^= (x >>> 21);
    x ^= (x << 7);
    return x;
  }

  class Consumer implements Runnable {
    public void run() {
      try {
        barrier.await();
        long sum = 0;
        for (int i = 0; i < trials; ++i) {
          int take = queue.take();
          sum += take;
        }
        takeSum.add(sum);
        barrier.await();
      } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }
  }

  public static void main(String[] args) {
    long start = System.currentTimeMillis();
    try (PutTakeTest test = new PutTakeTest(40, 1_000_000, 20)) {
      test.start();
      System.out.println("Test completed successfully");
    }
    System.out.println("Test duration: " + (System.currentTimeMillis() - start) + " ms");
  }
}
