package r.yelianevich.cuncurency.testing;

import org.junit.jupiter.api.Test;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

class BlockOperationTest {

    @Test
    void blockingOperation() throws InterruptedException {
        LinkedBlockingQueue<Integer> queue = new LinkedBlockingQueue<>();
        Thread taker = new Thread(() -> {
            try {
                Integer unused = queue.take();
              fail(""); // registers a failure if take() on empty queue succeed
                throw new IllegalStateException();
            } catch (InterruptedException success) { }
        });
        taker.start();
        TimeUnit.MILLISECONDS.sleep(500);
        taker.interrupt();
        taker.join(TimeUnit.SECONDS.toMillis(1));
      assertThat(taker.isAlive()).isFalse();
    }

}