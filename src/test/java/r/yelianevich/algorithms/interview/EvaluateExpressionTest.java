package r.yelianevich.algorithms.interview;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


class EvaluateExpressionTest {

  @Test
  void infixBasic() {
    // check parenthesis balance
    Double res = EvaluateExpression.infix("( 1 + ( ( 2 + 3 ) * ( 4 * 5 ) ) )");
    assertThat(res).isEqualTo(101);
  }

}