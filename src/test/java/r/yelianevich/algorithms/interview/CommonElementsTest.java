package r.yelianevich.algorithms.interview;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class CommonElementsTest {

  @Test
  void countCommon() {
    int[] a = {13, 27, 35, 40, 49, 55, 59, 70, 90};
    int[] b = {17, 35, 39, 40, 55, 58, 60};
    assertThat(CommonElements.countCommon(a, b)).isEqualTo(3);
  }
}