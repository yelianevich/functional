package r.yelianevich.algorithms.interview;

import edu.princeton.cs.algs4.Stopwatch;
import org.junit.jupiter.api.Test;
import r.yelianevich.datastruct.AlgsTest;

import static org.assertj.core.api.Assertions.assertThat;

class TreeSumTest {

  @Test
  void brutForce() {
    Stopwatch sw = new Stopwatch();
    int[] a = AlgsTest.readAllInts("algs4-data/1Kints.txt");
    int c = TreeSum.brutForce(a);
    assertThat(c).isEqualTo(70);
    System.out.println(c);
    System.out.println("Time = " + sw.elapsedTime());
  }

  @Test
  void fast() {
    Stopwatch sw = new Stopwatch();
    int[] a = AlgsTest.readAllInts("algs4-data/1Kints.txt");
    int c = TreeSum.fast(a);
    assertThat(c).isEqualTo(70);
    System.out.println(c);
    System.out.println("Time = " + sw.elapsedTime());
  }

}