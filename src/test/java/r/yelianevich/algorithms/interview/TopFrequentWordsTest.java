package r.yelianevich.algorithms.interview;

import org.junit.jupiter.api.Test;
import r.yelianevich.algorithms.sort.SortUtil;

import static org.assertj.core.api.Assertions.assertThat;

class TopFrequentWordsTest {

  @Test
  void topN() {
    String[] words = {
        "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", // 10
        "b", "b", "b", "b", "b", "b", // 6
        "c", "c", "c", "c", "c", // 5
        "d", "d", "d", "d", // 4
        "e", "e", "e", "e", // 4
        "f", "f", "f", // 3
        "g", "g" // 2
    };
    SortUtil.shuffle(words);
    assertThat(TopFrequentWords.topMostFrequent(3, words)).containsExactly("a", "b", "c");
  }
}