package r.yelianevich.algorithms.interview;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class BitonicTest {

  @Test
  void searchInTheMiddle() {
    assertThat(Bitonic.search(new int[]{1, 2, 3, 4, 5, 0, -1, -2, -3}, 3)).isEqualTo(2);
    assertThat(Bitonic.search(new int[]{1, 2, 3, 4, 5, 0, -1, -2, -3}, -2)).isEqualTo(7);
    assertThat(Bitonic.search(new int[]{1, 2, 3, 4, 5, 0, -1, -2, -3}, -1)).isEqualTo(6);
  }

  @Test
  void searchFirst() {
    int i = Bitonic.search(new int[]{1, 2, 3, 4, 5, 0, -1, -2, -3}, 1);
    assertThat(i).isEqualTo(0);
  }

  @Test
  void searchLast() {
    int i = Bitonic.search(new int[]{1, 2, 3, 4, 5, 0, -1, -2, -3}, -3);
    assertThat(i).isEqualTo(8);
  }

  @Test
  void searchPivot() {
    int[] a = {1, 2, 3, 4, 5, 0, -1, -2, -3};
    assertThat(Bitonic.search(a, 0)).isEqualTo(5);
    assertThat(Bitonic.search(a, 5)).isEqualTo(4);
  }

  @Test
  void bitonicPoint() {
    assertThat(Bitonic.bitonicPoint(new int[]{1, 2, 3, 4, 5, 0, -1, -2, -3})).isEqualTo(4);
    assertThat(Bitonic.bitonicPoint(new int[]{1, 2, 0, -1, -2, -3, -4, -5})).isEqualTo(1);
    assertThat(Bitonic.bitonicPoint(new int[]{1, 2, 3, 4, 5, 6, 7, 0})).isEqualTo(6);
  }

  /**
   * Test fast implementation
   */
  @Test
  void searchInTheMiddleFast() {
    assertThat(Bitonic.fast(new int[]{1, 2, 3, 4, 5, 0, -1, -2, -3}, 3)).isEqualTo(2);
    assertThat(Bitonic.fast(new int[]{1, 2, 3, 4, 5, 0, -1, -2, -3}, -2)).isEqualTo(7);
    assertThat(Bitonic.fast(new int[]{1, 2, 3, 4, 5, 0, -1, -2, -3}, -1)).isEqualTo(6);
  }

  @Test
  void searchFirstFast() {
    int i = Bitonic.fast(new int[]{1, 2, 3, 4, 5, 0, -1, -2, -3}, 1);
    assertThat(i).isEqualTo(0);
  }

  @Test
  void searchLastFast() {
    int i = Bitonic.fast(new int[]{1, 2, 3, 4, 5, 0, -1, -2, -3}, -3);
    assertThat(i).isEqualTo(8);
  }

  @Test
  void searchPivotFast() {
    int[] a = {1, 2, 3, 4, 5, 0, -1, -2, -3};
    assertThat(Bitonic.fast(a, 0)).isEqualTo(5);
    assertThat(Bitonic.fast(a, 5)).isEqualTo(4);
  }

}