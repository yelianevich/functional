package r.yelianevich.algorithms.interview;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


class BinaryTest {

  @Test
  void insertBits() {
    int n = 0b10000000000;
    int m = 0b10011;
    assertThat(Binary.insertBits(n, m, 2, 6)).isEqualTo(0b10001001100);
  }

  @Test
  void printLong() {
    assertThat(Binary.toString(12)).isEqualTo("1100");
    assertThat(Binary.toString(-3)).isEqualTo(Long.toBinaryString(-3));
    assertThat(Binary.toString(0)).isEqualTo(Long.toBinaryString(0));
  }

  @Test
  void printDouble() {
    assertThat(Binary.toString(0.72)).isEqualTo("ERROR");
    assertThat(Binary.toString(0.625)).isEqualTo("0.101");
    assertThat(Binary.toString(1.625)).isEqualTo("1.101");
    assertThat(Binary.toString(8.625)).isEqualTo("1000.101");
  }

  @Test
  void flipBitToWin() {
    assertThat(Binary.flipBitToWinBruteForce(0b110_1110_1111)).isEqualTo(8);
    assertThat(Binary.flipBitToWinOptimal(0b110_1110_1111)).isEqualTo(8);

    assertThat(Binary.flipBitToWinBruteForce(0b1110)).isEqualTo(4);
    assertThat(Binary.flipBitToWinOptimal(0b1110)).isEqualTo(4);

    assertThat(Binary.flipBitToWinBruteForce(0b1)).isEqualTo(2);
    assertThat(Binary.flipBitToWinOptimal(0b1)).isEqualTo(2);

    assertThat(Binary.flipBitToWinBruteForce(0b0)).isEqualTo(1);
    assertThat(Binary.flipBitToWinOptimal(0b0)).isEqualTo(1);

    assertThat(Binary.flipBitToWinBruteForce(0xFFFFFFFF)).isEqualTo(32);
    assertThat(Binary.flipBitToWinOptimal(0xFFFFFFFF)).isEqualTo(32);
  }

  @Test
  void nextAndPrevNumber() {
    assertThat(Binary.nextNumber(0xEFFF0000)).isEqualTo(-1);
    assertThat(Binary.prevNumber(0x0000FFFF)).isEqualTo(-1);

    assertThat(Binary.nextNumber(0b11_0110_0111_1100)).isEqualTo(0b11_0110_1000_1111);
    assertThat(Binary.prevNumber(0b10_0111_1000_0011)).isEqualTo(0b10_0111_0111_0000);

    assertThat(Binary.nextNumber(0b101)).isEqualTo(0b110);
    assertThat(Binary.prevNumber(0b110)).isEqualTo(0b101);

    assertThat(Binary.prevNumber(0b1000)).isEqualTo(0b0100);
    assertThat(Binary.nextNumber(0b1000)).isEqualTo(0b10000);
  }

  @Test
  void isPowerOfTwo() {
    assertThat(Binary.isPowerOfTwo(0)).isEqualTo(true);
    assertThat(Binary.isPowerOfTwo(1)).isEqualTo(true);
    assertThat(Binary.isPowerOfTwo(2)).isEqualTo(true);
    assertThat(Binary.isPowerOfTwo(4)).isEqualTo(true);
    assertThat(Binary.isPowerOfTwo((1 << 10))).isEqualTo(true);
    assertThat(Binary.isPowerOfTwo(3)).isEqualTo(false);
  }

  @Test
  void countBitsDiff() {
    assertThat(Binary.countBitsDiffSimple(0b11101, 0b01111)).isEqualTo(2);
    assertThat(Binary.countBitsDiffOptimal(0b11101, 0b01111)).isEqualTo(2);
  }

  @Test
  void pairwiseSwap() {
    assertThat(Binary.pairwiseSwap(0b00110110)).isEqualTo(0b00111001);
  }

  @Test
  void drawLine() {
    byte[] screen3x3 = new byte[]{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    Binary.drawLine(screen3x3, 24, 0, 3, 0);
    assertThat(screen3x3).containsExactly(0xF0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);

    screen3x3 = new byte[]{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    Binary.drawLine(screen3x3, 24, 0, 7, 0);
    assertThat(screen3x3).containsExactly(0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);

    screen3x3 = new byte[]{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    Binary.drawLine(screen3x3, 24, 0, 10, 0);
    assertThat(screen3x3).containsExactly(0xFF, 0xE0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);

    byte[] screen = new byte[]{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    Assertions.assertThatThrownBy(() -> Binary.drawLine(screen, 24, 0, 71, 0))
        .hasMessage("Not a horizontal line");
  }
}
