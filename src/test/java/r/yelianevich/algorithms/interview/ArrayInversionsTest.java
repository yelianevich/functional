package r.yelianevich.algorithms.interview;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


class ArrayInversionsTest {

  @Test
  void countOn2Elem() {
    assertThat(ArrayInversions.count(new int[] {1, 2})).isEqualTo(0);
    assertThat(ArrayInversions.count(new int[] {2, 1})).isEqualTo(1);
  }

  @Test
  void countOn3Elem() {
    assertThat(ArrayInversions.count(new int[] {1, 2, 3})).isEqualTo(0);
    assertThat(ArrayInversions.count(new int[] {1, 3, 2})).isEqualTo(1);
    assertThat(ArrayInversions.count(new int[] {3, 1, 2})).isEqualTo(2);
    assertThat(ArrayInversions.count(new int[] {3, 2, 1})).isEqualTo(3);
  }

  @Test
  void countOn4Elem() {
    assertThat(ArrayInversions.count(new int[] {1, 2, 3, 4})).isEqualTo(0);
    assertThat(ArrayInversions.count(new int[] {1, 2, 4, 3})).isEqualTo(1);
    assertThat(ArrayInversions.count(new int[] {1, 4, 2, 3})).isEqualTo(2);
    assertThat(ArrayInversions.count(new int[] {4, 1, 2, 3})).isEqualTo(3);
    assertThat(ArrayInversions.count(new int[] {4, 1, 3, 2})).isEqualTo(4);
    assertThat(ArrayInversions.count(new int[] {4, 3, 1, 2})).isEqualTo(5);
    assertThat(ArrayInversions.count(new int[] {4, 3, 2, 1})).isEqualTo(6);
  }

}