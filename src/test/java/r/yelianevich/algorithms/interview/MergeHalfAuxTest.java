package r.yelianevich.algorithms.interview;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


class MergeHalfAuxTest {

  @Test
  void merge() {
    int[] a = new int[]{1, 2, 3, 4, 5, 6, 7, 8};
    MergeHalfAux.merge(a);
    assertThat(a).containsExactly(1, 2, 3, 4, 5, 6, 7, 8);
  }

  @Test
  void mergeReversed() {
    int[] a = new int[]{5, 6, 7, 8, 1, 2, 3, 4};
    MergeHalfAux.merge(a);
    assertThat(a).containsExactly(1, 2, 3, 4, 5, 6, 7, 8);
  }

  @Test
  void mergeMixed() {
    int[] a = new int[]{1, 4, 6, 7, 2, 3, 5, 8};
    MergeHalfAux.merge(a);
    assertThat(a).containsExactly(1, 2, 3, 4, 5, 6, 7, 8);
  }

}