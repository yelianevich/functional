package r.yelianevich.algorithms.dynamic;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class EightQueensTest {

  @Test
  void placeQueens() {
    assertThat(EightQueens.placeQueens()).isEqualTo(92);
  }
}