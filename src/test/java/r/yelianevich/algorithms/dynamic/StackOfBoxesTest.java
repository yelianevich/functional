package r.yelianevich.algorithms.dynamic;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class StackOfBoxesTest {

  @Test
  void maxHeightOfCubes() {
    List<StackOfBoxes.Box> boxes = new ArrayList<>(Arrays.asList(
        new StackOfBoxes.Box(1, 1, 1),
        new StackOfBoxes.Box(3, 3, 3),
        new StackOfBoxes.Box(2, 2, 2),
        new StackOfBoxes.Box(4, 2, 2)
    ));
    assertThat(StackOfBoxes.maxHeight(boxes)).isEqualTo(6);
  }

  @Test
  void maxHeightWithOneVeryHigh() {
    List<StackOfBoxes.Box> boxes = new ArrayList<>(Arrays.asList(
        new StackOfBoxes.Box(2, 2, 2),
        new StackOfBoxes.Box(3, 3, 3),
        new StackOfBoxes.Box(2, 2, 2),
        new StackOfBoxes.Box(100, 1, 1)
    ));
    assertThat(StackOfBoxes.maxHeight(boxes)).isEqualTo(100);
  }
}