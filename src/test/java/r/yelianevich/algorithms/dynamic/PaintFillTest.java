package r.yelianevich.algorithms.dynamic;

import org.junit.jupiter.api.Test;
import r.yelianevich.algorithms.dynamic.PaintFill.Point;

import static org.assertj.core.api.Assertions.assertThat;

class PaintFillTest {

  @Test
  void paintFill3x3() {
    PaintFill paint = new PaintFill(new int[][]{
        {1, 1, 1},
        {1, 0, 1},
        {0, 0, 1}
    });
    paint.fill(Point.of(1, 1), 2);
    assertThat(paint.getScreen()).isEqualTo(new int[][]{
        {1, 1, 1},
        {1, 2, 1},
        {2, 2, 1}
    });
  }

  @Test
  void paintFill4x4() {
    PaintFill paint = new PaintFill(new int[][]{
        {1, 1, 1, 0},
        {1, 0, 0, 0},
        {0, 0, 1, 0},
        {0, 0, 1, 0}
    });
    paint.fill(Point.of(1, 1), 2);
    assertThat(paint.getScreen()).isEqualTo(new int[][]{
        {1, 1, 1, 2},
        {1, 2, 2, 2},
        {2, 2, 1, 2},
        {2, 2, 1, 2}
    });
  }

  @Test
  void paintFillAll4x4() {
    PaintFill paint = new PaintFill(new int[][]{
        {0, 0, 0, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0}
    });
    paint.fill(Point.of(1, 1), 2);
    assertThat(paint.getScreen()).isEqualTo(new int[][]{
        {2, 2, 2, 2},
        {2, 2, 2, 2},
        {2, 2, 2, 2},
        {2, 2, 2, 2}
    });
  }
}