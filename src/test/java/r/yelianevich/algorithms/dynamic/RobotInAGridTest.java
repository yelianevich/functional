package r.yelianevich.algorithms.dynamic;

import org.junit.jupiter.api.Test;
import r.yelianevich.algorithms.dynamic.RobotInAGrid.Point;

import static org.assertj.core.api.Assertions.assertThat;

class RobotInAGridTest {

  @Test
  void findThePath() {
    boolean[][] maze = new boolean[][]{
        {true, true, true, true, false},
        {true, false, true, false, true},
        {true, false, true, false, true},
        {false, false, true, true, true}
    };
    assertThat(RobotInAGrid.findThePath(maze)).containsExactly(
        Point.of(0, 0),
        Point.of(0, 1),
        Point.of(0, 2),
        Point.of(1, 2),
        Point.of(2, 2),
        Point.of(3, 2),
        Point.of(3, 3),
        Point.of(3, 4)
    );
  }
}