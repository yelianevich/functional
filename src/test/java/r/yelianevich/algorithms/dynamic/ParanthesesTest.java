package r.yelianevich.algorithms.dynamic;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class ParanthesesTest {

  @Test
  void validCombinations() {
    assertThat(Parantheses.validCombinations(0)).isEmpty();
    assertThat(Parantheses.validCombinations(1)).containsOnly("()");
    assertThat(Parantheses.validCombinations(3)).containsOnly("((()))", "(()())", "(())()", "()(())", "()()()");
  }
}