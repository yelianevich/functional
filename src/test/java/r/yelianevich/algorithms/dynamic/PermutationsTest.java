package r.yelianevich.algorithms.dynamic;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class PermutationsTest {

  @Test
  void permutationsWithoutDups() {
    assertThat(Permutations.genPermutations("")).containsOnly("");
    assertThat(Permutations.genPermutations("a")).containsOnly("a");
    assertThat(Permutations.genPermutations("ab")).containsOnly("ab", "ba");
    assertThat(Permutations.genPermutations("abc")).containsOnly("abc", "acb", "bac", "bca", "cab", "cba");

    // demonstrates that this algorithm doesn't work for strings with duplicated characters 
    assertThat(Permutations.genPermutations("aa")).containsOnly("aa", "aa");
  }

  @Test
  void permutationsWithDups() {
    assertThat(Permutations.genDupPermutations("")).containsOnly("");
    assertThat(Permutations.genDupPermutations("a")).containsOnly("a");
    assertThat(Permutations.genDupPermutations("ab")).containsOnly("ab", "ba");
    assertThat(Permutations.genDupPermutations("abc")).containsOnly("abc", "acb", "bac", "bca", "cab", "cba");

    assertThat(Permutations.genDupPermutations("aa")).containsOnly("aa");
    assertThat(Permutations.genDupPermutations("aaaaaaaaaaaa")).containsOnly("aaaaaaaaaaaa");
  }
}
