package r.yelianevich.algorithms.dynamic;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class RodCutTest {

  // rod length to price                 0  1  2  3  4  5   6   7   8   9   10
  private final int[] prices = new int[]{0, 1, 5, 8, 9, 10, 17, 17, 20, 24, 30};

  @Test
  void maxRevenueBruteForce() {
    RodCut cutter = new RodCut(prices);
    assertThat(cutter.maxRevenue(4)).isEqualTo(10);
  }

  @Test
  void maxRevenueTopDown() {
    RodCut cutter = new RodCut(prices);
    assertThat(cutter.topDownMaxRevenue(4)).isEqualTo(10);
  }

  @Test
  void maxRevenueBottomUp() {
    RodCut cutter = new RodCut(prices);
    assertThat(cutter.bottomUpMaxRevenue(4)).isEqualTo(10);
  }

  @Test
  void maxRevenueBottomUpWithSolution() {
    RodCut cutter = new RodCut(prices);
    RodCut.OptimalCut optimalCut = cutter.exetendedBottomUpMaxRevenue(4);
    assertThat(optimalCut.revenue).isEqualTo(10);
    assertThat(optimalCut.pieces).containsExactly(2, 2);

    optimalCut = cutter.exetendedBottomUpMaxRevenue(10);
    assertThat(optimalCut.revenue).isEqualTo(30);
    assertThat(optimalCut.pieces).containsExactly(10);

    optimalCut = cutter.exetendedBottomUpMaxRevenue(7);
    assertThat(optimalCut.revenue).isEqualTo(18);
    assertThat(optimalCut.pieces).containsExactly(1, 6);
  }
}