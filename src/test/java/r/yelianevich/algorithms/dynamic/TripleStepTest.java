package r.yelianevich.algorithms.dynamic;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class TripleStepTest {

  @Test
  void run() {
    assertThat(TripleStep.countWays(3)).isEqualTo(4);
    assertThat(TripleStep.countWays(4)).isEqualTo(7);
    assertThat(TripleStep.countWays(5)).isEqualTo(13);
    assertThat(TripleStep.countWays(6)).isEqualTo(24);
  }
}