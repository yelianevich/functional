package r.yelianevich.algorithms.dynamic;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


class FibonacciTest {

  @Test
  void recursive() {
    // takes around 3s
    assertThat(Fibonacci.recursive(0)).isEqualTo(0);
    assertThat(Fibonacci.recursive(1)).isEqualTo(1);
    assertThat(Fibonacci.recursive(2)).isEqualTo(1);
    assertThat(Fibonacci.recursive(3)).isEqualTo(2);
    assertThat(Fibonacci.recursive(4)).isEqualTo(3);
    assertThat(Fibonacci.recursive(5)).isEqualTo(5);
    assertThat(Fibonacci.recursive(6)).isEqualTo(8);
    assertThat(Fibonacci.recursive(7)).isEqualTo(13);
    assertThat(Fibonacci.recursive(44)).isEqualTo(701408733);
    //assertThat(Fibonacci.recursive(45)).isEqualTo(1134903170);
    //assertThat(Fibonacci.recursive(46)).isEqualTo(1836311903);
  }

  @Test
  void topDown() {
    // takes around several ms
    assertThat(Fibonacci.topDown(0)).isEqualTo(0);
    assertThat(Fibonacci.topDown(1)).isEqualTo(1);
    assertThat(Fibonacci.topDown(2)).isEqualTo(1);
    assertThat(Fibonacci.topDown(3)).isEqualTo(2);
    assertThat(Fibonacci.topDown(4)).isEqualTo(3);
    assertThat(Fibonacci.topDown(5)).isEqualTo(5);
    assertThat(Fibonacci.topDown(6)).isEqualTo(8);
    assertThat(Fibonacci.topDown(7)).isEqualTo(13);
    assertThat(Fibonacci.topDown(44)).isEqualTo(701408733);
    assertThat(Fibonacci.topDown(45)).isEqualTo(1134903170);
    assertThat(Fibonacci.topDown(46)).isEqualTo(1836311903);
  }

  @Test
  void bottomUp() {
    // takes around several ms
    assertThat(Fibonacci.bottomUp(0)).isEqualTo(0);
    assertThat(Fibonacci.bottomUp(1)).isEqualTo(1);
    assertThat(Fibonacci.bottomUp(2)).isEqualTo(1);
    assertThat(Fibonacci.bottomUp(3)).isEqualTo(2);
    assertThat(Fibonacci.bottomUp(4)).isEqualTo(3);
    assertThat(Fibonacci.bottomUp(5)).isEqualTo(5);
    assertThat(Fibonacci.bottomUp(6)).isEqualTo(8);
    assertThat(Fibonacci.bottomUp(7)).isEqualTo(13);
    assertThat(Fibonacci.bottomUp(44)).isEqualTo(701408733);
    assertThat(Fibonacci.bottomUp(45)).isEqualTo(1134903170);
    assertThat(Fibonacci.bottomUp(46)).isEqualTo(1836311903);
  }

  @Test
  void loop() {
    // takes around several ms
    assertThat(Fibonacci.loop(0)).isEqualTo(0);
    assertThat(Fibonacci.loop(1)).isEqualTo(1);
    assertThat(Fibonacci.loop(2)).isEqualTo(1);
    assertThat(Fibonacci.loop(3)).isEqualTo(2);
    assertThat(Fibonacci.loop(4)).isEqualTo(3);
    assertThat(Fibonacci.loop(5)).isEqualTo(5);
    assertThat(Fibonacci.loop(6)).isEqualTo(8);
    assertThat(Fibonacci.loop(7)).isEqualTo(13);
    assertThat(Fibonacci.loop(44)).isEqualTo(701408733);
    assertThat(Fibonacci.loop(45)).isEqualTo(1134903170);
    assertThat(Fibonacci.loop(46)).isEqualTo(1836311903);
  }

  @Test
  void matrix() {
    // takes around several ms
    assertThat(Fibonacci.matrix(0)).isEqualTo(0);
    assertThat(Fibonacci.matrix(1)).isEqualTo(1);
    assertThat(Fibonacci.matrix(2)).isEqualTo(1);
    assertThat(Fibonacci.matrix(3)).isEqualTo(2);
    assertThat(Fibonacci.matrix(4)).isEqualTo(3);
    assertThat(Fibonacci.matrix(5)).isEqualTo(5);
    assertThat(Fibonacci.matrix(6)).isEqualTo(8);
    assertThat(Fibonacci.matrix(7)).isEqualTo(13);
    assertThat(Fibonacci.matrix(8)).isEqualTo(21);
    assertThat(Fibonacci.matrix(44)).isEqualTo(701408733);
    assertThat(Fibonacci.matrix(45)).isEqualTo(1134903170);
    assertThat(Fibonacci.matrix(46)).isEqualTo(1836311903);
  }
}