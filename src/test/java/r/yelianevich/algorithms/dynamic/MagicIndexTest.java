package r.yelianevich.algorithms.dynamic;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class MagicIndexTest {

  @Test
  void findMagicIndex() {
    //          0  1  2  3  4  5
    int[] a = {-1, 0, 1, 2, 4, 10};
    assertThat(MagicIndex.findMagicIndex(a)).isEqualTo(4);
  }

  @Test
  void findMagicIndexWithDups() {
    //          0  1  2  3  4  5
    int[] a = {-1, 0, 1, 2, 4, 10};
    assertThat(MagicIndex.findMagicIndexWithDups(a)).isEqualTo(4);

    //              0   1  2  3  4  5  6  7  8  9   10
    a = new int[]{-10, -5, 2, 2, 2, 3, 4, 7, 9, 12, 13};
    assertThat(MagicIndex.findMagicIndexWithDups(a)).isEqualTo(2);

    //              0   1  2  3  4  5  6  7  8  9   10
    a = new int[]{-10, -5, 1, 2, 2, 3, 4, 7, 9, 12, 13};
    assertThat(MagicIndex.findMagicIndexWithDups(a)).isEqualTo(7);
  }
}