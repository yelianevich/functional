package r.yelianevich.algorithms.dynamic;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class CoinsTest {

  @Test
  void change() {
    assertThat(Coins.change(1)).isEqualTo(1);
    assertThat(Coins.change(2)).isEqualTo(1);
    assertThat(Coins.change(4)).isEqualTo(1);
    assertThat(Coins.change(5)).isEqualTo(2);
    assertThat(Coins.change(10)).isEqualTo(4);
    assertThat(Coins.change(11)).isEqualTo(4);
    assertThat(Coins.change(15)).isEqualTo(6);
    assertThat(Coins.change(100)).isEqualTo(242);
    assertThat(Coins.countChange(100)).isEqualTo(242);
  }
}