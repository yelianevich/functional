package r.yelianevich.algorithms.dynamic;

import org.junit.jupiter.api.Test;

import java.util.Deque;

import static org.assertj.core.api.Assertions.assertThat;

class HanoiTowersTest {

  @Test
  void solve3() {
    HanoiTowers towers = new HanoiTowers(3);
    Deque<Integer> solution = towers.solve();
    assertThat(solution).containsOnly(1, 2, 3);
  }

  @Test
  void solve4() {
    HanoiTowers towers = new HanoiTowers(4);
    Deque<Integer> solution = towers.solve();
    assertThat(solution).containsExactly(1, 2, 3, 4);
  }

  @Test
  void solve10() {
    HanoiTowers towers = new HanoiTowers(10);
    Deque<Integer> solution = towers.solve();
    assertThat(solution).containsExactly(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
  }

  @Test
  void solve21() {
    HanoiTowers towers = new HanoiTowers(21);
    Deque<Integer> solution = towers.solve();
    assertThat(solution).containsExactly(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21);
  }
}