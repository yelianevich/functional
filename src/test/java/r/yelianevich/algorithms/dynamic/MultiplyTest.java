package r.yelianevich.algorithms.dynamic;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class MultiplyTest {

  @Test
  void mul() {
    assertThat(Multiply.mul(9, 8)).isEqualTo(72);
    assertThat(Multiply.mul(123456, 123)).isEqualTo(15185088);

    assertThat(Multiply.minProduct(9, 8)).isEqualTo(72);
    assertThat(Multiply.minProduct(123456, 123)).isEqualTo(15185088);

    assertThat(Multiply.minProductWithMemo(9, 8)).isEqualTo(72);
    assertThat(Multiply.minProductWithMemo(123456, 123)).isEqualTo(15185088);
  }
}