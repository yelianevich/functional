package r.yelianevich.algorithms.dynamic;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

class PowersetTest {

  @Test
  void powerset() {
    Set<Integer> set = new HashSet<>(Arrays.asList(1, 2, 3));
    assertThat(Powerset.of(set)).containsOnly(
        Collections.emptySet(),
        new HashSet<>(Arrays.asList(1)),
        new HashSet<>(Arrays.asList(2)),
        new HashSet<>(Arrays.asList(3)),
        new HashSet<>(Arrays.asList(1, 2)),
        new HashSet<>(Arrays.asList(1, 3)),
        new HashSet<>(Arrays.asList(2, 3)),
        new HashSet<>(Arrays.asList(1, 2, 3))
    );

    Set<Character> xyz = new HashSet<>(Arrays.asList('x', 'y', 'z'));
    assertThat(Powerset.of(xyz)).containsOnly(
        Collections.emptySet(),
        new HashSet<>(Arrays.asList('x')),
        new HashSet<>(Arrays.asList('y')),
        new HashSet<>(Arrays.asList('z')),
        new HashSet<>(Arrays.asList('x', 'y')),
        new HashSet<>(Arrays.asList('x', 'z')),
        new HashSet<>(Arrays.asList('y', 'z')),
        new HashSet<>(Arrays.asList('x', 'y', 'z'))
    );
  }

  @Test
  void powersetCombinatorial() {
    Set<Integer> set = new HashSet<>(Arrays.asList(1, 2, 3));
    assertThat(Powerset.combinatorialOf(set)).containsOnly(
        Collections.emptySet(),
        new HashSet<>(Arrays.asList(1)),
        new HashSet<>(Arrays.asList(2)),
        new HashSet<>(Arrays.asList(3)),
        new HashSet<>(Arrays.asList(1, 2)),
        new HashSet<>(Arrays.asList(1, 3)),
        new HashSet<>(Arrays.asList(2, 3)),
        new HashSet<>(Arrays.asList(1, 2, 3))
    );

    Set<Character> xyz = new HashSet<>(Arrays.asList('x', 'y', 'z'));
    assertThat(Powerset.combinatorialOf(xyz)).containsOnly(
        Collections.emptySet(),
        new HashSet<>(Arrays.asList('x')),
        new HashSet<>(Arrays.asList('y')),
        new HashSet<>(Arrays.asList('z')),
        new HashSet<>(Arrays.asList('x', 'y')),
        new HashSet<>(Arrays.asList('x', 'z')),
        new HashSet<>(Arrays.asList('y', 'z')),
        new HashSet<>(Arrays.asList('x', 'y', 'z'))
    );
  }
}