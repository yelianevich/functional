package r.yelianevich.algorithms.sort.noncomparison;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.DoubleStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.withPrecision;


class BucketTest {

  @Test
  void sort() {
    double[] arr = {.79, .13, .16, .64, .39, .20, .89, .99, .53, .71, .42};
    Bucket.sort(arr);
    assertThat(arr)
        .containsExactly(.13, .16, .20, .39, .42, .53, .64, .71, .79, .89, .99);
  }

  @Test
  void sortOptimized() {
    double[] arr = {.79, .13, .16, .64, .39, .20, .89, .99, .53, .71, .42};
    Bucket.sortOptimized(arr);
    assertThat(arr)
        .containsExactly(.13, .16, .20, .39, .42, .53, .64, .71, .79, .89, .99);
  }

  @Test
  void randomizedSort() {
    Random rand = new Random();
    double[] arr = DoubleStream.generate(rand::nextDouble).limit(300).toArray();
    double[] arrClone = arr.clone();
    Bucket.sort(arr);
    Arrays.sort(arrClone);
    assertThat(arr)
        .containsExactly(arrClone, withPrecision(.05));
  }

  @Test
  void randomizedSortOptimized() {
    Random rand = new Random();
    double[] arr = DoubleStream.generate(rand::nextDouble).limit(300).toArray();
    double[] arrClone = arr.clone();
    Bucket.sortOptimized(arr);
    Arrays.sort(arrClone);
    assertThat(arr)
        .containsExactly(arrClone, withPrecision(.05));
  }
}
