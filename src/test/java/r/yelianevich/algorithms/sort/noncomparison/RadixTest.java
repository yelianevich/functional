package r.yelianevich.algorithms.sort.noncomparison;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


class RadixTest {

  @Test
  void sort() {
    int[] arr = {600, 0, 200, 0, 100, 300, 400, 600, 100, 300, 200};
    assertThat(Radix.sort(arr))
        .containsExactly(0, 0, 100, 100, 200, 200, 300, 300, 400, 600, 600);
  }

}