package r.yelianevich.algorithms.sort.noncomparison;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


class CountingTest {

  @Test
  void sortPositive() {
    int[] arr = {6, 0, 2, 0, 1, 3, 4, 6, 1, 3, 2};
    assertThat(Counting.sortClassic(arr, 6))
        .containsExactly(0, 0, 1, 1, 2, 2, 3, 3, 4, 6, 6);
    assertThat(Counting.sort(arr))
        .containsExactly(0, 0, 1, 1, 2, 2, 3, 3, 4, 6, 6);

    Counting.sortInPlace(arr);
    assertThat(arr).containsExactly(0, 0, 1, 1, 2, 2, 3, 3, 4, 6, 6);
  }

  @Test
  void sortSingleElement() {
    int[] arr = {0};
    assertThat(Counting.sortClassic(arr, 0)).containsExactly(0);
    assertThat(Counting.sort(arr)).containsExactly(0);

    Counting.sortInPlace(arr);
    assertThat(arr).containsExactly(0);
  }

  @Test
  void sortEmpty() {
    int[] arrEmpty = {};
    assertThat(Counting.sortClassic(arrEmpty, 9)).isEmpty();
    assertThat(Counting.sort(arrEmpty)).isEmpty();

    Counting.sortInPlace(arrEmpty);
    assertThat(arrEmpty).isEmpty();
  }

  @Test
  void sortNegative() {
    int[] arr = {6, 0, 2, 0, -1, 3, 4, 6, -1, 3, 2};
    assertThat(Counting.sort(arr))
        .containsExactly(-1, -1, 0, 0, 2, 2, 3, 3, 4, 6, 6);

    Counting.sortInPlace(arr);
    assertThat(arr).containsExactly(-1, -1, 0, 0, 2, 2, 3, 3, 4, 6, 6);
  }
}