package r.yelianevich.algorithms.sort;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class InsertionTest {

  @Test
  void sort() {
    double[] arr = {6, 0, 2, 0, 1, 3, 4, 6, 1, 3, 2};
    Insertion.sort(arr);
    assertThat(arr)
        .containsExactly(0, 0, 1, 1, 2, 2, 3, 3, 4, 6, 6);
  }
}
