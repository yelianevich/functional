package r.yelianevich.algorithms.sort;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


class QuickXTest {

  @Test
  void medianOfTreeSimple() {
    Integer[] a = {1, 5, 3};
    int mid = QuickX.medianOfTree(a, 0, 2);
    assertThat(a).containsExactly(3, 1, 5);
    assertThat(mid).isEqualTo(0);
  }

  @Test
  void medianOfTree() {
    Integer[] a = {10, 5, 3, 6, 2, 9, 10, 11};
    int mid = QuickX.medianOfTree(a, 2, 6);
    assertThat(a).containsExactly(10, 5, 6, 3, 2, 9, 10, 11);
    assertThat(mid).isEqualTo(2);
  }
}