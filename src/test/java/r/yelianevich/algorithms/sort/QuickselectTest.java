package r.yelianevich.algorithms.sort;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class QuickselectTest {

  @Test
  void find() {
    int[] arr = {800, 700, 600, 500, 400, 300, 200, 100, 0};
    assertThat(Quickselect.find(arr, 4)).isEqualTo(400);
    assertThat(Quickselect.find(arr, 0)).isEqualTo(0);
    assertThat(Quickselect.find(arr, 8)).isEqualTo(800);

  }
}