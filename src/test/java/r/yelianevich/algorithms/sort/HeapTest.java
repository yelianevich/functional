package r.yelianevich.algorithms.sort;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class HeapTest {

    @Test
    void sort() {
        Integer[] arr = {7, 3, 2, 5, 6, 10, 9, 8, 1};
        Heap.sort(arr);
        assertThat(arr).isSorted();
    }
}