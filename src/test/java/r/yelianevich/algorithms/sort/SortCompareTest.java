package r.yelianevich.algorithms.sort;

import org.junit.jupiter.api.Test;

public class SortCompareTest {

  public static final int N = 10000;
  public static final int TIMES = 5;

  @Test
  void selection() {
    SortCompare.timeRandomInput("Selection", N, TIMES);
    SortCompare.timeRandomInput("Selection", 1, 1);
    SortCompare.timeRandomInput("Selection", 2, 1);
    SortCompare.timeRandomInput("Selection", 3, 1);
  }

  @Test
  void insertion() {
    SortCompare.timeRandomInput("Insertion", N, TIMES);
    SortCompare.timeRandomInput("Insertion", 1, 1);
    SortCompare.timeRandomInput("Insertion", 2, 1);
    SortCompare.timeRandomInput("Insertion", 3, 1);
  }

  @Test
  void shell() {
    SortCompare.timeRandomInput("Shell", N, TIMES);
    SortCompare.timeRandomInput("Shell", 1, 1);
    SortCompare.timeRandomInput("Shell", 2, 1);
    SortCompare.timeRandomInput("Shell", 3, 1);
  }

  @Test
  void merge() {
    SortCompare.timeRandomInput("Merge", N, TIMES);
    SortCompare.timeRandomInput("Merge", 1, 1);
    SortCompare.timeRandomInput("Merge", 2, 1);
    SortCompare.timeRandomInput("Merge", 3, 1);
  }

  @Test
  void mergeBu() {
    SortCompare.timeRandomInput("MergeBU", N, TIMES);
    SortCompare.timeRandomInput("MergeBU", 1, 1);
    SortCompare.timeRandomInput("MergeBU", 2, 1);
    SortCompare.timeRandomInput("MergeBU", 3, 1);
  }

  @Test
  void mergeX() {
    SortCompare.timeRandomInput("MergeX", N, TIMES);
    SortCompare.timeRandomInput("MergeX", 1, 1);
    SortCompare.timeRandomInput("MergeX", 2, 1);
    SortCompare.timeRandomInput("MergeX", 3, 1);
  }

  @Test
  void quick() {
    SortCompare.timeRandomInput("Quick", N, TIMES);
    SortCompare.timeRandomInput("Quick", 1, 1);
    SortCompare.timeRandomInput("Quick", 2, 1);
    SortCompare.timeRandomInput("Quick", 3, 1);
  }

  @Test
  void quick3way() {
    SortCompare.timeRandomInput("Quick3way", N, TIMES);
    SortCompare.timeRandomInput("Quick3way", 1, 1);
    SortCompare.timeRandomInput("Quick3way", 2, 1);
    SortCompare.timeRandomInput("Quick3way", 3, 1);
  }

  @Test
  void quickFast3way() {
    SortCompare.timeRandomInput("QuickFast3way", N, TIMES);
    SortCompare.timeRandomInput("QuickFast3way", 1, 1);
    SortCompare.timeRandomInput("QuickFast3way", 2, 1);
    SortCompare.timeRandomInput("QuickFast3way", 3, 1);
  }

  @Test
  void quickX() {
    SortCompare.timeRandomInput("QuickX", N, TIMES);
    SortCompare.timeRandomInput("QuickX", 1, 1);
    SortCompare.timeRandomInput("QuickX", 2, 1);
    SortCompare.timeRandomInput("QuickX", 3, 1);
  }

  @Test
  void heap() {
    SortCompare.timeRandomInput("Heap", N, TIMES);
    SortCompare.timeRandomInput("Heap", 1, 1);
    SortCompare.timeRandomInput("Heap", 2, 1);
    SortCompare.timeRandomInput("Heap", 3, 1);
  }

}