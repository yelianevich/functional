package r.yelianevich.algorithms.strings;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class KmpTest {

    @Test
    void buildPartialMatchTable() {
        int[] t = Kmp.buildPartialMatchTable("ABCDABCA");
        assertThat(t).containsExactly(0, 0, 0, 0, 1, 2, 3, 1);
    }

    @Test
    void buildPartialMatchTableWithFallBack() {
        int[] t = Kmp.buildPartialMatchTable("aabaabaaa");
        assertThat(t)
                .as("Check computed pos for last a; should be 2, cause there is 'aa' prefix")
                .containsExactly(0, 1, 0, 1, 2, 3, 4, 5, 2);
    }

    @Test
    void shouldFindExisting() {
        int index = Kmp.indexOf("hello", "ll");
        assertThat(index).isEqualTo(2);
    }

    @Test
    void shouldFindExistingWithManySlides() {
        var pattern = "aaaaab";
        int[] table = Kmp.buildPartialMatchTable(pattern);
        assertThat(table).containsExactly(0, 1, 2, 3, 4, 0);
        int index = Kmp.indexOf("aaaaaaaab", pattern);
        assertThat(index).isEqualTo(3);
    }

    @Test
    void shouldFindExistingWithManySlides2() {
        String text = "abcxabcdabcdabcy";
        String pattern = "abcdabcy";
        int index = Kmp.indexOf(text, pattern);
        assertThat(index).isEqualTo(8);
    }
}