package r.yelianevich.algorithms;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class BinarySearchTest {

    @ParameterizedTest
    @MethodSource
    void rank(int[] nums, int value, int expectedRank, String description) {
        assertThat(BinarySearch.rank(nums, value)).as(description).isEqualTo(expectedRank);
    }

    private static Stream<Arguments> rank() {
        int[] oneToNineInts = {1, 2, 3, 4, 5, 7, 8, 9};
        return Stream.of(
                Arguments.of(oneToNineInts, 2, 1, ""),
                Arguments.of(oneToNineInts, 8, 6, ""),
                Arguments.of(oneToNineInts, 6, 5, "rank() for a non-existing value in the middle of the array"),
                Arguments.of(oneToNineInts, 10, 8, "rank() for a non-existing value that is greater than any other"),
                Arguments.of(oneToNineInts, -2, 0, "rank() for a non-existing value that is smaller than any other")
        );
    }

    @ParameterizedTest
    @MethodSource
    void indexOfAsc(int[] nums, int key, int expectedIndex) {
        assertThat(BinarySearch.indexOfAsc(nums, key)).isEqualTo(expectedIndex);
    }

    private static Stream<Arguments> indexOfAsc() {
        return Stream.of(
                Arguments.of(new int[]{1, 2, 3, 4, 5, 7, 8, 9}, 2, 1),
                Arguments.of(new int[]{1, 2, 3, 4, 5, 7, 8, 9}, 8, 6),
                Arguments.of(new int[]{1, 2, 3, 4, 5, 7, 8, 9}, 6, -6),
                Arguments.of(new int[]{1, 2, 3, 4, 5, 7, 8, 9}, 0, -1)
        );
    }

    @Test
    void insertionPointOfBinarySearch() {
        int[] a = {1, 2, 3, 4, 5, 7, 8, 9};
        int index = BinarySearch.indexOfAsc(a, 6);
        assertThat(index).isEqualTo(-6);
        assertThat(-index - 1)
                .as("Binary search allows to distinguish when element is not present and restore insertion point")
                .isEqualTo(BinarySearch.rank(a, 6));

        index = BinarySearch.indexOfAsc(a, -10);
        assertThat(index).isEqualTo(-1);
        assertThat(-index - 1)
                .as("Binary search allows to distinguish when element is not present and restore insertion point")
                .isEqualTo(BinarySearch.rank(a, -10));
    }

    @ParameterizedTest
    @MethodSource
    void indexOfDesc(int[] nums, int key, int expectedIndex) {
        assertThat(BinarySearch.indexOfDesc(nums, key)).isEqualTo(expectedIndex);

        assertThat(BinarySearch.indexOfDesc(new int[]{9, 8, 7, 5, 4, 3, 2, 1}, 2)).isEqualTo(6);
        assertThat(BinarySearch.indexOfDesc(new int[]{9, 8, 7, 5, 4, 3, 2, 1}, 8)).isEqualTo(1);
        assertThat(BinarySearch.indexOfDesc(new int[]{9, 8, 7, 5, 4, 3, 2, 1}, 6)).isEqualTo(-4);
        assertThat(BinarySearch.indexOfDesc(new int[]{9, 8, 7, 5, 4, 3, 2, 1}, 10)).isEqualTo(-1);
    }

    private static Stream<Arguments> indexOfDesc() {
        return Stream.of(
                Arguments.of(new int[]{9, 8, 7, 5, 4, 3, 2, 1}, 2, 6),
                Arguments.of(new int[]{9, 8, 7, 5, 4, 3, 2, 1}, 8, 1),
                Arguments.of(new int[]{9, 8, 7, 5, 4, 3, 2, 1}, 6, -4),
                Arguments.of(new int[]{9, 8, 7, 5, 4, 3, 2, 1}, 10, -1)
        );
    }

    @ParameterizedTest()
    @MethodSource
    void leftmostIndexOf(int[] nums, int key, int expectedIndex, String description) {
        assertThat(BinarySearch.leftmostIndexOf(nums, key)).as(description).isEqualTo(expectedIndex);
    }

    private static Stream<Arguments> leftmostIndexOf() {
        return Stream.of(
                Arguments.of(new int[]{1, 3, 3, 3, 3, 7, 8, 9}, 3, 1, ""),
                Arguments.of(new int[]{1, 2, 3, 4, 5, 7, 8, 9}, 8, 6, ""),
                Arguments.of(new int[]{1, 2, 3, 4, 5, 7, 8, 9}, 6, 5, ""),
                Arguments.of(new int[]{1, 2, 3, 4, 5, 7, 8, 9}, -10, 0, ""),
                Arguments.of(new int[]{1, 2, 3, 4, 5, 7, 8, 9}, 10, 8, "leftmost stays within [0; length]")
        );
    }

    @ParameterizedTest
    @MethodSource
    void rightmostIndexOf(int[] nums, int key, int expectedIndex, String description) {
        assertThat(BinarySearch.rightmostIndexOf(nums, key))
                .as(description)
                .isEqualTo(expectedIndex);
    }

    private static Stream<Arguments> rightmostIndexOf() {
        return Stream.of(
                Arguments.of(new int[]{1, 3, 3, 3, 3, 7, 8, 9}, 3, 4, ""),
                Arguments.of(new int[]{1, 2, 3, 4, 5, 7, 8, 9}, 8, 6, ""),
                Arguments.of(new int[]{1, 2, 3, 4, 5, 7, 8, 9}, 6, 4, ""),
                Arguments.of(new int[]{1, 2, 3, 4, 5, 7, 8, 9}, -10, -1, ""),
                Arguments.of(new int[]{1, 2, 3, 4, 5, 7, 8, 9}, 10, 7, "leftmost stays within [0; length]")
        );
    }
}
