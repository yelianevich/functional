package r.yelianevich.algorithms;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


class ApplicationsTest {

  @Test
  void distinctCountTest() {
    int distinct = Applications.distinctCount(new String[]{"a", "ab", "abc", "a", "b", "ab", "abc"});
    assertThat(distinct).isEqualTo(4);
  }

  @Test
  void mostFrequentTest() {
    String mostFreq = Applications.mostFreqItem(new String[]{"a", "ab", "abc", "a", "b", "ab", "abc", "a", "a"});
    assertThat(mostFreq).isEqualTo("a");
    mostFreq = Applications.mostFreqItem(new String[]{"a"});
    assertThat(mostFreq).isEqualTo("a");
    mostFreq = Applications.mostFreqItem(new String[]{"a", "a"});
    assertThat(mostFreq).isEqualTo("a");
  }

  @Test
  void selectTest() {
    Integer[] a = {6, 3, 2, 1, 7, 4, 8, 5, 11, 10, 9};
    int medianK = a.length / 2;
    Integer median = Applications.select(a, medianK);
    assertThat(median).isEqualTo(6);
  }

  @Test
  void dedupTest() {
    String[] deduped = Applications.dedup(new String[]{"c", "a", "c", "b", "b", "a", "d", "x", "a"});
    assertThat(deduped).containsExactly("a", "b", "c", "d", "x", null, null, null, null);

    deduped = Applications.dedup(new String[]{"c", "a", "x"});
    assertThat(deduped).containsExactly("a", "c", "x");
  }

}