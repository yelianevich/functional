import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class RandomizedQueueTest {

  @Test
  void scenario() {
    RandomizedQueue<Integer> rq = new RandomizedQueue<>();
    assertThat(rq.isEmpty()).isTrue();
    assertThat(rq.size()).isZero();
    rq.enqueue(1);
    rq.enqueue(2);
    rq.enqueue(3);
    rq.enqueue(4);
    rq.enqueue(5);
    rq.enqueue(6);
    assertThat(rq.sample()).isBetween(1, 6);
    assertThat(rq.sample()).isBetween(1, 6);
    assertThat(rq.sample()).isBetween(1, 6);
    assertThat(rq).containsOnly(1, 2, 3, 4, 5, 6);
    assertThat(rq.size()).isEqualTo(6);
    assertThat(rq.dequeue()).isBetween(1, 6);
    assertThat(rq.dequeue()).isBetween(1, 6);
    assertThat(rq.dequeue()).isBetween(1, 6);
    assertThat(rq.size()).isEqualTo(3);
    assertThat(rq.dequeue()).isBetween(1, 6);
    assertThat(rq.dequeue()).isBetween(1, 6);
    assertThat(rq.dequeue()).isBetween(1, 6);
    assertThat(rq.size()).isZero();
    assertThat(rq).isEmpty();
    rq.enqueue(1);
    rq.enqueue(2);
    rq.enqueue(3);
    rq.enqueue(4);
    rq.enqueue(5);
    rq.enqueue(6);
    assertThat(rq).containsOnly(1, 2, 3, 4, 5, 6);
  }

}