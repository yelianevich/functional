import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class BoardTest {

  @Test
  void manhattan() {
    Board b = new Board(new int[][]{{8, 1, 3}, {4, 0, 2}, {7, 6, 5}});
    assertThat(b.manhattan()).isEqualTo(10);
  }

  @Test
  void manhattan2() {
    Board b = new Board(new int[][]{{5, 1, 8}, {2, 7, 3}, {4, 0, 6}});
    assertThat(b.manhattan()).isEqualTo(13);
  }

  @Test
  void manhattan3() {
    Board b = new Board(new int[][]{{5, 8, 7}, {1, 4, 6}, {3, 0, 2}});
    assertThat(b.manhattan()).isEqualTo(17);
  }

  @Test
  void hamming() {
    Board b = new Board(new int[][]{{8, 1, 3}, {4, 0, 2}, {7, 6, 5}});
    assertThat(b.hamming()).isEqualTo(5);
  }

  @Test
  void dimension() {
    Board b = new Board(new int[][]{{8, 1, 3}, {4, 0, 2}, {7, 6, 5}});
    assertThat(b.dimension()).isEqualTo(3);
  }

  @Test
  void isGoal() {
    Board b = new Board(new int[][]{{8, 1, 3}, {4, 0, 2}, {7, 6, 5}});
    assertThat(b.isGoal()).isFalse();
    b = new Board(new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 0}});
    assertThat(b.isGoal()).isTrue();
  }

  @Test
  void equals() {
    Board b1 = new Board(new int[][]{{8, 1, 3}, {4, 0, 2}, {7, 6, 5}});
    Board b1Twin = new Board(new int[][]{{8, 1, 3}, {4, 0, 2}, {7, 6, 5}});
    assertThat(b1.equals(b1Twin)).isTrue();
    Board another = new Board(new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 0}});
    assertThat(b1.equals(another)).isFalse();
  }

  @Test
  void neighbors4() {
    Board b1 = new Board(new int[][]{{8, 1, 3}, {4, 0, 2}, {7, 6, 5}});
    Iterable<Board> neighbors = b1.neighbors();
    assertThat(neighbors).containsOnlyOnce(
        new Board(new int[][]{{8, 0, 3}, {4, 1, 2}, {7, 6, 5}}),
        new Board(new int[][]{{8, 1, 3}, {0, 4, 2}, {7, 6, 5}}),
        new Board(new int[][]{{8, 1, 3}, {4, 2, 0}, {7, 6, 5}}),
        new Board(new int[][]{{8, 1, 3}, {4, 6, 2}, {7, 0, 5}})
    );
  }

  @Test
  void neighbors2() {
    Board b1 = new Board(new int[][]{{0, 8, 3}, {4, 1, 2}, {7, 6, 5}});
    Iterable<Board> ns = b1.neighbors();
    assertThat(ns).containsOnlyOnce(
        new Board(new int[][]{{8, 0, 3}, {4, 1, 2}, {7, 6, 5}}),
        new Board(new int[][]{{4, 8, 3}, {0, 1, 2}, {7, 6, 5}})
    );
  }

  @Test
  void twin() {
    Board b1 = new Board(new int[][]{{8, 1, 3}, {4, 0, 2}, {7, 6, 5}});
    assertThat(b1.twin()).isNotEqualTo(b1);
  }
}


