/* TOP 20% of students by GPA. Rank is used intentionally to get "extra" students if GPA is the same */
SELECT * FROM
  (
    SELECT
      student_id,
      gpa,
      RANK() OVER(ORDER BY gpa DESC) as gpa_rank
    FROM
      (
        SELECT student_id, avg(grade) as gpa
        FROM grade 
        GROUP BY student_id
      ) student_gpa
  ) student_ranked
WHERE gpa_rank <= (SELECT count(*) * 0.2 FROM student)
