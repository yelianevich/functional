CREATE TABLE student
(
    student_id INT NOT NULL,
    first_name VARCHAR(128),
    last_name  VARCHAR(128),
    PRIMARY KEY (student_id)
);

INSERT INTO student
VALUES (1, 'John1', 'Smith1'),
       (2, 'John2', 'Smith2'),
       (3, 'John3', 'Smith3'),
       (4, 'John4', 'Smith4'),
       (5, 'John5', 'Smith5'),
       (6, 'John6', 'Smith6'),
       (7, 'John7', 'Smith7'),
       (8, 'John8', 'Smith8'),
       (9, 'John9', 'Smith9'),
       (10, 'John10', 'Smith10');

CREATE TABLE subject
(
    subject_id    INT NOT NULL,
    subject_title VARCHAR(128),
    PRIMARY KEY (subject_id)
);
INSERT INTO subject
VALUES (1, 'Math'),
       (2, 'Lit'),
       (3, 'Prog');

CREATE TABLE grade
(
    student_id INT NOT NULL,
    subject_id INT NOT NULL,
    grade      INT,
    FOREIGN KEY (student_id) REFERENCES student (student_id),
    FOREIGN KEY (subject_id) REFERENCES subject (subject_id)
);
INSERT INTO grade
VALUES (1, 1, 70),
       (1, 2, 90),
       (1, 3, 50),
       (2, 1, 60),
       (2, 2, 80),
       (2, 3, 40),
       (3, 1, 60),
       (3, 2, 80),
       (3, 3, 40),
       (4, 1, 40),
       (4, 2, 60),
       (4, 3, 20),
       (5, 1, 40),
       (5, 2, 50),
       (5, 3, 20);