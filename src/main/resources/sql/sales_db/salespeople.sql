USE db;

/* a. The names of all salespeople that have an order with Samsonic.  */

/* Using EXISTS subquery */
SELECT salesperson.name
FROM salesperson
WHERE EXISTS(
              SELECT 1
              FROM orders
              WHERE orders.cust_id = (SELECT id FROM customer WHERE name = 'Samsonic')
                AND salesperson.id = orders.salesperson_id
          );


/* b. The names of all salespeople that do not have any order with Samsonic. */
SELECT salesperson.name
FROM salesperson
WHERE NOT EXISTS(
        SELECT 1
        FROM orders
        WHERE orders.cust_id = (SELECT id FROM customer WHERE name = 'Samsonic')
          AND salesperson.id = orders.salesperson_id
    );


/* c. The names of salespeople that have 2 or more orders. */
SELECT salesperson.name
FROM (
         SELECT salesperson_id, count(*) as orders_count
         FROM orders
         WHERE cust_id = (SELECT id FROM customer WHERE name = 'Samsonic')
         GROUP BY salesperson_id
         HAVING count(*) >= 2
     ) performers
         JOIN salesperson
              ON salesperson.id = performers.salesperson_id;

/* d. The names and ages of all salespersons must having a salary of 100,000 or greater. */
/* ??? */

/* e. What sales people have sold more than 1400 total units? */
SELECT salesperson.name
FROM (
         SELECT salesperson_id, sum(amount) as total_units
         FROM orders
         GROUP BY salesperson_id
         HAVING sum(amount) > 1400
     ) performers
         JOIN salesperson
              ON performers.salesperson_id = salesperson.id;


/* f. When was the earliest and latest order made to Samony? */
SELECT min(orders.order_date) as earliest,
       max(orders.order_date) as latest
FROM orders
WHERE orders.cust_id = (SELECT id FROM customer WHERE name = 'Samony');
