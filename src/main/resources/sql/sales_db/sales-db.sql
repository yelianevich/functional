CREATE TABLE IF NOT EXISTS salesperson
(
    id     int,
    name   varchar(128),
    age    int,
    salary int
);
INSERT INTO salesperson
VALUES (1, 'Abe', 61, 140000),
       (2, 'Bob', 34, 44000),
       (5, 'Chris', 34, 40000),
       (7, 'Dan', 41, 52000),
       (8, 'Ken', 57, 115000),
       (11, 'Joe', 38, 38000);

CREATE TABLE IF NOT EXISTS customer
(
    id            int,
    name          varchar(128),
    city          varchar(128),
    industry_type varchar(128)
);
INSERT INTO customer
VALUES (4, 'Samsonic', 'pleasant', 'J'),
       (2, 'Panasung', 'oaktown', 'J'),
       (7, 'Samsony', 'jackson', 'B'),
       (9, 'Orange', 'Jackson', 'B');

CREATE TABLE IF NOT EXISTS orders
(
    number         int,
    order_date     date,
    cust_id        int,
    salesperson_id int,
    amount         int
);
INSERT INTO orders
VALUES (10, '1996-08-02', 4, 2, 540),
       (11, '1997-08-02', 4, 2, 1540),
       (12, '1997-08-03', 4, 2, 1140),
       (20, '1999-01-30', 4, 8, 1800),
       (30, '1995-07-14', 9, 1, 460),
       (40, '1998-01-29', 7, 2, 2400),
       (50, '1998-02-03', 6, 7, 600),
       (60, '1998-03-02', 6, 7, 720),
       (70, '1998-05-06', 9, 7, 150);
