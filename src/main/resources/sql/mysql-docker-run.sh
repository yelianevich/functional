#!/bin/bash

docker run --name mysql-practice -p 3306:3306 \
-e MYSQL_ROOT_PASSWORD=pass \
-e MYSQL_DATABASE=db \
-e MYSQL_USER=user \
-e MYSQL_PASSWORD=pass \
-d mysql:8.0.19