import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Board {

  private final int[][] tiles;
  private final int len;

  // create a board from an n-by-n array of tiles,
  // where tiles[row][col] = tile at (row, col)
  public Board(int[][] tiles) {
    this.len = tiles.length;
    this.tiles = clone(tiles);
  }

  // string representation of this board
  public String toString() {
    StringBuilder s = new StringBuilder();
    s.append(len).append('\n');
    for (int i = 0; i < len; i++) {
      for (int j = 0; j < len; j++) {
        s.append(String.format("%2d ", tiles[i][j]));
      }
      s.append("\n");
    }
    return s.toString();
  }

  // board dimension n
  public int dimension() {
    return len;
  }

  // number of tiles out of place
  public int hamming() {
    int distance = 0;
    for (int i = 0; i < len; i++) {
      int[] row = tiles[i];
      int rowPos = i * len;
      for (int j = 0; j < len; j++) {
        int tile = row[j];
        if (tile != 0) {
          int expectedTile = rowPos + j + 1;
          if (tile != expectedTile) {
            ++distance;
          }
        }
      }
    }
    return distance;
  }

  // sum of Manhattan distances between tiles and goal
  public int manhattan() {
    int distance = 0;
    for (int i = 0; i < len; i++) {
      int[] row = tiles[i];
      int rowPos = i * len;
      for (int j = 0; j < len; j++) {
        int tile = row[j];
        if (tile != 0) {
          int expectedTile = rowPos + j + 1;
          if (tile != expectedTile) {
            int expectedI = (tile - 1) / len;
            int expectedJ = (tile - 1) % len;
            distance += Math.abs(i - expectedI) + Math.abs(j - expectedJ);
          }
        }
      }
    }
    return distance;
  }

  // is this board the goal board?
  public boolean isGoal() {
    return hamming() == 0;
  }

  // does this board equal y?
  public boolean equals(Object y) {
    if (y == null) return false;
    if (this == y) return true;
    if (!y.getClass().equals(this.getClass())) return false;
    Board that = (Board) y;
    return Arrays.deepEquals(tiles, that.tiles);
  }

  // all neighboring boards
  public Iterable<Board> neighbors() {
    return generateNeighbors(tiles);
  }

  private Iterable<Board> generateNeighbors(int[][] arr) {
    int[] ij = findBlank(arr);
    int i = ij[0];
    int j = ij[1];
    List<Board> neighborsList = new ArrayList<>();
    if (i != 0) {
      int[][] neighbor = clone(arr);
      swap(neighbor, i, j, i - 1, j);
      neighborsList.add(new Board(neighbor));
    }
    if (j != arr.length - 1) {
      int[][] neighbor = clone(arr);
      swap(neighbor, i, j, i, j + 1);
      neighborsList.add(new Board(neighbor));
    }
    if (i != arr.length - 1) {
      int[][] neighbor = clone(arr);
      swap(neighbor, i, j, i + 1, j);
      neighborsList.add(new Board(neighbor));
    }
    if (j != 0) {
      int[][] neighbor = clone(arr);
      swap(neighbor, i, j, i, j - 1);
      neighborsList.add(new Board(neighbor));
    }
    return neighborsList;
  }

  private int[] findBlank(int[][] arr) {
    for (int i = 0; i < len; ++i) {
      int[] row = arr[i];
      for (int j = 0; j < len; ++j) {
        if (row[j] == 0) {
          return new int[]{i, j};
        }
      }
    }
    throw new IllegalStateException("blank element is missing");
  }

  private void swap(int[][] xs, int i, int j, int x, int y) {
    int tmp = xs[i][j];
    xs[i][j] = xs[x][y];
    xs[x][y] = tmp;
  }

  // a board that is obtained by exchanging any pair of tiles
  public Board twin() {
    return new Board(generateTwin(tiles));
  }

  private int[][] generateTwin(int[][] xs) {
    int[][] twin = clone(xs);
    int x = -1, y = -1, a, b;
    for (int i = 0; i < len; ++i) {
      for (int j = 0; j < len; ++j) {
        if (twin[i][j] != 0 && x == -1) {
          x = i;
          y = j;
        } else if (twin[i][j] != 0) {
          a = i;
          b = j;
          swap(twin, x, y, a, b);
          return twin;
        }
      }
    }
    return twin;
  }

  private int[][] clone(int[][] arr) {
    int len = arr.length;
    int[][] clone = new int[len][];
    for (int i = 0; i < len; ++i) {
      clone[i] = Arrays.copyOf(arr[i], len);
    }
    return clone;
  }
}
