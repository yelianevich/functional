import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;
import edu.princeton.cs.algs4.Stopwatch;

public class PercolationStats {
  private static final double CONFIDENCE_95 = 1.96;

  private final double mean;
  private final double stddev;
  private final double confidenceLo;
  private final double confidenceHi;

  /**
   * Perform trials independent experiments on an n-by-n grid.
   */
  public PercolationStats(int n, int trials) {
    if (n <= 0 || trials <= 0) {
      throw new IllegalArgumentException("n and trials should be positive numbers");
    }

    double[] openFractions = new double[trials];

    for (int t = 0; t < trials; ++t) {
      Percolation percolation = new Percolation(n);
      while (!percolation.percolates()) {
        int row = StdRandom.uniform(1, n + 1);
        int col = StdRandom.uniform(1, n + 1);
        percolation.open(row, col);
      }

      openFractions[t] = ((double) percolation.numberOfOpenSites()) / (n * n);
    }

    this.mean = StdStats.mean(openFractions);
    this.stddev = StdStats.stddev(openFractions);
    this.confidenceLo = mean - (CONFIDENCE_95 * stddev) / Math.sqrt(trials);
    this.confidenceHi = mean + (CONFIDENCE_95 * stddev) / Math.sqrt(trials);
  }

  /**
   * Sample mean of percolation threshold.
   */
  public double mean() {
    return mean;
  }

  /**
   * Sample standard deviation of percolation threshold.
   */
  public double stddev() {
    return stddev;
  }

  /**
   * Low  endpoint of 95% confidence interval.
   */
  public double confidenceLo() {
    return confidenceLo;
  }

  /**
   * High endpoint of 95% confidence interval.
   */
  public double confidenceHi() {
    return confidenceHi;
  }

  /**
   * Test client (described below).
   */
  public static void main(String[] args) {
    Stopwatch stopwatch = new Stopwatch();
    int n = Integer.parseInt(args[0]);
    int t = Integer.parseInt(args[1]);
    PercolationStats stats = new PercolationStats(n, t);
    StdOut.printf("mean                    = %f%n", stats.mean());
    StdOut.printf("stddev                  = %f%n", stats.stddev());
    StdOut.printf("95%% confidence interval = [%f, %f]%n",
        stats.confidenceLo(), stats.confidenceHi());
    StdOut.println("Elapsed = " + stopwatch.elapsedTime());
  }

}
