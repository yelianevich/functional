import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Percolation {
  private static final byte BLOCKED = 0;
  private static final byte OPEN = 1;
  private static final byte BOTTOM = 2;
  private static final byte TOP = 3;
  private static final byte BOTH = 5;

  private final WeightedQuickUnionUF uf;
  private final byte[] state;
  private final int n;
  private int openCount = 0;
  private boolean isPercolate = false;

  /**
   * Create n-by-n grid, with all sites blocked
   */
  public Percolation(int n) {
    if (n <= 0) {
      throw new IllegalArgumentException("Cannot initialize percolation model with n=" + n);
    }
    this.n = n;
    uf = new WeightedQuickUnionUF(1 + n * n);

    // CONNECTED COMPONENT states are located at [1..n*n] by id, all BLOCKED
    state = new byte[1 + n * n];
  }

  /**
   * Open index (row, col) if it is not open already
   */
  public void open(int row, int col) {
    validate(row, col);

    int p = index(row, col);
    if (!isOpen(p)) {
      openCount += 1;

      // can use p, cause pId == p at this point
      if (row == n && row == 1) state[p] = BOTH;
      else if (row == 1) state[p] = TOP;
      else if (row == n) state[p] = BOTTOM;
      else state[p] = OPEN;
      byte pState = state[p];

      // after union ops p != pId
      byte up = union(p, row - 1, col);
      byte right = union(p, row, col + 1);
      byte down = union(p, row + 1, col);
      byte left = union(p, row, col - 1);

      // find created component id
      int pId = uf.find(p);

      // compute final state  of created component
      boolean both = pState == BOTH || up == BOTH || right == BOTH || down == BOTH || left == BOTH;
      boolean top = (!both) && (pState == TOP || up == TOP || right == TOP || down == TOP || left == TOP);
      boolean bottom = (!both) && (pState == BOTTOM || up == BOTTOM || right == BOTTOM || down == BOTTOM || left == BOTTOM);
      if (both || (top && bottom)) {
        state[pId] = BOTH;
        isPercolate = true;
      } else if (top) {
        state[pId] = TOP;
      } else if (bottom) {
        state[pId] = BOTTOM;
      } else {
        state[pId] = OPEN;
      }
    }
  }

  /**
   * Connects p to (row, col)
   *
   * @return return (row, col) previous state
   */
  private byte union(int p, int row, int col) {
    if (inRange(row, col)) {
      int q = index(row, col);
      int qId = uf.find(q);
      if (state[qId] != BLOCKED) {
        uf.union(p, q);
        return state[qId];
      }
    }
    return BLOCKED;
  }

  private int index(int row, int col) {
    return (row - 1) * n + col;
  }

  private void validate(int i, int j) {
    if (!inRange(i, j)) {
      throw new IllegalArgumentException(
          String.format("Invalid indices for percolation system [i=%s] [j=%s]", i, j));
    }
  }

  private boolean inRange(int i, int j) {
    return (i >= 1 && i <= n && j >= 1 && j <= n);
  }

  /**
   * Is index (row, col) open?
   */
  public boolean isOpen(int row, int col) {
    validate(row, col);
    return isOpen(index(row, col));
  }

  /**
   * Can check isOpen by index, cause during open it's
   * initially set to non-BLOCKED state
   */
  private boolean isOpen(int p) {
    return state[p] != BLOCKED;
  }

  /**
   * Is index (row, col) full?
   */
  public boolean isFull(int row, int col) {
    validate(row, col);
    int pId = uf.find(index(row, col));
    byte siteState = state[pId];
    return siteState == BOTH || siteState == TOP;
  }

  /**
   * Number of open sites
   */
  public int numberOfOpenSites() {
    return openCount;
  }

  /**
   * Does the system percolate?
   */
  public boolean percolates() {
    return isPercolate;
  }

}
