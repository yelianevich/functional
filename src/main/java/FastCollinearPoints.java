import java.util.ArrayList;
import java.util.Arrays;

public class FastCollinearPoints {

    private final LineSegment[] segments;

    // finds all line segments containing 4 points
    public FastCollinearPoints(Point[] pointsIn) {
        if (pointsIn == null) {
            throw new IllegalArgumentException("points are null");
        }
        Point[] points = pointsIn.clone();
        int len = points.length;
        for (Point point : points) {
            if (point == null) {
                throw new IllegalArgumentException("point is null");
            }
        }

        // the same point will be together after sort
        Arrays.sort(points);
        for (int i = 0; i < len - 1; i++) {
            if (points[i].compareTo(points[i + 1]) == 0) {
                throw new IllegalArgumentException("2 point are equal");
            }
        }

        ArrayList<LineSegment> segmentsTmp = new ArrayList<>();
        for (int i = 0; i < len && len >= 3; i++) {
            // iterate through all points, finding collinear points for each of them
            Point p = pointsIn[i];

            // make all collinear points be grouped together,
            // might be multiple groups if there are multiple line segments through the point
            Arrays.sort(points, 0, len, p.slopeOrder());

            int collinearStart = 1;
            int collinearEnd = 1;

            // 0 is p itself (negative infinite value)
            for (int j = 1; j < len - 1; j++) {
                boolean areCollinear = p.slopeTo(points[j]) == p.slopeTo(points[j + 1]);
                if (areCollinear) {
                    collinearEnd += 1;
                }

                boolean lastElementProcessed = len == j + 2;
                if (!areCollinear || lastElementProcessed) {
                    if (collinearEnd - collinearStart < 2) {
                        // less than 4 points collinear: no subsequent collinear point
                        collinearStart = j + 1;
                        collinearEnd = j + 1;
                    } else {
                        // got some collinear points
                        Arrays.sort(points, collinearStart, collinearEnd + 1);

                        if (p.compareTo(points[collinearStart]) < 0) {
                            // p is a min point on the collinear line - add it, otherwise it's a duplicate
                            segmentsTmp.add(new LineSegment(p, points[collinearEnd]));
                        }
                        collinearStart = j + 1;
                        collinearEnd = j + 1;
                    }
                }
            }
        }
        this.segments = segmentsTmp.toArray(new LineSegment[0]);
    }

    // the number of line segments
    public int numberOfSegments() {
        return segments.length;
    }

    // the line segments
    public LineSegment[] segments() {
        return segments.clone();
    }

}
