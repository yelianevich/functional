import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.MinPQ;
import edu.princeton.cs.algs4.StdOut;

import java.util.LinkedList;
import java.util.List;

public class Solver {
  private final SearchNode solution;
  private final Iterable<Board> solutionMoves;
  private final boolean isSolvable;

  // find a solution to the initial board (using the A* algorithm)
  public Solver(Board initial) {
    if (initial == null) throw new IllegalArgumentException("board cannot be null");

    MinPQ<SearchNode> pq = new MinPQ<>();
    MinPQ<SearchNode> twinPq = new MinPQ<>();

    SearchNode twinSearchNode = new SearchNode(null, initial.twin());
    SearchNode searchNode = new SearchNode(null, initial);
    while (!searchNode.board.isGoal() && !twinSearchNode.board.isGoal()) {
      searchNode = nextSearchNode(pq, searchNode);
      twinSearchNode = nextSearchNode(twinPq, twinSearchNode);
    }
    if (twinSearchNode.board.isGoal() && !searchNode.board.isGoal()) {
      isSolvable = false;
      solutionMoves = null;
      solution = null;
    } else {
      isSolvable = true;
      solution = searchNode;
      solutionMoves = computeMoves(solution);
    }
    
  }

  private SearchNode nextSearchNode(MinPQ<SearchNode> pq, SearchNode searchNode) {
    addNeighbors(searchNode, pq);
    return pq.delMin();
  }

  private void addNeighbors(SearchNode search, MinPQ<SearchNode> pq) {
    boolean dupIsFound = false;
    for (Board board : search.board.neighbors()) {
      // critical: omit boards that are the same as grandparent
      if (search.prev == null || dupIsFound || !search.prev.board.equals(board)) {
        pq.insert(new SearchNode(search, board));
      } else {
        dupIsFound = true;
      }
    }
  }

  private List<Board> computeMoves(SearchNode node) {
    LinkedList<Board> moves = new LinkedList<>();
    SearchNode prevState = node;
    while (prevState != null) {
      moves.addFirst(prevState.board);
      prevState = prevState.prev;
    }
    return moves;
  }

  private static class SearchNode implements Comparable<SearchNode> {
    final SearchNode prev;
    final Board board;
    final int moves;
    final int complexity;

    SearchNode(SearchNode prev, Board board) {
      this.prev = prev;
      this.board = board;
      moves = prev == null ? 0 : prev.moves + 1;
      complexity = board.manhattan() + moves;
    }

    @Override
    public int compareTo(SearchNode that) {
      return Integer.compare(complexity, that.complexity);
    }
  }

  // is the initial board solvable? (see below)
  public boolean isSolvable() {
    return isSolvable;
  }

  // min number of moves to solve initial board
  public int moves() {
    return solution == null ? -1 : solution.moves;
  }

  // sequence of boards in a shortest solution
  public Iterable<Board> solution() {
    return solutionMoves;
  }

  // test client (see below) 
  public static void main(String[] args) {
    // create initial board from file
    In in = new In(args[0]);
    int n = in.readInt();
    int[][] tiles = new int[n][n];
    for (int i = 0; i < n; i++)
      for (int j = 0; j < n; j++)
        tiles[i][j] = in.readInt();
    Board initial = new Board(tiles);

    // solve the puzzle
    Solver solver = new Solver(initial);

    // print solution to standard output
    if (!solver.isSolvable())
      StdOut.println("No solution possible");
    else {
      StdOut.println("Minimum number of moves = " + solver.moves());
      for (Board board : solver.solution())
        StdOut.println(board);
    }
  }
}
