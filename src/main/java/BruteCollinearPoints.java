import java.util.ArrayList;
import java.util.Arrays;

public class BruteCollinearPoints {

    private final LineSegment[] segments;

    // finds all line segments containing 4 points
    public BruteCollinearPoints(Point[] pointsIn) {
        if (pointsIn == null) {
            throw new IllegalArgumentException("points are null");
        }
        Point[] points = pointsIn.clone();
        int len = points.length;
        for (Point point : points) {
            if (point == null) {
                throw new IllegalArgumentException("point is null");
            }
        }

        Arrays.sort(points);

        for (int i = 0; i < len - 1; i++) {
           if (points[i].equals(points[i + 1])) {
               throw new IllegalArgumentException("2 point are equal");
           }
        }

        ArrayList<LineSegment> segmentsTmp = new ArrayList<>();
        for (int i = 0; i < len; i++)
            for (int j = i + 1; j < len; j++)
                for (int k = j + 1; k < len; k++)
                    for (int t = k + 1; t < len; t++) {
                        Point p = points[i];
                        double s1 = p.slopeTo(points[j]);
                        double s2 = p.slopeTo(points[k]);
                        if (s1 == s2) {
                            double s3 = p.slopeTo(points[t]);
                            if (s2 == s3) {
                                segmentsTmp.add(new LineSegment(p, points[t]));
                            }
                        }
                    }
        this.segments = segmentsTmp.toArray(new LineSegment[0]);
    }

    // the number of line segments
    public int numberOfSegments() {
        return segments.length;
    }

    // the line segments
    public LineSegment[] segments() {
        return segments.clone();
    }

}
