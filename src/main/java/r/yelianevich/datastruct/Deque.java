package r.yelianevich.datastruct;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * A double-ended queue or deque (pronounced “deck”) is a generalization of a stack and a queue
 * that supports adding and removing items from either the front or the back of the data structure
 * @param <Item>
 */
public class Deque<Item> implements Iterable<Item> {

  private Node first = null;
  private Node last = null;
  private int size = 0;

  private class Node {
    Node next;
    Node prev;
    Item item;
    Node(Node prev, Item item, Node next) {
      this.next = next;
      this.prev = prev;
      this.item = item;
    }
  }

  /**
   * Construct an empty deque
   */
  public Deque() {
  }

  /**
   * Is the deque empty?
   */
  public boolean isEmpty() {
    return size == 0;
  }

  /**
   * Return the number of items on the deque
   */
  public int size() {
    return size;
  }

  /**
   * add the item to the front
   */
  public void addFirst(Item item) {
    if (item == null) throw new IllegalArgumentException("item cannot be null");
    if (!isEmpty()) {
      Node newFirst = new Node(null, item, first);
      first.prev = newFirst;
      first = newFirst;
    } else {
      first = new Node(null, item, null);
      last = first;
    }
    size++;
  }

  /**
   * Add the item to the end
   */
  public void addLast(Item item) {
    if (item == null) throw new IllegalArgumentException("item cannot be null");
    if (!isEmpty()) {
      last.next = new Node(last, item, null);
      last = last.next;
    } else {
      first = new Node(null, item, null);
      last = first;
    }
    size++;
  }

  /**
   * Remove and return the item from the front
   */
  public Item removeFirst() {
    if (isEmpty()) throw new NoSuchElementException("deque is empty");
    Item r = first.item;
    if (size > 1) {
      first = first.next;
      first.prev = null;
    } else if (size == 1) {
      last = null;
      first = null;
    } else {
      throw new IllegalStateException("Size cannot be negative: " + size);
    }
    --size;
    return r;
  }

  /**
   * Remove and return the item from the end
   */
  public Item removeLast() {
    if (isEmpty()) throw new NoSuchElementException("deque is empty");
    Item r = last.item;
    if (size > 1) {
      last = last.prev;
      last.next = null;
    } else if (size == 1) {
      first = null;
      last = null;
    } else {
      throw new IllegalStateException("Size cannot be negative: " + size);
    }
    --size;
    return r;
  }
//

  /**
   * Return an iterator over items in order from front to end
   */
  public Iterator<Item> iterator() {
    return new DequeIter();
  }

  private class DequeIter implements Iterator<Item> {
    private Node curr = first;

    @Override
    public boolean hasNext() {
      return curr != null;
    }

    @Override
    public Item next() {
      if (curr == null) throw new NoSuchElementException("No more elements");
      Node n = curr;
      curr = curr.next;
      return n.item;
    }

  }

}