package r.yelianevich.datastruct.stack;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Stack that can return current max item.
 * Use two stacks, one to store all of the items and a second stack to store the maximums.
 * @param <T> some comparable type
 */
public class LinkedMaxStack<T extends Comparable<T>> implements Stack<T>, Iterable<T> {
  private Node top = null;
  private Node max = null;

  private class Node {
    Node next;
    T item;

    Node(Node next, T item) {
      this.next = next;
      this.item = item;
    }
  }

  public T max() {
    if (max == null) throw new NoSuchElementException("max");
    return max.item;
  }

  public T popMax() {
    T x = max.item;
    Deque<T> buffer = new ArrayDeque<>();
    while (top.item.compareTo(x) != 0) {
      buffer.push(pop());
    }
    pop();
    while (!buffer.isEmpty()) {
      push(buffer.pop());
    }
    return x;
  }

  @Override
  public void push(T item) {
    if (item == null) throw new NullPointerException("item is null");
    top = new Node(top, item);
    if (max != null) {
      if (item.compareTo(max.item) > 0) {
        max = new Node(max, item);
      }
    } else {
      max = new Node(max, item);
    }
  }

  @Override
  public T pop() {
    if (top == null) throw new NoSuchElementException("pop");
    T item = top.item;
    top = top.next;
    if (max != null && item.compareTo(max.item) == 0) {
      max = max.next;
    }
    return item;
  }

  @Override
  public Iterator<T> iterator() {
    return new StackIterator();
  }

  @Override
  public boolean isEmpty() {
    return top == null;
  }

  private class StackIterator implements Iterator<T> {
    private Node curr = top;

    public boolean hasNext() {
      return curr != null;
    }

    @Override
    public T next() {
      if (curr == null) throw new NoSuchElementException("Iterator is empty");
      T item = curr.item;
      curr = curr.next;
      return item;
    }
  }
}
