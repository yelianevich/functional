package r.yelianevich.datastruct.stack;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedStack<T> implements Iterable<T>, Stack<T> {

  private Node top = null;

  private class Node {
    T item;
    Node next;
    Node(T item, Node next) {
      this.item = item;
      this.next = next;
    }
  }

  @Override
  public void push(T item) {
    top = new Node(item, top);
  }

  @Override
  public T pop() {
    if (isEmpty()) throw new NoSuchElementException("stack is empty");
    T item = top.item;
    top = top.next;
    return item;
  }

  @Override
  public boolean isEmpty() {
    return top == null;
  }

  @Override
  public Iterator<T> iterator() {
    return new StackIterator();
  }

  private class StackIterator implements Iterator<T> {
    private Node curr = top;

    public boolean hasNext() {
      return curr != null;
    }

    @Override
    public T next() {
      if (curr == null) throw new NoSuchElementException("Iterator is empty");
      T item = curr.item;
      curr = curr.next;
      return item;
    }
  }

}
