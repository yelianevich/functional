package r.yelianevich.datastruct.list;

import java.util.Iterator;
import java.util.Objects;
import java.util.stream.IntStream;

public class ArrList<E> implements Iterable, ListApi<E> {

  public static final int DEFAULT_CAPACITY = 10;

  private E[] elements;
  private int size;

  @SuppressWarnings("unchecked")
  public ArrList() {
    // might be optimized to start with empty array and grow to default on the first element;
    this.elements = (E[]) new Object[DEFAULT_CAPACITY];
    this.size = 0;
  }

  public int size() {
    return this.size;
  }

  public boolean isEmpty() {
    return this.size == 0;
  }

  public void add(E elem) {
    if (elements.length == size) {
      growArray();
    }
    elements[size] = elem;
    ++size;
  }

  private void growArray() {
    int oldSize = elements.length;
    int newSize = oldSize + (oldSize >> 2);
    @SuppressWarnings("unchecked")
    E[] newElements = (E[]) new Object[newSize];
    System.arraycopy(elements, 0, newElements, 0, size);
    elements = newElements;
  }

  public E get(int i) {
    checkRange(i);
    return elements[i];
  }

  private void checkRange(int i) {
    if (i < 0 || i >= size) {
      throw new IllegalArgumentException("Index " + i + " is out of bounds");
    }
  }

  public E set(int i, E elem) {
    checkRange(i);
    E prev = elements[i];
    elements[i] = elem;
    return prev;
  }

  public int indexOf(E elem) {
    for (int i = 0; i < size; ++i) {
      E current = elements[i];
      if (Objects.equals(current, elem)) {
        return i;
      }
    }
    return -1;
  }

  public E remove(int i) {
    checkRange(i);
    E elem = elements[i];
    System.arraycopy(elements, i + 1, elements, i, size - i);
    elements[--size] = null; // GC
    return elem;
  }

  @Override
  public Iterator<E> iterator() {
    return new Iter();
  }

  private class Iter implements Iterator<E> {
    private int cursor = 0;

    @Override
    public E next() {
      // might want to check modification
      return get(cursor++);
    }

    @Override
    public boolean hasNext() {
      // might want to check modification
      return cursor < size();
    }
  }

  public static void main(String[] args) {
    ArrList<Integer> alist = new ArrList<>();
    System.out.println("isEmpty() = " + alist.isEmpty());
    System.out.println("size() = " + alist.size());

    IntStream.range(0, 50).forEach(alist::add);
    System.out.println("isEmpty() = " + alist.isEmpty());
    System.out.println("size() = " + alist.size());
    alist.set(4, 400);

    alist.remove(5);
    alist.remove(5);
    alist.remove(5);

    System.out.println("Elements:");
    for (Object e : alist) {
      System.out.println(e);
    }

    System.out.println("Index of 5 is = " + alist.indexOf(5));

    System.out.println("isEmpty = " + alist.isEmpty());
    System.out.println("size() = " + alist.size());


  }

}
