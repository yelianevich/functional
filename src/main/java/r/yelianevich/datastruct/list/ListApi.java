package r.yelianevich.datastruct.list;

public interface ListApi<E> {

  void add(E elem);

  E get(int i);

  E set(int i, E elem);

  E remove(int i);

  boolean isEmpty();

  int size();

}
