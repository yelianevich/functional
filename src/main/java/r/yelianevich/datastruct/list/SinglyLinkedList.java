package r.yelianevich.datastruct.list;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.stream.IntStream;

/**
 * Singly Linked List implementation of basic list API
 */
public class SinglyLinkedList<E> implements Iterable<E>, ListApi<E> {

  private Node<E> head;
  private Node<E> tail;
  private int size;

  public SinglyLinkedList() {
    this.size = 0;
  }

  @Override
  public void add(E elem) {
    Node<E> newNode = new Node<>(null, elem);
    if (head == null) {
      head = newNode;
      tail = newNode;
    } else {
      tail.next = newNode;
      tail = newNode;
    }
    ++size;
  }

  @Override
  public E get(int index) {
    checkIndex(index);
    return node(index).elem;
  }

  private Node<E> node(int index) {
    Node<E> node = head;
    for (int i = 0; i < index; ++i) {
      node = node.next;
    }
    return node;
  }

  private void checkIndex(int i) {
    if (i < 0 || i >= size) {
      throw new IndexOutOfBoundsException("Index '" + i + "' is out of bounds of list of " + size + " size");
    }
  }

  @Override
  public E set(int i, E elem) {
    return node(i).elem = elem;
  }

  @Override
  public E remove(int i) {
    checkIndex(i);

    Node<E> removed;

    if (i == 0) { // delete head
      // delete using head
      removed = head;
      removed.next = null;
      head = head.next;

      // list became empty - free up tail
      if (head == null) {
        tail = null;
      }
    } else {
      Node<E> prev = node(i - 1);
      removed = prev.next;
      prev.next = removed.next;
      removed.next = null;
      if (removed == tail) {
        tail = prev;
      }
    }
    --size;
    return removed.elem;
  }

  @Override
  public boolean isEmpty() {
    return size == 0;
  }

  @Override
  public int size() {
    return size;
  }

  @Override
  public Iterator<E> iterator() {
    return new It();
  }

  private class It implements Iterator<E> {

    private Node<E> cursor = head;

    @Override
    public boolean hasNext() {
      return cursor != null;
    }

    @Override
    public E next() {
      if (cursor != null) {
        E elem = cursor.elem;
        cursor = cursor.next;
        return elem;
      } else {
        throw new NoSuchElementException();
      }
    }
  }

  private static class Node<E> {
    // Single-linked list node, single-linked list node contains only next link
    Node<E> next;
    E elem;

    Node(Node<E> next, E elem) {
      this.next = next;
      this.elem = elem;
    }
  }

  public static void main(String[] args) {
    SinglyLinkedList<Integer> alist = new SinglyLinkedList<>();
    System.out.println("isEmpty() = " + alist.isEmpty());
    System.out.println("size() = " + alist.size());

    alist.add(1);
    alist.remove(0);
    printElements(alist);

    alist.add(2);
    alist.add(3);
    alist.remove(1);
    printElements(alist);

    IntStream.range(0, 50).forEach(alist::add);
    System.out.println("isEmpty() = " + alist.isEmpty());
    System.out.println("size() = " + alist.size());
    alist.set(4, 400);

    alist.remove(5);
    alist.remove(5);
    alist.remove(5);
    printElements(alist);


    System.out.println("isEmpty = " + alist.isEmpty());
    System.out.println("size() = " + alist.size());
  }

  private static <T> void printElements(SinglyLinkedList<T> l) {
    System.out.println("Elements:");
    for (T e : l) {
      System.out.println(e);
    }
  }

}
