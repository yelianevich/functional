package r.yelianevich.datastruct.list;

import java.util.Iterator;
import java.util.stream.IntStream;

public class DoublyLinkedList<E> implements Iterable<E>, ListApi<E> {

  private Node<E> head;
  private Node<E> tail;
  private int size = 0;

  private static class Node<E> {
    Node<E> prev;
    E elem;
    Node<E> next;

    public Node(Node<E> prev, E elem, Node<E> next) {
      this.prev = prev;
      this.elem = elem;
      this.next = next;
    }
  }

  @Override
  public void add(E elem) {
    if (head == null) {
      Node<E> n = new Node<>(null, elem, null);
      head = n;
      tail = n;
    } else {
      Node<E> n = new Node<>(tail, elem, null);
      tail.next = n;
      tail = n;
    }
    ++size;
  }

  @Override
  public E get(int i) {
    checkIndex(i);
    return node(i).elem;
  }

  private Node<E> node(int index) {
    if (index < (size >> 1)) {
      Node<E> currNode = head;
      for (int i = 0; i != index; ++i) {
        currNode = currNode.next;
      }
      return currNode;
    } else {
      Node<E> currNode = tail;
      for (int i = size - 1; i != index; --i) {
        currNode = currNode.prev;
      }
      return currNode;
    }
  }

  private void checkIndex(int i) {
    if (i < 0 || i >= size) {
      throw new IndexOutOfBoundsException("Index " + i + " out of range 0.." + size);
    }
  }

  @Override
  public E set(int index, E elem) {
    E prev = node(index).elem;
    node(index).elem = elem;
    return prev;
  }

  @Override
  public E remove(int i) {
    checkIndex(i);
    E elem;
    if (i == 0) {
      elem = head.elem;
      head = head.next;

      if (head == null) {
        tail = null;
      }
    } else {
      Node<E> node = node(i);
      node.prev.next = node.next;
      if (node == tail) {
        tail = node.prev;
      }
      elem = node.elem;
    }
    --size;
    return elem;
  }

  @Override
  public boolean isEmpty() {
    return size == 0;
  }

  @Override
  public int size() {
    return size;
  }

  @Override
  public Iterator<E> iterator() {
    return new It();
  }

  private class It implements Iterator<E> {
    private Node<E> curr = head;

    @Override
    public boolean hasNext() {
      return curr != null;
    }

    @Override
    public E next() {
      E elem = curr.elem;
      curr = curr.next;
      return elem;
    }
  }

  public static void main(String[] args) {
    DoublyLinkedList<Integer> alist = new DoublyLinkedList<>();
    System.out.println("isEmpty() = " + alist.isEmpty());
    System.out.println("size() = " + alist.size());

    alist.add(1);
    alist.remove(0);
    printElements(alist);

    alist.add(2);
    alist.add(3);
    alist.remove(1);
    printElements(alist);

    IntStream.range(0, 50).forEach(alist::add);
    System.out.println("isEmpty() = " + alist.isEmpty());
    System.out.println("size() = " + alist.size());
    alist.set(4, 400);

    alist.remove(5);
    alist.remove(5);
    alist.remove(5);
    printElements(alist);


    System.out.println("isEmpty = " + alist.isEmpty());
    System.out.println("size() = " + alist.size());
  }

  private static <T> void printElements(Iterable<T> l) {
    System.out.println("Elements:");
    for (T e : l) {
      System.out.println(e);
    }
  }

}
