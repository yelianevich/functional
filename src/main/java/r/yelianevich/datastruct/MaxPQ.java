package r.yelianevich.datastruct;

import java.util.NoSuchElementException;

/**
 * Max priority queue. Returns max element with remove operation.
 * Optimizations:
 * - 2.4.26 reduce exchanges by half
 *
 * @param <Key> comparable type
 */
public class MaxPQ<Key extends Comparable<Key>> {

  /* Contains keys at [1..N]; pq[0] is unused */
  Key[] pq;

  private static final int DEFAULT_CAPACITY = 16;

  private int capacity;
  private int n;

  public MaxPQ() {
    this(DEFAULT_CAPACITY);
  }

  @SuppressWarnings("unchecked")
  public MaxPQ(Key[] a) {
    // Array sorted array in descending order is already a heap.
    // Here I assume no prior knowledge about collection
    int n = a.length;
    this.capacity = n + 1;
    this.pq = (Key[]) new Comparable[capacity];
    this.n = n;
    System.arraycopy(a, 0, pq, 1, n);
    for (int i = n / 2; i > 0; --i) {
      sink(i);
    }
  }

  @SuppressWarnings("unchecked")
  public MaxPQ(int capacity) {
    this.capacity = capacity + 1;
    this.n = 0;
    this.pq = (Key[]) new Comparable[this.capacity];
  }

  public void insert(Key k) {
    checkCapacity();
    pq[++n] = k;
    swim(n);
  }

  public Key delMax() {
    checkEmpty();
    Key max = pq[1];
    exch(1, n);
    pq[n--] = null; // throw max item to the garbage bin
    sink(1);
    checkCapacity();
    return max;
  }

  private void checkEmpty() {
    if (isEmpty()) {
      throw new NoSuchElementException("Priority queue is empty. Operation is not permitted");
    }
  }

  public int size() {
    return n;
  }

  public boolean isEmpty() {
    return n == 0;
  }

  public boolean nonEmpty() {
    return !isEmpty();
  }

  /**
   * Top-down reheapify: sink smaller (lower priority) item to the bottom of the binary heap
   * @param k index of the item to sink
   */
  private void sink(int k) {
    Key v = pq[k];
    while (k * 2 < n) {
      int j = k * 2;
      if (j < n && less(j, j + 1)) j++;
      if (!less(v, pq[j])) break;
      // exch(k, j); - same optimization as in swim
      pq[k] = pq[j];
      k = j;
    }
    pq[k] = v;
  }

  /**
   * Bottom-up reheapify: swim bigger (higher priority) element to the top of the binary heap
   * @param k index of the item to swim
   */
  private void swim(int k) {
    Key v = pq[k];
    while (k > 1 && less(pq[k / 2], v)) {
      // We keep pq[k] in variable v, no need to set it all the time to new position during exchange.
      // This optimization reduce number of array accesses by 2
      pq[k] = pq[k / 2];
      k = k / 2;
    }
    pq[k] = v;
  }

  private boolean less(int i, int j) {
    return pq[i].compareTo(pq[j]) < 0;
  }

  private boolean less(Key x, Key y) {
    return x.compareTo(y) < 0;
  }

  private void exch(int i, int j) {
    Key tmp = pq[i];
    pq[i] = pq[j];
    pq[j] = tmp;
  }

  private void checkCapacity() {
    if (n == capacity - 1) {
      resize(capacity * 2);
    } else if (n > 0 && (n * 4 <= capacity - 1)) {
      resize(capacity / 2);
    }
  }

  @SuppressWarnings("unchecked")
  private void resize(int newCapacity) {
    Key[] newPq = (Key[]) new Comparable[newCapacity];
    System.arraycopy(pq, 1, newPq, 1, n);
    this.capacity = newCapacity;
    this.pq = newPq;
  }

}
