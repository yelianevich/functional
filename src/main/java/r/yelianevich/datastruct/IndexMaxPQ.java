package r.yelianevich.datastruct;

import java.util.Arrays;
import java.util.NoSuchElementException;

/**
 * TODO usage is not clear. This implementation differs from the one in the book
 * @param <Key>
 */
public class IndexMaxPQ<Key extends Comparable<Key>> {
  private int[] pq;       // holds indices of keys; only heap here
  private int[] qp;       // inverse of qp; gives position of i in pq; -1 means that i is not in PQ
  private Key[] keys;     // holds keys indexed by index
  private int capacity;
  private int n;

  @SuppressWarnings("unchecked")
  public IndexMaxPQ(int capacity) {
    this.capacity = capacity + 1;
    this.n = 0;
    this.pq = new int[this.capacity];
    this.qp = new int[this.capacity];
    Arrays.fill(qp, -1);
    this.keys = (Key[]) new Comparable[this.capacity];
  }

  public void insert(int i, Key key) {
    checkIndex(i);
    n += 1;
    keys[n] = key;
    pq[n] = i;
    qp[i] = n;
    swim(n);
  }

  private void checkIndex(int i) {
    if (i < 0 || i > capacity) {
      throw new IllegalArgumentException(
          "Index '" + i + "' is invalid. Should be from 0 to " + capacity);
    }
  }

  private void swim(int k) {
    while (k > 1 && less(k / 2, k)) {
      exch(k / 2, k);
      k = k / 2;
    }
  }

  private boolean less(int i, int j) {
    return keys[i].compareTo(keys[j]) < 0;
  }

  private void exch(int i, int j) {
    exch(keys, i, j);
    exch(pq, i, j);
    exch(qp, pq[i], pq[j]);
  }

  private <T> void exch(T[] a, int i, int j) {
    T tmp = a[i];
    a[i] = a[j];
    a[j] = tmp;
  }

  private void exch(int[] a, int i, int j) {
    int tmp = a[i];
    a[i] = a[j];
    a[j] = tmp;
  }

  public void changeKey(int i, Key key) {
    if (!contains(i)) throw new NoSuchElementException("index is not in the priority queue");
    keys[qp[i]] = key;
    swim(i);
    sink(i);
  }

  public boolean contains(int i) {
    checkIndex(i);
    return qp[i] != -1;
  }

  public void delete(int i) {
    int k = qp[i];
    exch(k, n);
    delLast();
    swim(k);
    sink(k);
  }

  public Key keyOf(int i) {
    checkIndex(i);
    return keys[qp[i]];
  }

  public boolean isEmpty() {
    return n == 0;
  }

  public int size() {
    return n;
  }

  public Key maxKey() {
    checkEmpty();
    return keys[1];
  }

  private void checkEmpty() {
    if (isEmpty()) throw new NoSuchElementException("PQ is empty");
  }

  public int delMax() {
    checkEmpty();
    exch(1, n);
    int i = delLast();
    sink(1);
    return i;
  }

  private int delLast() {
    keys[n] = null;
    int i = pq[n];
    qp[i] = -1;
    n -= 1;
    return i;
  }

  private void sink(int k) {
    while (k * 2 <= n) {
      int j = k * 2;
      if (j < n && less(j, j + 1)) j++;
      if (!less(k, j)) break;
      exch(k, j);
      k = j;
    }
  }

  /**
   * Returns an index associated with a maximum key.
   */
  public int maxIndex() {
    checkEmpty();
    return pq[1];
  }

}
