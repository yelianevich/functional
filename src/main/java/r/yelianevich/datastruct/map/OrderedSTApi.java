package r.yelianevich.datastruct.map;

public interface OrderedSTApi <Key> {

  Key max();
  Key min();

  /**
   * Max key that is less of equal to the given key
   */
  Key floor(Key key);

  /**
   * Min key that is greater or equal to the given key
   */
  Key ceiling(Key key);

  //Key select(int rank);

  /**
   * Number of keys that are smaller than the given key
   */
  int rank(Key key);

  /**
   * Find a key of a given rank
   */
  Key select(int rank);

}
