package r.yelianevich.datastruct.map;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * <p>Left-leaning red-black balanced binary search tree that implements symbol table abstract data type.
 * <p>
 * Tree maintains 1-1 correspondence with 2-3 tree:
 * <ol>
 * <li>Red link leans left.</li>
 * <li>Node can have only 1 red link connected to it.</li>
 * <li>Tree has perfect <em>black</em> balance.</li>
 * <ol/>
 */
public class RedBlackBST<Key extends Comparable<Key>, Value>
    implements SymbolTableApi<Key, Value> {

  private static final boolean RED = true;
  private static final boolean BLACK = false;

  private Node root;

  private class Node {
    Key key;
    Value value;
    Node left;
    Node right;
    boolean color; // color of the link from the parent to current Node
    int size;

    Node(Key key, Value value, boolean color, int size) {
      this.key = key;
      this.value = value;
      this.color = color;
      this.size = size;
    }

    @Override
    public String toString() {
      return "Node{" + "key=" + key +
          ", value=" + value +
          ", color=" + color +
          ", size=" + size +
          '}';
    }
  }

  private boolean isRed(Node node) {
    return node != null ? node.color == RED : BLACK;
  }

  private Node rotateLeft(Node node) {
    Node x = node.right;
    node.right = x.left;
    x.left = node;
    x.color = node.color;
    node.color = RED;
    x.size = node.size; // size of the given tree do not change - reset from previous root
    node.size = 1 + size(node.left) + size(node.right); // given node accept new right link - need to recompute size
    return x;
  }

  private Node rotateRight(Node node) {
    Node x = node.left;
    node.left = x.right;
    x.right = node;
    x.color = node.color;
    node.color = RED;
    x.size = node.size;
    node.size = 1 + size(node.left) + size(node.right);
    return x;
  }

  private void flipColors(Node n) {
    n.color = !n.color;
    n.left.color = !n.left.color;
    n.right.color = !n.right.color;
  }

  @Override
  public Value put(Key key, Value value) {
    root = put(root, checkKey(key), value);
    root.color = BLACK; // always set to black, cause color flip could make it RED
    return null;
  }

  private Key checkKey(Key key) {
    if (key == null) throw new IllegalArgumentException("Key cannot be null");
    return key;
  }

  public Node put(Node node, Key key, Value value) {
    if (node == null) {
      return new Node(key, value, RED, 1);
    }

    int cmp = key.compareTo(node.key);
    if (cmp < 0) node.left = put(node.left, key, value);
    else if (cmp > 0) node.right = put(node.right, key, value);
    else node.value = value;

    // fix right leaning link
    if (!isRed(node.left) && isRed(node.right)) {
      node = rotateLeft(node);
    }
    // fix 2 red links in a row (if left is red - it's not null)
    if (isRed(node.left) && isRed(node.left.left)) {
      node = rotateRight(node);
    }
    // fix both children are red
    if (isRed(node.left) && isRed(node.right)) {
      flipColors(node);
    }
    node.size = 1 + size(node.left) + size(node.right);
    return node;
  }

  @Override
  public Value get(Key key) {
    return get(root, checkKey(key));
  }

  private Value get(Node node, Key key) {
    while (node != null) {
      int cmp = key.compareTo(node.key);
      if (cmp < 0) node = node.left;
      else if (cmp > 0) node = node.right;
      else return node.value;
    }
    return null;
  }

  @Override
  public int size() {
    return size(root);
  }

  private int size(Node node) {
    return node != null ? node.size : 0;
  }

  public Key min() {
    if (isEmpty()) throw new NoSuchElementException("Symbol table is empty, cannot take min");
    return min(root).key;
  }

  private Node min(Node h) {
    if (h.left == null) return h;
    else return min(h.left);
  }

  @Override
  public Value delete(Key key) {
    checkKey(key);
    if (isEmpty()) throw new NoSuchElementException("Cannot delete from empty symbol table");

    // since we do many transformations on the way down and up the search path
    // it's better to ensure key exists
    if (!contains(key)) return null;

    root = delete(root, key);
    if (!isEmpty()) root.color = BLACK;
    return null;
  }

  private Node delete(Node h, Key key) {
    if (key.compareTo(h.key) < 0) {
      if (!isRed(h.left) && !isRed(h.left.left)) {
        h = moveRedLeft(h);
      }
      h.left = delete(h.left, key);
    } else {
      if (isRed(h.left)) {
        h = rotateRight(h);
      }

      if (key.compareTo(h.key) == 0 && isRed(h) && h.right == null) {
        // TODO
      }

      if (!isRed(h.right) && !isRed(h.right.right)) {
        // TODO
      }

    }
    return balance(h);
  }

  public void deleteMin() {
    if (isEmpty()) throw new NoSuchElementException("BST underflow");
    root = deleteMin(root);
    if (!isEmpty()) root.color = BLACK;
  }

  private Node deleteMin(Node h) {
    if (h.left == null) return null;

    // if left node is not a 2-node
    if (!isRed(h.left) && !isRed(h.left.left)) {
      h = moveRedLeft(h);
    }
    h.left = deleteMin(h.left);
    return balance(h);
  }

  // Assuming that h is red and both h.left and h.left.left
  // are black, make h.left or one of its children red.
  private Node moveRedLeft(Node h) {
    // in simplest case flip of color will make left node a 3-node
    flipColors(h);

    // but if right node is a 3-node - borrow from there
    if (h.right != null && isRed(h.right.left)) {
      h.right = rotateRight(h.right);
      h = rotateLeft(h);
      flipColors(h);
    }
    return h;
  }

  // restore red-black tree invariant
  private Node balance(Node h) {
    if (isRed(h.right)) h = rotateLeft(h); // this line is different from put balance implementation!
    if (isRed(h.left) && isRed(h.left.left)) h = rotateRight(h);
    if (isRed(h.left) && isRed(h.right)) flipColors(h);
    h.size = 1 + size(h.left) + size(h.right);
    return h;
  }

  public void deleteMax() {
    if (isEmpty()) throw new NoSuchElementException("Symbol table is empty, cannot deleteMax");
    root = deleteMax(root);
    if (!isEmpty()) root.color = BLACK;
  }

  private Node deleteMax(Node h) {
    if (isRed(h.left)) {
      h = rotateRight(h);
    }

    if (h.right == null) return null; // actual delete is here :)

    if (!isRed(h.right) && !isRed(h.right.right)) {
      h = moveRedRight(h);
    }
    h.right = deleteMax(h.right);
    return balance(h);
  }

  private Node moveRedRight(Node h) {
    flipColors(h);
    if (h.left != null && isRed(h.left.left)) {
      h = rotateRight(h);
      flipColors(h);
    }
    return h;
  }

  @Override
  public Iterable<Key> keys() {
    return root != null ? inOrderKeys(root) : Collections.emptyList();
  }

  Iterable<Key> inOrderKeys(Node node) {
    List<Key> keys = new ArrayList<>();
    Deque<Node> stack = new ArrayDeque<>();
    Node curr = node;
    while (curr != null || !stack.isEmpty()) {
      if (curr != null) {
        // go to the smallest key in the subtree
        while (curr.left != null) {
          stack.push(curr);
          curr = curr.left;
        }
      } else {
        curr = stack.pop();
      }
      keys.add(curr.key);
      if (curr.right != null) {
        curr = curr.right;
      } else {
        curr = null;
      }
    }
    return keys;
  }

  @Override
  public String toString() {
    return "RedBlackBST{" + "root=" + root +
        '}';
  }

}
