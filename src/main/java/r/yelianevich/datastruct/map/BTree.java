package r.yelianevich.datastruct.map;


/**
 * B-Trees are balanced trees that are designed to work well on disks or other direct-access storage devices.
 * <p>
 * B-Trees are similar to Red-black trees, but they are better at minimizing disk I/O operations.
 * Many database systems use BTrees or its variants to store information.
 * <p>
 * This simple implementation stores all "satellite" information together with a Key.
 * In practice, B-Tree variant 'B+tree' stores all information in leaf (external) nodes and internal nodes contain
 * only keys and pointers to children.
 */
public class BTree<Key extends Comparable<Key>, Value> implements SymbolTableApi<Key, Value> {

  private static final int MIN_T = 2;

  // minimum degree of a BTree
  private final int t;
  private final int nodeCapacity;

  private int height;
  private int size;
  private Node<Key, Value> root;

  public BTree() {
    this(MIN_T);
  }

  public BTree(int t) {
    if (t < MIN_T) {
      throw new IllegalArgumentException("Minimum allowed degree of a BTree is 2");
    }
    this.t = t;
    this.nodeCapacity = 2 * t - 1;
    this.root = new Node<>(nodeCapacity, 0, true);
    this.height = 0;
    this.size = 0;
  }

  private static final class Node<K extends Comparable<K>, V> {
    final boolean isLeaf;   // determines if the node has any children
    int n;            // number of keys currently stored in a Node

    // (n + 1) Entries:
    // 1. n keys with values and children Nodes
    // 2. 1 extra node for 
    final Entry<K, V>[] entries;

    @SuppressWarnings("unchecked")
    Node(int capacity, int n, boolean isLeaf) {
      this.n = n;
      this.isLeaf = isLeaf;
      this.entries = (Entry<K, V>[]) new Entry[capacity + 1];
    }
  }

  private static final class Entry<K extends Comparable<K>, V> {
    final K key;
    final V value;
    final Node<K, V> child; // child keys are <= than this key

    Entry(K key, V value, Node<K, V> child) {
      this.key = key;
      this.value = value;
      this.child = child;
    }

    @Override
    public String toString() {
      return String.format("Entry{key=%s, value=%s, child=%s}", key, value, child);
    }
  }

  @Override
  public Value put(Key key, Value value) {
    if (isFull(root)) {
      Node<Key, Value> newRoot = new Node<>(nodeCapacity, 0, false);
      newRoot.entries[0] = new Entry<>(null, null, root);
      root = newRoot;
      splitChild(root, 0);
      height++;
    }
    return insertNonFull(root, key, value);
  }

  private boolean isFull(Node<Key, Value> node) {
    return node.n == nodeCapacity;
  }

  private void splitChild(Node<Key, Value> root, int i) {
    if (root.n == nodeCapacity) {
      throw new IllegalArgumentException("Cannot split child when parent is full");
    }
    Node<Key, Value> child = root.entries[i].child;
    if (child.n != nodeCapacity) {
      throw new IllegalArgumentException("Cannot split child that is not full");
    }

    // new child is set up
    Node<Key, Value> newChild = new Node<>(nodeCapacity, t - 1, child.isLeaf);
    System.arraycopy(child.entries, t, newChild.entries, 0, t); // extra element for sentinel node

    // allocate space for an element in the parent node (+1 => keep 1 extra sentinel node in mind)
    System.arraycopy(root.entries, i, root.entries, i + 1, root.n - i + 1);

    // assign median entry to a parent node
    Entry<Key, Value> middle = child.entries[t - 1];
    root.entries[i] = new Entry<>(middle.key, middle.value, child);
    root.n += 1;  // reflect new capacity

    Entry<Key, Value> moved = root.entries[i + 1];
    root.entries[i + 1] = new Entry<>(moved.key, moved.value, newChild);

    // clean up redundant links
    for (int j = t; j <= child.n; ++j) {
      child.entries[j] = null;
    }
    // keep a link from a middle element
    child.entries[t - 1] = new Entry<>(null, null, middle.child);
    child.n = t - 1; // shrink child to a new size
  }

  private Value insertNonFull(Node<Key, Value> node, Key key, Value value) {
    int i = node.n - 1;

    // search for correct position of the new Node
    while (i >= 0 && less(key, node.entries[i].key)) {
      i -= 1;
    }

    if (i >= 0 && eq(key, node.entries[i].key)) {
      // replace existing value
      Value oldValue = node.entries[i].value;
      Node<Key, Value> child = node.entries[i].child;
      node.entries[i] = new Entry<>(key, value, child);
      return oldValue;
    } else {
      if (node.isLeaf) {
        System.arraycopy(node.entries, i + 1, node.entries, i + 2, node.n - i);
        node.entries[i + 1] = new Entry<>(key, value, null);
        node.n += 1;
        size += 1;
        return null;
      } else {
        i += 1;
        if (isFull(node.entries, i)) {
          splitChild(node, i);
          return insertNonFull(node, key, value);
        } else {
          return insertNonFull(node.entries[i].child, key, value);
        }
      }
    }
  }

  private boolean isFull(Entry<Key, Value>[] entries, int i) {
    return entries[i].child.n == nodeCapacity;
  }

  @Override
  public Value get(Key key) {
    if (key == null) throw new IllegalArgumentException("Key cannot be null");
    return search(root, key);
  }

  private Value search(Node<Key, Value> root, Key key) {
    if (root == null) {
      return null;
    }
    Entry<Key, Value>[] entries = root.entries;

    // finds either entry with the key or index of the child to search
    int i = 0;
    while (i < root.n && less(entries[i].key, key)) {
      i += 1;
    }

    if (i < root.n && eq(entries[i].key, key)) {
      // value is found in the node
      return entries[i].value;
    } else if (root.isLeaf) {
      // value not found and no child to search
      return null;
    } else {
      // search the child
      return search(root.entries[i].child, key);
    }
  }

  private <T extends Comparable<T>> boolean eq(T x, T y) {
    return x.compareTo(y) == 0;
  }

  private <T extends Comparable<T>> boolean less(T x, T y) {
    return x.compareTo(y) < 0;
  }

  @Override
  public Value delete(Key key) {
    // TODO
    return null;
  }

  @Override
  public Iterable<Key> keys() {
    // TODO
    throw null;
  }

  public int getHeight() {
    return height;
  }

  @Override
  public int size() {
    return size;
  }

  /**
   * Returns a string representation of this B-tree (for debugging).
   *
   * @return a string representation of this B-tree.
   */
  public String toString() {
    return String.format("BTree(nodeCapacity=%s, height=%s)\n%s\n",
        nodeCapacity, height, toString(root, height, "|"));
  }

  private String toString(Node<Key, Value> h, int height, String indent) {
    StringBuilder s = new StringBuilder();
    Entry<Key, Value>[] entries = h.entries;

    if (h.isLeaf) {
      for (int i = 0; i < h.n; i++) {
        s.append(indent).append(entries[i].key).append(" ").append(entries[i].value).append("\n");
      }
    } else {
      s.append(indent).append("-\n");
      for (int i = 0; i <= h.n; i++) {
        if (i < h.n) {
          s.append(indent).append("(").append(entries[i].key).append("=").append(entries[i].value).append(")\n");
          s.append(toString(entries[i].child, height - 1, indent + "         |"));
        }
        if (i == h.n) {
          s.append(indent).append("(.)\n");
          if (entries[i] != null && entries[i].child != null) {
            s.append(toString(entries[i].child, height - 1, indent + "         |"));
          }
          s.append(indent).append("_\n");
        }
      }
    }
    return s.toString();
  }
}
