package r.yelianevich.datastruct.map;

import java.util.Arrays;
import java.util.Objects;

/**
 * Simple hash table implementation with linear probing collision resolution
 * @param <K>
 * @param <V>
 */
public class BasicHashTable<K, V> {

  private static final int DEFAULT_CAPACITY = 16;

  private static final float LOAD_FACTOR = 0.75f;

  private Entry<K, V>[] table;

  private int size;
  private int capacity;

  private static class Entry<K, V> {
    K key;
    V value;

    Entry(K key, V value) {
      this.key = key;
      this.value = value;
    }

    @Override
    public String toString() {
      return key + " -> " + value;
    }
  }

  public BasicHashTable() {
    this(DEFAULT_CAPACITY);
  }

  @SuppressWarnings("unchecked")
  public BasicHashTable(int capacity) {
    this.capacity = capacity;
    this.table = new Entry[capacity];
    this.size = 0;
  }

  public int size() {
    return this.size;
  }

  public V get(K key) {
    Entry<K, V> entry = getEntry(key);
    return entry == null ? null : entry.value;
  }

  private Entry<K, V> getEntry(K key) {
    return table[hash(key)];
  }

  private int hash(K key) {
    int hash = (key.hashCode() & 0x7FFFFFFF) % this.capacity;
    // resolve collision with linear probing
    while (table[hash] != null && !table[hash].key.equals(key)) {
      hash = (hash + 1) % capacity;
    }
    return hash;
  }

  public void put(K key, V value) {
    int hash = hash(key);
    table[hash] = new Entry<>(key, value);
    size++;
  }

  public V remove(K key) {
    int hash = hash(key);

    if (table[hash] == null) {
      return null;
    } else {
      Entry<K, V> del = table[hash];
      table[hash] = null;
      size--;

      // Linear probing may break after deletion,
      // due to hash collisions of deleted item and
      // the one that was inserted afterwards.
      // Start looking for collided items and swap to vacant bucket until next item is null,
      int nextIndex = (hash + 1) % capacity;
      while (table[nextIndex] != null) {
        int hashOfNext = hash(table[nextIndex].key);
        if (hashOfNext == hash) {
          // collision fount, swap and start from the index of found item
          System.out.println("Rehash during deletion " + table[nextIndex]);
          table[hash] = table[nextIndex];
          table[nextIndex] = null;
          hash = nextIndex;
        }
        nextIndex = (nextIndex + 1) % capacity;
      }

      return del.value;
    }
  }

  public boolean containsKey(K key) {
    return getEntry(key) != null;
  }

  public boolean containsValue(V value) {
    for (int i = 0; i < capacity; ++i) {
      Entry<K, V> entry = table[i];
      if (entry != null && Objects.equals(entry.value, value)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public String toString() {
    return Arrays.toString(table);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    BasicHashTable<?, ?> that = (BasicHashTable<?, ?>) o;
    return size == that.size &&
        capacity == that.capacity &&
        Arrays.equals(table, that.table);
  }

  @Override
  public int hashCode() {
    int result = Objects.hash(size, capacity);
    result = 31 * result + Arrays.hashCode(table);
    return result;
  }
}
