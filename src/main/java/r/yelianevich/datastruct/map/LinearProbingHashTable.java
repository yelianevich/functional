package r.yelianevich.datastruct.map;

import java.util.Arrays;
import java.util.Iterator;

/**
 * Hash table with the simplest open addressing collision resolution technique: linear probing.
 * <p>
 * Hash table will resize if there are too few slots available (double capacity) and when there are too few
 * elements is the table (half capacity).
 *
 * Keys and values are stored in two separate arrays that should reduce memory consumption
 *
 * @param <Key>
 * @param <Value>
 */
public class LinearProbingHashTable<Key, Value> implements SymbolTableApi<Key, Value> {

  static final float DEFAULT_LOAD_FACTOR = 0.5f;

  static final int DEFAULT_CAPACITY = 16;

  private Key[] keys;
  private Value[] values;

  private float loadFactor;
  private int capacity;
  private int size;

  public LinearProbingHashTable() {
    this(DEFAULT_CAPACITY, DEFAULT_LOAD_FACTOR);
  }

  @SuppressWarnings("unchecked")
  public LinearProbingHashTable(int capacity, float loadFactor) {
    this.loadFactor = loadFactor;
    this.capacity = capacity;
    this.size = 0;
    this.keys = (Key[]) new Object[this.capacity];
    this.values = (Value[]) new Object[this.capacity];
  }

  public int size() {
    return this.size;
  }

  public boolean isEmpty() {
    return this.size == 0;
  }

  int capacity() {
    return this.capacity;
  }

  public Value get(Key key) {
    for (int i = hash(check(key)); keys[i] != null; i = (i + 1) % capacity) {
      if (key.equals(keys[i])) {
        return values[i];
      }
    }
    return null;
  }

  private Key check(Key key) {
    if (key == null) {
      throw new IllegalArgumentException("null key is not allowed");
    } else {
      return key;
    }
  }

  private int hash(Key key) {
    // get rid of negative values using bit mask to delete sign bit
    return (key.hashCode() & 0x7FFFFFFF) % capacity;
  }

  public Value put(Key key, Value value) {
    check(key);
    if (size > capacity * loadFactor) {
      resizeTable(capacity << 1);
    }

    int hash = hash(key);
    while (keys[hash] != null && !key.equals(keys[hash])) {
      hash = (hash + 1) % capacity;
    }

    Value prevValue = values[hash];
    if (key.equals(keys[hash])) {
      values[hash] = value;
      return prevValue;
    } else {
      keys[hash] = key;
      values[hash] = value;
      size++;
      return prevValue;
    }
  }

  @SuppressWarnings("unchecked")
  private void resizeTable(int newCapacity) {
    LinearProbingHashTable<Key, Value> temp = new LinearProbingHashTable<>(newCapacity, this.loadFactor);
    for (int i = 0; i < keys.length; ++i) {
      Key oldKey = keys[i];
      if (oldKey != null) {
        temp.put(oldKey, values[i]);
      }
    }
    this.keys = temp.keys;
    this.values = temp.values;
    this.capacity = temp.capacity;
  }

  public Value delete(Key key) {
    int hash = hash(check(key));

    // try to find specified key
    while (keys[hash] != null && !key.equals(keys[hash])) {
      hash = (hash + 1) % capacity;
    }

    boolean keyExists = keys[hash] != null;
    if (keyExists) {
      // save value that will be removed
      Value prevValue = values[hash];

      // delete existing record
      keys[hash] = null;
      values[hash] = null;
      size--;

      // rehash next values to fix search, due to possible collisions at their values
      hash = (hash + 1) % capacity;
      while (keys[hash] != null) {
        Key tempKey = keys[hash];
        Value tempVal = values[hash];
        keys[hash] = null;
        values[hash] = null;
        size--;
        put(tempKey, tempVal);
        hash = (hash + 1) % capacity;
      }

      // if size is small comparing to capacity - shrink it
      if (size < ((capacity >> 1) * loadFactor)) {
        resizeTable(capacity >> 1);
      }

      return prevValue;
    } else {
      //nothing to delete
      return null;
    }
  }

  public boolean contains(Key key) {
    return get(key) != null;
  }

  @Override
  public String toString() {
    return Arrays.toString(keys);
  }

  public Iterable<Key> keys() {
    return KeysIterator::new;
  }

  private class KeysIterator implements Iterator<Key> {
    private int current;

    KeysIterator() {
      this.current = getNextKeyIndex(0);
    }

    private int getNextKeyIndex(int from) {
      int nextKeyIndex = from;
      while (nextKeyIndex < capacity && keys[nextKeyIndex] == null) {
        nextKeyIndex++;
      }
      return nextKeyIndex;
    }

    @Override
    public boolean hasNext() {
      return current < capacity;
    }

    @Override
    public Key next() {
      Key key = keys[current];
      current = getNextKeyIndex(current + 1);
      return key;
    }
  }
}
