package r.yelianevich.datastruct.map;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Unbalanced binary search tree (BST) that represent symbol table abstract data type.
 * Implements effectively ordered-based API: rank, select, floor, ceiling, min, max.
 * <p>
 * Special kind of binary tree:
 *  <li>Key is Comparable</>
 *  <li>left child is smaller than the parent</>
 *  <li>right child is greater than the parent</>
 *  <li>equals and hashCode are not used</li>
 * <p>
 * Pros:
 *  <li>In comparison to BinarySearchST, insert performance is logarithmic - 1.39 lgN -
 *  under random keys distribution assumption</li>
 * <p>
 * Cons:
 *   <li>Bad performance in worth-case scenario in case of non-random key distribution</li>
 *
 * @param <Key>   comparable key
 * @param <Value> any value type
 */
public class BinarySearchTree<Key extends Comparable<Key>, Value>
    implements SymbolTableApi<Key, Value>, OrderedSTApi<Key> {

  private Node root;

  private class Node {
    Key key;
    Value value;
    Node left;
    Node right;
    int size;

    Node(Key key, Value value, int size) {
      this.key = key;
      this.value = value;
      this.size = size;
    }

    @Override
    public String toString() {
      return "Node{" + "key=" + key +
          ", value=" + value +
          ", size=" + size +
          '}';
    }
  }

  public BinarySearchTree() {
  }

  @Override
  public Value put(Key key, Value value) {
    root = put(root, key, value);
    return null;
  }

  private Node put(Node node, Key key, Value value) {
    if (node == null) {
      return new Node(key, value, 1);
    }
    int cmp = key.compareTo(node.key);
    if (cmp < 0) {
      node.left = put(node.left, key, value);
    } else if (cmp > 0) {
      node.right = put(node.right, key, value);
    } else {
      node.value = value;
    }
    node.size = size(node.left) + size(node.right) + 1;
    return node;
  }

  @Override
  public Value get(Key key) {
    Node n = get(root, key);
    return n != null ? n.value : null;
  }

  private Node get(Node node, Key key) {
    if (node == null) {
      return null;
    }
    int cmp = key.compareTo(node.key);
    if (cmp < 0) {
      return get(node.left, key);
    } else if (cmp > 0) {
      return get(node.right, key);
    } else {
      return node;
    }
  }

  @Override
  public boolean contains(Key key) {
    return get(root, key) != null;
  }

  @Override
  public int size() {
    return size(root);
  }

  private int size(Node node) {
    return node != null ? node.size : 0;
  }

  @Override
  public boolean isEmpty() {
    return size() == 0;
  }

  @Override
  public Value delete(Key key) {
    root = delete(root, key);
    return null;
  }

  public Key min() {
    Node min = min(root);
    return min == null ? null : min.key;
  }

  private Node min(Node node) {
    Node min = node;
    while (min != null && min.left != null) {
      min = min.left;
    }
    return min;
  }

  public Key max() {
    Node max = max(root);
    return max != null ? max.key : null;
  }

  private Node max(Node node) {
    Node max = node;
    while (max != null && max.right != null) {
      max = max.right;
    }
    return max;
  }

  @Override
  public Key floor(Key key) {
    Node floor = floor(root, key);
    return floor != null ? floor.key : null;
  }

  private Node floor(Node node, Key key) {
    if (node == null) {
      return null;
    }
    int cmp = key.compareTo(node.key);
    if (cmp == 0) {
      return node;
    } else if (cmp < 0) {
      return floor(node.left, key);
    } else {
      Node smaller = floor(node.right, key);
      if (smaller != null) {
        return smaller;
      } else {
        return node;
      }
    }
  }

  @Override
  public Key ceiling(Key key) {
    Node ceiling = ceiling(root, key);
    return ceiling != null ? ceiling.key : null;
  }

  private Node ceiling(Node node, Key key) {
    if (node == null) {
      return null;
    }
    int cmp = node.key.compareTo(key);
    if (cmp == 0) {
      return node;
    } else if (cmp < 0) {
      return ceiling(node.right, key);
    }
    return node;
  }

  @Override
  public int rank(Key key) {
    return rank(root, key);
  }

  private int rank(Node node, Key key) {
    if (node == null) {
      return 0;
    }
    int cmp = key.compareTo(node.key);
    if (cmp == 0) {
      return size(node.left);
    } else if (cmp < 0) {
      return rank(node.left, key);
    } else {
      return 1 + size(node.left) + rank(node.right, key);
    }
  }

  @Override
  public Key select(int rank) {
    Node n = select(root, rank);
    return n != null ? n.key : null;
  }

  private Node select(Node node, int rank) {
    if (node == null) return null;

    int sizeLeft = size(node.left);
    if (sizeLeft > rank) {
      return select(node.left, rank);
    } else if (sizeLeft < rank) {
      return select(node.right, rank - sizeLeft - 1);
    } else {
      return node;
    }
  }

  public void deleteMin() {
    root = deleteMin(root);
  }

  private Node deleteMin(Node node) {
    if (node == null) return null;
    if (node.left == null) {
      return node.right;
    }
    node.left = deleteMin(node.left);
    node.size = size(node.left) + size(node.right) + 1;
    return node;
  }

  public void deleteMax() {
    root = deleteMax(root);
  }

  private Node deleteMax(Node node) {
    if (node == null) return null;
    if (node.right == null) {
      return node.left;
    }
    node.right = deleteMax(node.right);
    node.size = size(node.left) + size(node.right) + 1;
    return node;
  }

  private Node delete(Node node, Key key) {
    if (node == null) return null;
    int cmp = key.compareTo(node.key);
    if (cmp < 0) {
      node.left = delete(node.left, key);
    } else if (cmp > 0) {
      node.right = delete(node.right, key);
    } else {
      if (node.left == null) return node.right;
      if (node.right == null) return node.left;
      // we might want to use predecessor
      Node successor = min(node.right); // cannot be null due to previous checks
      successor.left = node.left;
      successor.right = deleteMin(node.right);
      return successor;
    }
    node.size = size(node.left) + size(node.right) + 1;
    return node;
  }

  public int height() {
    return height(root);
  }

  public int height(Node node) {
    if (node == null) return -1;
    return 1 + Math.max(height(node.left), height(node.right));
  }

  public void printInOrder() {
    printInOrder(root);
  }

  private void printInOrder(Node n) {
    if (n == null) return;
    printInOrder(n.left);
    System.out.println(n.key);
    printInOrder(n.right);
  }

  public void printLevelOrder() {
    System.out.println("Level-order traversal");
    System.out.println(strLevelOrder(root));
    System.out.println("=====================");
  }

  public String strLevelOrder(Node root) {
    Queue<Node> nodes = new LinkedList<>();
    nodes.add(root);
    StringBuilder sb = new StringBuilder();

    // process level by level
    while (!nodes.isEmpty()) {
      int  nodesOnLevel = nodes.size();
      for (int i = 0; i < nodesOnLevel; ++i) {
        Node n = nodes.poll();
        if (n != null) {
          sb.append(n.key);
          nodes.add(n.left);
          nodes.add(n.right);
        } else {
          sb.append("null");
        }
      }
      sb.append("\n");
    }
    return sb.toString();
  }

  @Override
  public Iterable<Key> keys() {
    return keys(min(), max());
  }

  private Iterable<Key> keys(Key lo, Key hi) {
    LinkedList<Key> q = new LinkedList<>();
    keys(root, lo, hi, q);
    return q;
  }

  private void keys(Node node, Key lo, Key hi, Queue<Key> q) {
    if (node == null) return;
    int cmpLo = lo.compareTo(node.key);
    int cmpHi = hi.compareTo(node.key);
    if (cmpLo < 0) keys(node.left, lo, hi, q);
    if (cmpLo <= 0 && cmpHi >= 0) q.add(node.key);
    if (cmpHi > 0) keys(node.right, lo, hi, q);
  }

  public Iterable<Key> preOrderedKeys() {
    return PreOrderIt::new;
  }

  /**
   * PreOrder depth-first tree traversal iterator implementation.
   * Visit: root -> left -> right
   */
  private class PreOrderIt implements Iterator<Key> {
    private Deque<Node> stack;

    PreOrderIt() {
      this.stack = new ArrayDeque<>();
      stack.push(root);
    }

    @Override
    public boolean hasNext() {
      return stack.peek() != null;
    }

    @Override
    public Key next() {
      Node curr = stack.peek();
      if (curr.left != null) {
        stack.push(curr.left);
      } else {
        Node r = stack.pop().right;
        while (r == null && !stack.isEmpty()) {
          r = stack.pop().right;
        }
        if (r != null) {
          stack.push(r);
        }
      }
      return curr.key;
    }
  }

  public Iterable<Key> orderedKeys() {
    return InOrderIt::new;
  }

  private class InOrderIt implements Iterator<Key> {
    private Deque<Node> stack;
    private Node curr;

    InOrderIt() {
      this.stack = new ArrayDeque<>();
      stack.push(root);
    }

    @Override
    public boolean hasNext() {
      return stack.peek() != null || curr != null;
    }

    @Override
    public Key next() {
      if (curr == null) { // tree was not traversed to the left (to smallest)
        curr = stack.pop();
        // move to the left most (smallest) key
        while (curr.left != null) {
          stack.push(curr);
          curr = curr.left;
        }
      }
      Key currKey = curr.key;
      if (curr.right != null) {
        stack.push(curr.right);
        curr = null;
      } else if (!stack.isEmpty()) {
        curr = stack.pop();
      } else {
        curr = null;
      }
      return currKey;
    }

  }

  @Override
  public String toString() {
    return "BST{" + "level-order tree:\n" + strLevelOrder(root) + "\n}";
  }

  public static void main(String[] args) {
    BinarySearchTree<Integer, String> bst = new BinarySearchTree<>();
    bst.put(6, "6");
    bst.put(8, "8");
    bst.put(3, "3");
    bst.put(1, "1");
    bst.put(2, "2");
    bst.put(7, "7");
    bst.put(4, "4");
    System.out.println("Height = " + bst.height());
    bst.printLevelOrder();
    bst.deleteMin();
    bst.deleteMax();
    bst.delete(4);
    System.out.println("in order");
    bst.printInOrder();
    System.out.println("range print");
    for (Integer key : bst.keys()) {
      System.out.println(key);
    }
  }

}
