package r.yelianevich.datastruct.map;

import java.util.Arrays;

/**
 * Symbol table that is implemented as sorted array list that uses binary search to increase lookup performance.
 *
 * Implementation is still is far from optimal to solve large problems, like the one in 'algs4-data/leipzig1M.txt'
 */
public class BinarySearchST<Key extends Comparable<Key>, Value> implements SymbolTableApi<Key, Value> {

  public static int DEFAULT_CAPACITY = 16;

  private Key[] keys;
  private Value[] values;

  private int capacity;
  private int size;

  public BinarySearchST() {
    this(DEFAULT_CAPACITY);
  }

  @SuppressWarnings("unchecked")
  public BinarySearchST(int capacity) {
    this.capacity = capacity;
    this.keys = (Key[]) new Comparable[capacity];
    this.values = (Value[]) new Object[capacity];
  }

  public int size() {
    return size;
  }

  int capacity() {
    return capacity;
  }

  public Value get(Key key) {
    if (isEmpty()) return null;
    int rank = rank(check(key));
    if (rank < size && keys[rank].equals(key)) {
      return values[rank];
    }
    return null;
  }

  public Value put(Key key, Value value) {
    check(key);

    int rank = rank(key);
    if (rank < size && key.equals(keys[rank])) {
      Value prevValue = values[rank];
      values[rank] = value;
      return prevValue;
    }
    if (size == capacity) {
      // grow 50%
      resize(capacity + (capacity >> 1));
    }

    // improves performance 3 times comparing to the loop
    if (size > rank) {
      System.arraycopy(keys, rank, keys, rank + 1, size - rank);
      System.arraycopy(values, rank, values, rank + 1, size - rank);
    }
    /*for (int i = size; i > rank; --i) {
      keys[i] = keys[i - 1];
      values[i] = values[i - 1];
    }*/

    keys[rank] = key;
    values[rank] = value;
    ++size;
    return null;
  }

  public Key check(Key key) {
    if (key == null) {
      throw new IllegalArgumentException("null key is not allowed");
    }
    return key;
  }

  @SuppressWarnings("unchecked")
  private void resize(int newCapacity) {
    Key[] newKeys = (Key[]) new Comparable[newCapacity];
    Value[] newValues = (Value[]) new Object[newCapacity];
    System.arraycopy(keys, 0, newKeys, 0, size);
    System.arraycopy(values, 0, newValues, 0, size);
    keys = newKeys;
    values = newValues;
    capacity = newCapacity;
  }

  private int rank(Key key) {
    // iterative binary search
    int lo = 0;
    int hi = size - 1;
    while (lo <= hi) {
      int mid = lo + (hi - lo) / 2;
      int cmp = key.compareTo(keys[mid]);
      if (cmp < 0) hi = mid - 1;
      else if (cmp > 0) lo = mid + 1;
      else return mid;
    }
    return lo;
  }

  int rank(Key key, int lo, int hi) {
    // recursive binary search
    if (hi < lo) {
      return lo;
    }
    int mid = lo + (hi - lo) / 2;
    int cmp = key.compareTo(keys[mid]);
    if (cmp < 0) {
      return rank(key, lo, mid - 1);
    } else if (cmp > 0) {
      return rank(key, mid + 1, hi);
    } else {
      return mid;
    }
  }

  public Value delete(Key key) {
    check(key);
    int rank = rank(check(key));
    if (rank < size && keys[rank].equals(key)) {
      Value prevValue = values[rank];

      System.arraycopy(keys, rank + 1, keys, rank, size - rank - 1);
      System.arraycopy(values, rank + 1, values, rank, size - rank - 1);
      /*for (int i = rank; i < size - 1; ++i) {
        keys[i] = keys[i + 1];
        values[i] = values[i + 1];
      }*/
      // free up for GC
      keys[size - 1] = null;
      values[size - 1] = null;
      --size;
      return prevValue;
    }
    return null;
  }

  @Override
  public Iterable<Key> keys() {
    Key[] toKeys = (Key[]) new Comparable[size];
    System.arraycopy(keys, 0, toKeys, 0, size);
    return Arrays.asList(toKeys);
  }

  public static void main(String[] args) {

  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("BinarySearchST{");
    sb.append("keys=").append(Arrays.toString(keys));
    sb.append(", values=").append(Arrays.toString(values));
    sb.append(", capacity=").append(capacity);
    sb.append(", size=").append(size);
    sb.append('}');
    return sb.toString();
  }
}
