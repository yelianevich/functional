package r.yelianevich.datastruct.map;

import java.util.Iterator;

public class SequentialSearchST<Key, Value> implements SymbolTableApi<Key, Value> {

  private Node head = null;

  private class Node {
    Key key;
    Value value;
    Node next;
    public Node(Key key, Value value, Node next) {
      this.key = key;
      this.value = value;
      this.next = next;
    }
  }

  public Value put(Key key, Value value) {
    check(key);
    for (Node curr = head; curr != null; curr = curr.next) {
      if (key.equals(curr.key)) {
        // node with a key exists
        Value replacedValue = curr.value;
        curr.value = value;
        return replacedValue;
      }
    }
    // prepend list with a new node if key is new
    head = new Node(key, value, head);
    return null;
  }

  private Key check(Key key) {
    if (key == null) throw new IllegalArgumentException("key cannot be null");
    return key;
  }

  public Value get(Key key) {
    check(key);
    for (Node curr = head; curr != null; curr = curr.next) {
      if (key.equals(curr.key)) {
        return curr.value;
      }
    }
    return null;
  }

  public boolean contains(Key key) {
    return get(key) != null;
  }

  public int size() {
    int size = 0;
    for (Node curr = head; curr != null; curr = curr.next) {
      ++size;
    }
    return size;
  }

  public Value delete(Key key) {
    check(key);

    if (head != null && head.key.equals(key)) {
      Value delValue = head.value;
      head = head.next;
      return delValue;
    }

    for (Node curr = head; curr != null; curr = curr.next) {
      Node next = curr.next;
      if (next != null && next.key.equals(key)) {
        curr.next = next.next;
        return next.value;
      }
    }
    return null;
  }

  public Iterable<Key> keys() {
    return KeyIterator::new;
  }

  private class KeyIterator implements Iterator<Key> {
    private Node current = head;

    @Override
    public boolean hasNext() {
      return current != null;
    }

    @Override
    public Key next() {
      Key val = current.key;
      current = current.next;
      return val;
    }
  }

}
