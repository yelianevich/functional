package r.yelianevich.datastruct.map;

public interface SymbolTableApi<Key, Value> {

  Value put(Key key, Value value);

  Value get(Key key);

  default boolean contains(Key key) {
    return get(key) != null;
  }

  int size();

  default boolean isEmpty() {
    return size() == 0;
  }

  Value delete(Key key);

  Iterable<Key> keys();

}
