package r.yelianevich.datastruct.unionfind;

public class QuickUnionUF implements UF {
  private final int[] parent;
  private int count;

  public QuickUnionUF(int n) {
    this.count = n;
    this.parent = new int[n];
    for (int i = 0; i < n; ++i) {
      parent[i] = i; // make every node a root
    }
  }

  @Override
  public void union(int p, int q) {
    int rootP = find(p);
    int rootQ = find(q);
    if (rootP != rootQ) {
      parent[rootP] = rootQ;
      count--;
    }
  }

  @Override
  public int find(int child) {
    while (child != parent[child]) {
      child = parent[child];
    }
    return child;
  }

  @Override
  public int count() {
    return count;
  }

}
