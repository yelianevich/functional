package r.yelianevich.datastruct.unionfind;

/**
 * Union-Find or Disjoint Set data structure
 */
public interface UF {

  /**
   * Unions vertices p and q
   */
  void union(int p, int q);

  /**
   * Finds the root node of the given vertex
   */
  int find(int p);

  /**
   * Checks if two vertices are connected
   */
  default boolean connected(int p, int q) {
    return find(p) == find(q);
  }

  /**
   * Returns the number of components.
   *
   * @return the number of components (between {@code 1} and {@code n})
   */
  int count();

}
