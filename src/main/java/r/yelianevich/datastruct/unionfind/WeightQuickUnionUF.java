package r.yelianevich.datastruct.unionfind;

/**
 * Weighted quick union UF data structure with path compression (see {@code find} method)
 */
public class WeightQuickUnionUF implements UF {
  private final int[] parent;
  private int count;
  private final int[] sz;

  public WeightQuickUnionUF(int n) {
    this.count = n;
    this.parent = new int[n];
    this.sz = new int[n];
    for (int i = 0; i < n; ++i) {
      parent[i] = i; // make every node a root for itself
      sz[i] = 1;
    }
  }

  @Override
  public void union(int p, int q) {
    int pRoot = find(p);
    int qRoot = find(q);
    if (pRoot != qRoot) {
      if (sz[pRoot] > sz[qRoot]) {
        parent[qRoot] = pRoot;
        sz[pRoot] += sz[qRoot];
      } else {
        parent[pRoot] = qRoot;
        sz[qRoot] += sz[pRoot];
      }
      count--;
    }
  }

  @Override
  public int find(int p) {
    while (p != parent[p]) {
      parent[p] = parent[parent[p]];
      p = parent[p];
    }
    return p;
  }

  @Override
  public int count() {
    return count;
  }

}
