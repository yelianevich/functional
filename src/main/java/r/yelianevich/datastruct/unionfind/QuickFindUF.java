package r.yelianevich.datastruct.unionfind;

public class QuickFindUF implements UF {
  private final int[] root;
  private int count; // count of connected components

  public QuickFindUF(int n) {
    count = n;
    root = new int[n];
    for (int i = 0; i < n; ++i) {
      root[i] = i;
    }
  }

  @Override
  public void union(int p, int q) {
    int pId = root[p];
    int qId = root[q];
    if (pId == qId) {
      return;
    }

    for (int i = 0; i < root.length; ++i) {
      if (root[i] == pId) {
        root[i] = qId;
      }
    }
    count--;
  }

  @Override
  public int find(int p) {
    return root[p];
  }

  @Override
  public int count() {
    return count;
  }

}
