package r.yelianevich.datastruct.unionfind;

import java.util.Arrays;

public class QuickUnionByRankUF implements UF {

    private final int[] parent;
    private final int[] rank;
    private int count;

    public QuickUnionByRankUF(int size) {
        this.parent = new int[size];
        for (int i = 0; i < parent.length; i++) {
            parent[i] = i;
        }
        this.rank = new int[size];
        Arrays.fill(rank, 1);
        this.count = size;
    }

    @Override
    public void union(int p, int q) {
        int rootP = find(p);
        int rootQ = find(q);
        if (rootP == rootQ) {
            return;
        }

        if (rank[rootP] > rank[rootQ]) {
            parent[rootQ] = rootP;
        } else if (rank[rootP] < rank[rootQ]) {
            parent[rootP] = rootQ;
        } else {
            parent[rootP] = rootQ;
            rank[rootQ]++;
        }
        count--;
    }

    @Override
    public int find(int p) {
        if (p == parent[p]) {
            return p;
        }
        // using path compression technique - connecting directly to the root during find
        return parent[p] = find(parent[p]);
    }

    @Override
    public int count() {
        return count;
    }
}
