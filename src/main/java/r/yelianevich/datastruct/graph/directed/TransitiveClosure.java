package r.yelianevich.datastruct.graph.directed;

/**
 * Solves 'all-pairs reachability' problem.
 * <p>
 * Given a digraph, support queries of the form:
 * Is there a directed path from a given vertex v to another given vertex w?
 * <p>
 * This solution is ideal for small or dense digraphs, but it is not a solution for the large digraphs we might
 * encounter in practice because the constructor uses space proportional to V^2 and time proportional to V (V+E).
 * <p>
 * No solution that is better than this yet.
 */
public class TransitiveClosure {
  private final DirectedDfs[] dfs;

  public TransitiveClosure(Digraph digraph) {
    this.dfs = new DirectedDfs[digraph.V()];
    for (int s = 0; s < digraph.V(); ++s) {
      dfs[s] = new DirectedDfs(digraph, s);
    }
  }

  boolean reachable(int v, int w) {
    return dfs[v].marked(w);
  }
}
