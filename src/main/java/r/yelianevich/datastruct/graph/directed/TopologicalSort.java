package r.yelianevich.datastruct.graph.directed;

import java.util.Collections;

/**
 * Depth-first topological sort.
 * <p>
 * Topological sort.
 * Given a digraph, put the vertices in order such that all its directed
 * edges point from a vertex earlier in the order to a vertex later
 * in the order (or report that doing so is not possible).
 */
public class TopologicalSort {

  private final Iterable<Integer> topologicalOrder;

  public TopologicalSort(Digraph digraph) {
    DirectedCycle cycleDetector = new DirectedCycle(digraph);
    if (!cycleDetector.hasCycle()) {
      topologicalOrder = new DepthFirstOrder(digraph).postReverse();
    } else {
      topologicalOrder = Collections.emptyList();
    }
  }

  public Iterable<Integer> getTopologicalOrder() {
    return topologicalOrder;
  }

  public boolean isDag() {
    return topologicalOrder.iterator().hasNext();
  }
}
