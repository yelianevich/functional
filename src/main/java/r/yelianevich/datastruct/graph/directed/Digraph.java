package r.yelianevich.datastruct.graph.directed;

import edu.princeton.cs.algs4.In;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Directed graph data type.
 * A directed graph (or digraph) is a set of vertices and a collection of directed edges.
 * Each directed edge connects an ordered pair of vertices.
 */
public class Digraph {

  private final int V;
  private int E;
  private List<Integer>[] adj;

  @SuppressWarnings("unchecked")
  public Digraph(int V) {
    this.V = V;
    this.E = 0;
    this.adj = (List<Integer>[]) new ArrayList[V];
    for (int i = 0; i < V; i++) {
      adj[i] = new ArrayList<>();
    }
  }

  public Digraph(In in) {
    this(Objects.requireNonNull(in).readInt());
    int edges = in.readInt();
    for (int i = 0; i < edges; ++i) {
      int v = in.readInt();
      int w = in.readInt();
      addEdge(v, w);
    }
  }

  public void addEdge(int v, int w) {
    validateVertex(v);
    validateVertex(w);
    adj[v].add(w); // the only difference between directed and undirected graphs - here edge is one-way
    E++;
  }

  private void validateVertex(int v) {
    if (v < 0 || v >= V) {
      throw new IllegalArgumentException("Vertex index " + v + " is not in range between 0 and " + (V - 1));
    }
  }

  public int V() {
    return V;
  }

  public int E() {
    return E;
  }

  public Iterable<Integer> adj(int v) {
    return adj[v];
  }

  public Digraph reverse() {
    Digraph r = new Digraph(V);
    for (int v = 0; v < V; ++v) {
      for (Integer w : adj(v)) {
        r.addEdge(w, v);
      }
    }
    return r;
  }

  public String toString() {
    String newLine = System.lineSeparator();
    StringBuilder s = new StringBuilder();
    s.append(V).append(" vertices, ").append(E).append(" edges ").append(newLine);
    for (int v = 0; v < V; v++) {
      s.append(v).append(": ");
      for (int w : adj[v]) {
        s.append(w).append(" ");
      }
      s.append(newLine);
    }
    return s.toString();
  }
}
