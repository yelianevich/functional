package r.yelianevich.datastruct.graph.directed;

import java.util.ArrayDeque;
import java.util.Deque;

public class DirectedCycle {

  private Deque<Integer> cycle;
  private final boolean[] marked;
  private final boolean[] onStack;
  private final int[] edgeTo;

  public DirectedCycle(Digraph digraph) {
    this.marked = new boolean[digraph.V()];
    this.onStack = new boolean[digraph.V()];
    this.edgeTo = new int[digraph.V()];
    for (int s = 0; s < digraph.V(); ++s) {
      if (hasCycle()) break;
      if (!marked[s]) {
        dfs(digraph, s);
      }
    }
  }

  private void dfs(Digraph digraph, int v) {
    marked[v] = true;
    onStack[v] = true;
    for (Integer w : digraph.adj(v)) {
      if (hasCycle()) return;

      if (!marked[w]) {
        edgeTo[w] = v;
        dfs(digraph, w);
      } else if (onStack[w]) {
        // visited for the second time during current source DFS 
        cycle = new ArrayDeque<>();
        for (int i = v; i != w; i = edgeTo[i]) {
          cycle.push(i);
        }
        cycle.push(w);
        cycle.push(v);
      }
    }
    onStack[v] = false;
  }

  public boolean hasCycle() {
    return cycle != null;
  }

  public Iterable<Integer> cycle() {
    return cycle;
  }
}
