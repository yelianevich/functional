package r.yelianevich.datastruct.graph.directed;

public class DirectedDfs {

  private boolean[] marked;

  public DirectedDfs(Digraph digraph, int s) {
    this.marked = new boolean[digraph.V()];
    dfs(digraph, s);
  }

  private void dfs(Digraph digraph, int v) {
    marked[v] = true;
    for (Integer w : digraph.adj(v)) {
      if (!marked[w]) {
        dfs(digraph, w);
      }
    }
  }

  public boolean marked(int v) {
    return marked[v];
  }
}
