package r.yelianevich.datastruct.graph.directed;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * 4.2.39 Queue-based topological sort for DAGs
 */
public class TopologicalSortQueue {

  private final Queue<Integer> topological = new ArrayDeque<>();

  public TopologicalSortQueue(Digraph digraph) {
    // The indegree of a vertex in a digraph is the number of directed edges that point to that vertex
    int[] indegree = new int[digraph.V()];
    for (int v = 0; v < digraph.V(); ++v) {
      for (Integer w : digraph.adj(v)) {
        indegree[w] += 1;
      }
    }

    // sources that do not depend on the others
    Queue<Integer> sources = new ArrayDeque<>();
    for (int v = 0; v < digraph.V(); ++v) {
      if (indegree[v] == 0) {
        sources.offer(v);
      }
    }

    while (!sources.isEmpty()) {
      int s = sources.poll();
      topological.offer(s);
      for (int v : digraph.adj(s)) {
        indegree[v] -= 1;
        if (indegree[v] == 0) {
          sources.offer(v);
        }
      }
    }
  }

  public Iterable<Integer> getTopologicalOrder() {
    return topological;
  }
}
