package r.yelianevich.datastruct.graph.directed;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Queue;

public class DepthFirstOrder {

  private final boolean[] marked;

  private final Queue<Integer> pre = new ArrayDeque<>();
  private final Queue<Integer> post = new ArrayDeque<>();
  private final Deque<Integer> postReverse = new ArrayDeque<>(); // stack

  public DepthFirstOrder(Digraph digraph) {
    this.marked = new boolean[digraph.V()];
    for (int s = 0; s < digraph.V(); ++s) {
      if (!marked[s]) {
        dfs(digraph, s);
      }
    }
  }

  private void dfs(Digraph digraph, int v) {
    marked[v] = true;
    pre.offer(v);
    for (Integer w : digraph.adj(v)) {
      if (!marked[w]) {
        dfs(digraph, w);
      }
    }
    post.offer(v);
    postReverse.push(v);
  }

  public Queue<Integer> pre() {
    return pre;
  }

  public Queue<Integer> post() {
    return post;
  }

  public Deque<Integer> postReverse() {
    return postReverse;
  }
}
