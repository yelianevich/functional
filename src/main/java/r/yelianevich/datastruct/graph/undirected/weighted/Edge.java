package r.yelianevich.datastruct.graph.undirected.weighted;

import java.util.Objects;

/**
 * One of the ways to represent the edge in a undirected edge-weighted graph.
 * <p>
 * Pros:
 * - it allows elegant client code
 * - only a single instance of the object is present in adjacency-lists
 * Cons:
 * - object overhead
 * - 2 links are kept in adjacency-lists
 * - redundant information: v in v's adjacency list
 */
public class Edge implements Comparable<Edge> {

  private final int v;
  private final int w;
  private final double weight;

  public Edge(int v, int w, double weight) {
    this.v = v;
    this.w = w;
    this.weight = weight;
  }

  public int either() {
    return v;
  }

  public double weight() {
    return weight;
  }

  public int other(int vertex) {
    if (v == vertex) return w;
    else if (w == vertex) return v;
    else throw new IllegalArgumentException("Edge is not connected to " + vertex);
  }

  @Override
  public int compareTo(Edge that) {
    return Double.compare(this.weight, that.weight);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Edge edge = (Edge) o;
    return v == edge.v &&
        w == edge.w &&
        Double.compare(edge.weight, weight) == 0;
  }

  @Override
  public int hashCode() {
    return Objects.hash(v, w, weight);
  }

  @Override
  public String toString() {
    return String.format("Edge{v=%d, w=%d, weight=%s}", v, w, weight);
  }
}
