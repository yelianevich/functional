package r.yelianevich.datastruct.graph.undirected;

import java.util.ArrayDeque;
import java.util.Deque;

public class Cycle {

  private final boolean[] marked;
  private Deque<Integer> cycle;
  private final int[] edgeTo;

  public Cycle(Graph graph) {
    this.marked = new boolean[graph.V()];
    this.edgeTo = new int[graph.V()];
    for (int s = 0; s < graph.V(); ++s) {
      if (hasCycle()) break;
      if (!marked[s]) {
        dfs(graph, -1, s);
      }
    }
  }

  private void dfs(Graph graph, int source, int v) {
    marked[v] = true;
    for (Integer w : graph.adj(v)) {
      if (hasCycle()) return;
      if (!marked[w]) {
        edgeTo[w] = v;
        dfs(graph, v, w);
      } else if (source != w) { // a single change in a standard DFS
        // if it's not a link to the node we came from then cycle is detected
        cycle = new ArrayDeque<>();
        for (int i = v; i != w; i = edgeTo[i]) {
          cycle.push(i);
        }
        cycle.push(w);
        cycle.push(v);
      }
    }
  }

  public boolean hasCycle() {
    return cycle != null;
  }

  public Iterable<Integer> cycle() {
    return cycle;
  }
}
