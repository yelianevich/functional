package r.yelianevich.datastruct.graph.undirected.weighted;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

/**
 * Class to build minimum spanning tree (forest) out of connected undirected edge-weighted graph.
 * <p>
 * Lazy implementation of Prim's algorithm
 * Runtime O(E * logE) - remove an element from PQ is ~logE in the worst case
 * Memory: O(E) - proportional to E
 */
public class LazyPrimMst {

  private final List<Edge> mst = new ArrayList<>();
  private double mstWeight = 0;

  private boolean[] marked;
  private PriorityQueue<Edge> minPq;

  public LazyPrimMst(EdgeWeightedGraph graph) {
    marked = new boolean[graph.V()];
    minPq = new PriorityQueue<>();

    // run for all vertices to get minimum spanning forest
    for (int s = 0; s < graph.V(); ++s) {
      if (!marked[s]) prim(graph, s);
    }
  }

  private void prim(EdgeWeightedGraph graph, int s) {
    visit(graph, s);
    while (!minPq.isEmpty()) {
      Edge e = minPq.poll();
      int v = e.either();
      int w = e.other(v);
      if (marked[v] && marked[w]) continue;
      mst.add(e);
      mstWeight += e.weight();
      if (!marked[v]) visit(graph, v);
      if (!marked[w]) visit(graph, w);
    }
  }

  private void visit(EdgeWeightedGraph graph, int v) {
    marked[v] = true;
    for (Edge e : graph.adj(v)) {
      if (!marked[e.other(v)]) {
        minPq.offer(e);
      }
    }
  }

  public Iterable<Edge> edges() {
    return mst;
  }

  public double weight() {
    return mstWeight;
  }
}
