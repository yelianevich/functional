package r.yelianevich.datastruct.graph.undirected.weighted;

import r.yelianevich.datastruct.unionfind.UF;
import r.yelianevich.datastruct.unionfind.WeightQuickUnionUF;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

/**
 * Kruskal's algorithm to build minimum-spanning tree on the undirected edge-weighted graph.
 * If the graph is not connected it builds minimum-spanning forest.
 * <p>
 * Kruskal's algorithm constructs a tree by adding an edge of a minimum weight of an entire graph whilst
 * Prim's algorithm considers only edges adjacent to a current MST.
 * <p>
 * Time: O(E * logE)
 * Memory: O(E)
 */
public class KruskalMst {

  private double weight;
  private List<Edge> mst = new ArrayList<>();

  public KruskalMst(EdgeWeightedGraph graph) {
    UF uf = new WeightQuickUnionUF(graph.V());
    PriorityQueue<Edge> minPq = new PriorityQueue<>();
    for (Edge e : graph.edges()) {
      minPq.offer(e);
    }
    while (!minPq.isEmpty() && mst.size() != graph.V() - 1) {
      Edge e = minPq.remove();
      int v = e.either();
      int w = e.other(v);
      if (!uf.connected(v, w)) {
        uf.union(v, w);
        mst.add(e);
        weight += e.weight();
      }
    }
  }

  public Iterable<Edge> edges() {
    return mst;
  }

  public double weight() {
    return weight;
  }
}
