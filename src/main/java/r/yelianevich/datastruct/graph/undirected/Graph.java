package r.yelianevich.datastruct.graph.undirected;

import edu.princeton.cs.algs4.In;

import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Objects;

/**
 * Adjacency-list undirected graph representation.
 * Each edge is present in both vertices adjacency lists.
 */
public class Graph {

  private final int V;
  private int E;
  private ArrayList<Integer>[] adj;

  /**
   * Initializes empty graph with V vertices
   *
   * @param V number of vertices in the graph
   */
  public Graph(int V) {
    this.V = V;
    this.E = 0;
    //noinspection unchecked
    this.adj = (ArrayList<Integer>[]) new ArrayList[V];
    for (int i = 0; i < V; ++i) {
      adj[i] = new ArrayList<>();
    }
  }

  /**
   * Initializes a graph from the specified input stream.
   * The format is the number of vertices <em>V</em>,
   * followed by the number of edges <em>E</em>,
   * followed by <em>E</em> pairs of vertices, with each entry separated by whitespace.
   */
  public Graph(In in) {
    this(Objects.requireNonNull(in).readInt());
    try {
      int E = in.readInt();
      for (int i = 0; i < E; ++i) {
        int v = in.readInt();
        int w = in.readInt();
        addEdge(v, w); // increments number of edges
      }
    } catch (NoSuchElementException e) {
      throw new IllegalArgumentException("invalid input format in Graph constructor", e);
    }
  }

  private void validateVertex(int v) {
    if (v < 0 || v >= V) {
      throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V - 1));
    }
  }

  public void addEdge(int v, int w) {
    validateVertex(v);
    validateVertex(w);
    adj[v].add(w);
    adj[w].add(v);
    E++;
  }

  public int E() {
    return E;
  }

  public int V() {
    return V;
  }

  public Iterable<Integer> adj(int v) {
    return adj[v];
  }

  @Override
  public String toString() {
    String newLine = System.lineSeparator();
    StringBuilder s = new StringBuilder();
    s.append(V).append(" vertices, ").append(E).append(" edges ").append(newLine);
    for (int v = 0; v < V; v++) {
      s.append(v).append(": ");
      for (int w : adj[v]) {
        s.append(w).append(" ");
      }
      s.append(newLine);
    }
    return s.toString();
  }
}
