package r.yelianevich.datastruct.graph.undirected;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Deque;

public class DepthFirstPaths {

  private final int[] edgeTo;
  private final boolean[] marked;
  private final int source;

  public DepthFirstPaths(Graph graph, int source) {
    this.edgeTo = new int[graph.V()];
    this.marked = new boolean[graph.V()];
    this.source = source;
    dfs(graph, source);
  }

  private void dfs(Graph graph, int v) {
    marked[v] = true;
    for (int w : graph.adj(v)) {
      if (!marked[w]) {
        edgeTo[w] = v;
        dfs(graph, w);
      }
    }
  }

  public Iterable<Integer> pathTo(int v) {
    if (!hasPathTo(v)) {
      return Collections.emptyList();
    }
    Deque<Integer> path = new ArrayDeque<>();
    for (int i = v; i != source; i = edgeTo[i]) {
      path.push(i);
    }
    path.push(source);
    return path;
  }

  public boolean hasPathTo(int v) {
    return marked[v];
  }
}
