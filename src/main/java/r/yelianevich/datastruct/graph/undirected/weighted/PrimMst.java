package r.yelianevich.datastruct.graph.undirected.weighted;

import edu.princeton.cs.algs4.IndexMinPQ;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Eager version of Prim's algorithm for finding minimum spanning tree for a undirected edge-weighted graph.
 * <p>
 * Time: O(E * logV) (worth case)
 * Space: O(V)
 */
public class PrimMst {

  private final List<Edge> mst;
  private final double weight;

  private boolean[] marked; // to check if on MST
  private Edge[] edgeTo; // shortest edge from a tree vertex
  private double[] distTo; // distTo[W] = edgeTo[w].weight(); remove??
  private IndexMinPQ<Double> indexMinPq; // eligible crossing edges

  public PrimMst(EdgeWeightedGraph graph) {
    this.marked = new boolean[graph.V()];
    this.edgeTo = new Edge[graph.V()];
    this.distTo = new double[graph.V()];
    this.indexMinPq = new IndexMinPQ<>(graph.V());
    Arrays.fill(distTo, Double.POSITIVE_INFINITY);

    // run for each edge to generate minimum spanning forest 
    for (int s = 0; s < graph.V(); ++s) {
      if (!marked[s]) {
        prim(graph, s);
      }
    }
    weight = Arrays.stream(distTo).sum();
    mst = new ArrayList<>(Arrays.asList(edgeTo).subList(1, edgeTo.length));
  }

  private void prim(EdgeWeightedGraph graph, int s) {
    indexMinPq.insert(s, 0.0);
    distTo[s] = 0.0;
    while (!indexMinPq.isEmpty()) {
      visit(graph, indexMinPq.delMin());
    }
  }

  private void visit(EdgeWeightedGraph graph, int v) {
    marked[v] = true;
    for (Edge e : graph.adj(v)) {
      int w = e.other(v);
      if (marked[w]) continue;
      if (e.weight() < distTo[w]) {
        distTo[w] = e.weight();
        edgeTo[w] = e;
        if (indexMinPq.contains(w)) {
          indexMinPq.changeKey(w, e.weight());
        } else {
          indexMinPq.insert(w, e.weight());
        }
      }
    }
  }

  public Iterable<Edge> edges() {
    return mst;
  }

  public double weight() {
    return weight;
  }
}
