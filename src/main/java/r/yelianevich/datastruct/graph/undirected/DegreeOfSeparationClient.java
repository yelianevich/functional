package r.yelianevich.datastruct.graph.undirected;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DegreeOfSeparationClient {

  private final BreadthFirstPaths bfs;
  private final SymbolGraph sg;

  public DegreeOfSeparationClient(SymbolGraph sg, String source) {
    if (!sg.contains(source)) {
      throw new IllegalArgumentException(source + " is not part of the graph");
    }
    this.sg = sg;

    // calculate shortest paths from source
    this.bfs = new BreadthFirstPaths(sg.graph(), sg.index(source));
  }

  public boolean connected(String sink) {
    return sg.contains(sink) && bfs.hasPathTo(sg.index(sink));
  }

  public Iterable<String> pathTo(String sink) {
    if (sg.contains(sink)) {
      List<String> path = new ArrayList<>();
      for (Integer step : bfs.pathTo(sg.index(sink))) {
        path.add(sg.name(step));
      }
      return path;
    } else {
      return Collections.emptyList();
    }
  }
}
