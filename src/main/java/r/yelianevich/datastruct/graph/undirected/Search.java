package r.yelianevich.datastruct.graph.undirected;

/**
 * Depth-first search (DFS): a classic recursive method for searching in a connected graph
 */
public class Search {

  private final boolean[] marked;
  private int count;

  public Search(Graph graph, int source) {
    this.marked = new boolean[graph.V()];
    dfs(graph, source);
  }

  private void dfs(Graph graph, int v) {
    marked[v] = true;
    count++;
    for (int w : graph.adj(v)) {
      if (!marked[w]) {
        dfs(graph, w);
      }
    }
  }

  public boolean marked(int v) {
    return marked[v];
  }

  public int count() {
    return count;
  }
}
