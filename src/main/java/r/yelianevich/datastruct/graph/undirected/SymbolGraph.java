package r.yelianevich.datastruct.graph.undirected;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.function.Supplier;

/**
 * Implementation of the graph when vertices are defined as strings rather than by an integer.
 * It builds internal index to map string name to an index in the graph.
 */
public class SymbolGraph {

  private final Map<String, Integer> index = new HashMap<>();
  private final String[] keys;
  private final Graph graph;

  public SymbolGraph(String filename, String delim) {
    Supplier<InputStream> fileSupplier = () -> new BufferedInputStream(Objects.requireNonNull(
        SymbolGraph.class.getClassLoader().getResourceAsStream(filename)));

    buildKeyToIndex(delim, fileSupplier);
    this.keys = new String[index.size()];
    buildIndexToKey();
    this.graph = new Graph(index.size());
    buildGraph(delim, fileSupplier);
  }

  private void buildKeyToIndex(String delim, Supplier<InputStream> fileRes) {
    try (Scanner scanner = new Scanner(fileRes.get())) {
      while (scanner.hasNextLine()) {
        String[] vertices = scanner.nextLine().split(delim);
        for (String vertex : vertices) {
          if (!index.containsKey(vertex)) {
            index.put(vertex, index.size());
          }
        }
      }
    }
  }

  private void buildIndexToKey() {
    for (Map.Entry<String, Integer> kv : index.entrySet()) {
      keys[kv.getValue()] = kv.getKey();
    }
  }

  private void buildGraph(String delim, Supplier<InputStream> fileRes) {
    try (Scanner scanner = new Scanner(fileRes.get())) {
      while (scanner.hasNextLine()) {
        String[] adj = scanner.nextLine().split(delim);
        int v = index.get(adj[0]);
        for (int w = 1; w < adj.length; ++w) {
          graph.addEdge(v, index.get(adj[w]));
        }
      }
    }
  }

  public boolean contains(String key) {
    return index.containsKey(key);
  }

  public int index(String key) {
    return index.get(key);
  }

  public String name(int v) {
    return keys[v];
  }

  public Graph graph() {
    return graph;
  }
}
