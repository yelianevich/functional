package r.yelianevich.datastruct.graph.undirected.weighted;

import edu.princeton.cs.algs4.In;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class EdgeWeightedGraph {

  private final int V;
  private int E;
  private final List<Edge>[] adj;

  @SuppressWarnings("unchecked")
  public EdgeWeightedGraph(int V) {
    this.V = V;
    this.E = 0;
    this.adj = (List<Edge>[]) new List[V];
    for (int v = 0; v < V; ++v) {
      adj[v] = new ArrayList<>();
    }
  }

  public EdgeWeightedGraph(In in) {
    this(Objects.requireNonNull(in).readInt());
    int edges = in.readInt();
    for (int i = 0; i < edges; ++i) {
      int v = in.readInt();
      int w = in.readInt();
      double weight = in.readDouble();
      addEdge(new Edge(v, w, weight));
    }
  }

  public void addEdge(Edge edge) {
    int v = edge.either();
    int w = edge.other(v);
    adj[v].add(edge);
    adj[w].add(edge);
    E++;
  }

  public Iterable<Edge> adj(int v) {
    return adj[v];
  }

  public Iterable<Edge> edges() {
    List<Edge> edges = new ArrayList<>();
    for (int v = 0; v < V; ++v) {
      for (Edge edge : adj[v]) {
        if (edge.other(v) > v) {
          edges.add(edge);
        }
      }
    }
    return edges;
  }

  public int V() {
    return V;
  }

  public int E() {
    return E;
  }

  @Override
  public String toString() {
    String newLine = System.lineSeparator();
    StringBuilder sb = new StringBuilder();
    sb.append(V).append(" vertices, ").append(E).append(" edges ").append(newLine);
    for (int v = 0; v < V; v++) {
      sb.append(v).append(": ");
      for (Edge e : adj[v]) {
        sb.append(e.other(v)).append(" ");
      }
      sb.append(newLine);
    }
    return sb.toString();
  }
}
