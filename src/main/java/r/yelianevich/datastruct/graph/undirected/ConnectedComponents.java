package r.yelianevich.datastruct.graph.undirected;

public class ConnectedComponents {

  private int count;
  private final int[] id;
  private final boolean[] marked;

  public ConnectedComponents(Graph graph) {
    this.id = new int[graph.V()];
    this.marked = new boolean[graph.V()];
    for (int s = 0; s < graph.V(); ++s) {
      if (!marked[s]) {
        dfs(graph, s);
        count++;
      }
    }
  }

  private void dfs(Graph graph, int v) {
    marked[v] = true;
    id[v] = count;
    for (int w : graph.adj(v)) {
      if (!marked[w]) {
        dfs(graph, w);
      }
    }
  }

  public boolean connected(int v, int w) {
    return id[v] == id[w];
  }

  public int count() {
    return count;
  }

  /**
   * v's connected component id
   *
   * @return id between [0, count()-1]
   */
  public int id(int v) {
    return id[v];
  }
}
