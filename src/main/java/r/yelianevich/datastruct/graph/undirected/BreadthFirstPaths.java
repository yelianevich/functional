package r.yelianevich.datastruct.graph.undirected;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Deque;
import java.util.Queue;

public class BreadthFirstPaths {

  private final int source;
  private final boolean[] marked;
  private final int[] edgeTo;

  public BreadthFirstPaths(Graph graph, int source) {
    this.source = source;
    this.marked = new boolean[graph.V()];
    this.edgeTo = new int[graph.V()];
    bfs(graph, source);
  }

  private void bfs(Graph graph, int source) {
    Queue<Integer> queue = new ArrayDeque<>();
    marked[source] = true; // all vertices on the queue should be marked first
    queue.offer(source);
    while (!queue.isEmpty()) {
      int v = queue.poll();
      for (Integer w : graph.adj(v)) {
        if (!marked[w]) {
          edgeTo[w] = v;
          marked[w] = true;
          queue.offer(w);
        }
      }
    }
  }

  public boolean hasPathTo(int v) {
    return marked[v];
  }

  public Iterable<Integer> pathTo(int v) {
    if (!hasPathTo(v)) {
      return Collections.emptyList();
    }
    Deque<Integer> path = new ArrayDeque<>();
    for (int i = v; i != source; i = edgeTo[i]) {
      path.push(i);
    }
    path.push(source);
    return path;
  }
}
