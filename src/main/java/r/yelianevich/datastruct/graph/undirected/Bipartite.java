package r.yelianevich.datastruct.graph.undirected;

/**
 * Is the given graph bipartite (two-colorable)?
 */
public class Bipartite {

  private final boolean[] marked;
  private final boolean[] color;
  private boolean isBipartite;

  public Bipartite(Graph graph) {
    this.marked = new boolean[graph.V()];
    this.color = new boolean[graph.V()];
    this.isBipartite = true;
    for (int s = 0; s < graph.V(); ++s) {
      if (!marked[s]) {
        dfs(graph, s);
      }
    }
  }

  private void dfs(Graph graph, int v) {
    marked[v] = true;
    for (int w : graph.adj(v)) {
      if (!marked[w]) {
        color[w] = !color[v];
        dfs(graph, w);
      } else if (isBipartite && color[v] == color[w]) {
        isBipartite = false;
      }
    }
  }

  public boolean isBipartite() {
    return isBipartite;
  }
}
