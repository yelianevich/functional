package r.yelianevich.datastruct.string;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * The {@code Trie} class represents a symbol table of key-value
 * pairs, with string keys and generic values.
 * <p>
 * The name “trie” is a bit of wordplay introduced by E. Fredkin in 1960
 * because the data structure is used for retrieval, but we pronounce it “try” to avoid confusion with “tree.”
 * </p>
 *
 * @param <Value> type of the value
 */
public class Trie<Value> implements StringSymbolTableApi<Value> {

  private static final int EXTENDED_ASCII_RADIX = 256;

  private static class Node {
    Object val;
    Node[] next;

    public Node(int radix) {
      this.next = new Node[radix];
    }
  }

  private final int radix;
  private Node root;

  public Trie() {
    this(EXTENDED_ASCII_RADIX);
  }

  public Trie(int radix) {
    this.radix = radix;
    this.root = new Node(radix);
  }

  @Override
  public Value put(String key, Value val) {
    root = put(root, key, val, 0);
    return null;
  }

  private Node put(Node root, String key, Value val, int depth) {
    if (root == null) {
      root = new Node(radix);
    }
    if (depth == key.length()) {
      root.val = val;
      return root;
    }
    char c = key.charAt(depth);
    root.next[c] = put(root.next[c], key, val, depth + 1);
    return root;
  }

  @Override
  @SuppressWarnings("unchecked")
  public Value get(String key) {
    Node node = get(root, key, 0);
    return node != null ? (Value) node.val : null;
  }

  private Node get(Node root, String key, int depth) {
    if (root == null) return null;
    if (depth == key.length()) return root;
    char c = key.charAt(depth);
    return get(root.next[c], key, depth + 1);
  }

  @Override
  public int size() {
    return size(root);
  }

  /**
   * Lazy implementation that calculates size of the trie on demand
   *
   * @return size of the {@code root}
   */
  private int size(Node root) {
    if (root == null) return 0;
    int count = 0;
    if (root.val != null) ++count;
    for (int i = 0; i < radix; ++i) {
      count += size(root.next[i]);
    }
    return count;
  }

  @Override
  public Value delete(String key) {
    root = delete(root, key, 0);
    return null;
  }

  private Node delete(Node root, String key, int depth) {
    if (root == null) return null;

    if (key.length() == depth) {
      root.val = null;
    } else {
      char c = key.charAt(depth);
      root.next[c] = delete(root.next[c], key, depth + 1);
    }

    // this node defines mapping - we can return 
    if (root.val != null) return root;

    // if node doesn't define a mapping, check that it has at least 1 child node
    for (char c = 0; c < radix; ++c) {
      if (root.next[c] != null) return root;
    }

    // delete if no children defined
    return null;
  }

  @Override
  public Iterable<String> keys() {
    return keysWithPrefix("");
  }

  @Override
  public Iterable<String> keysWithPrefix(String prefix) {
    Queue<String> keys = new ArrayDeque<>();
    Node prefixNode = get(root, prefix, 0);
    StringBuilder sb = new StringBuilder(prefix);
    collect(prefixNode, sb, keys);
    return keys;
  }

  private void collect(Node root, StringBuilder prefix, Queue<String> keys) {
    if (root == null) return;
    if (root.val != null) keys.offer(prefix.toString());
    for (char c = 0; c < radix; ++c) {
      int len = prefix.length();
      collect(root.next[c], prefix.append(c), keys);
      prefix.setLength(len);
    }
  }

  @Override
  public Iterable<String> keysThatMatch(String pattern) {
    Queue<String> keys = new ArrayDeque<>();
    StringBuilder prefix = new StringBuilder();
    collect(root, prefix, pattern, keys);
    return keys;
  }

  private void collect(Node root, StringBuilder prefix, String pattern, Queue<String> keys) {
    if (root == null) return;
    int depth = prefix.length();
    if (pattern.length() == depth && root.val != null) {
      keys.add(prefix.toString());
    }
    if (pattern.length() == depth) return;

    char next = pattern.charAt(depth);
    for (char c = 0; c < radix; ++c) {
      if (next == c || next == '.') {
        int len = prefix.length();
        collect(root.next[c], prefix.append(c), pattern, keys);
        prefix.setLength(len);
      }
    }
  }

  @Override
  public String longestPrefixOf(String key) {
    int len = search(root, key, 0, 0);
    return key.substring(0, len);
  }

  private int search(Node root, String key, int depth, int longest) {
    if (root == null) return longest;
    if (root.val != null) longest = depth;
    if (depth == key.length()) return longest;
    char c = key.charAt(depth);
    return search(root.next[c], key, depth + 1, longest);
  }
}
