package r.yelianevich.datastruct.string;

import r.yelianevich.datastruct.map.SymbolTableApi;

public interface StringSymbolTableApi<Value> extends SymbolTableApi<String, Value> {

  String longestPrefixOf(String s);

  Iterable<String> keysWithPrefix(String prefix);

  Iterable<String> keysThatMatch(String pattern);
}
