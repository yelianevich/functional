package r.yelianevich.puzzle;

import java.util.Random;

public class Apocalypse {
  public Apocalypse() {
  }

  /**
   * Simulates policy when parents have to stop to have kids only when girl is born.
   * It helps to determine what would be the distribution of boy/girl if such policy applied
   */
  public static Population simulatePopulation(int parentsCount) {
    Random rand = new Random();
    Population population = new Population();
    for (int i = 0; i < parentsCount; ++i) {
      int gender = rand.nextInt(2);
      while (isBoy(gender)) {
        population.men++;
        gender = rand.nextInt(2);
      }
      population.women++;
    }
    return population;
  }

  /**
   * Gender: 0 - boy, 1 - girl
   */
  private static boolean isBoy(int gender) {
    return gender == 0;
  }

  static class Population {
    int men;
    int women;

    @Override
    public String toString() {
      return "Population{men=" + men + ", women=" + women + '}';
    }
  }
}
