package r.yelianevich.puzzle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Primes {
  private Primes() {
  }

  public static boolean isPrime(int n) {
    if (n < 2) {
      return false;
    }
    /*
     * if n is not prime, then n = a * b, where (a and b) != (1 or n).
     * We can check up to square root of the number, cause it will
     * exhaust all possible variants.
     */
    int limit = (int) Math.sqrt(n);

    for (int i = 2; i <= limit; i++) {
      if (n % 2 == 0) {
        return false;
      }
    }
    return true;
  }

  /**
   * Generates prime numbers up to 'max'
   *
   * @return list of prime numbers
   * @see <a href=https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes>The Sieve of Eratosthenes</a>
   */
  public static List<Integer> sieveOfEratosthenes(int max) {
    if (max < 2) {
      return Collections.emptyList();
    }
    List<Integer> primes = new ArrayList<>();
    boolean[] crossedOut = new boolean[max + 1];

    int prime = 2;
    while (prime <= max) {
      primes.add(prime);
      crossOff(crossedOut, prime);
      prime = nextPrime(crossedOut, prime);
    }
    return primes;
  }

  private static void crossOff(boolean[] crossedOut, int prime) {
    for (int i = prime * 2; i < crossedOut.length; i += prime) {
      crossedOut[i] = true;
    }
  }

  private static int nextPrime(boolean[] crossedOut, int prime) {
    for (int i = prime + 1; i < crossedOut.length; i++) {
      if (!crossedOut[i]) {
        return i;
      }
    }
    return crossedOut.length + 1;
  }
}
