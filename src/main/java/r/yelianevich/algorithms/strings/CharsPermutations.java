package r.yelianevich.algorithms.strings;

import java.util.Arrays;

public class CharsPermutations {
  private CharsPermutations() {
  }

  /**
   * Time: O(n*log_n + n*log_n + n) = O(n*log_n)
   * Space: quicksort = O(log_n)
   */
  public static boolean arePermutations(String str1, String str2) {
    if (str1.length() != str2.length()) {
      return false;
    }
    char[] chars1 = str1.toCharArray();
    char[] chars2 = str2.toCharArray();
    Arrays.sort(chars1);
    Arrays.sort(chars2);
    for (int i = 0; i < chars1.length; ++i) {
      if (chars1[i] != chars2[i]) {
        return false;
      }
    }
    return true;
  }

  /**
   * ASCII characters set is 128 chars (256 for extended).
   * It leads to a simple optimization - use array as a 'map' for characters count
   */
  public static boolean areAsciiPermutations(String str1, String str2) {
    if (str1.length() != str2.length()) {
      return false;
    }
    int[] letters = new int[128]; // or 256
    for (int i = 0; i < str1.length(); ++i) {
      letters[str1.charAt(i)]++;
    }
    for (int i = 0; i < str2.length(); ++i) {
      letters[str2.charAt(i)]--;
      if (letters[str2.charAt(i)] < 0) {
        return false;
      }
    }
    return true;
  }
}
