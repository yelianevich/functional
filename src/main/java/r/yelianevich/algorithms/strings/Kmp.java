package r.yelianevich.algorithms.strings;

/**
 * <p>Knuth-Morris-Pratt (KMP) algorithm https://en.wikipedia.org/wiki/Knuth%E2%80%93Morris%E2%80%93Pratt_algorithm
 * of a substring search that works in a linear time. The idea behind is to preprocess a pattern and
 * on a mismatch reset an index of the pattern to a next non-matching character of a pattern and do not move
 * index of the text.
 *
 * <p>Video explanation of the implemented algorithm: https://www.youtube.com/watch?v=GTJr8OvyEVQ.
 *
 * <p>Time complexity: O(m) preprocessing + O(n) matching; space complexity: O(m)
 */
public final class Kmp {

    /**
     * <pre>
     *   A B C D A B D D D D D
     *   A B C D A B C A
     * t[0 0 0 0 1 2 3 1]
     * </pre>
     */
    public static int indexOf(String text, String pattern) {
        if (pattern.isEmpty()) {
            return 0;
        }
        int[] t = buildPartialMatchTable(pattern);
        int j = 0; // pattern index;
        for (int i = 0; i < text.length();) {
            if (text.charAt(i) == pattern.charAt(j)) {
                j++;
                i++;
            } else if (j == 0) {
                i++;
            } else {
                // reset to the index, that skips pattern characters that we know are matching
                j = t[j - 1];
            }
            if (j == pattern.length()) {
                return i - j;
            }
        }
        return -1;
    }

    /**
     * Entry of the computed array is an index to which pattern index could be reset on the mismatch with a text.
     * <pre>
     *   j
     *   A B C D A B C A
     *                 i
     *   A B C D A B C A
     * t[0 0 0 0 1 2 3 1]
     * </pre>
     * @return array with reset index on mismatch for any character in a pattern.
     */
    static int[] buildPartialMatchTable(String pattern) {
        int[] t = new int[pattern.length()];
        t[0] = 0;
        int i = 1; // mismatch position
        int j = 0; // prefix position
        while (i < pattern.length()) {
            if (pattern.charAt(i) == pattern.charAt(j)) {
                t[i] = j + 1;
                i++;
                j++;
            } else {
                if (j > 0) {
                    j = t[j - 1];
                } else {
                    t[i] = 0;
                    i++;
                }
            }
        }
        return t;
    }
}
