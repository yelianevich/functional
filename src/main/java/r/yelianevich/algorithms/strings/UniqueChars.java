package r.yelianevich.algorithms.strings;

import java.util.Arrays;

public class UniqueChars {
  private UniqueChars() {
  }

  /**
   * This code assumes that str is ASCII-encoded - only 128 possible characters
   * Time - O(n). Space - O(1)
   */
  public static boolean isUniqueAsciiChars(String str) {
    if (str.length() > 128) {
      return false;
    }

    boolean[] wasUsed = new boolean[128];
    for (int i = 0; i < str.length(); ++i) {
      char val = str.charAt(i);
      if (wasUsed[val]) {
        return false;
      } else {
        wasUsed[val] = true;
      }
    }
    return true;
  }

  /**
   * Assumes that str contains only a-z letters - can reduce space consumption to a single int using bit manipulation.
   * Time - O(n). Space - O(1)
   */
  public static boolean isUniqueLowerEnglish(String str) {
    if (str.length() > 26) {
      return false;
    }
    int checker = 0;
    for (int i = 0; i < str.length(); ++i) {
      int val = str.charAt(i) - 'a';
      if ((checker & (1 << val)) != 0) {
        return false;
      } else {
        // set char bit
        checker |= 1 << val;
      }
    }
    return true;
  }

  /**
   * O(n*log_n). Space - quick sort for primitives - O(log_n)
   */
  public static boolean isUnique(String str) {
    char[] chars = str.toCharArray();
    Arrays.sort(chars);
    for (int i = 1; i < chars.length; ++i) {
      if (chars[i - 1] == chars[i]) {
        return false;
      }
    }
    return true;
  }
}
