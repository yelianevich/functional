package r.yelianevich.algorithms.strings;

public class URLify {
  private URLify() {
  }

  public static void replaceSpaces(char[] str, int charsLen) {
    int endPointer = str.length - 1;
    int i = charsLen - 1;
    int spacesCount = (str.length - charsLen) / 2;
    while (i >= 0 && spacesCount > 0) {
      char val = str[i--];
      if (val != ' ') {
        str[endPointer--] = val;
      } else {
        str[endPointer--] = '0';
        str[endPointer--] = '2';
        str[endPointer--] = '%';
        spacesCount--;
      }
    }
  }
}
