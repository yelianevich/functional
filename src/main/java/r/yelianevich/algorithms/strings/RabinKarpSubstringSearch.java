package r.yelianevich.algorithms.strings;

/**
 * Simple implementation of a string-searching algorithm to find a pattern in a text in a linear time: O(t + p).
 */
public class RabinKarpSubstringSearch {

  public static final int RADIX = 128; // could be 256
  public static final int Q = 1013; // prime

  private RabinKarpSubstringSearch() {
  }

  public static int indexOf(String text, String pattern) {
    if (text.length() < pattern.length()) {
      return -1;
    }

    int len = pattern.length();

    int patternHash = hash(pattern, len);

    // initialize rolling hash and check for equality with pattern if needed 
    int rollingHash = hash(text, len);
    if (patternHash == rollingHash) {
      if (text.regionMatches(0, pattern, 0, len)) {
        return 0;
      }
    }

    // pre-compute multiplier for letter that get out of the sliding window
    int radixPow = 1;
    for (int i = 0; i < len - 1; i++) {
      radixPow = (radixPow * RADIX) % Q;
    }

    for (int i = 1, limit = text.length() - len; i <= limit; ++i) {
      char charOutOfWindow = text.charAt(i - 1);
      char newCharInWindow = text.charAt(i + len - 1);
      int outHash = (charOutOfWindow * radixPow) % Q;
      rollingHash = ((rollingHash + Q - outHash) * RADIX + newCharInWindow) % Q;
      if (patternHash == rollingHash) {
        if (text.regionMatches(i, pattern, 0, len)) {
          return i;
        }
      }
    }
    return -1;
  }

  private static int hash(String text, int len) {
    int hash = 0;
    for (int i = 0; i < len; i++) {
      hash = (hash * RADIX + text.charAt(i)) % Q;
    }
    return hash;
  }
}
