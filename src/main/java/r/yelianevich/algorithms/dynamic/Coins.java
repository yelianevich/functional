package r.yelianevich.algorithms.dynamic;

public class Coins {
  private Coins() {
  }

  /**
   * Calculate number of ways of representing n cents using 25, 10, 5, 1 cent coins
   *
   * @param n amount of cents
   * @return number of ways to represent n cents
   */
  public static int change(int n) {
    int[] coins = {1, 5, 10, 25}; // {25, 10, 5, 1};
    int coinIndex = 0;
    int[][] memo = new int[n + 1][4];
    return change(n, coins, coinIndex, memo);
  }

  private static int change(int n, int[] coins, int coinIndex, int[][] memo) {
    if (n < 0) {
      return 0;
    } else if (n == 0) {
      return 1;
    } else if (memo[n][coinIndex] != 0) {
      return memo[n][coinIndex];
    } else {
      int ways = 0;
      for (int i = coinIndex; i < coins.length; ++i) {
        ways += change(n - coins[i], coins, i, memo);
      }
      memo[n][coinIndex] = ways;
      return ways;
    }
  }

  public static int countChange(int money) {
    int[] coins = {25, 10, 5, 1};
    return countChange(money, coins, 0);
  }

  private static int countChange(int money, int[] coins, int coinIndex) {
    if (money == 0) {
      return 1;
    } else if (coinIndex >= coins.length || money < 0) {
      return 0;
    } else {
      return countChange(money - coins[coinIndex], coins, coinIndex) +
          countChange(money, coins, coinIndex + 1);
    }
  }
}
