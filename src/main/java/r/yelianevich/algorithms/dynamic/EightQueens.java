package r.yelianevich.algorithms.dynamic;

public class EightQueens {

  private EightQueens() {
  }

  public static final int GRID = 8;

  public static int placeQueens() {
    int[] queens = new int[GRID]; // column positions of the queens: queens[queen] == column 
    return placeQueens(7, queens);
  }

  private static int placeQueens(int queen, int[] queens) {
    if (queen < 0) {
      // we've made it! - all queens are placed and there is +1 way to place queens
      return 1;
    }

    int ways = 0;
    for (int col = 0; col < GRID; ++col) {
      if (isSafe(queen, col, queens)) {
        // col is vacant
        int[] withQueen = queens.clone();
        withQueen[queen] = col; // place 'queen' at col
        ways += placeQueens(queen - 1, withQueen);
      }
    }
    return ways;
  }

  /**
   * Check if previously placed queens are not on the same column or diagonal
   * And we can place 'queen' at 'col'
   */
  private static boolean isSafe(int queen, int col, int[] queens) {
    for (int prevQueen = queen + 1; prevQueen < GRID; ++prevQueen) {
      if (queens[prevQueen] == col) {
        return false;
      }
      if ((prevQueen - queen) == Math.abs(queens[prevQueen] - col)) {
        return false;
      }
    }
    return true;
  }
}
