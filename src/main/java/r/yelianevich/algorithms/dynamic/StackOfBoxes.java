package r.yelianevich.algorithms.dynamic;

import java.util.Comparator;
import java.util.List;

/**
 * Stack of Boxes: You have a stack of n boxes, with widths w1, heights hi, and depths di.
 * The boxes cannot be rotated and can only be stacked on top of one another
 * if each box in the stack is strictly larger than the box above it in width, height, and depth.
 * Implement a method to compute the height of the tallest possible stack.
 * The height of a stack is the sum of the heights of each box.
 */
public class StackOfBoxes {
  private StackOfBoxes() {
  }

  public static int maxHeight(List<Box> boxes) {
    if (boxes == null || boxes.isEmpty()) {
      return 0;
    }

    // cause boxes should be strictly *greater* in all dimensions
    // we can sort it by any parameter DESC and be sure that we do not have to
    // come back for previous values, cause they cannot be stacked on top of chosen bottom
    // see [for (int i = bottomIndex + 1; i < boxes.size(); ++i) {]
    boxes.sort(Comparator.comparingInt(Box::getHeight).reversed());

    int[] memo = new int[boxes.size()];
    int maxHeight = 0;
    for (int i = 0; i < boxes.size(); ++i) {
      int height = maxHeight(boxes, i, memo);
      maxHeight = Math.max(height, maxHeight);
    }
    return maxHeight;
  }

  public static int maxHeight(List<Box> boxes, int bottomIndex, int[] memo) {
    if (memo[bottomIndex] > 0) {
      return memo[bottomIndex];
    }
    Box bottom = boxes.get(bottomIndex);
    int maxHeight = 0;
    for (int i = bottomIndex + 1; i < boxes.size(); ++i) {
      if (boxes.get(i).canBePlacedOnTopOf(bottom)) {
        int height = maxHeight(boxes, i, memo);
        maxHeight = Math.max(height, maxHeight);
      }
    }
    maxHeight += bottom.height;
    memo[bottomIndex] = maxHeight;
    return maxHeight;
  }

  static class Box {
    final int height;
    final int width;
    final int depth;

    public Box(int height, int width, int depth) {
      this.height = height;
      this.width = width;
      this.depth = depth;
    }

    public int getHeight() {
      return height;
    }

    public boolean canBePlacedOnTopOf(Box o) {
      int cmp1 = Integer.compare(height, o.height);
      int cmp2 = Integer.compare(width, o.width);
      int cmp3 = Integer.compare(depth, o.depth);
      return cmp1 < 0 && cmp2 < 0 && cmp3 < 0; // strictly greater
    }
  }

}
