package r.yelianevich.algorithms.dynamic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Returns powerset of a given set.
 * Powerset is the set of all subsets of S, including the empty set and S itself (Wiki)
 */
public class Powerset {

  public static <E> List<Set<E>> of(Set<E> set) {
    if (set.isEmpty()) {
      return Collections.emptyList();
    } else if (set.size() == 1) {
      return Arrays.asList(Collections.emptySet(), set);
    }

    E elem = set.iterator().next();
    set.remove(elem);
    List<Set<E>> subsets = of(set);

    List<Set<E>> all = new ArrayList<>(subsets);
    for (Set<E> s : subsets) {
      Set<E> newSubSet = new HashSet<>(s);
      newSubSet.add(elem);
      all.add(newSubSet);
    }
    return all;
  }

  public static <E> List<Set<E>> combinatorialOf(Set<E> set) {
    int max = 1 << set.size(); // 2 ^ n
    @SuppressWarnings("unchecked")
    E[] elements = (E[]) set.toArray();
    List<Set<E>> allSubsets = new ArrayList<>();
    for (int i = 0; i < max; ++i) {
      Set<E> subset = generateSubset(elements, i);
      allSubsets.add(subset);
    }
    return allSubsets;
  }

  private static <E> Set<E> generateSubset(E[] elements, int combination) {
    Set<E> subset = new HashSet<>();
    int index = 0;
    for (int k = combination; k > 0; k >>>= 1, index++) {
      if ((k & 1) > 0) {
        subset.add(elements[index]);
      }
    }
    return subset;
  }
}
