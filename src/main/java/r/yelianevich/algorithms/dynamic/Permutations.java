package r.yelianevich.algorithms.dynamic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Permutations {

  private Permutations() {
  }

  /**
   * Generates all permutations of the string with unique characters
   *
   * @return list of all possible permutations
   */
  public static List<String> genPermutations(String s) {
    if (s == null) {
      return null;
    }
    return streamPermutations(s).collect(Collectors.toList());
  }

  private static Stream<String> streamPermutations(String s) {
    if (s.length() <= 1) {
      return Stream.of(s);
    }
    char first = s.charAt(0);
    String remainder = s.substring(1);
    Stream<String> reminderPerms = streamPermutations(remainder);
    return reminderPerms.flatMap(str -> streamCharPermutations(str, first));
  }

  private static Stream<String> streamCharPermutations(String str, char c) {
    return IntStream
        .range(0, str.length() + 1)
        .mapToObj(i -> {
          String start = str.substring(0, i);
          String end = str.substring(i);
          return start + c + end;
        });
  }

  /**
   * Generates all permutations of a string characters, which may well be duplicated.
   * Works much faster than O(n!) when input has many duplicated characters.
   * Complexity: O(n!)
   *
   * @return list of all possible permutations without repetition
   */
  public static List<String> genDupPermutations(String str) {
    if (str == null) {
      return null;
    }
    Map<Character, Integer> charFreqTable = buildCharFreqTable(str);
    List<String> result = new ArrayList<>();
    genDupPermutations(charFreqTable, "", str.length(), result);
    return result;
  }

  private static Map<Character, Integer> buildCharFreqTable(String str) {
    Map<Character, Integer> freq = new HashMap<>();
    for (char character : str.toCharArray()) {
      Integer count = freq.getOrDefault(character, 0);
      freq.put(character, count + 1);
    }
    return freq;
  }

  private static void genDupPermutations(Map<Character, Integer> chars, String prefix, int remaining, List<String> result) {
    if (remaining == 0) {
      result.add(prefix);
      return;
    }

    for (Character c : chars.keySet()) {
      int count = chars.get(c);
      if (count > 0) {
        chars.put(c, count - 1);
        genDupPermutations(chars, prefix + c, remaining - 1, result);
        chars.put(c, count);
      }
    }
  }
}
