package r.yelianevich.algorithms.dynamic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class RobotInAGrid {

  /**
   * Algorithm takes O(r*c) due to the memoization of the failed paths
   *
   * @return path from top left corner to the bottom right
   */
  public static List<Point> findThePath(boolean[][] maze) {
    Objects.requireNonNull(maze, "maze cannot be null");
    int lastRow = maze.length - 1;
    int lastCol = maze[0].length - 1;
    Set<Point> failedMemo = new HashSet<>();
    return findThePath(maze, lastRow, lastCol, failedMemo);
  }

  private static List<Point> findThePath(boolean[][] maze, int row, int col, Set<Point> failedMemo) {
    Point current = Point.of(row, col);

    if ((row < 0 || col < 0) || !maze[row][col] || failedMemo.contains(current)) {
      return Collections.emptyList();
    }

    if (row == 0 && col == 0) {
      ArrayList<Point> points = new ArrayList<>();
      points.add(Point.of(0, 0));
      return points;
    }

    List<Point> upperPath = findThePath(maze, row - 1, col, failedMemo);
    if (!upperPath.isEmpty()) {
      upperPath.add(current);
      return upperPath;
    }

    List<Point> leftPath = findThePath(maze, row, col - 1, failedMemo);
    if (!leftPath.isEmpty()) {
      leftPath.add(current);
      return leftPath;
    }

    failedMemo.add(current);
    return Collections.emptyList();
  }

  public static final class Point {
    public final int r;
    public final int c;

    private Point(int r, int c) {
      this.r = r;
      this.c = c;
    }

    public static Point of(int r, int c) {
      return new Point(r, c);
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Point point = (Point) o;
      return r == point.r && c == point.c;
    }

    @Override
    public int hashCode() {
      return Objects.hash(r, c);
    }
  }
}
