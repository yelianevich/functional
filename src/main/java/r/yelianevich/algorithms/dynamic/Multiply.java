package r.yelianevich.algorithms.dynamic;

public class Multiply {

  private Multiply() {
  }

  /**
   * Optimal solution.
   * Recursive function to multiply 2 numbers by using only +, -, >>, <<.
   * You need to minimize number of those operations.
   */
  public static int minProduct(int a, int b) {
    int bigger = Math.max(a, b);
    int smaller = Math.min(a, b);
    return minProductHelper(smaller, bigger);
  }

  private static int minProductHelper(int smaller, int bigger) {
    if (smaller == 0) {
      return 0;
    } else if (smaller == 1) {
      return bigger;
    }
    int result = minProductHelper(smaller >> 1, bigger) << 1;
    if (smaller % 2 == 1) {
      result += bigger;
    }
    return result;
  }

  /**
   * Not optimal.
   * Recursive function to multiply 2 numbers by using only +, -, >>, <<.
   * You need to minimize number of those operations.
   * This implementation uses memoization to avoid duplicated work.
   */
  public static int minProductWithMemo(int a, int b) {
    int bigger = Math.max(a, b);
    int smaller = Math.min(a, b);
    int[] memo = new int[smaller + 1];
    return minProductWithMemo(smaller, bigger, memo);
  }

  private static int minProductWithMemo(int smaller, int bigger, int[] memo) {
    if (smaller == 0) {
      return 0;
    } else if (smaller == 1) {
      return bigger;
    } else if (memo[smaller] != 0) {
      return memo[smaller];
    }
    int result;
    int s = smaller >> 1;
    int side1 = minProductWithMemo(s, bigger, memo);
    if (smaller % 2 == 1) {
      result = side1 + minProductWithMemo(smaller - s, bigger, memo);
    } else {
      result = side1 << 1;
    }
    memo[smaller] = result;
    return result;
  }

  /**
   * Solution that is based on binary representation of the number
   */
  public static int mul(int a, int b) {
    int bigger = Math.max(a, b);
    int smaller = Math.min(a, b);

    int result = (smaller & 1) == 1 ? bigger : 0;
    smaller >>>= 1;
    for (int i = 1; smaller > 0; i++, smaller >>>= 1) {
      if ((smaller & 1) == 1) {
        result += bigger << i;
      }
    }
    return result;
  }

}
