package r.yelianevich.algorithms.dynamic;

import java.util.ArrayDeque;
import java.util.Queue;

public class PaintFill {

  private final int[][] screen;

  public PaintFill(int[][] screen) {
    this.screen = screen;
  }

  /**
   * Fill up the area using BFS (breadth-first search)
   *
   * @param initPoint - point to start coloring
   * @param color     - desirable color of the area
   */
  public void fill(Point initPoint, int color) {
    if (!isValid(initPoint)) {
      return;
    }
    int colorToChange = colorAt(initPoint);
    Queue<Point> queue = new ArrayDeque<>();
    queue.add(initPoint);
    while (!queue.isEmpty()) {
      Point point = queue.remove();

      if (colorAt(point) == colorToChange) {
        screen[point.row][point.col] = color;

        visitNeighbours(Point.of(point.row - 1, point.col), queue, colorToChange);
        visitNeighbours(Point.of(point.row + 1, point.col), queue, colorToChange);
        visitNeighbours(Point.of(point.row, point.col + 1), queue, colorToChange);
        visitNeighbours(Point.of(point.row, point.col - 1), queue, colorToChange);
      }
    }
  }

  private void visitNeighbours(Point p, Queue<Point> queue, int colorToChange) {
    if (isValid(p) && colorAt(p) == colorToChange) {
      queue.add(p);
    }
  }

  public boolean isValid(Point p) {
    return p.row >= 0 && p.row < screen.length &&
        p.col >= 0 && p.col < screen[0].length;
  }

  public int[][] getScreen() {
    return screen;
  }

  public int colorAt(Point p) {
    return screen[p.row][p.col];
  }

  public static final class Point {
    public final int row;
    public final int col;

    private Point(int row, int col) {
      this.row = row;
      this.col = col;
    }

    public static Point of(int row, int col) {
      return new Point(row, col);
    }

    @Override
    public String toString() {
      return "Point{" +
          "row=" + row +
          ", col=" + col +
          '}';
    }
  }
}
