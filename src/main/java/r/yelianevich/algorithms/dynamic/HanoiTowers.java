package r.yelianevich.algorithms.dynamic;

import java.util.ArrayDeque;
import java.util.Deque;

public class HanoiTowers {

  private final int disksCount;
  private Deque<Integer> tower1;
  private Deque<Integer> tower2;
  private Deque<Integer> tower3;

  public HanoiTowers(int disksCount) {
    if (disksCount <= 0) {
      throw new IllegalArgumentException("there should be at least one disk, but was " + disksCount);
    }
    this.disksCount = disksCount;
  }

  public Deque<Integer> solve() {
    init();
    move(disksCount, tower1, tower3, tower2);
    return tower3;
  }

  private void move(int d, Deque<Integer> x, Deque<Integer> y, Deque<Integer> z) {
    if (d == 0) return;

    if (d == 1) {
      if (!y.isEmpty() && (y.element() < x.element())) {
        throw new IllegalArgumentException("Placing bigger disk on smaller is prohibited");
      }
      y.push(x.pop());
    } else {
      move(d - 1, x, z, y);
      move(1, x, y, z);
      move(d - 1, z, y, x);
    }
  }

  private void init() {
    tower1 = new ArrayDeque<>();
    tower2 = new ArrayDeque<>();
    tower3 = new ArrayDeque<>();
    for (int i = disksCount; i > 0; --i) {
      tower1.push(i);
    }
  }
}
