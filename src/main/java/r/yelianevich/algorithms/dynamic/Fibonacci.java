package r.yelianevich.algorithms.dynamic;

/**
 * Simplest illustration of dynamic programming approach (but an overkill for a such simple problem)
 */
public final class Fibonacci {

  private Fibonacci() {
  }

  /**
   * Runtime O(2^n) + memory O(n) (stack)
   */
  public static int recursive(int n) {
    if (n == 0 || n == 1) {
      return n;
    }
    return recursive(n - 1) + recursive(n - 2);
  }

  /**
   * Runtime O(n) + memory O(n)
   */
  public static int topDown(int n) {
    return topDown(n, new int[n + 1]);
  }

  private static int topDown(int n, int[] memo) {
    if (n == 0 || n == 1) {
      return n;
    }
    if (memo[n] == 0) {
      memo[n] = topDown(n - 1, memo) + topDown(n - 2, memo);
    }
    return memo[n];
  }

  /**
   * Runtime: O(n) + memory O(n)
   */
  public static int bottomUp(int n) {
    if (n == 0 | n == 1) return n;
    int[] memo = new int[n + 1];
    memo[0] = 0;
    memo[1] = 1;
    for (int i = 2; i <= n; ++i) {
      memo[i] = memo[i - 1] + memo[i - 2];
    }
    return memo[n];
  }

  /**
   * Runtime O(n) + memory O(1)
   */
  public static int loop(int n) {
    if (n == 0 | n == 1) return n;
    int a = 0;
    int b = 1;
    for (int i = 2; i <= n; ++i) {
      int c = a + b;
      a = b;
      b = c;
    }
    return b;
  }

  private static final int[][] Q = new int[][]{{1, 1}, {1, 0}};

  /**
   * Runtime: O(n) + memory O(n)
   */
  public static int matrix(int n) {
    if (n <= 1) return n;

    // Fn = Q^(n-1)
    int[][] r = pow(n - 1);
    return r[0][0];
  }

  private static int[][] pow(int n) {
    /*
     * Fn = Q^(n-1)
     * | Fn+1, Fn   |   | 1,  1 |n
     * | Fn  , Fn-1 | = | 1,  0 |
     */
    int[][][] memo = new int[n + 1][][];
    memo[1] = Q;
    return pow(Q, n, memo);
  }

  private static int[][] pow(int[][] q, int p, int[][][] memo) {
    if (memo[p] != null) {
      return memo[p];
    }
    int k = p / 2;
    int[][] powHalf = pow(q, k, memo);
    int[][] result = mul(powHalf, powHalf);
    if ((p & 1) != 0) {
      result = mul(result, Q);
    }
    memo[k] = result;
    return result;
  }

  private static int[][] mul(int[][] x, int[][] y) {
    int a00 = x[0][0] * y[0][0] + x[0][1] * y[0][1];
    int a01 = x[0][0] * y[1][0] + x[0][1] * y[1][1];
    int a10 = x[1][0] * y[0][0] + x[1][1] * y[0][1];
    int a11 = x[1][0] * y[1][0] + x[1][1] * y[1][1];
    return new int[][]{{a00, a01}, {a10, a11}};
  }
}
