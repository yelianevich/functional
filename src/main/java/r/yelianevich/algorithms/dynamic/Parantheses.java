package r.yelianevich.algorithms.dynamic;


import java.util.ArrayList;
import java.util.List;

public class Parantheses {

  private Parantheses() {
  }

  public static List<String> validCombinations(int n) {
    List<String> result = new ArrayList<>();
    if (n > 0) {
      validCombinations(new char[n * 2], n, n, 0, result);
    }
    return result;
  }

  private static void validCombinations(char[] str, int left, int right, int index, List<String> result) {
    // invalid combination
    if ((left < 0 || right < 0) || right < left) {
      return;
    }

    if (left == 0 && right == 0) {
      result.add(String.valueOf(str));
    } else {
      str[index] = '(';
      validCombinations(str, left - 1, right, index + 1, result);

      str[index] = ')';
      validCombinations(str, left, right - 1, index + 1, result);
    }
  }
}
