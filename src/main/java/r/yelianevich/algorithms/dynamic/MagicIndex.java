package r.yelianevich.algorithms.dynamic;

public class MagicIndex {

  /**
   * Finds magic index in sorted array, such as sorted[i] == i, where no dups are present
   */
  public static int findMagicIndex(int[] sorted) {
    int lo = 0;
    int hi = sorted.length - 1;
    while (lo <= hi) {
      int mid = (lo + hi) / 2;
      if (mid == sorted[mid]) {
        return mid;
      } else if (mid > sorted[mid]) {
        lo = mid + 1;
      } else {
        hi = mid - 1;
      }
    }
    return -1;
  }

  public static int findMagicIndexWithDups(int[] sorted) {
    return findMagicIndexWithDups(sorted, 0, sorted.length - 1);
  }

  /**
   * When dups are present it's not clear where magic index could be and both sides should be searched.
   * However, some optimizations could be made taking advantage of sorting.
   */
  private static int findMagicIndexWithDups(int[] sorted, int lo, int hi) {
    if (lo > hi) return -1;

    int midIndex = (lo + hi) / 2;
    int midValue = sorted[midIndex];

    if (midValue == midIndex) {
      return midIndex;
    }

    // skip some elements here based on the notion that leftValue <= midValue
    int leftIndex = Math.min(midIndex - 1, midValue);
    int left = findMagicIndexWithDups(sorted, lo, leftIndex);
    if (left != -1) {
      return left;
    }

    // skip some elements here based on the notion that rightValue >= midValue
    int rightIndex = Math.max(midIndex + 1, midValue);
    return findMagicIndexWithDups(sorted, rightIndex, hi);
  }
}
