package r.yelianevich.algorithms.dynamic;

public class TripleStep {

  public static int countWays(int ladder) {
    if (ladder == 0) return 0;
    else return countWays(ladder, new int[ladder + 1]);
  }

  private static int countWays(int ladder, int[] memo) {
    if (ladder == 0) {
      return 1; // reached the leaf of the combinations tree
    } else if (memo[ladder] != 0) {
      return memo[ladder];
    }
    int ways = 0;
    for (int hop = 1; hop <= 3 && ladder >= hop; ++hop) {
      ways += countWays(ladder - hop, memo);
    }
    memo[ladder] = ways;
    return ways;
  }
}
