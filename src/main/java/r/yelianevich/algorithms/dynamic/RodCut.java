package r.yelianevich.algorithms.dynamic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A classic problem of cutting a rod to maximise revenue, given prices for a piece of a certain length.
 */
public final class RodCut {

  // determines a price for a piece of length i: prices[i] 
  private final int[] price;

  public RodCut(int[] price) {
    this.price = price;
  }

  /**
   * Exponential complexity O(2^(n-1)), cause it examines all possible variants of cutting a rod
   */
  public int maxRevenue(int rodLen) {
    if (rodLen == 0) {
      return 0;
    }
    int maxRevenue = 0;
    for (int i = 1; i <= rodLen; ++i) {
      int revenue = price[i] + maxRevenue(rodLen - i);
      maxRevenue = Math.max(revenue, maxRevenue);
    }
    return maxRevenue;
  }

  /**
   * Quadratic complexity O(n^2)).
   */
  public int topDownMaxRevenue(int rodLen) {
    return topDownMaxRevenue(rodLen, new int[rodLen + 1]);
  }

  private int topDownMaxRevenue(int rodLen, int[] memo) {
    if (memo[rodLen] != 0) {
      return memo[rodLen];
    }
    int maxRevenue = 0;
    for (int i = 1; i <= rodLen; ++i) {
      int revenue = price[i] + topDownMaxRevenue(rodLen - i, memo);
      maxRevenue = Math.max(revenue, maxRevenue);
    }
    memo[rodLen] = maxRevenue;
    return maxRevenue;
  }

  /**
   * Quadratic complexity O(n^2)) (doubly nested loop)
   */
  public int bottomUpMaxRevenue(int rodLen) {
    int[] memo = new int[rodLen + 1];
    // start from the bottom: find max revenue for rod of length 1, then 2, till rodLen
    for (int i = 1; i <= rodLen; ++i) {
      int maxRevenue = 0;

      // to find a max revenue for a peace i
      for (int j = 1; j <= i; ++j) {
        // key part is that we don't need recursive call to get solution 
        // for the smaller sub problem - it's already memoized
        maxRevenue = Math.max(maxRevenue, price[j] + memo[i - j]);
      }
      memo[i] = maxRevenue;
    }
    return memo[rodLen];
  }

  public OptimalCut exetendedBottomUpMaxRevenue(int rodLen) {
    int[] memo = new int[rodLen + 1];
    int[] solution = new int[rodLen + 1];
    for (int i = 1; i <= rodLen; ++i) {
      int maxRevenue = 0;
      for (int j = 1; j <= i; ++j) {
        int revenue = price[j] + memo[i - j];
        if (maxRevenue < revenue) {
          maxRevenue = revenue;
          solution[i] = j; // remember cut that produce max revenue
        }
      }
      memo[i] = maxRevenue;
    }
    return OptimalCut.ofSolution(solution, memo[rodLen], rodLen);
  }

  public static class OptimalCut {
    public final int revenue;
    public final List<Integer> pieces;

    private OptimalCut(int revenue, List<Integer> pieces) {
      this.revenue = revenue;
      this.pieces = Collections.unmodifiableList(pieces);
    }

    private static OptimalCut ofSolution(int[] solution, int revenue, int rodLen) {
      List<Integer> steps = new ArrayList<>();
      int i = rodLen;
      while (i != 0) {
        steps.add(solution[i]);
        i -= solution[i];
      }
      return new OptimalCut(revenue, steps);
    }
  }
}
