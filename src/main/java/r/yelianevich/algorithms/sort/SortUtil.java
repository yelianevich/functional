package r.yelianevich.algorithms.sort;

import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Random;
import java.util.function.Consumer;

public final class SortUtil {

  private static final Random RANDOM = new Random();

  private SortUtil() {
  }

  public static void exch(int[] a, int i, int j) {
    int tmp = a[i];
    a[i] = a[j];
    a[j] = tmp;
  }

  public static void exch(double[] a, int i, int j) {
    double tmp = a[i];
    a[i] = a[j];
    a[j] = tmp;
  }

  public static <T> void exch(T[] a, int i, int j) {
    T tmp = a[i];
    a[i] = a[j];
    a[j] = tmp;
  }

  public static <Key extends Comparable<Key>> boolean less(Key x, Key y) {
    return x.compareTo(y) < 0;
  }

  public static <Key extends Comparable<Key>> boolean eq(Key x, Key y) {
    return x.compareTo(y) == 0;
  }

  public static <Key extends Comparable<Key>> boolean lessOrEqual(Key x, Key y) {
    return x.compareTo(y) <= 0;
  }

  public static <Key extends Comparable<Key>> boolean isSorted(Key[] a, int lo, int hi) {
    for (int i = lo + 1; i <= hi; i++)
      if (less(a[i], a[i - 1])) return false;
    return true;
  }

  public static void demo(Consumer<Comparable[]> sortFunc) {
    Integer[] a = {0, 5, 3, 1, 6, 2};
    System.out.println("init a = " + Arrays.toString(a));
    sortFunc.accept(a);
    System.out.println("sort a = " + Arrays.toString(a));
    Character[] c = {'e', 'a', 's', 'y', 'q', 'u', 'e', 's', 't', 'i', 'o', 'n'};
    System.out.println("init c = " + Arrays.toString(c));
    sortFunc.accept(c);
    System.out.println("sort c = " + Arrays.toString(c));
  }

  public static <T> void shuffle(T[] a) {
    int n = Objects.requireNonNull(a).length;
    for (int i = 0; i < n; ++i) {
      int j = i + RANDOM.nextInt(n - i);
      exch(a, i, j);
    }
  }

  public static void shuffle(int[] a) {
    int n = Objects.requireNonNull(a).length;
    for (int i = 0; i < n; ++i) {
      int j = i + RANDOM.nextInt(n - i);
      exch(a, i, j);
    }
  }

  /**
   * Arrays.stream(arr).max().getAsInt()
   */
  public static int max(int[] a) {
    if (a == null || a.length == 0) throw new NoSuchElementException();
    int max = a[0];
    for (int i = 1; i < a.length; ++i) {
      if (a[i] > max) max = a[i];
    }
    return max;
  }

  /**
   * Arrays.stream(arr).min().getAsInt()
   */
  public static int min(int[] a) {
    if (a == null || a.length == 0) throw new NoSuchElementException();
    int min = a[0];
    for (int i = 1; i < a.length; ++i) {
      if (a[i] < min) min = a[i];
    }
    return min;
  }

}
