package r.yelianevich.algorithms.sort;

import static r.yelianevich.algorithms.sort.SortUtil.exch;
import static r.yelianevich.algorithms.sort.SortUtil.less;

/**
 * Classic randomized implementation of Quicksort algorithm.
 */
public class Quick {

  private Quick() {
  }

  public static <Key extends Comparable<Key>> void sort(Key[] a) {
    // Quicksort uses ~N^2/2 compares in the worst case,
    // shuffle of the array protects against this case
    SortUtil.shuffle(a);

    sort(a, 0, a.length - 1);
  }

  private static <Key extends Comparable<Key>> void sort(Key[] a, int lo, int hi) {
    if (lo >= hi) return;
    int j = partition(a, lo, hi);
    sort(a, lo, j - 1);
    sort(a, j + 1, hi);
  }

  /**
   * Partition the subarray a[lo..hi] so that a[lo..j-1] <= a[j] <= a[j+1..hi]
   * @return index j
   */
  private static <Key extends Comparable<Key>> int partition(Key[] a, int lo, int hi) {
    Key pivot = a[lo];
    int i = lo;
    int j = hi + 1; // +1, cause a[--j] and we don't want to miss right most element
    while (true) {
      // find element that should be on the right side of the pivot
      while (less(a[++i], pivot)) {
        if (i == hi) break;
      }

      // find element that should be on the left side of the pivot
      while (less(pivot, a[--j])) {
        if (j == lo) break;
      }

      // left and right pointers met in the middle - break the loop
      if (i >= j) break;

      exch(a, i, j);
    }
    // now a[lo..j-1] <= a[j] <= a[j+1..hi]
    exch(a, lo, j);

    return j;
  }
}
