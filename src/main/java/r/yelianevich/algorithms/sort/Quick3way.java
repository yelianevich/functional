package r.yelianevich.algorithms.sort;

import static r.yelianevich.algorithms.sort.SortUtil.exch;

/**
 * Quicksort implementation that is using 3-way partitioning.
 * Works well in case of many duplicated values, comparing to standard quicksort implementation.
 */
public class Quick3way {

  private Quick3way() {
  }

  public static <Key extends Comparable<Key>> void sort(Key[] a) {
    SortUtil.shuffle(a);
    sort(a, 0, a.length - 1);
  }

  /*
   * Traditional 3-way (or "Dutch National Flag") schema:
   *
   *   left part    center part    right part
   * +-------------------------------------+
   * |  < p  |   == p   |     ?    |  > p  |
   * +-------------------------------------+
   *         ^           ^        ^
   *         |           |        |
   *         lt          i        gt
   *
   * Invariants:
   *
   *   all in (left, less)   < pivot
   *   all in [less, k)     == pivot
   *   all in (great, right) > pivot
   */
  private static <Key extends Comparable<Key>> void sort(Key[] a, int lo, int hi) {
    if (lo >= hi) return;

    int lt = lo;
    int gt = hi;
    Key p = a[lo]; // pivot element
    int i = lo + 1; // cursor of iteration
    while (i <= gt) {
      int cmp = a[i].compareTo(p);
      if (cmp < 0) exch(a, lt++, i++);
      else if (cmp > 0) exch(a, gt--, i);
      else ++i;
    }
    // now a[lo..lt-1] < v == a[lt..gt] <= a[gt+1..hi]

    // As a result we can skip a[lt..gt] from further sorting,
    // that can be big in practical situations with duplicated keys
    sort(a, lo, lt - 1);
    sort(a, gt + 1, hi);
  }

}
