package r.yelianevich.algorithms.sort;

/**
 * Classic Heapsort algorithm.
 * Pros:
 * - !the only alg! that is optimal in terms of space and time
 * -- guaranties O(N * logN) compares and O(1) space in worst case
 * - does not require extra memory - O(1)
 * Cons:
 * - not stable (can reorder the same elements)
 * - not adaptive (do not utilize presorted arrays)
 * - slower on average than quicksort and mergesort (poor cache hit)
 */
public class Heap {

  public Heap() {
  }

  public static <Key extends Comparable<Key>> void sort(Key[] a) {
    // heap construction
    int n = a.length;
    for (int k = n / 2; k >= 1; --k) {
      sink(a, k, n);
    }

    // sortdown
    while (n >= 1) {
      exch(a, 1, n--); // remove max element to the end of array and shrink heap
      sink(a, 1, n); // recover heap order
    }
  }

  /**
   * Top-down heapify of element in a heap
   * @param a binary heap array, where a[0] is used, that's why index operation should be adjusted
   */
  private static <Key extends Comparable<Key>> void sink(Key[] a, int k, int n) {
    while (k * 2 <= n) {
      int j = k * 2;
      if (j < n && less(a, j, j + 1)) ++j; // choose bigger child
      if (!less(a, k, j)) break; // if parent is >= big child - heap is in order
      exch(a, k, j);
      k = j;
    }
  }

  /**
   * less and exch help to translate index operations to 0-based arrays,
   * cause in classic impl a[0] is left unused
   */
  private static <Key extends Comparable<Key>> boolean less(Key[] a,  int i, int j) {
    return a[i - 1].compareTo(a[j - 1]) < 0;
  }

  private static <Key extends Comparable<Key>> void exch(Key[] a,  int i, int j) {
    Key tmp = a[i - 1];
    a[i - 1] = a[j - 1];
    a[j - 1] = tmp;
  }

}
