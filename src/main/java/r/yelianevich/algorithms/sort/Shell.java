package r.yelianevich.algorithms.sort;

import static r.yelianevich.algorithms.sort.SortUtil.exch;

/**
 * Shell sort implementation that sort elements in ASC order. Basis of the sorting method is
 * Knuth's incremental sequence 1, 4, 13, 40, 121, 364, 1093, ...
 *
 * There are many more complicated incremental sequences that can provide 20-30% performance gain,
 * but much more complicated to implement.
 */
public class Shell {

  private Shell() {
  }

  /**
   * Sorts array in ascending natural order
   */
  public static <K extends Comparable<K>> void sort(K[] a) {
    int n = a.length;

    // compute last element of Knuth's incremental sequence
    int h = 1;
    while (h < (n / 3)) h = (3 * h) + 1;  // 1, 4, 13, 40, 121, 364, 1093, ...

    // perform h-sorts of array
    for (; h > 0; h = h / 3) {
      for (int i = h; i < n; ++i) {
        for (int j = i; j >= h && SortUtil.less(a[j], a[j-h]); j -= h) {
          SortUtil.exch(a, j, j - h);
        }
      }
    }
  }

  public static void main(String[] args) {
    SortUtil.demo(Shell::sort);
  }

}
