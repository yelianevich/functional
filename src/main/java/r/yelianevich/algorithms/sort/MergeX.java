package r.yelianevich.algorithms.sort;

/**
 * Improved implementation of top-down mergesort:
 * <ul>
 * <li>Use insertion sort for small subarrays. We can improve most recursive algorithms by handling small cases differently. Switching to insertion sort for small subarrays will improve the running time of a typical mergesort implementation by 10 to 15 percent.</li>
 * <li>Test whether array is already in order. We can reduce the running time to be linear for arrays that are already in order by adding a test to skip call to merge() if a[mid] is less than or equal to a[mid+1]. With this change, we still do all the recursive calls, but the running time for any sorted subarray is linear.</li>
 * <li>Eliminate the copy to the auxiliary array. It is possible to eliminate the time (but not the space) taken to copy to the auxiliary array used for merging. To do so, we use two invocations of the sort method, one that takes its input from the given array and puts the sorted output in the auxiliary array; the other takes its input from the auxiliary array and puts the sorted output in the given array. With this approach, in a bit of mindbending recursive trickery, we can arrange the recursive calls such that the computation switches the roles of the input array and the auxiliary array at each level.</li>
 * <ul/>
 */
public class MergeX {

  private static final int CUTOFF = 20;

  private MergeX() {
  }

  public static <Key extends Comparable<Key>> void sort(Key[] a) {
    Key[] temp = a.clone();
    sort(temp, a, 0, a.length - 1);
  }

  private static <Key extends Comparable<Key>> void sort(Key[] temp, Key[] a, int lo, int hi) {
    if (hi <= lo + CUTOFF) {
      Insertion.sort(a, lo, hi);
    } else {
      int mid = lo + (hi - lo) / 2;

      // Swap temp array and a in sort call. Which means: sort data in temp instead of a.
      // As a result temp contains sorted halfs temp[lo, mid] and temp[mid + 1, hi]
      // and merge do not have to copy from a to temp as preparation step
      sort(a, temp, lo, mid);
      sort(a, temp, mid + 1, hi);

      if (!SortUtil.lessOrEqual(temp[mid], temp[mid + 1])) {
        // merge only if array a[lo..hi] is not sorted
        merge(temp, a, lo, mid, hi);
      } else {
        // a[lo..hi] is sorted - just copy to a
        System.arraycopy(temp, lo, a, lo, hi - lo + 1);
      }
    }
  }

  private static <Key extends Comparable<Key>> void merge(Key[] src, Key[] dst, int lo, int mid, int hi) {
    // do not need to copy from dst to src, cause src already contains required data
    int i = lo;
    int j = mid + 1;
    for (int k = lo; k <= hi; ++k) {
      if (i > mid) dst[k] = src[j++];
      else if (j > hi) dst[k] = src[i++];
      else if (SortUtil.less(src[i], src[j])) dst[k] = src[i++];
      else dst[k] = src[j++];
    }
  }

  public static void main(String[] args) {
    SortUtil.demo(MergeX::sort);
  }

}
