package r.yelianevich.algorithms.sort;

import static r.yelianevich.algorithms.sort.SortUtil.exch;
import static r.yelianevich.algorithms.sort.SortUtil.less;

public class Quickselect {
  private Quickselect() {
  }

  public static int find(int[] arr, int k) {
    if (arr.length < k) {
      throw new IllegalArgumentException("Trying to select kth element, but array is smaller than " + k);
    }
    SortUtil.shuffle(arr); // to prevent O(n^2) time complexity

    int lo = 0;
    int hi = arr.length - 1;
    while (true) {
      if (lo == hi) {
        return arr[lo];
      }
      int pivot = partition(arr, lo, hi);
      if (pivot == k) {
        return arr[pivot];
      } else if (k < pivot) {
        hi = pivot - 1;
      } else {
        lo = pivot + 1;
      }
    }
  }

  private static int partition(int[] a, int lo, int hi) {
    int pivot = a[lo];
    int i = lo;
    int j = hi + 1; // +1, cause a[--j] and we don't want to miss right most element
    while (true) {
      // find element that should be on the right side of the pivot
      while (less(a[++i], pivot)) {
        if (i == hi) break;
      }

      // find element that should be on the left side of the pivot
      while (less(pivot, a[--j])) {
        if (j == lo) break; // redundant, cause a[lo] cannot be less than itself
      }

      // left and right pointers met in the middle - break the loop
      if (i >= j) break;

      exch(a, i, j);
    }
    // now a[lo..j-1] <= a[j] <= a[j+1..hi]
    exch(a, lo, j);

    return j;
  }
}
