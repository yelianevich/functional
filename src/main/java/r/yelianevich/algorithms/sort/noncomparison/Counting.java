package r.yelianevich.algorithms.sort.noncomparison;

import r.yelianevich.algorithms.sort.SortUtil;

/**
 * Stable non-comparative sorting algorithm that sorts integer arrays in linear time.
 * <p>
 * Counting sort algorithm assumes that input array contains values in [0..k-1] range.
 * When k = O(n) (upper bound), the sort runs in (tight bound) O(n) time.
 */
public final class Counting {

  public static int[] sort(int[] arr) {
    if (arr.length == 0) return arr;
    int min = SortUtil.min(arr);
    int max = SortUtil.max(arr);
    return sort(arr, min, max);
  }

  /**
   * Assumes that values of array are in [0..k] range
   *
   * @return new sorted array
   */
  public static int[] sortClassic(int[] a, int k) {
    if (a.length == 0) return a;

    int[] c = new int[k + 1];
    for (int x : a) {
      c[x] += 1;
    }
    for (int i = 1; i < c.length; ++i) {
      c[i] += c[i - 1];
    }
    int[] b = new int[a.length];
    for (int i = a.length - 1; i >= 0; --i) {
      int x = a[i];
      b[c[x] - 1] = x;
      c[x] -= 1;
    }
    return b;
  }

  /**
   * Sorts array of element that are in [min..max] values range and return new sorted array
   *
   * @param arr - input array with elements in [min..max] range
   * @param min - min value in array
   * @param max - max value in array
   * @return new sorted array
   */
  public static int[] sort(int[] arr, int min, int max) {
    int range = max - min + 1;
    int indexShift = -min; // shift indexes to handle negative numbers and stay in array bounds
    int[] counts = new int[range];
    for (int x : arr) {
      counts[x + indexShift] += 1;
    }

    // after next step 'counts[i]' element contains number of elements that are <= x
    // having that information we can put element directly to the correct position
    for (int i = 1; i < counts.length; i++) {
      counts[i] += counts[i - 1];
    }

    int[] sorted = new int[arr.length];
    for (int i = arr.length - 1; i >= 0; i--) {
      int x = arr[i];
      int position = counts[x + indexShift] - 1; // position in sorted array
      counts[x + indexShift] -= 1; // adjust count to position equal elements correctly
      sorted[position] = x;
    }
    return sorted;
  }

  /**
   * This in-place modification of counting sort is not stable
   */
  public static void sortInPlace(int[] ints) {
    if (ints == null || ints.length <= 1) return;

    int min = ints[0];
    int max = ints[0];
    for (int c : ints) {
      min = Math.min(min, c);
      max = Math.max(max, c);
    }
    int countsLen = max - min + 1;

    int[] counts = new int[countsLen]; // for character arrays it could be 256 chars length for extended ASCII
    for (int c : ints) {
      counts[c - min]++;
    }

      for (int i = 0, p = 0; i < countsLen && p < ints.length; ++i) {
      while (counts[i] > 0) {
          ints[p++] = i + min;
        counts[i]--;
      }
    }
  }
}
