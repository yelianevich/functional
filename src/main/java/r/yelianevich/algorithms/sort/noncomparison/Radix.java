package r.yelianevich.algorithms.sort.noncomparison;

/**
 * Stable non-comparative sorting algorithm that sorts integer arrays in linear time.
 * <p>
 * Complexity: O(w/d * (n + 2<sup>d</sup>)) or O(nc) if for values in range [0, ..., n<sup>c</sup> - 1]
 */
public final class Radix {

  private Radix() {
  }

  public static int[] sort(int[] a) {
    int w = 32;
    int d = 8;
    int k = 1 << d;
    int parts = w / d;
    int[] b = null;

    // iterate through least significant to most significant bits;
    // sorting subroutine must be stable
    for (int p = 0; p < parts; ++p) {

      // counting sort; stable;
      int[] c = new int[k];
      for (int x : a) {
        int xp = (x >> d * p) & (k - 1);
        c[xp] += 1;
      }
      for (int i = 1; i < c.length; ++i) {
        c[i] += c[i - 1];
      }

      b = new int[a.length];
      for (int i = a.length - 1; i >= 0; --i) {
        int x = (a[i] >> d * p) & (k - 1);
        int pos = c[x] - 1;
        b[pos] = a[i];
        c[x] -= 1;
      }
      a = b;
    }
    return b;
  }
}
