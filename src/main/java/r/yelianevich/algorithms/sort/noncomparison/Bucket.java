package r.yelianevich.algorithms.sort.noncomparison;

import r.yelianevich.algorithms.sort.Insertion;

import java.util.Collections;
import java.util.LinkedList;

/**
 * Sorting algorithm that is designed to sort uniformly distributed values in range [0, 1).
 */
public final class Bucket {

  private Bucket() {
  }

  public static void sort(double[] a) {
    @SuppressWarnings("unchecked")
    LinkedList<Double>[] buckets = (LinkedList<Double>[]) new LinkedList[10];
    for (int i = 0; i < buckets.length; ++i) {
      buckets[i] = new LinkedList<>();
    }

    for (double x : a) {
      int i = (int) x * 10;
      buckets[i].add(x);
    }

    for (LinkedList<Double> bucket : buckets) {
      // online sorting algorithm can deal with linked lists effectively 
      Collections.sort(bucket);
    }

    int index = 0;
    for (LinkedList<Double> bucket : buckets) {
      for (double x : bucket) {
        a[index++] = x;
      }
    }
  }

  public static void sortOptimized(double[] a) {
    @SuppressWarnings("unchecked")
    LinkedList<Double>[] buckets = (LinkedList<Double>[]) new LinkedList[10];
    for (int i = 0; i < buckets.length; ++i) {
      buckets[i] = new LinkedList<>();
    }

    for (double x : a) {
      int i = (int) x * 10;
      buckets[i].add(x);
    }

    // common optimization is to put partially sorted data to the original list and insertion sort it;
    // Insertion sort is adaptive and as a result it's doing sorting much more efficiently
    int index = 0;
    for (LinkedList<Double> bucket : buckets) {
      for (double x : bucket) {
        a[index++] = x;
      }
    }
    Insertion.sort(a);
  }
}
