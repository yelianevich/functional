package r.yelianevich.algorithms.sort;

import static r.yelianevich.algorithms.sort.SortUtil.less;

/**
 * Top-down recursive mergesort implementation that uses copy to temp array
 * for merge operation.
 * <p>
 * Top-down mergesort uses between 1/2 N lg N and N lg N compares
 * and at most 6 N lg N array accesses to sort any array of length N.
 * </p>
 */
public class Merge {

  private Merge() {
  }

  @SuppressWarnings("unchecked")
  public static <Key extends Comparable<Key>> void sort(Key[] a) {
    Key[] temp = (Key[]) new Comparable[a.length];
    sort(temp, a, 0, a.length - 1);
  }

  /**
   * Recursive sort, that sort left array part first, then right part and finally merge sorted sides.
   * Recursion goes deeper until array will consist of just 2 elements, thus
   * left sorted array is first element and right sorted arrays is second element.
   */
  private static <Key extends Comparable<Key>> void sort(Key[] temp, Key[] a, int lo, int hi) {
    if (lo >= hi) return;
    int mid = lo + (hi - lo) / 2;
    sort(temp, a, lo, mid);
    sort(temp, a, mid + 1, hi);
    merge(temp, a, lo, mid, hi);
  }

  /**
   * Merges 2 sorted sub-arrays a[lo, mid] and a[mid + 1, hi] into a[lo, mid]
   * using copy to temp array.
   */
  public static <Key extends Comparable<Key>> void merge(Key[] temp, Key[] a, int lo, int mid, int hi) {
    System.arraycopy(a, lo, temp, lo, (hi - lo) + 1);
    int i = lo;
    int j = mid + 1;
    for (int k = lo; k <= hi; ++k) {
      if (j > hi) a[k] = temp[i++];
      else if (i > mid) a[k] = temp[j++];
      else if (less(temp[i], temp[j])) a[k] = temp[i++];
      else a[k] = temp[j++];
    }
  }

  public static void main(String[] args) {
    SortUtil.demo(Merge::sort);
  }

}
