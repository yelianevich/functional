package r.yelianevich.algorithms.sort;

import static r.yelianevich.algorithms.sort.SortUtil.exch;
import static r.yelianevich.algorithms.sort.SortUtil.less;

/**
 * <p>Optimized quicksort algorithm implementation.</p>
 * 1. Median-of-3
 * 2. Sentinels
 * 3. Cutoff for insertion sort
 * 5. TODO Fast 3-way partitioning. (J. Bentley and D. McIlroy)
 */
public class QuickX {

  /* Cutoff to insertion sort when the length of the array is less than CUTTOFF size */
  private static final int CUTOFF = 8;

  private QuickX() {
  }

  public static <Key extends Comparable<Key>> void sort(Key[] a) {
    SortUtil.shuffle(a);
    sort(a, 0, a.length - 1);
  }

  private static <Key extends Comparable<Key>> void sort(Key[] a, int lo, int hi) {
    if (lo + CUTOFF >= hi) {
      // insertion sort is faster on tiny array, comparing to recursive sort algorithms
      Insertion.sort(a, lo, hi);
    } else {
      int j = partition(a, lo, hi);
      sort(a, lo, j - 1);
      sort(a, j + 1, hi);
    }
  }


  private static <Key extends Comparable<Key>> int partition(Key[] a, int lo, int hi) {
    medianOfTree(a, lo, hi);

    Key p = a[lo];
    int i = lo;
    int j = hi + 1; // +1, cause a[--j] and we don't want to miss right most element
    while (true) {
      // find element that should be on the right side of the pivot
      while (less(a[++i], p)) {
        // a[hi] could not be less than p, cause medianOfTree put v (that is >= p) to the a[hi] position
        // if (i == hi) break;
      }

      // find element that should be on the left side of the pivot
      while (less(p, a[--j])) {
        // check is redundant, since p cannot be less than itself
        // if (j == lo) break;
      }

      // left and right pointers met in the middle - break the loop
      if (i >= j) break;

      exch(a, i, j);
    }
    // now a[lo..j-1] <= a[j] <= a[j+1..hi]
    exch(a, lo, j);

    return j;
  }

  /**
   * Put median into a[lo], smallest into a[lo + 1], and max into a[hi]
   * @return median position
   */
  static <Key extends Comparable<Key>> int medianOfTree(Key[] a, int lo, int hi) {
    int median = lo;
    int min = lo + 1;
    int max = hi;

    // make sure min element is in the a[min] (a[lo])
    if (less(a[median], a[min])) exch(a, median, min);
    if (less(a[max], a[min])) exch(a, max, min);

    // put median and max elements in the correct position
    if (less(a[max], a[median])) exch(a, max, median);
    return lo;
  }
}
