package r.yelianevich.algorithms.sort;

import static r.yelianevich.algorithms.sort.SortUtil.eq;
import static r.yelianevich.algorithms.sort.SortUtil.exch;
import static r.yelianevich.algorithms.sort.SortUtil.less;

/**
 * Quicksort with fast 3-way partitioning (J. Bentley and D. McIlroy)
 */
public class QuickFast3way {

  private QuickFast3way() {
  }

  public static <Key extends Comparable<Key>> void sort(Key[] a) {
    SortUtil.shuffle(a);
    sort(a, 0, a.length - 1);
  }

  private static <Key extends Comparable<Key>> void sort(Key[] a, int lo, int hi) {
    if (lo >= hi) return;

    /*
     * Fast 3-way partitioning:
     * 1) During:
     * +-------------------------------------------+
     * |  == p  |   < p   |     ?    |  > p  | ==p |
     * +-------------------------------------------+
     * ^         ^         ^        ^       ^      ^
     * |         |         |        |       |      |
     * lo        lt        i        j       gt     hi
     *
     * 2) After:
     * +-------------------------------------------+
     * |      < p      |     == p      |     > p   |
     * +-------------------------------------------+
     * ^              ^                 ^          ^
     * |              |                 |          |
     * lo             j                 i          hi
     */

    Key p = a[lo];
    int i = lo, lt = lo + 1;
    int j = hi + 1, gt = hi;

    while (true) {

      while (less(a[++i], p)) {
        if (i == hi) break;
      }

      while (less(p, a[--j])) {
        if (j == lo) break;
      }

      // pointers cross
      if (i == j && eq(a[i], p)) {
        exch(a, lt++, i);
      }
      if (i >= j) break;

      exch(a, i, j);
      if (eq(a[i], p)) exch(a, i, lt++);
      if (eq(a[j], p)) exch(a, j, gt--);
    }

    // j + 1, cause j and i can go to lo and hi respectively
    i = j + 1;
    for (int k = hi; k > gt; ++i, --k) {
      exch(a, i, k);
    }
    for (int k = lo; k < lt; --j, ++k) {
      exch(a, j, k);
    }

    sort(a, lo, j);
    sort(a, i, hi);
  }

}
