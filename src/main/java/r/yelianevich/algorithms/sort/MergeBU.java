package r.yelianevich.algorithms.sort;

/**
 * Bottom-up iterative mergesort implementation.
 * <p>
 * Bottom-up mergesort uses between 1/2 N lg N and N lg N compares
 * and at most 6 N lg N array accesses to sort any array of length N.
 * </p>
 */
public class MergeBU {

  private MergeBU() {
  }

  @SuppressWarnings("unchecked")
  public static <Key extends Comparable<Key>> void sort(Key[] a) {
    int n = a.length;
    Key[] temp = (Key[]) new Comparable[n];
    for (int sz = 1; sz <= n; sz *= 2) {
      for (int lo = 0; lo < n - sz; lo += 2 * sz) {
        int hi = Math.min(n - 1, lo + (2 * sz - 1));
        int mid = lo + (sz - 1);
        Merge.merge(temp, a, lo, mid, hi);
      }
    }
  }

  public static void main(String[] args) {
    SortUtil.demo(MergeBU::sort);
  }

}
