package r.yelianevich.algorithms.sort;

import static r.yelianevich.algorithms.sort.SortUtil.exch;
import static r.yelianevich.algorithms.sort.SortUtil.less;

/**
 * Insertion sort works very well on tiny arrays - when size is less than ~15.
 * Moreover, implementation is very compact.
 * <p>
 * Complexity:
 *   The average case is ~N^2/4 compares and ~N2/4 exchanges.
 *   The worst case is ~ N^2/2 compares and ~N2/2 exchanges.
 *   The best case is N-1 compares and 0 exchanges.
 * </p>
 */
public class Insertion {

  private Insertion() {
  }

  public static void sort(double[] a) {
    int n = a.length;
    for (int i = 0; i < n; ++i) {
      for (int j = i; j > 0 && a[j] < a[j - 1]; --j) {
        SortUtil.exch(a, j, j - 1);
      }
    }
  }

  public static <Key extends Comparable<Key>> void sort(Key[] a) {
    int n = a.length;
    for (int i = 0; i < n; ++i) {
      for (int j = i; j > 0 && less(a[j], a[j - 1]); --j) {
        exch(a, j, j - 1);
      }
    }
  }

  public static <Key extends Comparable<Key>> void sort(Key[] a, int lo, int hi) {
    for (int i = lo; i <= hi; ++i) {
      for (int j = i; j > lo && less(a[j], a[j - 1]); j--) {
        exch(a, j, j - 1);
      }
    }
  }

  public static void main(String[] args) {
    SortUtil.demo(Insertion::sort);
  }

}
