package r.yelianevich.algorithms.sort;

/**
 * One of the simplest sorting algorithms works as follows:
 * First, find the smallest item in the array, and exchange it with the first entry.
 * Then, find the next smallest item and exchange it with the second entry.
 * Continue in this way until the entire array is sorted.
 *
 * Complexity: ~n2/2 compares and n exchanges
 * Stability: not stable (B(1), B(2), A -> A, B(2), B(1))
 */
public class Selection {

  private Selection() {
  }

  public static <Key extends Comparable<Key>> void sort(Key[] a) {
    int n = a.length;
    for (int i = 0; i < n; ++i) {
      int min = i;
      for (int j = i + 1; j < n; ++j) {
        if (SortUtil.less(a[j], a[min])) min = j;
      }
      SortUtil.exch(a, i, min);
    }
  }

  public static void main(String[] args) {
    SortUtil.demo(Selection::sort);
  }

}
