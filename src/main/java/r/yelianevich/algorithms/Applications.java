package r.yelianevich.algorithms;

import r.yelianevich.algorithms.sort.SortUtil;

import java.util.Arrays;

import static r.yelianevich.algorithms.sort.SortUtil.exch;

public class Applications {

  private Applications() {
  }

  /**
   * Returns count of distinct keys
   */
  public static <T extends Comparable<T>> int distinctCount(T[] a) {
    Arrays.sort(a);
    int count = 1;
    for (int i = 1; i < a.length; ++i) {
      if (a[i - 1].compareTo(a[i]) != 0) {
        ++count;
      }
    }
    return count;
  }

  /**
   * Finds and return most frequent item in an array
   */
  public static <T extends Comparable<T>> T mostFreqItem(T[] a) {
    Arrays.sort(a);
    T freqItem = a[0];
    int mostFreqCount = 0;
    int currentCount = 1;
    for (int i = 1; i < a.length; ++i) {
      int cmp = a[i - 1].compareTo(a[i]);
      if (cmp == 0) {
        ++currentCount;
      } else {
        if (currentCount > mostFreqCount) {
          mostFreqCount = currentCount;
          freqItem = a[i - 1];
        }
        currentCount = 1;
      }
    }
    return freqItem;
  }

  /**
   * Selects k'th smallest item in array without fully sorting it. Takes linear time - O(n)
   * @param k index of the item in sorted array
   * @param <T> some comparable type
   */
  public static <T extends Comparable<T>> T select(T[] a, int k) {
    SortUtil.shuffle(a);
    int lo = 0;
    int hi = a.length - 1;
    while (lo < hi) {
      int j = partition(a, lo, hi);
      if (j == k) return a[k];
      else if (k < j) hi = j - 1;
      else lo = j + 1;
    }
    return a[k];
  }

  /**
   * Classic quicksort partitioning method
   * @return index j such that a[lo..j-1] <= a[j] <= a[j + 1, hi]
   */
  private static <T extends Comparable<T>> int partition(T[] a, int lo, int hi) {
    T pivot = a[lo];
    int i = lo;
    int j = hi + 1;
    while (true) {
      while (SortUtil.less(a[++i], pivot)) {
        if (i == hi) break;
      }
      while (SortUtil.less(pivot, a[--j])) {
        // no need to check boundaries - pivot is a sentinel
      }
      if (i >= j) break;
      exch(a, i, j);
    }
    exch(a, lo, j);
    return j;
  }

  /**
   * Return the objects in {@code a[]} with duplicates removed
   */
  public static <T extends Comparable<T>> T[] dedup(T[] a) {
    Arrays.sort(a);
    int n = 1; // pointer to the next element in deduped array
    for (int i = 1; i < a.length; ++i) {
      int cmp = a[i].compareTo(a[n - 1]);
      if (cmp != 0 && n != i) {
        a[n++] = a[i];
        a[i] = null;
      } else if (cmp != 0) {
        n++;
      } else {
        a[i] = null; // prevent loitering
      }
    }
    return a;
  }


}
