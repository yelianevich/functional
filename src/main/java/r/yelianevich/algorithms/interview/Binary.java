package r.yelianevich.algorithms.interview;

/**
 * Bit manipulations
 */
public final class Binary {

  private Binary() {
  }

  /**
   * Insert m bits (32-bit number) into n (32-bit number) at [i, j] bit indexes.
   */
  public static int insertBits(int n, int m, int i, int j) {
    int len = j - i + 1;
    int mask = (1 << len) - 1;
    int insertBits = (m & mask) << i; // might be optional if m has only bits set at [i, j]
    return (n & (~mask << i)) | insertBits;
  }

  public static String toString(long n) {
    // optimization:
    // create char[] by computing (64 - 'number of leading zeros')
    // and insert into appropriate position
    if (n == 0) return "0";
    StringBuilder sb = new StringBuilder();
    while (n != 0) {
      sb.append(n & 1);
      n >>>= 1;
    }
    return sb.reverse().toString();
  }

  public static String toString(byte b) {
    // shift operations are performed on int or long
    // b is cast to int first, so to make sure only byte bits are captured - mask is needed
    int n = bitsExact(b);
    if (n == 0) return "0";
    StringBuilder sb = new StringBuilder();
    while (n != 0) {
      sb.append(n & 1);
      n >>>= 1;
    }
    return sb.reverse().toString();
  }

  public static String toString(double n) {
    if (n < 0) {
      return "ERROR";
    }

    long whole = (long) n;
    double d = n - whole;
    if (d == 0) {
      return toString(whole) + ".0";
    }

    StringBuilder sb = new StringBuilder();
    while (d > 0) {
      if (sb.length() >= 32) {
        return "ERROR";
      }
      d = d * 2; // shift left
      if (d >= 1) {
        sb.append(1);
        d = d - 1; // remove shifted bit
      } else {
        sb.append(0);
      }
    }
    return toString(whole) + "." + sb.toString();
  }

  public static int flipBitToWinBruteForce(int n) {
    if (n == -1) return 32;
    if (n == 0) return 1;

    int longest = 1;

    // mask 0 bit with 1 and recalculate max length 
    for (int i = 0; i < 31; ++i) {
      int masked = n | (1 << i);
      if (masked == n) continue;

      // count '1's 
      while (masked > 0) {
        int seqLength = 0;
        int bit = masked & 1;

        while (masked > 0 && bit == 1) {
          seqLength += 1;
          masked >>>= 1;
          bit = masked & 1;
        }
        if (seqLength > longest) {
          longest = seqLength;
        }
        masked >>>= 1;
      }
    }
    return longest;
  }

  public static int flipBitToWinOptimal(int n) {
    if (n == -1) return Integer.BYTES * 8; // all 1s is a -1 in Java's two complement representation

    int longest = 1; // it's at least 1, cause we can flip
    int currSeq = 0;
    int prevSeq = 0;

    while (n > 0) {
      if ((n & 1) == 1) {
        currSeq++;
      } else if ((n & 1) == 0) {
        int nextBit = n & 2;
        prevSeq = nextBit != 0 ? currSeq : 0;
        currSeq = 0;
      }
      longest = Math.max(currSeq + prevSeq + 1, longest);
      n >>>= 1;
    }
    return longest;
  }

  public static int nextNumber(int n) {
    int c0 = 0;
    int c1 = 0;

    int temp = n;
    while ((temp & 1) == 0 && temp > 0) {
      c0++;
      temp >>>= 1;
    }
    while ((temp & 1) == 1 && temp > 0) {
      c1++;
      temp >>>= 1;
    }

    int p = c0 + c1; // rightmost non-trailing zero
    if (p == 31) {
      // 11111...00000 there is no way to get next number without adding additional bit
      return -1;
    }

    n |= (1 << p); // flip to 1
    c1--; // because of an additional 1 after flip
    n &= ~((1 << p) - 1); // reset all bits before
    return n | ((1 << c1) - 1);
  }

  public static int prevNumber(int n) {
    if (n <= 0) throw new IllegalArgumentException();

    int c1 = 0;
    int c0 = 0;

    int temp = n;
    while ((temp & 1) == 1 && temp > 0) {
      c1++;
      temp >>>= 1;
    }
    if (temp == 0) {
      // 0000...1111 there is no way to decrease number without reducing number of bits
      return -1;
    }
    while ((temp & 1) == 0 && temp > 0) {
      c0++;
      temp >>>= 1;
    }

    int p = c0 + c1; // position of rightmost non-trailing one
    n &= ((~0) << (p + 1)); // reset p bit and all preceding bits (~0 == 1111..11111)
    int mask = (1 << (c1 + 1)) - 1; // generate (c1 + 1) 1s
    mask <<= (c0 - 1); // insert (c0 - 1) 0s
    return n | mask;
  }

  public static boolean isPowerOfTwo(int i) {
    return (i & (i - 1)) == 0;
  }

  public static int countBitsDiffSimple(int a, int b) {
    int count = 0;
    int diff = a ^ b;
    // not optimal, cause we need to check every bit that comes out of a diff
    while (diff != 0) {
      count += (diff & 1);
      diff >>>= 1;
    }
    return count;
  }

  public static int countBitsDiffOptimal(int a, int b) {
    int count = 0;
    int diff = a ^ b;
    while (diff != 0) {
      count += 1;
      diff = diff & (diff - 1); // removes least significant bit; reduces complexity to the number of bits that different
    }
    return count;
  }

  public static int pairwiseSwap(int i) {
    int odd = (i >>> 1) & 0x55555555;
    int even = (i << 1) & 0xAAAAAAAA; // correct odd bits
    return odd | even;
  }

  /**
   * Draws horizontal line in the screen of pixels, where each byte represents 8 pixels
   */
  public static void drawLine(byte[] screen, int width, int x1, int x2, int y) {
    if ((x2 - x1) >= width) {
      throw new IllegalArgumentException("Not a horizontal line");
    }
    int widthBytes = width / 8;
    int yByteIndex = y * widthBytes;

    int lineStartIndex = yByteIndex + x1 / 8;
    int lineEndIndex = yByteIndex + x2 / 8;

    int startBitLeftOffset = x1 % 8;
    int endBitRightOffset = 7 - (x2 % 8);

    int fullByte = 0xFF;
    int startByteMask = fullByte >>> startBitLeftOffset;

    if (lineStartIndex == lineEndIndex) {
      // fill in just one byte
      int endByteMask = (fullByte << endBitRightOffset) & 0xFF;
      screen[lineStartIndex] = (byte) (bitsExact(screen[lineStartIndex]) | (startByteMask & endByteMask));
    } else {
      // fill in first byte
      screen[lineStartIndex] = (byte) (bitsExact(screen[lineStartIndex]) | startByteMask);

      // fill in full bytes between
      for (int i = lineStartIndex + 1; i < lineEndIndex; ++i) {
        screen[i] = (byte) 0xFF;
      }

      // fill in last byte
      int endByteMask = (fullByte << endBitRightOffset) & 0xFF;
      screen[lineEndIndex] = (byte) (bitsExact(screen[lineEndIndex]) | endByteMask);
    }
  }

  /**
   * Returns int that contains exactly the same bits information as byte
   */
  private static int bitsExact(byte b) {
    return b & 0xFF;
  }

  /*
   * Negative numbers in Java.
   * Java uses another approach, which is called two's complement.
   * Negative numbers are represented by negating (flipping) all the bits and then adding 1.
   * Still, if the leftmost bit is 0, the number is positive. Otherwise, it is negative.
   **/
  public static void main(String[] args) {
    System.out.println("Integer.toBinaryString(~0) = " + Integer.toBinaryString(~0));
    System.out.println("~0 == -1 == " + (~0 == -1));
    System.out.println("Integer.toBinaryString(-5) = " + Integer.toBinaryString(-5));
    System.out.println("Integer.toBinaryString(5) = " + Integer.toBinaryString(5));
    System.out.println("Integer.toHexString(5) = " + Integer.toHexString(5));
    System.out.println("Integer.toHexString(" + Integer.MAX_VALUE + ") = " + Integer.toHexString(Integer.MAX_VALUE));
    System.out.println("Integer.toOctalString(5) = " + Integer.toOctalString(5));

    // signed shift left
    System.out.println("-5 << 3 == " + (-5 << 3));
    System.out.println("5 << 3 == " + (5 << 3));

    // signed shift right
    System.out.println("-5 >> 1 == " + (-5 >> 1)); // -3 ??
    System.out.println("5 >> 1 == " + (5 >> 1)); // 2

    // unsigned shift right
    System.out.println("-5 >>> 1 == " + (-5 >>> 1));
    System.out.println("5 >>> 1 == " + (5 >>> 1));

    // bitwise operators
    System.out.println("Bitwise OR: 5 | 2 == " + (5 | 2));
    System.out.println("Bitwise AND: 5 & 2 == " + (5 & 2));
    System.out.println("Bitwise XOR: 5 ^ 2 == " + (5 ^ 2));
    System.out.println("Unary bitwise complement operator (negation): ~0xFFFFFFFE == " + (~0xFFFFFFFE));
  }
}
