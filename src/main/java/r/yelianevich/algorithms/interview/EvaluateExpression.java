package r.yelianevich.algorithms.interview;

import r.yelianevich.datastruct.stack.LinkedStack;
import r.yelianevich.datastruct.stack.Stack;

import java.util.Scanner;
import java.util.function.BiFunction;

/**
 * Dijkstra algorithm using 2 stacks
 */
public class EvaluateExpression {

  public static Double infix(String rawExpression) {
    Stack<Double> values = new LinkedStack<>();
    Stack<BiFunction<Double, Double, Double>> operations = new LinkedStack<>();

    BiFunction<Double, Double, Double> sum = (v1, v2) -> v1 + v2;
    BiFunction<Double, Double, Double> mul = (v1, v2) -> v1 * v2;

    Scanner scanner = new Scanner(rawExpression);
    if (!scanner.hasNext()) {
      throw new IllegalArgumentException("Empty expression");
    }
    while (scanner.hasNext()) {
      String next = scanner.next();
      if ("(".equals(next)) {
        // skip
      } else if ("+".equals(next)) {
        operations.push(sum);
      } else if ("*".equals(next)) {
        operations.push(mul);
      } else if (")".equals(next)) {
        Double val1 = values.pop();
        Double val2 = values.pop();
        Double result = operations.pop().apply(val1, val2);
        values.push(result);
      } else {
        values.push(Double.parseDouble(next));
      }
    }
    return values.pop();
  }

}
