package r.yelianevich.algorithms.interview;

/**
 * Merging with smaller auxiliary array.
 *
 * Suppose that the subarray a[0] to a[n−1] is sorted
 * and the subarray a[n] to a[2*n−1] is sorted.
 * How can you merge the two subarrays so that a[0] to a[2*n−1] is sorted
 * using an auxiliary array of length n (instead of 2n)?
 */
public class MergeHalfAux {

  public static void merge(int[] a) {
    if (a == null) {
      throw new IllegalArgumentException("array cannot be null");
    }
    int len = a.length;
    if (len % 2 != 0) {
      throw new IllegalArgumentException("array length should be multiple of 2");
    }

    int auxLen = len / 2;
    int[] aux = new int[auxLen];
    System.arraycopy(a, 0, aux, 0, auxLen);

    int i = 0; // aux array index
    int j = auxLen; // second part of initial array index

    // initial positioning
    //  k
    // [0, ..., n - 1, n, ..., 2n - 1]   sort array
    //                 j
    // [0, ..., n - 1]                   aux array
    //  i

    for (int k = 0; k < len && i < auxLen; k++) {
      if (j == len) a[k] = aux[i++]; // second part came to an end, copy from aux
      else if (aux[i] <= a[j]) a[k] = aux[i++];
      else a[k] = a[j++];
    }

  }

}
