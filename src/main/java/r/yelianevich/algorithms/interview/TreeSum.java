package r.yelianevich.algorithms.interview;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Finds count of all possible triples of numbers in an array that sum to zero
 */
public class TreeSum {

  private TreeSum() {
  }

  /**
   * O(N^3)
   */
  public static int brutForce(int[] a) {
    int len = a.length;
    int count = 0;
    for (int i = 0; i < len; ++i)
      for (int j = i + 1; j < len; ++j)
        for (int k = j + 1; k < len; ++k)
          if (a[i] + a[j] + a[k] == 0) ++count;
    return count;
  }

  /**
   * ~ N * N * 1/2 * lg(N)
   */
  public static int fast(int[] a) {
    // to be able to use binary search
    Arrays.sort(a);

    int count = 0;
    int n = a.length;
    for (int i = 0; i < n; ++i) {
      for (int j = i + 1; j < n; ++j) {
        int third = -(a[i] + a[j]); // third number that will give resulting zero
        int thirdIndex = Arrays.binarySearch(a, j + 1, n, third); // reduce third loop to log(n) operations
        if (thirdIndex >= 0) {
          ++count;
        }
      }
    }
    return count;
  }

  /**
   * Solves 3sum in O(n^2) time.
   * Finds all unique triplets in the array which gives the sum of zero.
   * <p>
   * See <a href="https://en.wikipedia.org/wiki/3SUM#Quadratic_algorithm">Wikipedia quadratic algorithm</a>
   */
  public List<List<Integer>> threeSum(int[] nums) {
    Arrays.sort(nums);
    List<List<Integer>> triplets = new ArrayList<>();
    for (int i = 0; i < nums.length - 2; ++i) {
      if (i > 0 && nums[i - 1] == nums[i]) {
        continue;
      }
      int a = nums[i];
      int lo = i + 1;
      int hi = nums.length - 1;
      while (lo < hi) {
        int b = nums[lo];
        int c = nums[hi];
        int sum = a + b + c;
        if (sum == 0) {
          triplets.add(Arrays.asList(a, b, c));
          // search for other triplets further without using duplicates
          while (lo < hi && nums[lo] == nums[lo]) lo++;
          while (lo < hi && nums[hi] == nums[hi]) hi++;
        } else if (sum > 0) {
          hi--;
        } else {
          lo++;
        }
      }
    }
    return triplets;
  }

}
