package r.yelianevich.algorithms.interview;

public class CommonElements {

  /**
   * It takes sorted arrays of distinct elements
   * Runtime: O(n)
   *
   * @return number of elements in common
   */
  public static int countCommon(int[] a, int[] b) {
    int commonCount = 0;
    int i = 0;
    int j = 0;
    while (i < a.length && j < b.length) {
      if (a[i] == b[j]) {
        ++commonCount;
        ++i;
        ++j;
      } else if (a[i] < b[j]) {
        ++i;
      } else {
        ++j;
      }
    }
    return commonCount;
  }
}
