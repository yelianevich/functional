package r.yelianevich.algorithms.interview;


/**
 * Counting inversions. An inversion in an array a[] is a pair of entries a[i] and a[j]
 * such that i < j but a[i] > a[j].
 * Given an array, design a linearithmic algorithm to count the number of inversions.
 **/
public class ArrayInversions {

  public static int count(int[] a) {
    if (a == null) throw new IllegalArgumentException("array cannot be null");
    int len = a.length;
    if (len < 2) return 0;
    else return inversions(a, 0, len - 1);
  }

  private static int inversions(int[] a, int lo, int hi) {
    if (lo >= hi) return 0;

    int mid = lo + (hi - lo) / 2;
    int left = inversions(a, lo, mid);
    int right = inversions(a, mid + 1, hi);

    int curr = 0;
    for (int i = lo; i <= mid; i++) {
      for (int j = mid + 1; j <= hi; j++) {
        if (a[i] > a[j]) {
          // inversion: sits on the left side, but greater than the element on the right
          curr++;
        }
      }
    }
    return left + right + curr;
  }

}
