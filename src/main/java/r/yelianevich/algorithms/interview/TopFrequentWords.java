package r.yelianevich.algorithms.interview;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class TopFrequentWords {
  private TopFrequentWords() {
  }

  public static String[] topMostFrequent(int limit, String[] words) {
    // calculate frequencies
    Map<String, Integer> freqMap = new HashMap<>();
    for (String word : words) {
      int freq = freqMap.getOrDefault(word, 0) + 1;
      freqMap.put(word, freq);
    }

    // construct min priority queue
    // a bit tricky comparator
    PriorityQueue<String> top = new PriorityQueue<>((w1, w2) -> {
      Integer f1 = freqMap.get(w1);
      Integer f2 = freqMap.get(w2);
      int cmp = f1.compareTo(f2);
      return cmp == 0 ? w1.compareTo(w2) : cmp;
    });

    // calculate top N by placing them to a PriorityQueue
    for (Map.Entry<String, Integer> wordToFreq : freqMap.entrySet()) {
      top.offer(wordToFreq.getKey());
      // keep no more than 'limit' elements
      if (top.size() > limit) {
        top.poll();
      }
    }

    // convert to an DESC ordered array
    String[] topWordsDesc = new String[top.size()];
    for (int i = top.size() - 1; i >= 0; --i) {
      topWordsDesc[i] = top.poll();
    }
    return topWordsDesc;
  }
}
