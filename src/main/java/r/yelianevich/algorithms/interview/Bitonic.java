package r.yelianevich.algorithms.interview;

import r.yelianevich.algorithms.BinarySearch;

/**
 * What is a bitonic array?
 * A bitonic array is an array of integers that is sorted in increasing
 * order to a certain point and then sorted in decreasing order after that point.
 * It is assumed that bitonic array DOES NOT contain duplicate elements.
 *
 * Here are some examples of biotonic arrays:
 * {1, 2, 3, 4, 5, 0, -1, -2, -3} -- pivot point occurs at index 5
 * {1, 3, 5, 7, 9, 8, 6, 4, 2, 0}	-- pivot point occurs at index 4
 * {1, 2, 3, 4, 5}	-- pivot points occurs at the last element
 * {5, 4, 3, 2, 1} -- pivot points occurs at first element
 */
public class Bitonic {

  private Bitonic() {
  }

  public static int fast(int[] a, int key) {
    int lo = 0;
    int hi = a.length - 1;
    while (lo <= hi) {
      int mid = lo + (hi - lo) / 2;
      int midVal = a[mid];
      if (midVal == key) {
        return mid;
      } else if (a[mid - 1] < midVal) {
        if (key < midVal) {
          // ASC part - can search on the left side
          int i = BinarySearch.indexOfAsc(a, lo, mid - 1, key);
          if (i >= 0) return i;
        }
        lo = mid + 1;
      } else { // a[mid - 1] > midVal (all elements are distinct, so it cannot be equal)
        if (key < midVal) {
          // DESC part - can search on the right side
          int i = BinarySearch.indexOfDesc(a, mid + 1, hi, key);
          if (i >= 0) return i;
        }
        hi = mid - 1;
      }
    }
    return -1;
  }

  /**
   * Basic implementation of bitonic search, which is a two steps process:
   * 1. Find bitonic point 2. binary search in appropriate part of the array
   * @param a
   * @param key
   * @return
   */
  public static int search(int[] a, int key) {
    int p = bitonicPoint(a);
    if (p == -1) {
      throw new IllegalArgumentException("Not a bitonic array");
    }

    int asc = BinarySearch.indexOfAsc(a, 0, p, key);
    if (asc >= 0) {
      return asc;
    }
    int desc = BinarySearch.indexOfDesc(a,p + 1, a.length - 1, key);
    if (desc >= 0) {
      return desc;
    }
    return -1;
  }

  /**
   * Bitonic point is an index in a bitonic array where element is greater
   * than the previous and greater than the next one {@code (prev < bitonic > next)}
   * Complexity is O(log(n))
   */
  public static int bitonicPoint(int[] a) {
    int lo = 0;
    int hi = a.length - 1;
    while (lo <= hi) {
      int mid = lo + (hi - lo) / 2;
      int midVal = a[mid];

      // does it satisfy bitonic point definition?
      if (a[mid - 1] < midVal && midVal > a[mid + 1]) {
        return mid;
      }

      if (a[mid + 1] < midVal) {
        hi = mid - 1;
      } else {
        lo = mid + 1;
      }
    }
    return -1;
  }

}
