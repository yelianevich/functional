package r.yelianevich.algorithms;

public class BinarySearch {

  private BinarySearch() {
  }

  public static int indexOfAsc(int[] a, int key) {
    return indexOfAsc(a, 0, a.length, key);
  }

  public static int indexOfDesc(int[] a, int key) {
    return indexOfDesc(a, 0, a.length, key);
  }

  /**
   * Returns number of elements that are smaller than a given 'val'.
   **/
  public static int rank(int[] a, int val) {
    int lo = 0;
    int hi = a.length - 1;
    while (lo <= hi) {
      int mid = lo + (hi - lo) / 2;  // to avoid int overflow do not use '(lo + hi) / 2'
      if (val < a[mid]) {
        hi = mid - 1;
      } else if (val > a[mid]) {
        lo = mid + 1;
      } else {
        return mid;
      }
    }
    return lo;
  }

  /**
   * Binary search in an array of asc order.
   *
   * @return See Arrays#binarySearch
   */
  public static int indexOfAsc(int[] a, int lo, int hi, int key) {
    while (lo <= hi) {
      int mid = lo + (hi - lo) / 2;
      if (key < a[mid]) {
        hi = mid - 1;
      } else if (key > a[mid]) {
        lo = mid + 1;
      } else {
        return mid; //found
      }
    }
    // + 1 is needed here to distinguish '0' (no values smaller than the given key)
    return -(lo + 1); // not found; returns negative index of insertion point
  }

  /**
   * Binary search in an array of desc order.
   *
   * @return See Arrays#binarySearch
   */
  public static int indexOfDesc(int[] a, int lo, int hi, int key) {
    while (lo <= hi) {
      int mid = lo + (hi - lo) / 2;

      if (key > a[mid]) {
        hi = mid - 1;
      } else if (key < a[mid]) {
        lo = mid + 1;
      } else {
        return mid; //found
      }
    }
    // + 1 is needed here to distinguish '0' (no values smaller than the given key)
    return -(lo + 1); // not found; returns negative index of insertion point
  }

  /**
   * Returns leftmost index of a given value. It's useful when the value may be duplicated.
   */
  public static int leftmostIndexOf(int[] a, int val) {
    int lo = 0;
    int hi = a.length; // note that hi equals to the length of the array
    while (lo < hi) {
      int mid = lo + (hi - lo) / 2;
      if (val > a[mid]) {
        lo = mid + 1;
      } else {
        hi = mid;
      }
    }
    return lo;
  }

  public static int rightmostIndexOf(int[] a, int val) {
    int lo = 0;
    int hi = a.length; // note the length
    while (lo < hi) {
      int mid = lo + (hi - lo) / 2;
      if (val >= a[mid]) {
        lo = mid + 1;
      } else {
        hi = mid;
      }
    }
    return hi - 1; // -1 to adjust to the index of the last element;
  }
}
