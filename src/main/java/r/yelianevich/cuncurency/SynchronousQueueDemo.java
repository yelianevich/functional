package r.yelianevich.cuncurency;

import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;

public class SynchronousQueueDemo {

  // a pattern to notify connected party that it's time to shut down
  private static final Integer POISON_PILL = -1;

  public static void main(String[] args) throws InterruptedException {
    final SynchronousQueue<Integer> queue = new SynchronousQueue<>();

    System.out.print("crazy fibs: ");
    Thread previousFibThread = new Thread(() -> {
      try {
        Integer curr = 0;
        while (!curr.equals(POISON_PILL)) {
          System.out.print(" " + curr);
          queue.put(curr);
          curr = queue.take();
        }
      } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
      }
    });

    Thread nextFibThread = new Thread(() -> {
      try {
        Integer next = 1;
        while (next < 1000) {
          Integer prev = queue.take();
          queue.put(next); // send state back to prevThread to reconstruct 'next previous' number
          next = prev + next;
        }
        queue.take();
        queue.put(POISON_PILL);
      } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
      }
    });

    previousFibThread.start();
    nextFibThread.start();

    TimeUnit.SECONDS.timedJoin(nextFibThread, 10);
    TimeUnit.SECONDS.timedJoin(previousFibThread, 10);

    System.out.println("");
    System.out.println("No more crazy stuff");
  }

}
