package r.yelianevich.cuncurency.custom;

/**
 * Thread gate that can be reused for multiple open/close cycles using condition queue
 */
public class ThreadGate {
  private boolean isOpen;
  private int generation;

  public synchronized void open() {
    generation++;
    isOpen = true;
    notifyAll();
  }

  public synchronized void close() {
    isOpen = false;
  }

  public synchronized void await() throws InterruptedException {
    int arrivalGeneration = generation; // change of generation means that the gate was open/closed - good to go
    while (!isOpen && arrivalGeneration == generation) {
      wait();
    }
  }
}
