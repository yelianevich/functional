package r.yelianevich.cuncurency.custom;

import java.util.concurrent.locks.AbstractQueuedSynchronizer;

/**
 * Binary latch implemented using AQS
 */
public class OneShotLatch {
  private final Sync sync = new Sync();

  public void aquire() throws InterruptedException {
    sync.acquireSharedInterruptibly(0);
  }

  public void signal() {
    sync.releaseShared(0);
  }

  private class Sync extends AbstractQueuedSynchronizer {
    @Override
    protected int tryAcquireShared(int notUsed) {
      return getState() == 1 ? 1 : -1;
    }

    @Override
    protected boolean tryReleaseShared(int notUsed) {
      setState(1);
      return true;
    }
  }
}
