package r.yelianevich.cuncurency.custom;

/**
 * Blocking bounded buffer that uses condition queues
 * to implement blocking put and take methods
 * @param <E>
 */
public class BoundedBuffer<E> extends BaseBoundedBuffer<E> {

  protected BoundedBuffer(int capacity) {
    super(capacity);
  }

  public synchronized void put(E elem) throws InterruptedException {
    while (isFull()) { // protects from false alarms - notifications made from this method
      wait(); // waits on this object monitor, until another thread notify[All]
    }
    doPut(elem);
    notifyAll(); // notify all threads waiting on 'this' monitor
  }

  public synchronized E take() throws InterruptedException {
    while (isEmpty()) { // protects from false alarms - notifications made from this method
      wait();
    }

    E elem = doTake();

    // Notify other threads waiting for space in the buffer.
    // As I understand, threads waiting in this method are also will be notified
    // and the loop will protect them from taking from empty buffer
    notifyAll();

    return elem;
  }
}
