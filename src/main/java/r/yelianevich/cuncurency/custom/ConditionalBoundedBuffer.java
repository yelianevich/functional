package r.yelianevich.cuncurency.custom;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConditionalBoundedBuffer<E> {
  private final Lock lock = new ReentrantLock();
  private final Condition notFull = lock.newCondition();
  private final Condition nonEmpty = lock.newCondition();

  // guarded by lock
  private final E[] buf;
  private int size;
  private int head;
  private int tail;

  @SuppressWarnings("unchecked")
  public ConditionalBoundedBuffer(int capacity) {
    this.buf = (E[]) new Object[capacity];
  }

  public void put(E elem) throws InterruptedException {
    lock.lock();
    try {
      while (size == buf.length) {
        notFull.await();
      }
      buf[tail++] = elem;
      if (tail == buf.length) {
        tail = 0;
      }
      ++size;
      nonEmpty.signal();
    } finally {
      lock.unlock();
    }
  }

  public E take() throws InterruptedException {
    lock.lock();
    try {
      while (size == 0) {
        nonEmpty.await();
      }
      E elem = buf[head];
      buf[head++] = null;
      if (head == buf.length) {
        head = 0;
      }
      --size;
      notFull.signal();
      return elem;
    } finally {
      lock.unlock();
    }
  }

}
