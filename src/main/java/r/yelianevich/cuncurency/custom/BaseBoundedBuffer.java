package r.yelianevich.cuncurency.custom;

public class BaseBoundedBuffer<E> {
  private E[] buf;
  private int head;
  private int tail;
  private int count;

  @SuppressWarnings("unchecked")
  protected BaseBoundedBuffer(int capacity) {
    this.buf = (E[]) new Object[capacity];
  }

  protected synchronized void doPut(E elem) {
    buf[tail++] = elem;
    tail = tail == buf.length ? 0 : tail;
    count++;
  }

  protected synchronized E doTake() {
    E elem = buf[head];
    buf[head] = null;
    head = ++head == buf.length ? 0 : head;
    count--;
    return elem;
  }

  public final synchronized boolean isFull() {
    return count == buf.length;
  }

  public final synchronized boolean isEmpty() {
    return count == 0;
  }
}
