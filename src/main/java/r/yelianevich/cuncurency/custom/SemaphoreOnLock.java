package r.yelianevich.cuncurency.custom;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * An exorcise to implement semaphore with a Lock.
 * Not like it's implemented in the library
 */
public class SemaphoreOnLock {
  private final Lock lock = new ReentrantLock();
  private final Condition permitsAvailable = lock.newCondition();

  // guarded by 'lock'
  private int permits;

  public SemaphoreOnLock(int initialPermits) {
    this.permits = initialPermits;
  }

  public void aquire() throws InterruptedException {
    lock.lock();
    try {
      while (permits <= 0) {
        permitsAvailable.await();
      }
      --permits;
    } finally {
      lock.unlock();
    }
  }

  public void release() {
    lock.lock();
    try {
      ++permits;
      permitsAvailable.signal();
    } finally {
      lock.unlock();
    }
  }

}
