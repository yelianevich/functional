package r.yelianevich.cuncurency.testing;

import java.util.concurrent.Semaphore;

/**
 * Thread-safe bounded buffer implementation
 *
 * @param <E>
 */
public class BoundedBuffer<E> {

  private final Semaphore availableSpace;
  private final Semaphore availableItems;
  private final int capacity;

  // both are guarded by the intrinsic lock
  private final E[] buffer;
  private int putPosition = 0;
  private int takePosition = 0;

  @SuppressWarnings("unchecked")
  public BoundedBuffer(int capacity) {
    this.capacity = capacity;
    this.buffer = (E[]) new Object[capacity];
    this.availableSpace = new Semaphore(capacity);
    this.availableItems = new Semaphore(0);
  }

  public void put(E element) throws InterruptedException {
    availableSpace.acquire();
    insert(element);
    availableItems.release();
  }

  private synchronized void insert(E element) {
    int i = putPosition;
    buffer[i++] = element;
    this.putPosition = i == capacity ? 0 : i;
  }

  public E take() throws InterruptedException {
    availableItems.acquire();
    E element = remove();
    availableSpace.release();
    return element;
  }

  private synchronized E remove() {
    int i = takePosition;
    E element = buffer[i];
    buffer[i++] = null;
    takePosition = i == capacity ? 0 : i;
    return element;
  }

  public boolean isEmpty() {
    return availableItems.availablePermits() == 0;
  }

  public boolean isFull() {
    return availableSpace.availablePermits() == 0;
  }

  public int getCapacity() {
    return capacity;
  }
}
