package r.yelianevich.cuncurency.testing;

public class BarierTimer implements Runnable {
  // all guarded by intrinsic lock
  private boolean started = false;
  private long start;
  private long end;

  public synchronized void run() {
    long time = System.nanoTime();
    if (!started) {
      start = time;
      started = true;
    } else {
      end = time;
    }
  }

  public synchronized long getTime() {
    return end - start;
  }

  public synchronized void clear() {
    started = false;
  }
}
