package r.yelianevich.cuncurency;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadStarvationDeadlock {

  public static void main(String[] args) throws Exception {
    // to make it work -- replace with executor that has at least 4 threads
    ExecutorService executor = Executors.newSingleThreadExecutor();
    // ExecutorService executor = Executors.newFixedThreadPool(4);

    String renderedPage = executor.submit(() -> {
      System.out.println("Trying to render header");
      System.out.println("it will deadlock -- waiting results of subtask");
      String header = executor.submit(new HtmlRenderTask<>("header")).get();

      System.out.println("Trying to render footer");
      String footer = executor.submit(new HtmlRenderTask<>("footer")).get();

      System.out.println("Trying to render body");
      String body = executor.submit(new HtmlRenderTask<>("body")).get();
      return header + footer + body;
    }).get();

    System.out.println("redered page: " + renderedPage);
  }




  static class HtmlRenderTask<T> implements Callable<T> {
    private T part;

    HtmlRenderTask(T part) {
      this.part = part;
    }

    @Override
    public T call() {
      return part;
    }
  }


}
