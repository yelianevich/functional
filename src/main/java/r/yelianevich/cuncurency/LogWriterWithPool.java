package r.yelianevich.cuncurency;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.LongAdder;
import java.util.stream.IntStream;

public class LogWriterWithPool {
  private final Logger log = LoggerFactory.getLogger(getClass());

  // thread factory provides control over threads creation and exception handling
  private final ExecutorService executor = Executors.newSingleThreadExecutor(new LogWriterThreadFactory());

  private final PrintStream writer;

  private LogWriterWithPool(PrintStream writer) {
    this.writer = writer;
  }

  public void stop() {
    try {
      ExecutorUtil.shutdownAndAwaitTermination(executor, 60, TimeUnit.SECONDS);
    } finally {
      writer.close();
    }
  }

  public void log(String msg) {
    try {
      executor.execute(new WriterTask(msg));
    } catch (RejectedExecutionException e) {
      // ignore rejection
    }
  }

  // message counter to emulate exception on the BOOM_COUNT message
  private LongAdder boomCounter = new LongAdder();

  private class WriterTask implements Runnable {
    private String msg;
    private static final int BOOM_COUNT = 10;

    private WriterTask(String msg) {
      this.msg = msg;
    }

    public void run() {
      boomCounter.increment();
      if (boomCounter.longValue() == BOOM_COUNT) {
        throw new IllegalStateException("Boom! Cannot write log message");
      }
      writer.println(msg);
    }
  }

  /**
   * A callback for threads that dies with uncaught exception.
   * Thread prints to System.err by default, which is almost always not what you want
   */
  class ExceptionHandler implements Thread.UncaughtExceptionHandler {

    @Override
    public void uncaughtException(Thread t, Throwable e) {
      log.error("Uncaught exception in log writer thread " + t.getName(), e);
    }
  }
  /**
   * Creates a daemon thread with exception handler
   */
  class LogWriterThreadFactory implements ThreadFactory {

    private Thread.UncaughtExceptionHandler handler = new ExceptionHandler();
    @Override
    public Thread newThread(Runnable r) {
      Thread thread = new Thread(r);
      thread.setUncaughtExceptionHandler(handler);
      thread.setName("log-writer-thread");
      thread.setDaemon(true);
      return thread;
    }
  }

  public static void main(String[] args) throws Exception {
    // make queue capacity very little to make queue.put block
    LogWriterWithPool logger = new LogWriterWithPool(System.out);

    // adding messages from a separate thread to make log and write to be fully asynchronous
    new Thread(() ->
        IntStream.range(0, 1000).forEach(i -> logger.log("Msg " + i))
    ).start();
    // do some 'processing'
    TimeUnit.MILLISECONDS.sleep(100);

    // having isShutdown and reservations counter makes log writer resilient to dead locks in queue.put()
    // when stop is requested at the time queue is full and queue.put blocks
    logger.stop();

    System.out.println("Logger stopped");
  }

}
