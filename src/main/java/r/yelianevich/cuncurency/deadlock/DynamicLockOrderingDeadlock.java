package r.yelianevich.cuncurency.deadlock;

/**
 * Deadlock-prone.
 * <p>
 * We do not control arguments, that are passed to the method.
 * It may well happen to transfer money in both ways concurrently:
 * Thread1 transfers A1 -> A2: ---> lock(A1) ---> cannot get A2 lock => deadlock
 * Thread2 transfers A2 -> A1: ---> lock(A2) ---> cannot get A1 lock => deadlock
 * <p>
 * The best way to fix it by using comparable unique key, that will order lock acquisition, like account id.
 */
public class DynamicLockOrderingDeadlock {

  public void deadlockTransfer(Account from, Account to) {
    synchronized (from) {
      synchronized (to) {
        System.out.println(String.format("Transfer money from %s to %s", from.id, to.id));
      }
    }
  }

  public void safeTransfer(Account from, Account to) {
    if (from.id == to.id) {
      throw new IllegalArgumentException("Cannot transfer money within account");
    }

    if (from.id < to.id) {
      synchronized (from) {
        synchronized (to) {
          System.out.println(String.format("Transfer money from %s to %s", from.id, to.id));
        }
      }
    }

    if (to.id < from.id) {
      synchronized (to) {
        synchronized (from) {
          System.out.println(String.format("Transfer money from %s to %s", from.id, to.id));
        }
      }
    }

  }

  static class Account {
    int id;
  }

}
