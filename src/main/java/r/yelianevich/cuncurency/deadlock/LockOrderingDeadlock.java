package r.yelianevich.cuncurency.deadlock;

/**
 * Deadlock-prone class. Methods try to acquire locks in mixed order: L-R and R-L
 *
 * A program will be free of lock-ordering deadlocks if all
 * threads acquire the locks they need in a fixed global order
 */
public class LockOrderingDeadlock {
  private final Object left = new Object();
  private final Object right = new Object();

  public void leftRight() {
    synchronized (left) {
      synchronized (right) {
        System.out.println("left-right locks");
      }
    }
  }

  public void rightLeft() {
    synchronized (right) {
      synchronized (left) {
        System.out.println("right-left locks");
      }
    }
  }

}
