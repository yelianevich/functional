package r.yelianevich.cuncurency;


import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class ThreadId {

  private static final AtomicInteger nextId = new AtomicInteger(0);

  private static final ThreadLocal<Integer> threadId = ThreadLocal.withInitial(nextId::getAndIncrement);

  public static Integer get() {
    return threadId.get();
  }

  public static void main(String[] args) {
    System.out.println(ThreadId.get());
    Stream<Thread> threads = Stream.generate(() -> new Thread(() -> System.out.println(ThreadId.get())));
    threads.limit(100).peek(Thread::start).forEach(t -> {
      try {
        t.join();
      } catch (InterruptedException e) {
        // restore interrupted thread status
        Thread.currentThread().interrupt();
      }
    });
  }

}
