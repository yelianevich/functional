package r.yelianevich.cuncurency;

import java.io.PrintStream;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

/**
 * Asynchronous writer of the logs.
 * Demonstrate correct implementation of lifecycle stop and start methods for thread-based service.
 */
public class LogWriter {
  private final PrintStream writer;
  private final BlockingQueue<String> queue;
  private WriterThread writerThread;

  // guarded by LogWriter.this
  private boolean isShutdown = false;

  // guarded by LogWriter.this
  private int reservations = 0;

  private LogWriter(PrintStream writer, int bufferCapacity) {
    this.writer = writer;
    this.queue = new LinkedBlockingDeque<>(bufferCapacity);
  }

  public void start() {
    writerThread = new WriterThread();
    writerThread.start();
  }

  public void stop() {
    synchronized (this) {
      isShutdown = true;
    }
    writerThread.interrupt();
  }

  public void log(String msg) {
    synchronized (this) {
      if (isShutdown) {
        return;
      }
      reservations++;
    }
    try {
      queue.put(msg);
    } catch (InterruptedException e) {
      // set interrupt flag back to be handled up the call stack
      Thread.currentThread().interrupt();
    }
  }

  private class WriterThread extends Thread {

    public void run() {
      try {
        while (true) {
          try {
            synchronized (LogWriter.this) {
              if (isShutdown && reservations == 0) {
                break;
              }
            }
            String msg = queue.take();
            synchronized (LogWriter.this) {
              reservations--;
            }
            writer.println(msg);
          } catch (InterruptedException e) {
            // ignore, to let finish with reservations
          }
        }
      } finally {
        writer.close();
      }
    }

  }

  public static void main(String[] args) throws Exception {
    // make queue capacity very little to make queue.put block
    LogWriter logger = new LogWriter(System.out, 2);

    logger.start();
    System.out.println("Logger started");

    // adding messages from a separate thread to make log and write to be fully asynchronous
    new Thread(() ->
        IntStream.range(0, 1000).forEach(i -> logger.log("Msg " + i))
    ).start();
    // do some 'processing'
    TimeUnit.MILLISECONDS.sleep(10);

    // having isShutdown and reservations counter makes log writer resilient to dead locks in queue.put()
    // when stop is requested at the time queue is full and queue.put blocks
    logger.stop();

    System.out.println("Logger stopped");
  }

}
