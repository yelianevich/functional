package r.yelianevich.cuncurency.puzzle;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicInteger;

public class ConcurrentPuzzleSolver<P, M> {
  private final Puzzle<P, M> puzzle;
  private final ExecutorService exec;
  private ValueLatch<Optional<Node<P, M>>> solutionNode = new ValueLatch<>();
  private final ConcurrentMap<P, Boolean> seen = new ConcurrentHashMap<>();

  // to detect when no solution exists
  private final AtomicInteger runningSolvers = new AtomicInteger(0);

  public ConcurrentPuzzleSolver(Puzzle<P, M> puzzle, int parallelism) {
    this.puzzle = puzzle;
    this.exec = Executors.newCachedThreadPool();

    // example of how to customize ThreadPoolExecutor after using a factory method
    if (exec instanceof ThreadPoolExecutor) {
      ThreadPoolExecutor executor = (ThreadPoolExecutor) exec;

      // silently rejects tasks when solution is found and shutdown method was already called
      executor.setRejectedExecutionHandler(new ThreadPoolExecutor.DiscardPolicy());
    }
  }

  public Optional<List<M>> solve() throws InterruptedException {
    try {
      P pos = puzzle.initialPosition();
      exec.submit(new SolverTask(pos, null, null));
      return solutionNode.get().map(Node::asMoveList);
    } finally {
      exec.shutdown();
    }
  }

  class SolverTask implements Runnable {
    Node<P, M> node;

    SolverTask(P position, M move, Node<P, M> prev) {
      node = new Node<>(position, move, prev);
      runningSolvers.incrementAndGet();
    }

    @Override
    public void run() {
      try {
        // check that solution is not found and position was not seen by concurrent solver
        if (solutionNode.isNotSet() && seen.putIfAbsent(node.position, true) == null) {
          if (puzzle.isGoal(node.position)) {
            solutionNode.set(Optional.of(node));
          } else {
            for (M move : puzzle.legalMoves(node.position)) {
              P newPosition = puzzle.move(node.position, move);
              exec.submit(new SolverTask(newPosition, move, node));
            }
          }
        }
      } finally {
        if (runningSolvers.decrementAndGet() == 0) {
          solutionNode.set(Optional.empty());
        }
      }
    }
  }

}
