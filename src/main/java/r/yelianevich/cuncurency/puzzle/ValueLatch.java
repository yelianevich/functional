package r.yelianevich.cuncurency.puzzle;

import java.util.concurrent.CountDownLatch;

/**
 * Thread-safe. Result-bearing latch to bring result of computation from one thread to another
 *
 * @param <T> a type of the value to transmit
 */
public class ValueLatch<T> {
  private T value;
  private final CountDownLatch isSetLatch = new CountDownLatch(1);

  public synchronized void set(T value) {
    if (isNotSet()) {
      this.value = value;
      isSetLatch.countDown();
    }
  }

  public boolean isNotSet() {
    return !isSet();
  }

  public boolean isSet() {
    return isSetLatch.getCount() == 0;
  }

  public T get() throws InterruptedException {
    isSetLatch.await();
    synchronized (this) {
      return value;
    }
  }

}
