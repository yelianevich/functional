package r.yelianevich.cuncurency.puzzle;

import java.util.LinkedList;
import java.util.List;

public class Node<P, M> {
  final P position;
  final M move;
  final Node<P, M> prev;

  /**
   * @param position that was reached after {@code move}
   * @param move move that brought to the position
   * @param prev previous solution node
   */
  public Node(P position, M move, Node<P, M> prev) {
    this.position = position;
    this.move = move;
    this.prev = prev;
  }

  public List<M> asMoveList() {
    LinkedList<M> solution = new LinkedList<>();
    for (Node<P, M> head = this; head.prev != null; head = head.prev) {
      solution.push(head.move);
    }
    return solution;
  }

}
