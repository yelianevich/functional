package r.yelianevich.cuncurency.puzzle;

import java.util.Set;

/**
 * 8.5.1 CIP - generic puzzle interface
 * http://www.puzzleworld.org/SlidingBlockPuzzles
 * @param <P> position type
 * @param <M> move type
 */
public interface Puzzle<P, M> {

  P initialPosition();

  boolean isGoal(P position);

  Set<M> legalMoves(P position);

  P move(P position, M move);
}
