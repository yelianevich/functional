package r.yelianevich.cuncurency.nonblocking;

import java.util.concurrent.atomic.AtomicReference;

/**
 * Treiber's non-blocking stack algorithm
 * @param <E>
 */
public class NonblockingStack<E> implements NonblockingBag<E> {
  private AtomicReference<Node<E>> top = new AtomicReference<>(null);

  public void push(E elem) {
    Node<E> newHead = new Node<E>(elem);
    Node<E> oldHead;
    do {
      oldHead = top.get();
      newHead.next = oldHead;
    } while (!top.compareAndSet(oldHead, newHead));
  }

  public E pop() {
    Node<E> newHead;
    Node<E> oldHead;
    do {
      oldHead = top.get();
      if (oldHead == null) {
        return null;
      }
      newHead = oldHead.next;
    } while (!top.compareAndSet(oldHead, newHead));
    return oldHead.elem;
  }

  @Override
  public void put(E elem) {
    push(elem);
  }

  @Override
  public E get() {
    return pop();
  }

  private static class Node<E> {
    final E elem;
    Node<E> next;
    public Node(E elem) {
      this.elem = elem;
    }
  }
}
