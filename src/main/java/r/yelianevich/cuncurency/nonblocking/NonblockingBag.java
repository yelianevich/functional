package r.yelianevich.cuncurency.nonblocking;

public interface NonblockingBag<E> {

  void put(E elem);

  E get();
}
