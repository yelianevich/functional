package r.yelianevich.cuncurency.nonblocking;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Nonblocking linked queue algorithm (Michael & Scott).
 * Used in {@link ConcurrentLinkedQueue}
 *
 * @param <E>
 */
public class NonblockingLinkedQueue<E> implements NonblockingBag<E> {

  private Node<E> dummy = new Node<>(null, null);

  private AtomicReference<Node<E>> head = new AtomicReference<>(dummy);
  private AtomicReference<Node<E>> tail = new AtomicReference<>(dummy);

  @Override
  public void put(E elem) {
    // create a node we are trying to insert
    Node<E> newNode = new Node<>(elem, null);

    while (true) {
      // get tail information
      Node<E> curTail = tail.get();
      Node<E> curTailNext = curTail.next.get();

      if (curTail == tail.get()) { // not modified by D
        if (curTailNext == null) {
          // queue in quiescent state - try to add new node
          if (curTail.next.compareAndSet(null, newNode)) {
            // new node was inserted, move tail to it
            tail.compareAndSet(curTail, newNode);                  // (D)
            return;
          }
        } else {
          // queue in intermediate state, try advance tail pointer
          tail.compareAndSet(curTail, curTailNext);
        }
      }
    }
  }

  @Override
  public E get() {
    return take();
  }

  public E take() {
    while (true) {
      Node<E> curHead = head.get();
      Node<E> curTail = tail.get();
      Node<E> next = curHead.next.get();

      if (curHead == head.get()) { // make sure head is consistent
        if (curHead == curTail) { // empty or tail is falling behind?
          if (next == null) {
            // queue is empty
            return null;
          } else {
            // tail is falling behind - try to advance it
            tail.compareAndSet(curTail, next);
          }
        } else {
          if (head.compareAndSet(curHead, next)) {
            return next.item;
          }
        }
      }
    }
  }

  private static class Node<E> {
    final E item;
    final AtomicReference<Node<E>> next;

    public Node(E item, Node<E> next) {
      this.item = item;
      this.next = new AtomicReference<>(next);
    }
  }

  public static void main(String[] args) {
    NonblockingLinkedQueue<Integer> queue = new NonblockingLinkedQueue<>();
    queue.put(1);
    queue.put(2);
    queue.put(3);
    System.out.println(queue.take());
    System.out.println(queue.take());
    System.out.println(queue.take());
    System.out.println(queue.take());
  }
}
