package r.yelianevich.cuncurency.nonblocking;

import java.util.concurrent.ConcurrentLinkedQueue;

public class ConcurrentLinkedQueueAdapter<E> implements NonblockingBag<E> {

  private final ConcurrentLinkedQueue<E> queue = new ConcurrentLinkedQueue<>();

  @Override
  public void put(E elem) {
    queue.add(elem);
  }

  @Override
  public E get() {
    return queue.poll();
  }
}
