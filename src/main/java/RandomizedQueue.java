import edu.princeton.cs.algs4.StdRandom;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * A randomized queue is similar to a stack or queue, except
 * that the item removed is chosen uniformly at random from items in the data structure.
 */
public class RandomizedQueue<Item> implements Iterable<Item> {

  private static final int DEFAULT_CAPACITY = 4;

  private Item[] items;
  private int size = 0;
  private int capacity = DEFAULT_CAPACITY;

  /**
   * construct an empty randomized queue
   */
  public RandomizedQueue() {
    this.items = createArray(capacity);
  }

  @SuppressWarnings("unchecked")
  private Item[] createArray(int capacity) {
    return (Item[]) new Object[capacity];
  }

  /**
   * is the randomized queue empty?
   */
  public boolean isEmpty() {
    return size == 0;
  }

  /**
   * return the number of items on the randomized queue
   */
  public int size() {
    return size;
  }

  /**
   * add the item
   */
  public void enqueue(Item item) {
    if (item == null) throw new IllegalArgumentException("null item");
    ensureCapacity();
    int pos = StdRandom.uniform(0, size + 1);
    if (pos == size) {
      items[size++] = item;
    } else {
      items[size++] = items[pos];
      items[pos] = item;
    }
  }

  private void ensureCapacity() {
    if (size == capacity) {
      // grow
      capacity = capacity + (capacity >> 1);
      resize();
    } else if (capacity > DEFAULT_CAPACITY && size <= (capacity >> 1)) {
      // shrink
      capacity = capacity - (capacity >> 1);
      resize();
    }
  }

  private void resize() {
    Item[] newItems = createArray(capacity);
    System.arraycopy(items, 0, newItems, 0, size);
    items = newItems;
  }

  /**
   * remove and return a random item
   */
  public Item dequeue() {
    if (isEmpty()) throw new NoSuchElementException("empty queue");
    Item r = items[--size];
    items[size] = null;
    ensureCapacity();
    return r;
  }

  /**
   * return a random item (but do not remove it)
   */
  public Item sample() {
    if (isEmpty()) throw new NoSuchElementException("empty queue");
    return items[StdRandom.uniform(0, size)];
  }

  /**
   * return an independent iterator over items in random order
   */
  public Iterator<Item> iterator() {
    return new RandQueueIterator();
  }

  private class RandQueueIterator implements Iterator<Item> {
    private final Item[] itItems;
    private int curr = 0;

    public RandQueueIterator() {
      this.itItems = createArray(size);
      System.arraycopy(items, 0, itItems, 0, size);
      StdRandom.shuffle(itItems);
    }

    @Override
    public boolean hasNext() {
      return curr < itItems.length;
    }

    @Override
    public Item next() {
      if (curr >= itItems.length) throw new NoSuchElementException("iterator has no more elements");
      return itItems[curr++];
    }
  }

}
