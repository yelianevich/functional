package r.yelianevich.algorithms

import scala.annotation.tailrec

object BinarySearchScala {

  def indexOf[T: Ordering](a: Array[T], key: T): Int = {
    val order = implicitly[Ordering[T]]
    var lo = 0
    var hi = a.length - 1
    var index = -1
    while (lo <= hi) {
      val mid = lo + (hi - lo) / 2
      if (order.lt(key, a(mid))) hi = mid - 1
      else if (order.gt(key, a(mid))) lo = mid + 1
      else {
        index = mid
        lo = hi + 1
      }
    }
    index
  }

  def indexOfRec(a: Array[Int], key: Int): Int = {

    @tailrec
    def index(lo: Int, hi: Int): Int =
      if (lo > hi) -1
      else {
        val mid: Int = lo + (hi - lo) / 2
        if (key < a(mid)) index(lo, mid - 1)
        else if (key > a(mid)) index(mid + 1, hi)
        else mid
      }

    index(0, a.length - 1)
  }

  def main(args: Array[String]): Unit = {
    println("start!")
    val a = Array(-6, 1, 2, 3, 4, 5, 10, 16)

    require(indexOf(a, 10) == 6)
    require(indexOf(a, 9999) == -1)
    require(indexOf(a, -6) == 0)
    require(indexOf(a, 4) == 4)

    require(indexOfRec(a, 10) == 6)
    require(indexOfRec(a, 9999) == -1)
    require(indexOfRec(a, -6) == 0)
    require(indexOfRec(a, 4) == 4)

    println("done!")

  }

}
