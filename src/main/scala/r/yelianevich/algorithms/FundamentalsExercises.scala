package r.yelianevich.algorithms

object FundamentalsExercises {

  def main(args: Array[String]): Unit = {

    println((0 + 15) / 2)
    val v = 2.0e-6 * 100000000.1
    println(v)
    println(1 + 2 + 3 + "4")
    println(1 + 2 + 3 + 4.0)

    println(binString(787676545))


  }

  def binString(i: Int): String = {
    require(i >= 0)
    val zeroBitMask = 1
    var t = i
    val sb = new StringBuilder()
    do {
      val bit = t & zeroBitMask
      sb append bit
      t >>>= 1
    } while (t != 0)
    sb.reverseContents.result
  }

}
