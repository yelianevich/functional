package r.yelianevich.scala99

import java.util.NoSuchElementException

import scala.annotation.tailrec

/**
  * S-99: Ninety-Nine Scala Problems
  * [[http://aperiodic.net/phil/scala/s-99/]]
  */
object Lists {

  /**
    * P01
    * `xs.last`
    *
    * Less elegant solution {{{
    * xs match {
    *   case head :: Nil => head
    *   case _ :: tail => last(tail)
    *   case _ => throw new NoSuchElementException
    * }
    * }}}
    */
  def last[E](xs: List[E]): E =
    xs match {
      case Nil => throw new NoSuchElementException
      case init :+ last => last
    }

  /**
    * P02
    * `xs.init.last`
    */
  @tailrec
  def penultimate[E](xs: List[E]): E =
    xs match {
      case target :: last :: Nil => target
      case head :: tail => penultimate(tail)
      case _ => throw new NoSuchElementException
    }

  /**
    * P03
    * `xs(n)`
    */
  @tailrec
  def nth[E](n: Int, xs: List[E]): E =
    (n, xs) match {
      case (0, head :: tail) => head
      case (i, head :: tail) => nth(i - 1, tail)
      case _ => throw new NoSuchElementException
    }

  /**
    * P04
    * `xs.length`
    * `xs.foldLeft(0) { case (acc, _) => acc + 1 }`
    */
  def length[E](xs: List[E]): Int = {
    // more complex, but tailrec solution
    @tailrec
    def loop(len: Int, l: List[E]): Int =
      l match {
        case Nil => len
        case head :: tail => loop(len + 1, tail)
      }

    loop(len = 0, xs)
  }

  /**
    * P05
    * `xs.reverse`
    * `xs.foldLeft(List.empty) { case (acc, elem) => elem :: acc }`
    */
  def reverse[E](xs: List[E]): List[E] = {
    @tailrec
    def reverseRec(result: List[E], curList: List[E]): List[E] =
      curList match {
        case Nil => result
        case head :: tail => reverseRec(head :: result, tail)
      }

    reverseRec(List.empty, xs)
  }

  /** P06 */
  def isPalindrome[E](xs: List[E]): Boolean =
    xs == xs.reverse

  /**
    * P07
    * {{{
    *   xs match {
    *       case Nil => Nil
    *       case head :: tail =>
    *         head match {
    *           case l: List[Any] => flatten(l) ::: flatten(tail)
    *           case x => x :: flatten(tail)
    *         }
    *     }
    * }}}
    */
  def flatten(xs: List[Any]): List[Any] =
    xs.flatMap {
      case list: List[Any] => flatten(list)
      case elem => List(elem)
    }

  /**
    * P08
    * Simple recursive (inefficient)
    * {{{
    *   xs match {
    *       case Nil => Nil
    *       case head :: tail => head :: compress(tail.dropWhile(_ == head))
    *   }
    * }}}
    * Tail recursive (ugly)
    * {{{
    *   @tailrec
    *     def compressLoop(compressed: List[E], curList: List[E]): List[E] =
    *       curList match {
    *         case Nil => compressed
    *         case head :: tail => compressLoop(compressed :+ head, tail.dropWhile(_ == head))
    *       }
    *
    *     compressLoop(List.empty, xs)
    *   }
    * }}}
    *
    * Here is a nice functional way to compress sorted list
    */
  def compress[E](xs: List[E]): List[E] = {
    xs.foldRight(List.empty[E]) {
      case (elem, compressed) =>
        if (compressed.isEmpty || compressed.head != elem) elem :: compressed
        else compressed
    }
  }

  /**
    * P09
    *
    * In this alternative implementation method `span` is pretty interesting.
    * {{{
    *   def pack[A](ls: List[A]): List[List[A]] =
    *     if (ls.isEmpty) List.empty
    *     else {
    *       val (packed, next) = ls.span(_ == ls.head)
    *       if (next == Nil) List(packed)
    *       else packed :: pack(next)
    *     }
    * }}}
    */
  def pack[E](xs: List[E]): List[List[E]] =
    xs.foldRight(List.empty[List[E]]) {
      case (elem, packed) =>
        if (packed.isEmpty || packed.head.head != elem) List(elem) :: packed
        else (elem :: packed.head) :: packed.tail
    }

  /** P10 */
  def encode[E](xs: List[E]): List[(Int, E)] =
    pack(xs).map(dups => dups.length -> dups.head)

  /** P11 (*) Modified run-length encoding. */
  def encodeModified[E](xs: List[E]): List[Any] =
    pack(xs).map {
      case List(e) => e
      case dups => dups.length -> dups.head
    }

  /** P11' */
  def encodeWithEither[E](xs: List[E]): List[Either[(Int, E), E]] =
    pack(xs).map {
      case List(e) => Right(e)
      case dups => Left(dups.length -> dups.head)
    }

  /** P12 (**) Decode a run-length encoded list. */
  def decode[E](xs: List[(Int, E)]): List[E] =
    xs.flatMap { case (count, elem) => List.fill(count)(elem) }

  /** P13 (**) Run-length encoding of a list (direct solution). */
  def encodeDirect[E](xs: List[E]): List[(Int, E)] =
    if (xs.isEmpty) List.empty
    else {
      val (prefix, suffix) = xs.span(_ == xs.head)
      (prefix.length, prefix.head) :: encodeDirect(suffix)
    }

  /** P14 (*) Duplicate the elements of a list. */
  def duplicate[E](xs: List[E]): List[E] =
    xs.flatMap(x => List(x, x))

  /** P15 (**) Duplicate the elements of a list a given number of times. */
  def duplicateN[E](n: Int, xs: List[E]): List[E] =
    xs.flatMap(x => List.fill(n)(x))

  /** P16 (**) Drop every Nth element from a list. */
  def drop[E](n: Int, xs: List[E]): List[E] =
    xs.zipWithIndex
        .withFilter { case (_, i) => (i + 1) % n != 0 }
        .map { case (elem, _) => elem }

  /**
    * P17 (*) Split a list into two parts. 
    *
    * xs.splitAt(n)
    **/
  def split[E](n: Int, xs: List[E]): (List[E], List[E]) =
    xs.take(n) -> xs.drop(n)

  /**
    * P18 (**) Extract a slice from a list.
    *
    * xs.slice(i, k)
    * xs.drop(i).take(k - i)
    */
  def slice[E](i: Int, k: Int, xs: List[E]): List[E] = {
    @tailrec
    def sliceLoop(j: Int, result: List[E], current: List[E]): List[E] = {
      current match {
        case Nil => result
        case head :: tail =>
          if (j >= k) result
          else if (j >= i) sliceLoop(j + 1, head :: result, tail)
          else sliceLoop(j + 1, result, tail)
      }
    }

    sliceLoop(0, List.empty, xs).reverse
  }

  /**
    * P19 (**) Rotate a list N places to the left.
    **/
  def rotate[E](n: Int, xs: List[E]): List[E] = ???

}
