package r.yelianevich.cats

// Define a very simple JSON AST
sealed trait Json

final case class JsObject(get: Map[String, Json]) extends Json

final case class JsString(get: String) extends Json

final case class JsNumber(get: Double) extends Json

case object JsNull extends Json


// The "serialize to JSON" behaviour is encoded in this trait
// type class
trait JsonWriter[A] {
  def write(value: A): Json
}


final case class Person(name: String, email: String)


// type instances or implicit values
object JsonWriterInstances {

  implicit val stringWriter: JsonWriter[String] = JsString.apply

  implicit val personWriter: JsonWriter[Person] = person =>
    JsObject(Map(
      "name" -> JsString(person.name),
      "email" -> JsString(person.email)
    ))

  // implicit def: constructs writer based on the other writer, solved by recursive implicit resolution
  // parameter is implicit! without implicit it's an implicit conversion, which is a bad practice
  implicit def optionalWriter[A](implicit writer: JsonWriter[A]): JsonWriter[Option[A]] = {
    // matching anonymous function
    case Some(value) => writer.write(value)
    case None => JsNull
  }
}

// There are 2 common patterns how to use type instances

// 1) interface object - the simplest way. Creates an interface that uses a type class
object Json {
  def toJson[A](value: A)(implicit writer: JsonWriter[A]): Json = {
    writer.write(value)
  }
}

// 2) extension methods. Cats refers to them as "syntax".
// defines extension methods that use JsonWriter implicit instances
object JsonSyntax {
  implicit class JsonWriterOps[A](value: A) {
    def toJson(implicit writer: JsonWriter[A]): Json = {
      writer.write(value)
    }
  }
}

object JsonDemo {
  def main(args: Array[String]): Unit = {
    import JsonWriterInstances._

    // uses JsonWriterInstances implicitly
    println(Json.toJson(Person("John", "john.smith@gmail.com")))

    import JsonSyntax._ // uses extension method; needs JsonWriterInstances to satisfy required parameters
    println(Person("John", "john.smish@gmail.com").toJson)

    // we can summon any implicit value using generic type interface from standard scala library
    val writer: JsonWriter[String] = implicitly[JsonWriter[String]]
    println(writer.write("str"))

    println(Json.toJson(Option("optional String")))
    println(Json.toJson(Option.empty[String])) // None doesn't work
  }
}