package r.yelianevich.cats

// these 2 just import everything: type classes, instances, syntax

import cats._
import cats.implicits._

object MonoidCats {
  def main(args: Array[String]): Unit = {
    println(
      // apply in the companion object allows to summon the correct instance
      Monoid[String].combine( // Monoid.apply[String].combine(
        Semigroup[String].combine("Hello, ", "Cats"),
        Monoid[String].empty
      )
    )

    println(
      Monoid[Option[Int]].combine(Some(1), Some(2))
    )
  }
}
