package r.yelianevich.cats

object ShowDemo {

  import cats.Show
  import cats.instances.int._
  import cats.instances.string._

  // get implicit show explicitly :)
  private val showInt: Show[Int] = Show.apply[Int]
  private val showString: Show[String] = Show.apply[String]

  case class SomeClass(x: Int)

  // let's define custom show for our SomeClass
  implicit val customSomeClassShow: Show[SomeClass] =
    // SAM (single abstract method) coudl be used to create custom instance
    Show.show(someObj => s"SomeClass(x=${someObj.x})");

  //(someObj: SomeClass) => s"SomeClass(x=${someObj.x})"


  def main(args: Array[String]): Unit = {
    // check explicit show instances
    assert(showInt.show(123) == "123")
    assert(showString.show("abc") == "abc")

    // use syntax to import extension methods
    import cats.syntax.show._
    println(123.show)
    println("234".show)

    // import all implicit type class instances and all of the syntax in one import
    import cats.implicits._
    println(123.asLeft)

    val someObj = SomeClass(505)
    // Ops methods are available; type class (Show) instance was picked up by the syntax
    println(Show[SomeClass].show(someObj))
  }
}
