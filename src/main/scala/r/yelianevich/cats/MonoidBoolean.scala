package r.yelianevich.cats

import cats.Monoid

object MonoidBoolean {

  def main(args: Array[String]): Unit = {

    implicit val or: Monoid[Boolean] = new Monoid[Boolean] {
      override def empty: Boolean = false
      override def combine(x: Boolean, y: Boolean): Boolean = x || y
    }

    idLaw(or)
    associativityLaw(or)

    // last 2 rows of the truth table demonstrates that false is an identity value
    // arg1 arg2 res
    //----------------
    // true true false
    // true false true
    // false true true
    // false false false
    implicit val xor: Monoid[Boolean] = new Monoid[Boolean] {
      override def empty: Boolean = false
      override def combine(x: Boolean, y: Boolean): Boolean = x ^ y
    }

    idLaw(xor)
    associativityLaw(xor)

    // similarly to xor - xnor could be defined
    implicit val xnor: Monoid[Boolean] = new Monoid[Boolean] {
      override def empty: Boolean = true
      override def combine(x: Boolean, y: Boolean): Boolean = !(x ^ y)
    }

    idLaw(xnor)
    associativityLaw(xnor)

    implicit val and: Monoid[Boolean] = new Monoid[Boolean] {
      override def empty: Boolean = true
      override def combine(x: Boolean, y: Boolean): Boolean = x && y
    }

    idLaw(and)
    associativityLaw(and)
  }

  private def idLaw(m: Monoid[Boolean]): Unit = {
    require(m.combine(m.empty, true) == m.combine(true, m.empty))
    require(m.combine(m.empty, false) == m.combine(false, m.empty))
  }

  private def associativityLaw(m: Monoid[Boolean]): Unit = {
    require(m.combine(m.combine(m.empty, true), true) == m.combine(true, m.combine(m.empty, true)))
    require(m.combine(m.combine(m.empty, false), false) == m.combine(false, m.combine(m.empty, false)))
  }
}
