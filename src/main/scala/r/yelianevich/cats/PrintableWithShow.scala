package r.yelianevich.cats

import cats.Show
import cats.instances.int._
import cats.instances.string._
import cats.syntax.show._

object ShowInstances {
  implicit val printableWithCats: Show[Cat] = Show.show(cat => {
    val name = cat.name.show
    val age = cat.age.show
    val color = cat.color.show
    s"$name is a $age year-old $color cat."
  })
}

object PrintableWithShowDemo {
  def main(args: Array[String]): Unit = {
    val cat = Cat("Chicken", 3, "Yellow")

    import ShowInstances._

    val showCat: Show[Cat] = Show.apply
    showCat.show(cat)

    import cats.syntax.show._
    println(Cat("Garfield", 41, "ginger and black").show)
  }
}