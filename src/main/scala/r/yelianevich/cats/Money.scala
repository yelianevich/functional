package r.yelianevich.cats


sealed trait Currency
object Currency {
  case object USD extends Currency
  case object EUR extends Currency
  case object GBP extends Currency
}

case class Money(val value: Double, val currency: Currency)

object MoneySyntax {
  // values classes: https://docs.scala-lang.org/overviews/core/value-classes.html
  implicit class RichMoney(val value: Double) extends AnyVal {
    def dollar: Money = Money(value, Currency.USD)
    def euro: Money = Money(value, Currency.EUR)
    def pound: Money = Money(value, Currency.GBP)
  }
}

object MoneyDemo {
  def main(args: Array[String]): Unit = {
    import MoneySyntax._

    println(13.pound)
    println(12.dollar)
    println(11.euro)
  }
}
