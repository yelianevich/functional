package r.yelianevich.cats

case class Student(id: Int, name: String)
case class Professor(id: Int, name: String, degree: String)

trait Printer[A] {
  def print(a: A): String
}

object PrinterInstances {
  implicit val studentPrinter: Printer[Student] =
    (a: Student) => s"Student(id=${a.id}, name=${a.name})"

  implicit val professorPrinter: Printer[Professor] =
    (a: Professor) => s"Professor(id=${a.id}, name=${a.name}, degree=${a.degree})"
}

object PrinterSyntax {
  implicit class PrinterOps[A](a: A) {
    def print(implicit printer: Printer[A]): String = printer.print(a)
  }
}

object PrinterDemo {

  def main(args: Array[String]): Unit = {
    import PrinterInstances._
    import PrinterSyntax._

    println(Student(1, "John Smith").print)
    println(Professor(1, "Donald Knuth", "Superman").print)
  }
}
