package r.yelianevich.cats

import java.util.Date

object EqDemo {

  def main(args: Array[String]): Unit = {
    import cats.Eq
    import cats.instances.int._

    val eqInt = Eq[Int]

    require(eqInt.eqv(123, 123))
    require(eqInt.neqv(123, 1))

    // compile error: type mismatch
    // require(eqInt.eqv(123, "123"))

    import cats.syntax.eq._
    require(123 === 123)
    require(123.eqv(123))
    require(123 =!= 1)
    require(123.neqv(1))

    // type mismatch compiler error
    // require(123 === "123")

    import cats.instances.option._
    import cats.syntax.option._ // 1.some and none

    // require(Some(1) =!= None) - types do not match
    // require(None =!= Option(1)) - cannot resolve symbol
    require(Option(1) =!= Option.empty)
    require(Option(1) =!= Some(10))

    // special syntax
    require(1.some =!= 10.some)
    require(1.some =!= none)
    require(1.some === 1.some)

    import cats.instances.long._

    implicit val dateEq: Eq[Date] =
      Eq.instance((date1, date2) => date1.getTime === date2.getTime)

    val x = new Date();
    val y = new Date(x.getTime + 1);
    require(x === x)
    require(x =!= y)

    // 1.5.5 Exercise: Equality, Liberty, and Felinity
    implicit val catEq: Eq[Cat] = Eq.instance((cat1, cat2) =>
      if (cat1.eq(cat2)) true // same ref
      else if (cat1 == null || cat2 == null) false
      else {
        (cat1.name === cat2.name) &&
          (cat1.age === cat2.age) &&
          (cat1.color === cat2.color)
      }
    )

    val cat0: Cat = null
    val cat1 = Cat("Garfield", 38, "orange and black")
    val cat2 = Cat("Heathcliff", 33, "orange and black")

    require(cat1 =!= cat2)
    require(cat1 === cat1)
    require(cat0 =!= cat1)
    require(cat0 === cat0)

    val optionCat1 = Option(cat1)
    val optionCat2 = Option.empty[Cat]
    require(optionCat1 === optionCat1)
    require(optionCat1 =!= optionCat2)
  }
}
