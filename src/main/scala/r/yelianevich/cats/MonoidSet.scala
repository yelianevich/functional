package r.yelianevich.cats

import cats.Monoid
import cats.Semigroup

object MonoidSet {

  def main(args: Array[String]): Unit = {

    // we use def to make it generic with a type parameter A
    implicit def unionAll[A]: Monoid[Set[A]] = new Monoid[Set[A]] {
      override def empty: Set[A] = Set.empty

      override def combine(a: Set[A], b: Set[A]): Set[A] = a union b
    }

    val unionInt: Monoid[Set[Int]] = Monoid.apply[Set[Int]]

    // id check
    require(
      unionInt.combine(Set.empty, Set(1, 2)) == unionInt.combine(Set(1, 2), Set.empty)
    )

    // associativity check
    require(
      unionInt.combine(unionInt.combine(Set.empty, Set(1, 2)), Set(3))
        == unionInt.combine(Set.empty, unionInt.combine(Set(1, 2), Set(3)))
    )

    // has no identity value
    implicit def intersectSemi[A]: Semigroup[Set[A]] = new Semigroup[Set[A]] {
      override def combine(x: Set[A], y: Set[A]): Set[A] = x intersect y
    }

    // associativity check
    require(
      intersectSemi.combine(intersectSemi.combine(Set.empty, Set(1, 2)), Set(2))
        == intersectSemi.combine(Set.empty, intersectSemi.combine(Set(1, 2), Set(2)))
    )

  }
}
