package r.yelianevich.cats

// type class
trait Area[A] {
  def area(a: A): Double
}

object Area {
  // similarly to Monoid.apply
  def apply[A](implicit ev: Area[A]): Area[A] = ev
}

case class Circle(radius: Double)
case class Rectangle(width: Double, length: Double)

// type class instances
object AreaInstances {
  implicit val circleArea: Area[Circle] = (a: Circle) => Math.PI * a.radius * a.radius
  implicit val rectangleArea: Area[Rectangle] = (a: Rectangle) => a.width * a.length
}

// an interface object
object AreaCalculator {
  def areaOf[A : Area](shape: A): Double = {
    Area[A].area(shape)
  }
}

// extension method for shapes that have implicit area instance
// cats refer to this as "syntax"
object ShapeAreaSyntax {
  implicit class ShapeAreaOps[A](a: A) {
    def areaOf(implicit area: Area[A]): Double = area.area(a)
  }
}

object MainArea {

  import AreaInstances._
  import ShapeAreaSyntax._

  def main(args: Array[String]): Unit = {
    val circle = Circle(2)
    val rectangle = Rectangle(2, 3)

    // using "syntax" classes
    println("Circle area = " + circle.areaOf)
    println("Rectangle area = " + rectangle.areaOf)

    // or

    // using interface objects
    println("Circle area = " + AreaCalculator.areaOf(circle))
    println("Rectangle area = " + AreaCalculator.areaOf(rectangle))
  }
}
