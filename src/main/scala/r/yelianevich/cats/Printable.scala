package r.yelianevich.cats

trait Printable[A] {
  def format(v: A): String
}

object Printable {
  def format[A](value: A)(implicit printer: Printable[A]): String =
    printer.format(value)

  def print[A](value: A)(implicit printer: Printable[A]): Unit =
    println(printer.format(value))
}

object PrintableInstances {
  implicit val printableInt: Printable[Int] = v => v.toString

  implicit val printableString: Printable[String] = s => s

  implicit val printableCat: Printable[Cat] = cat => s"${cat.name} is a ${cat.age} year-old ${cat.color} cat."
}

final case class Cat(name: String, age: Int, color: String)

object PrintableSyntax {
  implicit class PrintableOps[A](value: A) {

    def format(implicit printable: Printable[A]): String =
      printable.format(value)

    def print(implicit printable: Printable[A]): Unit =
      println(format(printable))
  }
}

object PrintableDemo {

  def main(args: Array[String]): Unit = {
    import PrintableInstances._

    val cat = Cat("Chicken", 3, "Yellow")

    Printable.print(cat)

    import PrintableSyntax._
    Cat("Garfield", 41, "ginger and black").print
    println("Hey, " + cat.format)
  }
}