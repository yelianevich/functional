package r.yelianevich.cats

import cats.kernel.Semigroup
import cats.kernel.Monoid

import cats.instances.int._

object CatsSemigroupMonoid {

  implicit class CombineOps[A](collection: Seq[A]) {
    implicit def myCombine(implicit combiner: Monoid[A]): A = {
      combiner.combineAll(collection)
    }
  }

  implicit val stringMonoid: Monoid[String] = Monoid.instance("", (a, b) => a + b)
  implicit val intMonoid: Monoid[Int] = Monoid.instance(0, (a, b) => a + b)

  def main(args: Array[String]): Unit = {
    val onePlusTwo = Semigroup[Int].combine(1, 2)
    println(onePlusTwo)

    val xs = List(1, 2, 3)
    println(xs.myCombine)
  }
}
