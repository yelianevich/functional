package r.yelianevich.fcmgg

import breeze.linalg.{DenseMatrix, det, inv}

import scala.annotation.tailrec


class FcmGgAlgorithm {
  val MaxError = 0.001
  val IterationGuard = 1000
  type Entry = (Double, Double)
  type CovarianceMatrixes = (DenseMatrix[Double], DenseMatrix[Double])

  def fcmGg(data: Map[Entry, Entry], epsilon: Double): Map[Entry, Entry] = {
    @tailrec
    def fcmGgLoop(data: Map[Entry, Entry], error: Double, iteration: Int): Map[Entry, Entry] = {
      if (error >= epsilon && iteration < IterationGuard) {
        val cores = defineClusterCores(data)
        val covarianceMatrix = defineCovarianceMatrix(data, cores)
        val coresProb = defineCoresProb(data)

        val updatedData = updatMembershipMatrix(data, cores, coresProb, covarianceMatrix)
        val calcError = maxDifference(data.values, updatedData.values)
        fcmGgLoop(updatedData, calcError, iteration + 1)
      } else {
        data
      }
    }

    fcmGgLoop(data, 1, 0)
  }

  def updatMembershipMatrix(data: Map[Entry, Entry], cores: (Entry, Entry),
      coreProbability: Entry, covarianceMatrix: CovarianceMatrixes): Map[Entry, Entry] = {
    data.foldLeft[Map[Entry, Entry]](Map[Entry, Entry]()) { (m, e) =>
      val normXCore1 = norm(e._1, cores._1, coreProbability._1, covarianceMatrix._1)
      val normXCore2 = norm(e._1, cores._2, coreProbability._2, covarianceMatrix._2)
      val u1 = math.pow(normXCore1 / normXCore1, 2) + math.pow(normXCore1 / normXCore2, 2)
      val u2 = math.pow(normXCore2 / normXCore1, 2) + math.pow(normXCore2 / normXCore2, 2)
      m + (e._1 -> (1 / u1, 1 / u2))
    }
  }

  def defineClusterCores(data: Map[Entry, Entry]): (Entry, Entry) = {
    val num1 = data.foldLeft[Entry]((0, 0))(
      (acc, data) => (acc._1 + data._1._1 * data._2._1, acc._2 + data._1._2 * data._2._1))
    val num2 = data.foldLeft[Entry]((0, 0))(
      (acc, data) => (acc._1 + data._1._1 * data._2._2, acc._2 + data._1._2 * data._2._2))
    val denom = data.foldLeft[Entry](0, 0)(
      (acc, p) => (acc._1 + p._2._1, acc._2 + p._2._2))
    val core1 = (num1._1 / denom._1, num1._2 / denom._1)
    val core2 = (num2._1 / denom._2, num2._2 / denom._2)
    (core1, core2)
  }

  def defineCoresProb(data: Map[Entry, Entry]): Entry = {
    val num1 = data.values.foldLeft[Double](0)((acc, data) => acc + data._1)
    val num2 = data.values.foldLeft[Double](0)((acc, data) => acc + data._2)
    val denom = data.values.foldLeft[Double](0)((acc, data) => acc + data._1 + data._2)
    (num1 / denom, num2 / denom)
  }

  def defineCovarianceMatrix(data: Map[Entry, Entry], cores: (Entry, Entry)): CovarianceMatrixes = {
    val numerator1 = data.foldLeft[DenseMatrix[Double]](DenseMatrix.zeros[Double](2, 2)) {
      (acc, data) =>
        val vectorData = Array[Double](data._1._1 - cores._1._1, data._1._2 - cores._1._2)
        val vector = DenseMatrix(vectorData)
        val vectorT = vector.t
        val vectorMul = vectorT * vector
        val covarianceMatrix = vectorMul * data._2._1
        acc += covarianceMatrix
    }
    val numerator2 = data.foldLeft[DenseMatrix[Double]](DenseMatrix.zeros[Double](2, 2)) {
      (acc, data) =>
        val vectorData = Array[Double](data._1._1 - cores._2._1, data._1._2 - cores._2._2)
        val xMinusCore = DenseMatrix(vectorData)
        val xMinusCoreT = xMinusCore.t
        val vectorMul = xMinusCoreT * xMinusCore
        val covarianceMatrix = vectorMul * data._2._2
        acc += covarianceMatrix
    }
    val denom = data.foldLeft[Entry](0, 0)(
      (acc, p) => (acc._1 + p._2._1, acc._2 + p._2._2))
    (numerator1 / denom._1, numerator2 / denom._2)
  }

  def norm(v: Entry, core: Entry, coreProbability: Double, covarianceMatrix: DenseMatrix[Double]): Double = {
    val arrayData = Array[Double](v._1 - core._1, v._2 - core._2)
    val vc = DenseMatrix(arrayData)
    (1 / coreProbability) * math.sqrt(det(covarianceMatrix)) * math.exp((vc * inv(covarianceMatrix) * vc.t).data(0) / 2)
  }

  def maxDifference(prev: Iterable[Entry], next: Iterable[Entry]): Double = {
    if (prev.size != next.size) throw new IllegalArgumentException()
    val listPrev = prev.flatMap(x => List(x._1, x._2))
    val listNext = next.flatMap(x => List(x._1, x._2))
    val diffs = for ((prev, next) <- (listPrev zip listNext)) yield math.abs(prev - next)
    diffs.max
  }
}