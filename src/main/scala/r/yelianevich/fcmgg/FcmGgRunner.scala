package r.yelianevich.fcmgg

import scala.math._

object FcmGgRunner extends App {
  val fcmAlg = new FcmGgAlgorithm

  type Entry = fcmAlg.Entry
  val MaxError = fcmAlg.MaxError

  val coordList = List[Entry]((0, 1), (0, 3), (1, 1), (1, 2), (1, 3), (1, 4), (2, 2), (2, 3), (3, 2.5), (4, 2), (4, 3), (5, 1), (5, 2), (5, 3), (5, 4), (6, 1), (6, 3))
  val data = generate(coordList)
  // val data = generateData(3)

  println("Initial FCM-GG data:")
  data.foreach(p => println(p))

  val fcmData = fcmAlg.fcmGg(data, MaxError)
  println("FCM-GG data:")
  data foreach println

  /*  val f = Figure()
    val p = f.subplot(0)
    val x = linspace(0.0, 1.0)*/

  /* val xCoords = for (xy <- data.keys) yield xy._1
   val yCoords = for (xy <- data.keys) yield xy._2
   val xVector = DenseVector(arrayX)
   val yVector = DenseVector(arrayY)

   p += plot(xVector, yVector, '+')
   p += plot(x, x :^ 3.0, '.')
   p.xlabel = "x axis"
   p.ylabel = "y axis"
   f.saveas("lines.png") */
  // save current figure as a .png, eps and pdf also supported

  /*data.foreach({(x) =>
    if (x._2._1 > x._2._2) {
      p += plot(x._1._1, x._1._2, '+')
    } else {
      
      p += plot(x._1._1, x._1._2, '-')
    }
  })
  
  val diffs = for ((prev, next) <- (listPrev zip listNext)) yield abs(prev - next) */

  def generate(coord: List[Entry]): Map[Entry, Entry] = {
    coord.foldLeft[Map[Entry, Entry]](Map[Entry, Entry]()) { (m, e) =>
      val u1 = random
      val u2 = random
      val normDenom = u1 + u2
      m + (e -> (u1 / normDenom, u2 / normDenom))
    }
  }

  def generateData(num: Int): Map[Entry, Entry] = {
    def generate(num: Int, map: Map[Entry, Entry]): Map[Entry, Entry] = {
      if (num != 0) {
        val u1 = random
        val u2 = random
        val normDenom = u1 + u2
        val m = map + ((random * 100, random * 100) -> (u1 / normDenom, u2 / normDenom))
        generate(num - 1, m)
      } else {
        map
      }
    }
    generate(num, Map[Entry, Entry]())
  }

}