package r.yelianevich

import java.math.BigInteger
import java.nio.charset.StandardCharsets
import java.util.UUID

import com.google.common.primitives.Longs
import redis.clients.jedis.Jedis

import scala.util.Random

object RedisMaxZipEntryTest {

  val zipmapEntries = 512
  val zipmapValues = 64

  def main(args: Array[String]): Unit = {
    val jedis = new Jedis("localhost")

    val bucket = "5f"
    val bucketSize = bucket.length

    // hash key
    val hashMapKeyBytes = s"global:user:$bucket".getBytes(StandardCharsets.UTF_8)

    (1 to 100) foreach { i =>

      // generate new UUID without 2 first letters (I hard-coded them, so every value go to the same hash)
      val humanIdField = UUID.randomUUID().toString.replace("-", "").substring(bucketSize)
      val humanIdfieldBytes = new BigInteger(humanIdField, 16).toByteArray

      // random long dim_user_gid
      val dimUserGidBytes = Longs.toByteArray(Random.nextLong())

      // key bytes: 14, fieldBytes: 16 (or 15), dimUserGidBytes: 8
      println(s"key bytes: ${hashMapKeyBytes.size}, fieldBytes: ${humanIdfieldBytes.size}, dimUserGidBytes: ${dimUserGidBytes.size}")

      jedis.hset(hashMapKeyBytes, humanIdfieldBytes, dimUserGidBytes)
    }

    jedis.close()
  }

}
