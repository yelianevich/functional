package r.yelianevich

import java.util.concurrent.TimeUnit

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success}

object FuturesPractice {

  def main(args: Array[String]): Unit = {
    import scala.concurrent.ExecutionContext.Implicits.global

    val f = Future {
      throw new IllegalArgumentException("fail!")
    }

    awaitAndCollectResults(Seq(f))(e => MergeFailedException("Orc-store merge of input data failed", e))

    println("After fail ready. Try to get value")

    println(f.value)
  }

  case class MergeFailedException(msg: String, cause: Throwable = null) extends Exception(msg, cause)

  def await[T](futures: Traversable[Future[T]]): Unit = {
    futures foreach { future => Await.ready(future, Duration(1, TimeUnit.DAYS)) }
  }

  /** Collect results or throw an exception if future failed */
  def awaitAndCollectResults[T](futures: Traversable[Future[T]])(customException: Throwable => Throwable): Traversable[T] = {
    await(futures)
    futures map {
      _.value match {
        case Some(Success(result)) => result
        case Some(Failure(e)) => throw customException(e)
        case _ => throw customException(new IllegalStateException("Future did not return value"))
      }
    }
  }

}
