package r.yelianevich.programming.scala.ch14.collectionlib

object CakePattern {

  trait PersistentService {
    def persist(): Unit
  }

  trait MidTier {
    def doLogic(): Unit
  }

  trait Ui {
    def render(): Unit
  }

  trait Database extends PersistentService {
    def persist(): Unit = println("Save to database")
  }

  trait BusinessLogic extends MidTier {
    def doLogic(): Unit = println("I'm doing complex logic")
  }

  trait ConsoleUi extends Ui {
    def render(): Unit = println("Render to the console")
  }

  trait App { this: PersistentService with MidTier with Ui =>
    def handleRequest(): Unit = {
      persist()
      doLogic()
      render()
    }
  }

  object SimpleApp extends App with Database with BusinessLogic with ConsoleUi

  def main(args: Array[String]): Unit = {
    SimpleApp.handleRequest()
  }

}
