package r.yelianevich.programming.scala.ch14.collectionlib

import scala.collection.BitSet

object CollectionsTest {

  def main(args: Array[String]): Unit = {
    val bitSet: BitSet = BitSet(1, 2, 3, 4)
    val newSet = bitSet + 3 + 4
    newSet foreach println

    val s = 1 #:: 2 #:: Stream.empty
    s foreach println

    val vector = Vector(1, 2, 3, 4)
    println(vector(0))

    val m = Map("1" -> 1)
  }

}
