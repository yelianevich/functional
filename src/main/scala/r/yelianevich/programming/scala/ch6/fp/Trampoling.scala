package r.yelianevich.programming.scala.ch6.fp


object Trampoling {

  def main(args: Array[String]): Unit = {

    import scala.util.control.TailCalls._

    def isEven(xs: List[Int]): TailRec[Boolean] =
      if (xs.isEmpty) done(true) else tailcall(isOdd(xs.tail))

    def isOdd(xs: List[Int]): TailRec[Boolean] =
      if (xs.isEmpty) done(false) else tailcall(isEven(xs.tail))

    for (i <- 1 to 5) {
      val even = isEven((1 to i).toList).result
      println(s"$i is even? $even")
    }

    def fib(n: Int): TailRec[Int] =
      if (n < 2) done(n) else for {
        x <- tailcall(fib(n - 1))
        y <- tailcall(fib(n - 2))
      } yield x + y

    for (i <- 1 to 40) {
      val f = fib(i).result
      println(s"$i fib = $f")
    }

  }

}
