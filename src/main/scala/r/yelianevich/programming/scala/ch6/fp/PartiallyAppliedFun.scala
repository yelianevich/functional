package r.yelianevich.programming.scala.ch6.fp

import scala.util.control.NonFatal

object PartiallyAppliedFun {

  def main(args: Array[String]): Unit = {

    def cat(s1: String)(s2: String): String = s1 + s2
    val helloCat = cat("Hello, ")(_)

    def catSimple(s1: String, s2: String): String = s1 + s2
    val helloCat1 = (catSimple _).curried


    println(helloCat)
    println(helloCat("partially applied function"))

    // doesn't work
   /*
    def cat1(s1: String, s2: String): String = s1 + s2
    val helloCat1 = cat1("Hello, ", _)

    println(helloCat1)
    println(helloCat1("partially applied function"))
    */

    // doesn't work
    /*def cat1(s1: String)(s2: String): String = s1 + s2
    val helloCat1 = cat1(_)("partially applied function")

    println(helloCat1)
    println(helloCat1("Hello, "))*/

    def cat1(s1: String): String = cat(s1)("partially applied function")

    println(cat1("Hello, "))

    val partFunc: PartialFunction[String, String] = {
      case "part" => "PART"
    }

    val liftedPartFun = partFunc.lift

    try {
      partFunc("non-valid")
    } catch {
      case NonFatal(e) => e.printStackTrace()
    }

    println(partFunc("part"))

    println("lifted:")
    println(liftedPartFun("non-valid"))
    println(liftedPartFun("part"))

    println("unlifted:")
    println(Function.unlift(liftedPartFun)("part"))

  }

}
