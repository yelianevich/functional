package r.yelianevich.programming.scala.ch6

package object fp {

  type Seq[+A] = scala.collection.immutable.Seq[A]
  val Seq = scala.collection.immutable.Seq

}
