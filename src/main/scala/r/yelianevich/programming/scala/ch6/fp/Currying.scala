package r.yelianevich.programming.scala.ch6.fp

object Currying {

  def main(args: Array[String]): Unit = {

    def cat2(s1: String): String => String = (s2: String) => s1 + s2

    val cat1 = cat2("Hello")
    println(cat1(", world"))


    def c2(s1: String) = (s2: String) => s1 + s2

    println(c2 _)

    def mul(x1: Int, x2: Int): Int = x1 * x2
    val tupledMul = Function.tupled(mul _)
    val argsTuple = (3, 2)
    println(tupledMul(argsTuple))

    def calcModel(data: String)(model: String): Unit = {
      println(s"processed $data with $model")
    }
    val data = "dataframe"
    val processByModel = calcModel(data)(_)
    processByModel("GDBT")
    processByModel("RandomForest")

    def calcModel1(data: String, model: String): Unit = {
      println(s"processed $data with $model")
    }
    val processByModel1: String => Unit = calcModel1(data, _)
    processByModel1("GDBT")
    processByModel1 { "RandomForest" }

  }

}
