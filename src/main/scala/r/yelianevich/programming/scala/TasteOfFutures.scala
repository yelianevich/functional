package r.yelianevich.programming.scala

import java.util.concurrent.TimeUnit
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success}

object TasteOfFutures {

  def main(args: Array[String]): Unit = {

    def stringGen(i: Int) = {
      Thread.sleep((math.random * 1000).toLong)
      if (i == 5) throw new IllegalArgumentException("Bad luck" + foo)
      s"$i-String"
    }

    val futures = (1 to 10) map { i =>
      val future: Future[String] = Future(stringGen(i))

      future.onComplete {
        case Success(i) => println(s"Success $i")
        case Failure(t) => println(s"Failure $t")
      }

      future
    }

    futures.foreach {
      Await.ready(_, Duration(6, TimeUnit.SECONDS))
    }




  }
}
