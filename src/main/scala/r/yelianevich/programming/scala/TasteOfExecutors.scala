package r.yelianevich.programming.scala

import java.util.concurrent.{Executors, TimeUnit}

import com.google.common.util.concurrent.ThreadFactoryBuilder
import org.slf4j.LoggerFactory

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success}

object TasteOfExecutors {
private val log = LoggerFactory.getLogger(getClass)

  private val concatThreadFactory = new ThreadFactoryBuilder().setNameFormat("concat-validate-%d").setDaemon(true).build()

  private implicit val executionContext = ExecutionContext.fromExecutor(
    Executors.newFixedThreadPool(50, concatThreadFactory)
  )

  def main(args: Array[String]): Unit = {

    val futures = new ArrayBuffer[Future[String]]()

    futures appendAll Seq[Future[String]](
      Future {
        log.info("1")
        "1"
      },
      Future("2"),
      Future(throw new IllegalArgumentException("4")))

    futures appendAll Seq[Future[String]](
      Future("4"),
      Future(throw new IllegalArgumentException("4")),
      Future("5"))

    futures.map { f =>
      Await.ready(f, Duration(10, TimeUnit.SECONDS))
      f.value match {
        case Some(Success(message)) =>
          log.info(message)
          true
        case Some(Failure(e)) =>
          log.error(e.getMessage + " failed")
          false
        case None =>
          false
      }
    } forall identity

  }

}
