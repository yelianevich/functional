package r.yelianevich.programming.scala.ch5.implicits

import scala.annotation.implicitNotFound

object ErrorMessages {

  case class ListWrapper(l: List[Int])


  def main(args: Array[String]): Unit = {

    println(List(1, 2, 3).map(_ * 2).to(Seq))

    // won't compile due to missing CanBuildFrom for ListWrapper
    // println(List(1, 2, 3).map[Int, ListWrapper](_ * 2))

    // comment to see error message
    implicit val formatter = QuotesFormatter
    Printer.printFormatted("Hi implicits")
  }

  object Printer {
    def printFormatted(s: String)(implicit format: StrFormatter): Unit = {
      println(format(s))
    }
  }

}

@implicitNotFound("Cannot format string, cause no formatter provided in scope")
trait StrFormatter {
  def apply(s: String): String
}

object QuotesFormatter extends StrFormatter {
  override def apply(s: String): String = s"'$s'"
}
