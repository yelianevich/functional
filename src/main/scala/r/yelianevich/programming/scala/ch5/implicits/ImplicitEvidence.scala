package r.yelianevich.programming.scala.ch5.implicits

object ImplicitEvidence {

  def main(args: Array[String]): Unit = {

    val l1 = List(1, 2, 3)
    // val m1 = l1.toMap // Cannot prove that Int <:< (T, U).

    val s1 = Seq(1, 2, 3) // Cannot prove that Int <:< (T, U).
    // val m2 = s1.toMap

    val s2 = Seq ((1,1), (2, 2), (3, 3))
    val m = s2.toMap // evidence was generated and method works
    println(m)
  }

}
