package r.yelianevich.programming.scala.ch5.implicits

object Implicitly {

  def main(args: Array[String]): Unit = {

    class MyList[A](list: List[A]) {

      def sortBy1[B](f: A => B)(implicit ord: Ordering[B]): List[A] = {
        list.sortBy(f)(ord)
      }

      def sortBy2[B : Ordering](f: A => B): List[A] = {
        list.sortBy(f)(implicitly[Ordering[B]])
      }

      def sortBy3[B : Ordering](f: A => B): List[A] = {
        list.sortBy(f)
      }

    }

    val l = new MyList(List(1, 2, 3))
    val l1 = l.sortBy1(i => -i) mkString ", "
    println(l1)
    val l2 = l.sortBy2(i => -i) mkString ", "
    println(l2)
    val l3 = l.sortBy3(i => -i) mkString ", "
    println(l3)

  }

}
