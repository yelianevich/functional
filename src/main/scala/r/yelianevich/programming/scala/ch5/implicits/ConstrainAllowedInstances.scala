package r.yelianevich.programming.scala.ch5.implicits

import scala.collection.immutable.Seq

object ConstrainAllowedInstances {

  def main(args: Array[String]): Unit = {
    val seq: Seq[Int] = Seq(1, 2, 3, 4)
    val list: List[Int] = seq.map(_ * 2).to(List)
    println(list)

    val set: Set[Int] = seq.map(_ * 2).to(Set)
    println(set)
  }

}
