package r.yelianevich.programming.scala.ch5.implicits

object WorkingAroundErasure {

  // do not compile due to types erasure
 /* object C {
    def m(seq: Seq[Int]): Unit = println(s"Seq[Int]: $seq")
    def m(seq: Seq[String]): Unit = println(s"Seq[String]: $seq")
  }*/

  object Erasure {

    implicit object IntMarker
    implicit object StringMarker

    def m(seq: Seq[Int])(implicit ev: IntMarker.type): Unit = println(s"Seq[Int]: $seq")
    def m(seq: Seq[String])(implicit ev: StringMarker.type): Unit = println(s"Seq[String]: $seq")
  }

  def main(args: Array[String]): Unit = {
    Erasure.m(Seq(1, 2, 3))
  }

}
