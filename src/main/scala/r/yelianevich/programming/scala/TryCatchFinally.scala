package r.yelianevich.programming.scala

import scala.io.Source
import scala.util.control.NonFatal

object TryCatchFinally {

  def main(args: Array[String]): Unit = {
    args foreach (arg => countLines(arg))
    args foreach (arg => countLinesWithArm(arg))
  }

  def countLines(fileName: String) = {
    println() // Add a blank line for legibility
    var source: Option[Source] = None
    try {
      source = Some(Source.fromFile(fileName))
      val size = source.get.getLines.size
      println(s"file $fileName has $size lines")
    }
    catch {
      case NonFatal(ex) => println(s"Non fatal exception! $ex")
    } finally {
      for (s <- source) {
        println(s"Closing $fileName...")
        s.close
      }
    }
  }

  def countLinesWithArm(fileName: String): Unit = {
    println() // Add a blank line for legibility
    manage(Source.fromFile(fileName)) { source =>
      val size = source.getLines.size
      println(s"file $fileName has $size lines")
      if (size > 20) throw new RuntimeException("Big file!")
    }
  }

  object manage {
    def apply[R <: {def close() : Unit}, T](resource: => R)(f: R => T) = {
      var res: Option[R] = None
      try {
        res = Some(resource) // Only reference "resource" once!!
        f(res.get)
      } catch {
        case NonFatal(ex) => println(s"Non fatal exception! $ex")
      } finally {
        if (res.isDefined) {
          println(s"Closing resource...")
          res.get.close
        }
      }
    }
  }

}
