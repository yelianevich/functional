package r.yelianevich.programming.scala.ch10.typesystem

object TypeSystemTests {

  sealed abstract class Animal {
    def name: String
  }
  case class Cat(name: String) extends Animal
  case class Dog(name: String) extends Animal

  trait Printer[-A] {
    // With contravariant type parameter Printer declare that it can handle A and all subtypes.
    // As a result Printer[SuperType] can be passed to a method that expects Printer[SubType]
    def print(x: A)
  }
  class AnimalPrinter extends Printer[Animal] {
    def print(x: Animal): Unit = println("Animal name is: " + x.name)
  }

  class CatPrinter extends Printer[Cat] {
    def print(cat: Cat): Unit =
      println("The cat's name is: " + cat.name)
  }

  def main(args: Array[String]): Unit = {
    val animals = Vector[Animal](Cat("1"), Dog("2"))
    printNames(animals)(_.toUpperCase)

    val animalPrinter: Printer[Animal] = new AnimalPrinter
    animalPrinter.print(Cat("Fluffy"))
    animalPrinter.print(Dog("Barky"))

    val myCat: Cat = Cat("Boots")

    def printMyCat(printer: Printer[Cat]): Unit = {
      printer.print(myCat)
    }

    val catPrinter: Printer[Cat] = new CatPrinter

    printMyCat(catPrinter)
    printMyCat(animalPrinter)

  }

  /** Covariance example */
  def printNames[R](s: Vector[Animal])(format: String => R): Unit = {
    s.foreach(a => println(format(a.name)))
  }

}
