package r.yelianevich.programming.scala


object EnumerationExample {

  def main(args: Array[String]): Unit = {
    println("Weekdays:")
    WeekDay.values foreach println

    println("Working days:")
    WeekDay.values filter WeekDay.isWorkingDay foreach println
  }

  object WeekDay extends Enumeration {
    type WeekDay = Value
    val Mon = Value("mon")
    val Tue, Wed, Thu, Fri, Sat, Sun = Value

    def isWorkingDay(day: WeekDay): Boolean = day != Sat && day != Sun

  }

}
