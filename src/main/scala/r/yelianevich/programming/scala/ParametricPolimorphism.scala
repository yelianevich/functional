package r.yelianevich.programming.scala

object ParametricPolimorphism {

  def main(args: Array[String]): Unit = {
    val covariantList = List[String](foo, bar)
    def covariantTypeMakeThisHappen(l: List[Any]): Unit = l foreach println
    covariantTypeMakeThisHappen(covariantList)


  }

  class RegularContainer[T](elem: T) {
    var e = elem
  }

  // TODO
  class CovariantContainer[+T](elem: T) {
    //var e = elem
  }

  class ContravariantContainer[-T](elem: T) {
    //val e: T = elem
  }

}
