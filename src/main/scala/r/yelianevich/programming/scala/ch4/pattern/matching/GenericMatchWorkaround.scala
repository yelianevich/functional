package r.yelianevich.programming.scala.ch4.pattern.matching

object GenericMatchWorkaround {

  def main(args: Array[String]): Unit = {
    val seq0: Seq[Int] = (1 #:: 2 #:: 3 #:: LazyList.empty)
    val seq1 = Seq[String]("Hi", "Bye")
    val seq2 = Seq[Double](1, 2)
    val seq3 = Seq[Int](2, 3)
    val seq4 = Seq[Boolean](true, false)

    seq0 match {
      case head :: tail => println("matched")
      case _ => println("not matched on :: !!!!!")
    }

    seq0 match {
      case head +: tail => println("matched on +: !!!!!")
      case _ => println("not matched")
    }


    for (s <- Seq(seq1, seq2, seq3, seq4)) {
      matchSeq(s)
    }

    def matchSeq[T](seq: Seq[T]): Unit = {
      seq match {
        case Nil => println("Empty sequence")
        case head +: _ => head match { // workaround to get sequence generic type
          case _: Double => println("Double sequence")
          case _: Int => println("Int sequence")
          case _: String => println("String sequence")
          case _ => println("Other sequence")
        }
      }
    }
  }

}
