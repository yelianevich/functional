package r.yelianevich.programming.scala.ch4.pattern.matching

object BindVariable {

  case class Address(street: String, city: String, country: String)
  case class Person(name: String, age: Int, address: Address)

  val alice = Person("Alice", 25, Address("1 Scala Lane", "Chicago", "USA"))
  val bob = Person("Bob", 29, Address("2 Java Ave.", "Miami", "USA"))
  val charlie = Person("Charlie", 32, Address("3 Python Ct.", "Boston", "USA"))

  def main(args: Array[String]): Unit = {
    for (person <- Seq(bob, alice, charlie)) {
      person match {
        case p @ Person("Alice", 25, address) => println(s"Hi Alice! $p")
        case p @ Person("Bob", 29, a @ Address(street, city, country)) => println(s"Hi ${p.name} from $country")
        case p: Person => println(s"Who are you ${p.name}?")
      }
    }
  }

}
