package r.yelianevich.programming.scala.ch4.pattern.matching

object Misc {

  def main(args: Array[String]): Unit = {
    case class Person(fn: String, ln: String)

    val Person(fn, ln) = Person("f", "l")
    println(s"fn=$fn, ln=$ln")

    val head1 :: tail1 = List(1, 1, 1)
    println(s"head=$head1, tail=$tail1")

    val head2 +: tail2 = List(2, 2, 2)
    println(s"head=$head2, tail=$tail2")

    val head3 +: tail3 = Seq(3, 3, 3)
    println(s"head1=$head3, tail1=$tail3")

    val (v1, v2) = ("1", "2")
    println(s"v1=$v1, v2=$v2")

    val p = Person("fn", "ln")
    if (p == Person("fn", "ln")) {
      println("nice pattern matching (??) in if clause")
    }

    p match {
      case Person("fn", "ln") => println("PM not hacked")
      case _ => println("PM hacked")
    }


    // PM in map function
    case class Address(street: String, city: String, country: String)
    case class Person1(name: String, age: Int)
    val as = Seq(
      Address("1 Scala Lane", "Anytown", "USA"), Address("2 Clojure Lane", "Othertown", "USA"))
    val ps = Seq(
      Person1("Buck Trends", 29), Person1("Clo Jure", 28))
    val pas = ps zip as

    // Ugly way:
    val tmp = pas map { tup =>
      val Person1(name, age) = tup._1
      val Address(street, city, country) = tup._2
      s"$name (age: $age) lives at $street, $city, in $country"
    }
    println(tmp)

    // Nicer way:
    val s = pas map {
      case (Person1(name, age), Address(street, city, country)) =>
        s"$name (age: $age) lives at $street, $city, in $country"
    }
    println(s)

    val cols = """\*|[\w, ]+""" // for columns
    val table = """\w+"""
    val tail = """.*"""
    val selectRE = s"""SELECT\\s*(DISTINCT)?\\s+($cols)\\s*FROM\\s+($table)\\s*($tail)?;""".r
    val selectRE(distinct1, cols1, table1, otherClauses) = "SELECT DISTINCT * FROM atable;"
    println(s"distinct1=$distinct1, cols1=$cols1,table1=$table1, otherClauses=$otherClauses")
  }

}
