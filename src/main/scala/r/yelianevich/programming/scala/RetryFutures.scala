package r.yelianevich.programming.scala

import java.util.concurrent.TimeUnit
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success}

object RetryFutures {

  def main(args: Array[String]): Unit = {

    class FailureDelete {
      var i = 0

      def delete: Int = {
        println("deleting " + i)
        i += 1
        if (i < 5) throw new IllegalArgumentException("Bad luck " + i)
        i
      }
    }

    val failureDelete = new FailureDelete

    // call-by-name future! => cannot create a future aside
    val retryDelete = retry(3, pauseSec = 1) {
      Future(failureDelete.delete)
    }
    retryDelete.onComplete {
      case Success(i) => println(s"Success $i")
      case Failure(t) => println(s"Failure $t")
    }

    Await.ready(retryDelete, Duration(10, TimeUnit.SECONDS))

    Thread.sleep(Duration(5, TimeUnit.SECONDS).toMillis)
    retryDelete.value
  }

  /**
    * Retry execution of the Future specified number of `times` with `pauseSec` interval
    */
  def retry[T](times: Int, pauseSec: Int = 3)(f: => Future[T])(implicit executor: ExecutionContext): Future[T] = {
    f.recoverWith({ case _ if times > 0 =>
      Thread.sleep(Duration(pauseSec, TimeUnit.SECONDS).toMillis)
      retry(times - 1, pauseSec)(f)(executor)
    })(executor)
  }
}
