package r.yelianevich.fcm

import scala.math._

class FcmAlgorithm {
  val MaxError = 0.01
  type Entry = Tuple2[Double, Double]

  def fcm(data: Map[Entry, Entry], epsilon: Double): Map[Entry, Entry] = {
    def fcmLoop(data: Map[Entry, Entry], error: Double): Map[Entry, Entry] = {
      if (error >= epsilon) {
        val cores = defineClusterCores(data)
        val updatedData = updatMembershipMatrix(data, cores)
        val calcError = maxDifference(data.values, updatedData.values)
        fcmLoop(updatedData, calcError)
      } else {
        data
      }
    }

    fcmLoop(data, 1)
  }

  def updatMembershipMatrix(data: Map[Entry, Entry], cores: (Entry, Entry)): Map[Entry, Entry] = {
    data.foldLeft[Map[Entry, Entry]](Map[Entry, Entry]()) { (m, e) =>
      val u1 = pow(norm(e._1, cores._1) / norm(e._1, cores._1), 2) + pow(norm(e._1, cores._1) / norm(e._1, cores._2), 2)
      val u2 = pow(norm(e._1, cores._2) / norm(e._1, cores._1), 2) + pow(norm(e._1, cores._2) / norm(e._1, cores._2), 2)
      m + (e._1 -> (1 / u1, 1 / u2))
    }
  }

  def defineClusterCores(data: Map[Entry, Entry]): (Entry, Entry) = {
    val num1 = data.foldLeft[Entry]((0, 0))(
      (acc, data) => (acc._1 + data._1._1 * data._2._1, acc._2 + data._1._2 * data._2._1))
    val num2 = data.foldLeft[Entry]((0, 0))(
      (acc, data) => (acc._1 + data._1._1 * data._2._2, acc._2 + data._1._2 * data._2._2))
    val denom = data.foldLeft[Entry](0, 0)(
      (acc, p) => (acc._1 + p._2._1, acc._2 + p._2._2))
    val core1 = (num1._1 / denom._1, num1._2 / denom._1)
    val core2 = (num2._1 / denom._2, num2._2 / denom._2)
    (core1, core2)
  }

  def norm(v1: Entry, v2: Entry) =
    sqrt((v1._1 - v2._1) * (v1._1 - v2._1) + (v1._2 - v2._2) * (v1._2 - v2._2))


  def maxDifference(prev: Iterable[Entry], next: Iterable[Entry]): Double = {
    if (prev.size != next.size) throw new IllegalArgumentException()
    val listPrev = prev.flatMap(x => List(x._1, x._2))
    val listNext = next.flatMap(x => List(x._1, x._2))
    val diffs = for ((prev, next) <- listPrev zip listNext) yield abs(prev - next)
    diffs.max
  }
}