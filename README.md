## Test data
Large test data sets that are used in the tests can be downloaded 
from [Algorithms, 4th Edition](https://algs4.cs.princeton.edu/code/) website.
Direct link to the test data is [algs4-data.zip](https://algs4.cs.princeton.edu/code/algs4-data.zip).

At the moment all tests with `algs4-data/leipzig1M.txt` data is `@Ignore`ed.

## Git LFS
Git LFS is configured to track `src/main/resources/algs4-data/*` files (configured in `.gitattributes`).

See [BitBucket documentation](https://support.atlassian.com/bitbucket-cloud/docs/use-git-lfs-with-bitbucket/) how
to use install it. Use `git lfs pull` to download binary data.
Make sure to use 'App Password' when pulling the files (https://bitbucket.org/account/settings/app-passwords/)