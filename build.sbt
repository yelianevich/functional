name := "functional"

version := "1.0"

scalaVersion := "2.13.14"

javacOptions ++= Seq("-source", "21", "-target", "21")
scalacOptions += "-target:21"

resolvers += MavenCache("local-maven", file("libs"))

libraryDependencies ++= Seq(
  "edu.princeton.cs" % "algs4" % "1.0.4",

  "org.scalanlp" %% "breeze" % "2.1.0",
  "org.scalanlp" %% "breeze-natives" % "2.1.0",
  "org.scalanlp" %% "breeze-viz" % "2.1.0",

  "org.apache.logging.log4j" % "log4j-api" % "2.23.1",
  "org.apache.logging.log4j" % "log4j-core" % "2.23.1",
  "org.apache.logging.log4j" % "log4j-slf4j-impl" % "2.23.1",

  "redis.clients" % "jedis" % "5.1.5",
  "com.google.guava" % "guava" % "33.3.0-jre",

  "org.scalameta" %% "scalameta" % "4.9.9",
  "org.typelevel" %% "cats-core" % "2.12.0",

  "com.github.sbt.junit" % "jupiter-interface" % JupiterKeys.jupiterVersion.value % Test,
  "org.junit.jupiter" % "junit-jupiter" % "5.11.0" % Test,
  "org.junit.platform" % "junit-platform-launcher" % "1.11.0" % Test,

  "org.scalatest" %% "scalatest" % "3.2.19" % Test,
  "org.assertj" % "assertj-core" % "3.26.3" % Test
)
